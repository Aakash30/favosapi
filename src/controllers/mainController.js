const UserAuthManagement = require("./User/UserAuthController");
const UserManagement = require("./User/UserManagement")
const CountryManagement = require("./Country/CountryController");
const AddressManagement = require("./Address/AddressManagement");
const FavouritesAndShortCut = require("./FavouritesAndShortcut/FavouritesAndShortcutController");
const AdminRestaurantManagement = require("./Admin/RestaurantController");
const AdminUserManagement = require("./Admin/UserController");
const AdminAuthManagement = require("./Admin/AdminAuth");
const MerchantRestaurantController = require("./Restaurant/RestaurantManagementController")
const UserRestaurantController = require("./User/RestaurantController");
const MenuManagement = require("./Restaurant/MenuManagement");
const RestaurantOrderManagement = require('./Restaurant/OrdersController');


module.exports = {
    UserAuthManagement,
    UserManagement,
    CountryManagement,
    AddressManagement,
    FavouritesAndShortCut,
    AdminRestaurantManagement,
    AdminUserManagement,
    AdminAuthManagement,
    MerchantRestaurantController,
    UserRestaurantController,
    MenuManagement,
    RestaurantOrderManagement
}