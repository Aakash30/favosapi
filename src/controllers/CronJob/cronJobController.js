var cron = require("cron");
var moment = require("moment");
moment.suppressDeprecationWarnings = true;
const User = require("../../models/User");
const Orders = require("../../models/Orders");
const Notification = require("../../models/Notification");
const Product = require("../../models/Product");
const PushNotif = require("../../middlewares/push-notification");
const AuthTable = require("../../models/AuthTable");
const OrderStatusFlowManagement = require("../../models/OrderStatusManagement");
const EMAIL = require("../../middlewares/email");
const STRIPE = require("../../middlewares/stripe");

module.exports = {
    //Crone Run On Every Minute
    everyMinuteCrone: () => {
        let job = new cron.CronJob(
            "* * * * *",
            async function () {
                console.log("every minute cron runs")

                //delete user account if user not logged in till 30 days
                let users = await User.query().select('id', 'delete_request_date', 'deleteRequest').where({'deleteRequest': 2, "user_type": "USER"});
                if(users.length > 0){
                    for (let i = 0; i < users.length; i++) {
                        if(users[i].delete_request_date != "" || users[i].delete_request_date != null){
                            let delete_request_date = moment(users[i].delete_request_date);//moment('2020-08-01T11:42:13.446');
                            let current_date = moment();
                            let duration = moment.duration(current_date.diff(delete_request_date));
                            let dayDifference = duration.asDays();
                            //account deleted in 30 days
                            if (dayDifference >= 30) {
                                //update delete status 3 in users table
                                await User.query()
                                .patch({"deleteRequest": 3})
                                .where({ id: users[i].id });
                            }  
                        }
                    }
                }

                
                // Update Email in 7 days otherwise remove it.
                let usersWithNotVerification = await User.query().select('id', 'email_sentout_date', 'verify_email_token', 'created_at', 'is_email_verified', 'is_mobile_verified', 'mobile', 'country_isd_code')
                        .whereNot('deleteRequest', 3).where({"user_type": "USER"})
                if (usersWithNotVerification.length > 0){
                    for (let i = 0; i < usersWithNotVerification.length; i++) {
                        // // Update (Mobile and Countrty Code) AND (Email) in 7 days otherwise remove it.
                        // if((usersWithNotVerification[i].is_mobile_verified == false) && (usersWithNotVerification[i].email_sentout_date != null && usersWithNotVerification[i].is_email_verified == false)){
                        //     let created_at = usersWithNotVerification[i].created_at //moment('2020-08-01T11:42:13.446');
                        //     let email_date = usersWithNotVerification[i].email_sentout_date
                        //     let current_date = moment();
                        //     let duration_1 = moment.duration(current_date.diff(created_at));
                        //     let duration_2 = moment.duration(current_date.diff(email_date));
                        //     let dayDifference_1 = duration_1.asDays();
                        //     let dayDifference_2 = duration_2.asDays();
                        //     if ((dayDifference_1>7 && usersWithNotVerification[i].is_mobile_verified == false) && (dayDifference_2>7 && usersWithNotVerification[i].is_email_verified == false)){
                        //         //update mobile and country isd code blank
                        //         await User.query().patch({mobile: '', country_isd_code: '', email: '', email_sentout_date: null, verify_email_token: null }).where({ id: usersWithNotVerification[i].id });
                        //     }
                        // }
                        
                        // Update Email in 7 days otherwise remove it.
                        if(usersWithNotVerification[i].email_sentout_date != null && usersWithNotVerification[i].is_email_verified == false){
                            let created_at = usersWithNotVerification[i].email_sentout_date //moment('2020-08-01T11:42:13.446');
                            let current_date = moment();
                            let duration = moment.duration(current_date.diff(created_at));
                            let dayDifference = duration.asDays();
                            if (dayDifference >7 && usersWithNotVerification[i].is_email_verified == false){
                                //update email blank
                                await User.query().patch({email: '', email_sentout_date: null, verify_email_token: null }).where({ id: usersWithNotVerification[i].id });
                            }
                        }

                        // Update Mobile and Countrty Code in 7 days otherwise remove it.
                        if(usersWithNotVerification[i].is_mobile_verified == false){
                            let created_at = usersWithNotVerification[i].created_at //moment('2020-08-01T11:42:13.446');
                            let current_date = moment();
                            let duration = moment.duration(current_date.diff(created_at));
                            let dayDifference = duration.asDays();
                            if (dayDifference>7 && usersWithNotVerification[i].is_mobile_verified == false){
                                //update mobile and country isd code blank
                                await User.query().patch({mobile: '', country_isd_code: '' }).where({ id: usersWithNotVerification[i].id });
                            }
                        }
                    }
                }
                // let usersWithEmailNotVerified = await User.query().select('id', 'email_sentout_date', 'verify_email_token', 'created_at', 'is_email_verified').whereNot('email_sentout_date', null).where({'is_email_verified': false, "user_type": "USER"})
                // if (usersWithEmailNotVerified.length > 0){
                //     for (let i = 0; i < usersWithEmailNotVerified.length; i++) {
                //         if(usersWithEmailNotVerified[i].email_sentout_date != null){
                //             let created_at = usersWithEmailNotVerified[i].email_sentout_date //moment('2020-08-01T11:42:13.446');
                //             let current_date = moment();
                //             let duration = moment.duration(current_date.diff(created_at));
                //             let dayDifference = duration.asDays();
                //             if (dayDifference >7 && usersWithEmailNotVerified[i].is_email_verified == false){
                //                 //update email blank
                //                 await User.query().patch({email: '', email_sentout_date: null, verify_email_token: null }).where({ id: usersWithEmailNotVerified[i].id });
                //             }
                //         }
                //     }
                // }

                //Link Valid up to 30 minutes (Forgot password Link For Admin / Merchant)
                let usersLinkValidate = await User.query().select('id', 'reset_password_token_date', 'reset_password_token').whereNot({'reset_password_token': null});
                if(usersLinkValidate.length > 0){
                    for(let i=0 ; i<usersLinkValidate.length ; i++){
                        let current_date = moment();
                        // console.log(current_date);
                        let token_date = moment(usersLinkValidate[i].reset_password_token_date);
                        // console.log(token_date);
                        let difference = current_date.diff(token_date, "minutes");

                        if(difference > 30){
                            await User.query().patch({ reset_password_token: null, reset_password_token_date: null}).where("id", usersLinkValidate[i].id)
                        }
                    }
                }


                // Automatically Cancel the order when order is not accepted by merchant and 15 minutes completed.
                let orders = await Orders.query().select("*")
                    .eager('[orders_of_users, orders_of_merchant]')
                    .modifyEager('orders_of_users', builder => {
                        return builder.select("id", "first_name", "last_name", "email", "notification_toggle");
                    })
                    .modifyEager('orders_of_merchant', builder => {
                        return builder.select("id", "first_name", "last_name", "email", "restaurant_name");
                    }).where("order_status", 1).whereNot("order_type_id", null)

                if(orders.length > 0){
                    for (let i = 0; i < orders.length; i++) {
                        let created_at = moment(orders[i].created_at);
                        let till_time = created_at;
                        till_time.minutes((till_time.minutes() + 15));
                        let current_time = moment();
                        let duration = moment.duration(till_time.diff(current_time));
                        let second = duration.asSeconds();
                        if (second < 0) {

                            //----------------------Send Push Notification (Accept Order) {START}------------------------------
                            let notificationData = {
                                user_id: orders[i].user_id,
                                order_id: orders[i].id,
                                type: "Order Cancelled",
                                message: `Oh nooo! unfortunately your order has been auto cancelled because it is not accepted by ${orders[i].orders_of_merchant.restaurant_name}. We know how annoying this can be and we are sorry :(`,
                                read_status: false,
                            };

                            let inserted_noti = await Notification.query().insert(notificationData);
                            let unread_count = await Notification.query().count('id')
                                .where({ user_id: orders[i].user_id, read_status: false }).first();
                            let unread_noti = unread_count.count;


                            //-------------------- Cancel Stripe Payment (START) -------------------------------
                            let str = orders[i].transaction_id;
                            str = str.substring(0, 3);
                            console.log(str);
                            if(str == "ch_"){
                                console.log("Cancel Stripe payment by Charge Id");
                            }else if(str == "pi_"){
                                console.log("Cancel Stripe Payment Intent");
                                STRIPE.cancelPaymentIntent(orders[i].transaction_id);
                            }
                            //-------------------- Cancel Stripe Payment (CANCEL) -------------------------------

                            //---------------------- update order status to cancel--------------------------
                            await Orders.query().update({
                                order_status: 7,
                                order_cancellation_reason: "Order Not Accepted"
                            }).where("id", orders[i].id);

                            //-------------Table done free--------------
                            if(orders[i].table_id != null){
                                await Orders.query().patch({ table_id: null }).where({ id: orders[i].id }).first();
                            }

                            //---------------------- maintain order status flow management------------------
                            let orderStatusFlowMaintain = await OrderStatusFlowManagement.query().insert({ order_id: orders[i].id, order_status: 7 }).returning('*');


                            let UserAuthDevices = await AuthTable.query().select("*").where("user_id", orders[i].user_id).whereNot({
                                device_type: null,
                                device_token: null
                            })

                            //-------Send Email Of Order Cancellation-----------
                            let emailData = {
                                email: orders[i].orders_of_users.email,
                                user_name: orders[i].orders_of_users.first_name, //+ ' ' + orders[i].orders_of_users.last_name,
                                order_number: orders[i].order_number,
                                merchant_name: orders[i].orders_of_merchant.restaurant_name,
                                order_cancellation_reason: "Order Not Accepted"
                            }
                            EMAIL.sendCancelledOrderDetails(emailData)

                            if (orders[i].orders_of_users.notification_toggle == true) {
                                // console.log(orders[i].orders_of_merchant.restaurant_name, "===========================================")
                                for (let i = 0; i < UserAuthDevices.length; i++) {
                                    let msg = `Oh nooo! unfortunately your order has been auto cancelled because it is not accepted by ${orders[i].orders_of_merchant.restaurant_name}. We know how annoying this can be and we are sorry :(`
                                    let sendData = {
                                        content: msg,
                                        device_token: UserAuthDevices[i].device_token,
                                        device_type: UserAuthDevices[i].device_type,
                                        user_type: 'USER',
                                        type: 'Order Cancelled',
                                        noti_count: unread_noti,
                                        orderId: orders[i].id,
                                        notificationId: inserted_noti.id,
                                        orderstatusId: 7,
                                        orderTypeId: orders[i].order_type_id
                                    }
                                    PushNotif.sendPushNotif(sendData, {});
                                }
                            }
                            //----------------------Send Push Notification (Accept Order) {END}------------------------------
                        }
                    }
                }
            },
            null,
            true,
            "Pacific/Auckland"
        );
    },

    //Crone Run On Every Day at 23:55 PM
    croneRunAtTheEndOfDay: () => {
        let job = new cron.CronJob(
            "55 23 * * *",
            async function (){
                console.log("Crone Run On Every Day at 23:55 PM")
                //Refill product quantity according to replenish days
                let ProductList = await Product.query().select("*").where("delete_status", false)
                if(ProductList.length > 0){
                    for (let i = 0; i < ProductList.length; i++){
                        let updated_at = moment(ProductList[i].updated_at);
                        let current_date = moment();

                        let dayDifference = current_date.diff(updated_at, 'days');
                        // moment.duration(current_date.diff(updated_at));
                        // let dayDifference = duration.asDays();
                        // console.log(dayDifference, "=======", ProductList[i].id)

                        if((ProductList[i].product_replenish_days > 0 && ProductList[i].product_replenish_days != null) && (ProductList[i].product_quantity > 0 && ProductList[i].product_quantity != null)){
                            if (dayDifference == ProductList[i].product_replenish_days) {
                                let refilledProductQuantity = ProductList[i].product_quantity; //ProductList[i].product_quantity_counter + ProductList[i].product_quantity;
                                let updateProductQuantity = await Product.query().update({
                                    product_quantity_counter: refilledProductQuantity,
                                    updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                                    status: 1
                                }).where('id', ProductList[i].id);
                            }
                        }
                    }
                }
            },
            null,
            true,
            "Pacific/Auckland"
        );
    },

    //Crone Run at Every Day at 00:01 AM
    croneRunAtTheStartOfDay: () =>{
        let job = new cron.CronJob(
            "1 00 * * *",
            async function () {
                //auto change status of product 1 means active if merchant done product out of stock today only
                let todaysOutOfStockProducts = await Product.query().select("*").where("status", 3).andWhereNot('today_out_of_stock', null);
                if (todaysOutOfStockProducts.length > 0) {
                    for (let i = 0; i < todaysOutOfStockProducts.length; i++) {
                        let out_of_stock_date = moment(todaysOutOfStockProducts[i].today_out_of_stock);
                        let current_date = moment();

                        if (current_date > out_of_stock_date) {
                            await Product.query().update({
                                status: 1
                            }).where('id', todaysOutOfStockProducts[i].id)
                        }
                    }
                }
            },
            null,
            true,
            "Pacific/Auckland"
        );
    }
}
