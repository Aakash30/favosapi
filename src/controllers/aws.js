'use strict';

const AWS = require('aws-sdk');
const {
    creds,
    bucketRegion,
    bucketName
} = require('../middlewares/aws');

const multer = require('multer');
const multerS3 = require('multer-s3');
//AWS.config.creds;
AWS.config.accessKeyId = creds.accessKeyId;
AWS.config.secretAccessKey = creds.secretAccessKey;
AWS.config.region = creds.region;
//console.log(creds); return;
const s3 = new AWS.S3();
const acl = 'public-read';

//const BASE_URL = 'https://easyorder-s3.s3-ap-southeast-2.amazonaws.com/';
const metaData = function (req, file, cb) {
    cb(null, {
        fieldName: file.fieldname
    });
}


/**
 * @description: function is used To upload User Profile Image
 */
const user_image = multer({
    storage: multerS3({
        s3: s3,
        bucket: bucketName,
        acl: acl,
        metadata: metaData,
        key: function (req, file, cb) {
            cb(null, 'user_image/' + Date.now().toString() + '_' + file.originalname)
        }
    })
});


/**
 * @description: function is used To upload Restaurant Image on register a business by user profile
 */
const business_registered = multer({
    storage: multerS3({
        s3: s3,
        bucket: bucketName,
        acl: acl,
        metadata: metaData,
        key: function (req, file, cb) {
            cb(null, 'business_registered/' + Date.now().toString() + '_' + file.originalname)
        }
    })
});


/**
 * @description: function is used To upload Review Image
 */
const review_image = multer({
    storage: multerS3({
        s3: s3,
        bucket: bucketName,
        acl: acl,
        metadata: metaData,
        key: function (req, file, cb) {
            cb(null, 'review_image/' + Date.now().toString() + '_' + file.originalname)
        }
    })
})

/**
 * @description: function is used To upload Restaurant Image
 */
const restaurant_image = multer({
    storage: multerS3({
        s3: s3,
        bucket: bucketName,
        acl: acl,
        metadata: metaData,
        key: function (req, file, cb) {
            cb(null, 'restaurant_image/' + Date.now().toString() + '_' + file.originalname)
        }
    })
})


module.exports = {
    user_image,
    business_registered,
    review_image,
    restaurant_image
};
