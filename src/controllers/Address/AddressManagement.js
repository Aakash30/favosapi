'use strict';

/**
 * @author 'SHUBHAM GUPTA'
 */

//----------------------------------------------CountryController Start------------------------------------------------------------------

//Import Model Here
const Address = require('../../models/Address');

//=====================================================================================

/**
 * Insert Address
 * @description: fuction is used to Insert Address
 */
const InsertNewAddress = async (req, res) => {
	try {
		let data = req.body;

		if (!data.lat)
			throw badRequestError(res, 'Please enter the lattitude of address.');
		if (!data.lng)
			throw badRequestError(res, 'Please enter the longitude of address.');
		if (!data.full_address)
			throw badRequestError(res, 'Please enter the full address.');
		if (!data.address_type)
			throw badRequestError(res, 'Please enter the address type.');
		// if (data.active_address == "" || data.active_address == null || data.active_address == undefined) throw badRequestError(res, "Please enter the active address.");

		// console.log(data.howToReach.length)
		if (data.howToReach.length > 100)
			throw badRequestError(res, 'how to reach char length is above 100!');

		data.user_id = req.user.user_id;
		// data.selected=false;
		let addAddress = await Address.query().insert(data).returning('*');

		if (!addAddress) throw badRequestError(res, 'Someting Went Wrong!');

		return createdResponse(
			res,
			addAddress,
			'Congratulations! New address added successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Update Address
 * @description: fuction is used to Update Address Detail
 */
const UpdateExistAddress = async (req, res) => {
	try {
		let data = req.body;

		let address_id = req.body.addressId;

		if (!data.addressId)
			throw badRequestError(res, 'Please enter addressId to update address.');
		if (!data.lat)
			throw badRequestError(res, 'Please enter the lattitude of address.');
		if (!data.lng)
			throw badRequestError(res, 'Please enter the longitude of address.');
		if (!data.full_address)
			throw badRequestError(res, 'Please enter the full address.');
		if (!data.address_type)
			throw badRequestError(res, 'Please enter the address type.');
		// if (data.active_address == "" || data.active_address == null || data.active_address == undefined) throw badRequestError(res, "Please enter the active address.");
		data.user_id = req.user.user_id;

		delete data.addressId;

		if (data.howToReach.length > 100)
			throw badRequestError(res, 'how to reach char length is above 100!');

		let chekAddressExist = await Address.query()
			.where({ id: address_id, user_id: req.user.user_id })
			.first()
			.returning('*');
		if (chekAddressExist == undefined)
			throw badRequestError(res, 'Address with this id does not exist!');

		let updateAddress = await Address.query()
			.update(data)
			.where({ id: address_id, user_id: req.user.user_id })
			.first()
			.returning('*');

		// console.log(updateAddress)

		if (!updateAddress) throw badRequestError(res, 'Someting Went Wrong!');

		return okResponse(res, updateAddress, 'Address updated successfully!');
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Delete Address
 * @description: fuction is used to Delete Address
 */
const DeleteExistAddress = async (req, res) => {
	try {
		if (!req.query.addressId)
			throw badRequestError(res, 'please pass addressId in query string.');
		let address_id = req.query.addressId;

		let chekAddressExist = await Address.query()
			.where({ id: address_id, user_id: req.user.user_id })
			.first()
			.returning('*');
		if (chekAddressExist == undefined)
			throw badRequestError(res, 'Address with this id does not exist!');

		let deleteAddress = await Address.query()
			.delete()
			.where({ id: address_id, user_id: req.user.user_id })
			.first()
			.returning('*');
		return okResponse(
			res,
			deleteAddress,
			'Address data deleted successfully!!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Get Address Detail
 * @description: fuction is used to Get Address Detail By Id
 */
const GetAddressDetail = async (req, res) => {
	try {
		if (!req.query.addressId)
			throw badRequestError(res, 'please pass addressId in query string.');
		let address_id = req.query.addressId;

		let chekAddressExist = await Address.query()
			.where({ id: address_id, user_id: req.user.user_id })
			.first()
			.returning('*');
		if (chekAddressExist == undefined)
			throw badRequestError(res, 'Address with this id does not exist!');

		let getAddress = await Address.query()
			.delete()
			.where({ id: address_id, user_id: req.user.user_id })
			.first()
			.returning('*');
		return okResponse(res, getAddress, 'Address details get successfully!!');
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//=====================================================================================

module.exports = {
	InsertNewAddress,
	UpdateExistAddress,
	DeleteExistAddress,
	GetAddressDetail,
};

//----------------------------------------------CountryController End------------------------------------------------------------------
