"use strict";

/**
 * @author SHUBHAM GUPTA
 */

//----------------------------------------------RestaurantManagement Controller (Start)------------------------------------------------------------------

const moment = require("moment")

//Import Models Here
const Orders = require("../../models/Orders");
const MerchantCharacteristics = require("../../models/Merchant_Characteristics");
const Table = require("../../models/Table");
const Notification = require("../../models/Notification");
const AuthTable = require("../../models/AuthTable");
const OrderStatusFlowManagement = require("../../models/OrderStatusManagement");

const PushNotif = require("../../middlewares/push-notification")
let knexConfig = require('../../../db/knexfile');
const Knex = require('knex')(knexConfig['development']);
const STRIPE = require("../../middlewares/stripe");
const EMAIL = require("../../middlewares/email");


/**
 *list Orders
 * @description: function is used to listing Orders
 */
const ListOrders = async (req, res) => {
    try {
        /*-------------------Check Required Fields----------------------------*/
        if (!req.query.order_status) throw badRequestError(res, "Please pass order_status in query string.");
        if (!req.query.order_type_id) throw badRequestError(res, "Please pass order_type_id in query string.");
        if (!req.query.page) throw badRequestError(res, "Please pass page in query string.");

        let NewOrders = await Orders.query()
                .count("id as NewOrders")
                .where({ merchant_id: req.user.user_id, order_status: 1 })
                .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
                }).first();

        let InKitchen = await Orders.query()
                .count("id as InKitchen")
                .where({ merchant_id: req.user.user_id, order_status: 2 })
                .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
                }).first();

        let ReadyOrders =  await Orders.query()
                .count("id as ReadyOrders")
                .where({ merchant_id: req.user.user_id, order_status: 3 })
                .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
                }).first();

        let ServedOrders =  await Orders.query()
                .count("id as ServedOrders")
                .where({ merchant_id: req.user.user_id, order_status: 5 })
                .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
                }).first();

        let CompletedOrders =  await Orders.query()
                .count("id as CompletedOrders")
                .where({ merchant_id: req.user.user_id, order_status: 6 })
                .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
                }).first();

        let CancelledOrders =  await Orders.query()
                .count("id as CancelledOrders")
                .where({ merchant_id: req.user.user_id, order_status: 7 })
                .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
                }).first();


        let orders = await Orders.query()
            .select("*")
            .eager('[ordered_products_relation, orders_of_users]')
            .modifyEager('orders_of_users', builder => {
                return builder.select('users.first_name', 'users.last_name', 'users.mobile', 'users.profile_image')
            })
            .modifyEager('ordered_products_relation', builder => {
                return builder.select('ordered_product_quantity', 'product_name', 'product_price')
                    .eager('[products_customization]')
                    .modifyEager('products_customization', builder => {
                        return builder.select('').eager('[modifier_customization, order_product_modifier_detail]')
                            .modifyEager('order_product_modifier_detail', builder => {
                                return builder.select('modifier_quantity').eager('modifier_product_detail_relations')
                                    .modifyEager('modifier_product_detail_relations', builder => {
                                        return builder.select('modifier_product', 'selling_price', 'compare_at_price')
                                    })
                            })
                            .modifyEager('modifier_customization', builder => {
                                return builder.select('modifier_name')
                            })
                    })
            })
            .where({ merchant_id: req.user.user_id, order_status: req.query.order_status })
            .where(builder => {
                    if (req.query.order_type_id == 1) {
                        return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                    }
                    else {
                        return builder.where("order_type_id", req.query.order_type_id);
                    }
            })
            .where(builder => {
                if (req.query.search !== '') {
                    return builder.where("orders.order_number", "iLike", "%" + req.query.search + "%")
                }
            })
            .orderBy("created_at", "desc")
            .page(req.query.page, 10)
            .returning("*");

        if (orders.results.length > 0){
            if (req.query.order_status == 1){
                //-----------Default 15 Minute Timer code (START)-------------------------
                for (let i = 0; i < orders.results.length; i++) {
                    if ((orders.results[i].accept_date_time == null || orders.results[i].accept_date_time == "") && orders.results[i].estimated_time == null){
                        let created_at = moment(orders.results[i].created_at);
                        let till_time = created_at;
                        till_time.minutes((till_time.minutes() + 15));
                        let current_time = moment();
                        let duration = moment.duration(till_time.diff(current_time));
                        let second = duration.asSeconds();
                        if (second < 0) {
                            await Orders.query().update({
                                order_status: 7,
                                order_cancellation_reason: "Order Not Accepted"
                            }).where("id", orders.results[i].id);
                            second = 0;
                        }
                        orders.results[i].seconds = second
                    }
                }
                //--------------Default 15 Minute Timer code (END)------------------------
            }

            //-----------Time Elapse Timer code (START)-------------------------
            for (let i = 0; i < orders.results.length; i++) {
                if (orders.results[i].accept_date_time != null && orders.results[i].estimated_time != null) {
                    let created_at = moment(orders.results[i].accept_date_time);
                    let till_time = created_at;
                    till_time.minutes((till_time.minutes() + orders.results[i].estimated_time));
                    let current_time = moment();
                    let duration = moment.duration(till_time.diff(current_time));
                    let second = duration.asSeconds();
                    if (second < 0) {
                        second = 0;
                    }
                    orders.results[i].time_elapse = second;
                }
            }
            //-----------Time Elapse Timer code (END)-------------------------
        }
    
        orders.NewOrders = NewOrders;
        orders.InKitchen = InKitchen;
        orders.ReadyOrders = ReadyOrders;
        orders.ServedOrders = ServedOrders;
        orders.CompletedOrders = CompletedOrders;;
        orders.CancelledOrders = CancelledOrders;;


        return okResponse(res, orders, "Orders List fetched successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 *Get Order Details
 * @description: function is used to get Order Details
 */
const getOrderDetails = async (req, res) => {
    try {

        /*-------------------Check Required Fields----------------------------*/
        if (!req.query.order_id) throw badRequestError(res, "Please pass order_id in query string.");

        let orders = await Orders.query()
            .select("*")
            .eager('[orders_of_users, orders_of_merchant, merchant_address_relation, ordered_products_relation.[products_customization]]')
            .modifyEager('orders_of_users', builder => {
                return builder.select('users.first_name', 'users.last_name', 'users.mobile', 'users.country_isd_code', 'users.profile_image')
            })
            .modifyEager('orders_of_merchant', builder => {
                return builder.select('users.first_name', 'users.last_name', 'users.mobile', 'users.country_isd_code', 'users.restaurant_name')
                    .eager('merchant_characteristic_relation')
                    .modifyEager('merchant_characteristic_relation', builder => {
                        return builder.select('gst_number')
                    })
            })
            .modifyEager('merchant_address_relation', (builder) => {
                return builder.select('full_address');
            })
            .modifyEager('ordered_products_relation', (builder) => {
                return (
                    builder
                        .select('product_name', 'ordered_product_quantity', 'product_price')
                        //.eager('[products_relation]')
                        .modifyEager('products_customization', (builder) => {
                            return builder
                                .select(
                                    Knex.raw(
                                        '(select "modifier_name" from modifier where "modifier"."id" = "order_customization"."modifier_id")'
                                    ),
                                    Knex.raw(
                                        '(select "display_name" from modifier where "modifier"."id" = "order_customization"."modifier_id")'
                                    )
                                )
                                .eager('order_product_modifier_detail')
                                .modifyEager('order_product_modifier_detail', (builder) => {
                                    return builder.select(
                                        '*'
                                        // Knex.raw(
                                        //     '(select modifier_product from "modifier_product_details" where "id" = "order_product_modifier_detail"."modifier_product_detail_id" )'
                                        // ),
                                        // Knex.raw(
                                        //     '(select selling_price from "modifier_product_details" where "id" = "order_product_modifier_detail"."modifier_product_detail_id" )'
                                        // )
                                    )
                                    .eager('modifier_product_detail_relations')
                                    .modifyEager('modifier_product_detail_relations', builder => {
                                        return builder.select("modifier_product")
                                    })
                                });
                        })
                );
            })
            .where({ merchant_id: req.user.user_id, id: req.query.order_id })
            .first();


        if(orders.accept_date_time != null){
            let accept_at = moment(orders.accept_date_time);
            accept_at.minutes((accept_at.minutes() + orders.estimated_time));
            orders.estimated_date_with_time = accept_at
        }


        let price;
        for (let i = 0; i < orders.ordered_products_relation.length; i++){
            price = 0;
            for (let j = 0; j < orders.ordered_products_relation[i].products_customization.length; j++){
                for (let k = 0; k < orders.ordered_products_relation[i].products_customization[j].order_product_modifier_detail.length; k++) {
                    price = price + (orders.ordered_products_relation[i].products_customization[j].order_product_modifier_detail[k].modifier_quantity * orders.ordered_products_relation[i].products_customization[j].order_product_modifier_detail[k].selling_price);
                }
            }
            orders.ordered_products_relation[i].productPriceWithModifier = ((orders.ordered_products_relation[i].product_price * orders.ordered_products_relation[i].ordered_product_quantity) + price).toFixed(2);
        }

        return okResponse(res, orders, "Order Details fetched successfully.");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Change Order Status
 * @description: function is used to Change Order Status
 */
const changeOrderStatus = async (req, res) => {
    try {
        let orders, notificationMessage, notificationType, emailData, orderType;

        /*-------------------Check Required Fields----------------------------*/
        if (!req.body.order_status) throw badRequestError(res, "Please enter order_status.");
        if (!req.body.order_id) throw badRequestError(res, "Please enter order_id.");

        //----------------------------To get Order Details (START)------------------------------------------------------------------------
        let order_details = await Orders.query().select("*").eager('[orders_of_users, orders_of_merchant]')
            .modifyEager('orders_of_users', builder => {
                return builder.select("id", "first_name", "last_name", "notification_toggle", "email");
            })
            .modifyEager('orders_of_merchant', builder => {
                return builder.select("id", "first_name", "last_name", "restaurant_name");
            })
            .where({ merchant_id: req.user.user_id, id: req.body.order_id }).first().returning("*");

        
        let UserAuthDevices = await AuthTable.query().select("*").where("user_id", order_details.user_id).whereNot({
            device_type: null,
            device_token: null
        })
        //----------------------------To get Order Details (END)------------------------------------------------------------------------
        
        if (order_details.order_type_id == 1 || order_details.order_type_id == 5 || order_details.order_type_id == 6){
            orderType = "Dinein"
        } else if (order_details.order_type_id == 2) {
            orderType = "Pickup"
        } else if (order_details.order_type_id == 4) {
            orderType = "Delivery"
        }

        let str = order_details.transaction_id;
        str = str.substring(0, 3);
        console.log(str);

        if (req.body.order_status == 2) {
            if (!req.body.estimated_time)
                throw badRequestError(res, "Enter Estimated Time.");

            let accepted_datetime = moment().format("YYYY-MM-DD HH:mm:ss");

            orders = await Orders.query()
                .patch({
                    order_status: req.body.order_status,
                    estimated_time: req.body.estimated_time,
                    accept_date_time: accepted_datetime
                })
                .where({ merchant_id: req.user.user_id, id: req.body.order_id })
                .first();

            if (req.body.table_id && req.body.table_id>0 && req.body.table_id!=null && req.body.table_id!=undefined){
                let tableOccupied = await Table.query().update({occupied_by: req.body.order_id})
                    .where({ merchant_id: req.user.user_id, id: req.body.table_id })
            }

            notificationMessage = `${order_details.orders_of_merchant.restaurant_name} has accepted your order ${order_details.order_number} is being processed.`;
            notificationType = "Order Confirmed"

            emailData = {
                email: order_details.orders_of_users.email,
                user_name: order_details.orders_of_users.first_name,// + ' ' + order_details.orders_of_users.last_name,
                order_number: order_details.order_number,
                merchant_name: order_details.orders_of_merchant.restaurant_name,
                order_type: orderType,
                accepted_date: req.body.estimated_time + ' ' +"minutes"
            }
            EMAIL.sendConfirmedOrderDetails(emailData)

        } else if (req.body.order_status == 7) {
          if (!req.body.order_cancellation_reason)
            throw badRequestError(res, "Enter Order Cancellation Reason.");

          orders = await Orders.query()
            .patch({
              order_status: req.body.order_status,
              order_cancellation_reason: req.body.order_cancellation_reason,
            })
            .where({ merchant_id: req.user.user_id, id: req.body.order_id })
            .first();


            if (req.body.table_id && req.body.table_id>0 && req.body.table_id!=null && req.body.table_id!=undefined){
                let tableFree = await Table.query().update({occupied_by: null})
                    .where({ merchant_id: req.user.user_id, id: req.body.table_id }).first();
                
                await Orders.query().patch({ table_id: null }).where({ merchant_id: req.user.user_id, id: req.body.order_id }).first();
            }

            if(req.body.status == "auto_cancel"){
                notificationMessage = `Oh nooo! unfortunately your order has been auto cancelled because it is not accepted by ${order_details.orders_of_merchant.restaurant_name}. We know how annoying this can be and we are sorry :(`;
                notificationType = "Order Cancelled"
            }else{
                notificationMessage = `Oh nooo! unfortunately your order has been cancelled by ${order_details.orders_of_merchant.restaurant_name} due to ${req.body.order_cancellation_reason}. We know how annoying this can be and we are sorry :(`;
                notificationType = "Order Cancelled"
            }

            //--------------------Cancel Stripe Payment-------------------------------
            if(str == "ch_"){
                console.log("Cancel Stripe payment by Charge Id");
            }else if(str == "pi_"){
                console.log("Cancel Stripe Payment Intent");
                STRIPE.cancelPaymentIntent(order_details.transaction_id);
            }

            //-------Send Email Of Order Cancellation-----------
            emailData = {
                email: order_details.orders_of_users.email,
                user_name: order_details.orders_of_users.first_name, //+ ' ' + order_details.orders_of_users.last_name,
                order_number: order_details.order_number,
                merchant_name: order_details.orders_of_merchant.restaurant_name,
                order_cancellation_reason: req.body.order_cancellation_reason
            }
            EMAIL.sendCancelledOrderDetails(emailData)
        } 
        else {
            if (req.body.order_status == 6){
                if (req.body.table_id && req.body.table_id>0 && req.body.table_id!=null && req.body.table_id!=undefined){
                    let tableFree = await Table.query().update({occupied_by: null})
                        .where({ merchant_id: req.user.user_id, id: req.body.table_id }).first();

                    await Orders.query().patch({ table_id: null }).where({ merchant_id: req.user.user_id, id: req.body.order_id }).first();
                }

                if(order_details.order_type_id == 1 || order_details.order_type_id == 5 || order_details.order_type_id == 6){
                    notificationMessage = `Chee hoo! Your order ${order_details.order_number} from ${order_details.orders_of_merchant.restaurant_name} is completed! Leave a review here.`
                    notificationType = "Order Completed"
                } else if (order_details.order_type_id == 2) {
                    notificationMessage = `Chee hoo! Your order ${order_details.order_number} from ${order_details.orders_of_merchant.restaurant_name} is completed! Leave a review here.`
                    notificationType = "Order Completed"
                } else if (order_details.order_type_id == 4) {
                    notificationMessage = `Chee hoo! Your order ${order_details.order_number} from ${order_details.orders_of_merchant.restaurant_name} is completed! Leave a review here.`;
                    notificationType = "Order Completed"
                }

                //--------------------Capture Stripe Payment-------------------------------
                if(str == "ch_"){
                    console.log("Capture Stripe payment by Charge Id");
                    STRIPE.capturePayment(order_details.transaction_id)
                }else if(str == "pi_"){
                    console.log("Capture Stripe Payment Intent");
                    STRIPE.capturePaymentIntent(order_details.transaction_id);
                }
            }
            else if (req.body.order_status == 3 && (order_details.order_type_id == 2 || order_details.order_type_id == 6)) {
                notificationMessage = `Chee hoo! Your order ${order_details.order_number} from ${order_details.orders_of_merchant.restaurant_name} is ready for pick up! Leave a review here.`;
                notificationType = "Ready for pickup"

                //-------Send Email Of Order Cancellation-----------
                emailData = {
                    email: order_details.orders_of_users.email,
                    user_name: order_details.orders_of_users.first_name, // + ' ' + order_details.orders_of_users.last_name,
                    order_number: order_details.order_number,
                    merchant_name: order_details.orders_of_merchant.restaurant_name,
                }
                EMAIL.sendReadyForPickupOrderDetails(emailData)
            }
            else if (req.body.order_status == 5 && order_details.order_type_id == 4) {
                notificationMessage = `Chee hoo! Your order ${order_details.order_number} from ${order_details.orders_of_merchant.restaurant_name} has been dispatched!`;
                notificationType = "Order dispatched"

                //-------Send Email Of Order Cancellation-----------
                emailData = {
                    email: order_details.orders_of_users.email,
                    user_name: order_details.orders_of_users.first_name, // + ' ' + order_details.orders_of_users.last_name,
                    order_number: order_details.order_number,
                    merchant_name: order_details.orders_of_merchant.restaurant_name,
                    delivery_address: order_details.delivery_address
                }
                EMAIL.sendDispatchForDeliveryOrderDetails(emailData)
            }

            orders = await Orders.query()
                .patch({ order_status: req.body.order_status })
                .where({ merchant_id: req.user.user_id, id: req.body.order_id })
                .first();
        }
   
        //----------------------Send Push Notification {START}------------------------------
        let notificationData = {
            user_id: order_details.orders_of_users.id,
            order_id: req.body.order_id,
            type: notificationType,
            message: notificationMessage,
            read_status: false,
        };

        let inserted_noti = await Notification.query().insert(notificationData);
        let unread_count = await Notification.query().count('id')
            .where({ user_id: order_details.orders_of_users.id, read_status: false }).first();
        let unread_noti = unread_count.count;

        if (order_details.orders_of_users.notification_toggle == true) {
            for (let i = 0; i < UserAuthDevices.length; i++) {
                PushNotif.sendPushNotif({
                    content: notificationMessage,
                    device_token: UserAuthDevices[i].device_token,
                    device_type: UserAuthDevices[i].device_type,
                    user_type: 'USER',
                    type: notificationType,
                    noti_count: unread_noti,
                    orderId: req.body.order_id,
                    notificationId: inserted_noti.id,
                    orderstatusId: req.body.order_status,
                    orderTypeId: order_details.order_type_id
                }, {});
            }
        }
        //----------------------Send Push Notification {END}------------------------------

        let orderStatusFlowMaintain = await OrderStatusFlowManagement.query().insert({ order_id: req.body.order_id, order_status: req.body.order_status }).returning('*');
        if (!orderStatusFlowMaintain) throw badRequestError(res, "Something Went Wrong.");

        return okResponse(res, orders, "Order Status updated successfully.");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 *Get Order Overview
 * @description: function is used to get Order Overview
 */
const OrderOverView = async (req, res) => {
    try{
        let newOrdersDineIn = await Orders.query()
            .count('id as newOrdersDineIn')
            .where({ merchant_id: req.user.user_id, order_status: 1 })
            .andWhere(builder => {
                return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6)
            })
            
        let newOrdersPickUp = await Orders.query()
            .count('id as newOrdersPickUp')
            .where({ merchant_id: req.user.user_id, order_type_id: 2, order_status: 1 });

        let newOrdersDelivery = await Orders.query()
            .count('id as newOrdersDelivery')
            .where({ merchant_id: req.user.user_id, order_type_id: 4, order_status: 1 });

        let completedOrdersDineIn = await Orders.query()
            .count('id as completedOrdersDineIn')
            .where({ merchant_id: req.user.user_id, order_status: 6 })
            .andWhere(builder => {
                return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6)
            })

        let completedOrdersPickUp = await Orders.query()
            .count('id as completedOrdersPickUp')
            .where({ merchant_id: req.user.user_id, order_type_id: 2, order_status: 6 });

        let completedOrdersDelivery = await Orders.query()
            .count('id as completedOrdersDelivery')
            .where({ merchant_id: req.user.user_id, order_type_id: 4, order_status: 6 }); 

        let cancelledOrdersDineIn = await Orders.query()
            .count('id as cancelledOrdersDineIn')
            .where({ merchant_id: req.user.user_id, order_status: 7 })
            .andWhere(builder => {
                return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6)
            })

        let cancelledOrdersPickUp = await Orders.query()
            .count('id as cancelledOrdersPickUp')
            .where({ merchant_id: req.user.user_id, order_type_id: 2, order_status: 7 });

        let cancelledOrdersDelivery = await Orders.query()
            .count('id as cancelledOrdersDelivery')
            .where({ merchant_id: req.user.user_id, order_type_id: 4, order_status: 7 });

        let unpaidOrders = await Orders.query()
            .count('id as unpaidOrders')
            .where({ merchant_id: req.user.user_id, payment_id: null, transaction_id: null }); 
        
        let onlineOrderStatus = await MerchantCharacteristics.query()
            .select("id", "online_order_status", "opening_status")
            .where({ merchant_id: req.user.user_id })
            .first();

        let order_overview = {
            "newOrdersDineIn": newOrdersDineIn ? newOrdersDineIn : {"newOrdersDineIn": "0"},
            "newOrdersPickUp": newOrdersPickUp ? newOrdersPickUp : {"newOrdersPickUp": "0"},
            "newOrdersDelivery": newOrdersDelivery ? newOrdersDelivery : {"newOrdersDelivery": "0"} ,
            "completedOrdersDineIn": completedOrdersDineIn ? completedOrdersDineIn : {"completedOrdersDineIn": "0"},   
            "completedOrdersPickUp": completedOrdersPickUp ? completedOrdersPickUp : {"completedOrdersPickUp": "0"},
            "completedOrdersDelivery": completedOrdersDelivery ? completedOrdersDelivery : {"completedOrdersDelivery": "0"},
            "cancelledOrdersDineIn": cancelledOrdersDineIn ? cancelledOrdersDineIn : {"cancelledOrdersDineIn": "0"},
            "cancelledOrdersPickUp": cancelledOrdersPickUp ? cancelledOrdersPickUp : {"cancelledOrdersPickUp": "0"},
            "cancelledOrdersDelivery": cancelledOrdersDelivery ? cancelledOrdersDelivery : {"cancelledOrdersDelivery": "0"},
            "unpaidOrders": unpaidOrders ? unpaidOrders : {"unpaidOrders": "0"},
            "onlineOrderStatus": onlineOrderStatus
        }

        return okResponse(res, order_overview, "Order Overview details fetched successfully.");
        
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Change Online Order Status
 * @description: function is used to Change Online Order Status
 */
const changeOnlineOrderStatus = async (req, res) => {
    try {
        let MerchantOpeningAndOnlineOrders;
        
        if(req.body.type == 1){
            MerchantOpeningAndOnlineOrders = await MerchantCharacteristics.query()
              .patch({ online_order_status: req.body.order_status })
              .where({ merchant_id: req.user.user_id })
              .first();
        }else if (req.body.type == 2){
            MerchantOpeningAndOnlineOrders = await MerchantCharacteristics.query()
              .patch({ opening_status: req.body.order_status })
              .where({ merchant_id: req.user.user_id })
              .first();
        }

        return okResponse(res, MerchantOpeningAndOnlineOrders, "Status updated successfully.");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}



//---Export the modules here------
module.exports = {
    ListOrders,
    getOrderDetails,
    changeOrderStatus,
    OrderOverView,
    changeOnlineOrderStatus
}
