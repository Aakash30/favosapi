'use strict';

/**
 * @author SHUBHAM GUPTA
 */

//----------------------------------------------RestaurantManagement Controller (Start)------------------------------------------------------------------

//Import Dependency 3rd Party Modules Here
const validator = require('validator');

//Import Models Here
const Menu = require('../../models/MenuManagement');
const Category = require('../../models/Categories');
const Modifier = require('../../models/Modifier');
const GroupModifier = require('../../models/ModifierGroup');
const Product = require('../../models/Product');
const ProductMenu = require('../../models/ProductMenu');
const Table = require('../../models/Table');
const MenuOrderType = require('../../models/MenuOrderType');
const ProductTags = require('../../models/ProductTags');
const Suggestions = require('../../models/Suggestions');
let knexConfig = require('../../../db/knexfile');
const Knex = require('knex')(knexConfig['development']);
const ProductPricing = require("../../models/ProductPricing");
const ProductCategory = require("../../models/ProductCategory");
const { transaction, ref } = require('objection');


const Buffer = require('buffer').Buffer;
const moment = require("moment");


//================================================= MENU Apis (START) ==================================================================
/**
 * Add Menu
 * @description: function is used to add menu
 */
const AddMenu = async (req, res) => {
	try {
		let data = req.body;
		data.merchant_id = req.user.user_id;
		/*-------------------Check Required Fields----------------------------*/
		if (!data.menu_name)
			throw badRequestError(res, 'Enter menu name.');
		if (!data.menu_name.length > 120)
			throw badRequestError(res, 'Menu Name has exceeded the maximum limit of 120 characters.');

		let MenuNameExist = await Menu.query()
			.select('id', 'menu_name', 'delete_status', 'merchant_id')
			.where({
				menu_name: data.menu_name,
				delete_status: false,
				merchant_id: req.user.user_id,
			})
			.first();

		//-------Check if Email already exist or not--------------------
		if (MenuNameExist) {
			throw badRequestError(res, 'Menu name already exists.');
		}

		let createMenu = await Menu.query().insertGraph(data).returning('id');

		return createdResponse(
			res,
			createMenu,
			'Menu added successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Menu Listing
 * @description: function is used to list all menus
 */
const MenuList = async (req, res) => {
	try {
		let MenuList;
		let query = Menu.query()
			.select('*')
			.where({ merchant_id: req.user.user_id, delete_status: false })
			.orderBy('created_at', 'desc')
			.returning('*');

		if (req.query.page || req.query.search) {
			MenuList = await query
				.where((builder) => {
					if (req.query.search !== '') {
						return builder.whereRaw(
							"LOWER(menu_management.menu_name) LIKE '%' || LOWER(?) || '%'",
							req.query.search
						);
					}
				})
				.page(req.query.page, 10);

		} else {
			MenuList = await query
		}

		return okResponse(
			res,
			MenuList,
			'Menu List fetched successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Change Menu Status
 * @description: function is used to Change Menu Status
 */
const ChangeMenuStatus = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.query.menu_id)
			throw badRequestError(res, 'Please pass menu_id in query string.');
		if (!req.query.status)
			throw badRequestError(res, 'Please pass status in query string.');

		let ChangeMenuStatus = await Menu.query()
			.update({ status: req.query.status })
			.where({ id: req.query.menu_id })
			.first();

		return okResponse(
			res,
			ChangeMenuStatus,
			'Menu status changed successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Delete Menu
 * @description: function is used to Delete Menu
 */
const deleteMenu = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.body.menu_id) throw badRequestError(res, 'Please enter menu_id.');

		let ProductMenus = await ProductMenu.query().select("*")
			.eager('[product_menu_belongsto, products_associated]')
			.modifyEager('product_menu_belongsto', builder => {
				return builder.select("*").where('delete_status', false)
			})
			.modifyEager('products_associated', builder => {
				return builder.select("*").where('delete_status', false)
			})
			.where({
				menu_id: req.body.menu_id
			})

		let count = 0;
		for (let i = 0; i < ProductMenus.length ; i++){
			if (ProductMenus[i].product_menu_belongsto != null && ProductMenus[i].products_associated != null){
				count++;
			}
		}		
		if(count > 0){
			throw badRequestError(res, "Menu can not be deleted. It is associated with multiple products");
		}

		let deleteMenu = await Menu.query()
			.update({ delete_status: true })
			.where({ id: req.body.menu_id })
			.first();

		return okResponse(
			res,
			deleteMenu,
			'Menu deleted successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Change Menu Order Type Status
 * @description: function is used to Change Menu Order Type Status
 */
const ChangeMenuOrderTypeStatus = async (req, res) => {
	try {
		let data = req.body;

		/*-------------------Check Required Fields----------------------------*/
		if (!data.menu_id) throw badRequestError(res, 'Please enter menu_id.');
		if (!data.order_type_id)
			throw badRequestError(res, 'Please enter order_type_id.');

		let ChangeOrderStatus;

		if (data.order_type_id == 1) {
			ChangeOrderStatus = await Menu.query()
				.update({ DineIn: data.order_type_status })
				.where({ id: data.menu_id })
				.first();
		} else if (data.order_type_id == 2) {
			ChangeOrderStatus = await Menu.query()
				.update({ PickUp: data.order_type_status })
				.where({ id: data.menu_id })
				.first();
		} else if (data.order_type_id == 4) {
			ChangeOrderStatus = await Menu.query()
				.update({ Delivery: data.order_type_status })
				.where({ id: data.menu_id })
				.first();
		}

		if (data.order_type_status == true) {
			delete data.order_type_status;
			await MenuOrderType.query().insert(data).returning('*');
		} else {
			delete data.order_type_status;
			await MenuOrderType.query()
				.delete()
				.where({ menu_id: data.menu_id, order_type_id: data.order_type_id })
				.first();
		}

		return okResponse(
			res,
			ChangeOrderStatus,
			'Menu order status changed successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Update Menu
 * @description: function is used to Update menu
 */
const UpdateMenu = async (req, res) => {
	try {
		let data = req.body;
		data.merchant_id = req.user.user_id;
		
		/*-------------------Check Required Fields----------------------------*/
		if (!data.menu_name)
			throw badRequestError(res, 'Enter menu name.');
		if (!data.menu_name.length > 120)
			throw badRequestError(res, 'Menu Name has exceeded the maximum limit of 120 characters.');

		let MenuNameExist = await Menu.query()
			.select(('id', 'menu_name', 'delete_status', 'merchant_id'))
			.whereNot('id', data.id)
			.where({
				menu_name: data.menu_name,
				delete_status: false,
				merchant_id: req.user.user_id,
			})
			.first();

		//-------Check if Email already exist or not--------------------
		if (MenuNameExist) {
			throw badRequestError(res, 'Menu name already exists.');
		}

		let updateMenu = await Menu.query().upsertGraph(data).returning('id');

		return createdResponse(
			res,
			updateMenu,
			'Menu updated successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


//================================================= MENU Apis (END) ==================================================================

//================================================= CATEGORY Apis (START) ==================================================================
/**
 * Add Category
 * @description: function is used to add Category
 */
const AddCategory = async (req, res) => {
	try {
		let data = req.body;
		data.merchant_id = req.user.user_id;
		
		/*-------------------Check Required Fields----------------------------*/
		if (!data.category_name)
			throw badRequestError(res, 'Enter category name.');
		if (!data.category_name.length > 120)
			throw badRequestError(res, 'Category Name has exceeded the maximum limit of 120 characters.');

		let CategoryNameExist = await Category.query()
			.select('id', 'category_name')
			.where({
				category_name: data.category_name,
				delete_status: false,
				merchant_id: req.user.user_id,
			})
			.first();

		//-------Check if Email already exist or not--------------------
		if (CategoryNameExist) {
			throw badRequestError(res, 'Category name already exists.');
		}

		let createCategory = await Category.query().insert(data).returning('id');

		return createdResponse(
			res,
			createCategory,
			'Category added successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Category Listing
 * @description: function is used to list all categories
 */
const CategoryList = async (req, res) => {
	try {
		let CategoryList;
		let query = Category.query()
			.select('*')
			.eager('category_product')
			.modifyEager('category_product', builder => {
				return builder.select("*").where("delete_status", false);
			})
			.where({ merchant_id: req.user.user_id, delete_status: false })
			.orderBy('created_at', 'desc')
			.returning('*');

		if (req.query.page || req.query.search) {
			CategoryList = await query
				.where((builder) => {
					if (req.query.search !== '') {
						return builder.whereRaw(
							"LOWER(categories.category_name) LIKE '%' || LOWER(?) || '%'",
							req.query.search
						);
					}
				})
				.page(req.query.page || 0, 10);

			if (CategoryList) {
				CategoryList.results.map(function (value) {
					value.productCount = value.category_product.length;
				});
			}

		} else {
			CategoryList = await query
		}



		return okResponse(
			res,
			CategoryList,
			'Category List fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Get Category Info
 * @description: function is used to Get Category Info
 */
const GetCategoryInfo = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.query.category_id)
			throw badRequestError(res, 'Please pass category_id in query string.');

		let GetCategoryInfo = await Category.query()
			.select('*')
			.where({ id: req.query.category_id })
			.returning('*');

		return okResponse(
			res,
			GetCategoryInfo,
			'Category Details fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Update Category
 * @description: function is used to Update Category
 */
const UpdateCategory = async (req, res) => {
	try {
		let data = req.body;
		data.merchant_id = req.user.user_id;
		
		/*-------------------Check Required Fields----------------------------*/
		if (!data.category_name)
			throw badRequestError(res, 'Enter category name.');
		if (!data.category_name.length > 120)
			throw badRequestError(res, 'Category Name has exceeded the maximum limit of 120 characters.');

		let CategoryNameExist = await Category.query()
			.select('id', 'category_name')
			.whereNot('id', data.id)
			.where({
				category_name: data.category_name,
				delete_status: false,
				merchant_id: req.user.user_id,
			})
			.first();

		//-------Check if Email already exist or not--------------------
		if (CategoryNameExist) {
			throw badRequestError(res, 'Category name already exists.');
		}

		let updateCategory = await Category.query()
			.update(data)
			.where('id', data.id)
			.returning('id');

		await ProductCategory.query().patch({category_name : data.category_name}).where('category_id', data.id)

		return okResponse(
			res,
			updateCategory,
			'Category updated successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Change Category Status
 * @description: function is used to Change Category Status
 */
const ChangeCategoryStatus = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.query.category_id)
			throw badRequestError(res, 'Please pass category_id in query string.');
		if (!req.query.status)
			throw badRequestError(res, 'Please pass status in query string.');

		let ChangeCategoryStatus = await Category.query()
			.update({ status: req.query.status })
			.where({ id: req.query.category_id })
			.first();

		return okResponse(
			res,
			ChangeCategoryStatus,
			'Category status changed successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Delete Category
 * @description: function is used to Delete Category
 */
const deleteCategory = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.body.category_id)
			throw badRequestError(res, 'Please enter category_id.');

		// if (!req.body.productCount)
		// 	throw badRequestError(res, 'Please enter productCount.');

		if (req.body.productCount > 0){
			throw badRequestError(res, 'Category can not be deleted because it is associated with number of products.');
		}
		
		let deleteCategory = await Category.query()
			.update({ delete_status: true })
			.where({ id: req.body.category_id })
			.first();

		return okResponse(
			res,
			deleteCategory,
			'Category deleted successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================= CATEGORY Apis (END) ==================================================================

//================================================= MODIFIER Apis (START) ==================================================================
/**
 * Add Modifier
 * @description: function is used to add Modifier
 */
const AddModifier = async (req, res) => {
    try {
        let err, data = req.body;
        data.merchant_id = req.user.user_id;
        
        /*-------------------Check Required Fields----------------------------*/
		if (!data.modifier_name) throw badRequestError(res, "Enter modifier_name.");
		if (!data.modifier_name.length > 120)
			throw badRequestError(res, 'Modifier Name has exceeded the maximum limit of 120 characters.');
		// if (!data.display_name) throw badRequestError(res, "Enter modifier display name.");
		if (!data.display_name.length > 120)
			throw badRequestError(res, 'Display Name has exceeded the maximum limit of 120 characters.');

        if (data.min_user_must_select == "" || data.min_user_must_select == null){
            data.min_user_must_select = 0
        }
        if (data.max_user_must_select == "" || data.max_user_must_select == null){
            data.max_user_must_select = 0
		} 
		
		if (data.min_user_must_select > 0 && data.max_user_must_select > 0){
			if (data.min_user_must_select > data.max_user_must_select) {
				throw badRequestError(res, "Minimum modifier can't be greater than Maximum modifiers limit");
			}
		}

        let ModifierNameExist = await Modifier.query()
            .select("id", "modifier_name").where({ modifier_name: data.modifier_name, delete_status: false, merchant_id: req.user.user_id}).first();

        // let DisplayNameExist = await Modifier.query()
        //     .select("id", "display_name").where({ display_name: data.display_name, delete_status: false, merchant_id: req.user.user_id }).first();
        
        //-------Check if modifier name already exist or not--------------------
        if (ModifierNameExist) {
            throw badRequestError(res, "Modifier name already exists.");
        }

        //-------Check if Display Name already exist or not--------------------
        // if (DisplayNameExist) {
        //     throw badRequestError(res, "Display name already exists.");
        // }

		let counter = 0, modifierAsDefault = 0;
		if (data.min_user_must_select > 0) {
			for (let i = 0; i < data.modifier_product_detail_relation.length; i++) {
				if (data.modifier_product_detail_relation[i].default == false) {
					counter++;
					// console.log("counter")
				}
				if (data.modifier_product_detail_relation[i].default == true) {
					modifierAsDefault++;
					// console.log("counter")
				}
			}
			if (data.modifier_product_detail_relation.length < data.min_user_must_select || modifierAsDefault < data.min_user_must_select) {
				throw badRequestError(
					res,
					`Add at least ${data.min_user_must_select} modifier product and select those modifier products as default.`
				);
			}
			if (data.modifier_product_detail_relation.length == counter) {
				throw badRequestError(
				res,
				"Select at least one default modifier product."
				);
			}
		}

		let modifierAsDefaultMax = 0;
		if(data.max_user_must_select > 0) {
			for (let i = 0; i < data.modifier_product_detail_relation.length; i++) {
				if (data.modifier_product_detail_relation[i].default == true) {
					modifierAsDefaultMax++;
					// console.log("counter")
				}
			}

			if (data.modifier_product_detail_relation.length < data.max_user_must_select) {
				throw badRequestError(
					res,
					`You have to add modifier product maximum ${data.max_user_must_select}.`
				);
			}

			// if (data.modifier_product_detail_relation.length > data.max_user_must_select || modifierAsDefaultMax > data.max_user_must_select) {
			// 	throw badRequestError(
			// 		res,
			// 		`You can add modifier product maximum ${data.max_user_must_select} and you can select maximum ${data.max_user_must_select} modifier products as default.`
			// 	);
			// }
		}

		for (let i = 0; i < data.modifier_product_detail_relation.length ; i++){
			if (data.modifier_product_detail_relation[i].compare_at_price > 0) {
				if (data.modifier_product_detail_relation[i].compare_at_price < data.modifier_product_detail_relation[i].selling_price) {
					throw badRequestError(res, "Compare at price can not be greater than selling price.")
				}
			}

			// if (data.modifier_product_detail_relation[i].selling_price == 0) {
			// 	throw badRequestError(res, "Selling price value has to be greater than 0")
			// }
			if (data.modifier_product_detail_relation[i].modifier_product.length > 120) {
				throw badRequestError(res, "Modier Product has exceeded the maximum limit of 120 characters.")
			}
			
		}
	
		let createModifier;
		[err, createModifier] = await to(
			transaction(Modifier.knex(), (trx) => {
				return Modifier.query(trx).insertGraph(data);
			})
		);
	
		return createdResponse(
			res,
			createModifier,
			"Modifier added successfully!"
		);
  } catch (error) {
    console.log(error);
    throw badRequestError(res, error);
  }
};


/**
 * Get Modifier Info
 * @description: function is used to Get Modifier Info
 */
const GetModifierInfo = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.query.modifier_id)
			throw badRequestError(res, 'Please pass modifier_id in query string.');

		let GetModifierInfo = await Modifier.query()
			.select('*')
			.eager('modifier_product_detail_relation')
			.where({ id: req.query.modifier_id })
			.returning('*');

		return okResponse(
			res,
			GetModifierInfo,
			'Modifier Details fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Update Modifier
 * @description: function is used to Update Modifier
 */
const UpdateModifier = async (req, res) => {
    try {
        let data = req.body;
        data.merchant_id = req.user.user_id;
        
        /*-------------------Check Required Fields----------------------------*/
        if (!data.modifier_name) throw badRequestError(res, "Enter modifier name.");
        if (!data.modifier_name.length > 120)
			throw badRequestError(res, 'Modifier Name has exceeded the maximum limit of 120 characters.');
		// if (!data.display_name) throw badRequestError(res, "Enter modifier display name.");
		if (!data.display_name.length > 120)
			throw badRequestError(res, 'Display Name has exceeded the maximum limit of 120 characters.');

        if (data.min_user_must_select == "" || data.min_user_must_select == null) {
            data.min_user_must_select = 0
        }
        if (data.max_user_must_select == "" || data.max_user_must_select == null){
            data.max_user_must_select = 0
		} 
		if (data.min_user_must_select > 0 && data.max_user_must_select > 0) {
			if (data.min_user_must_select > data.max_user_must_select) {
				throw badRequestError(res, "Minimum modifier can't be greater than Maximum modifiers limit");
			}
		}

        let ModifierNameExist = await Modifier.query()
            .select("id", "modifier_name").whereNot("id", data.id).where({ modifier_name: data.modifier_name, delete_status: false, merchant_id: req.user.user_id }).first();

        // let DisplayNameExist = await Modifier.query()
        //     .select("id", "display_name").whereNot("id", data.id).where({ display_name: data.display_name, delete_status: false, merchant_id: req.user.user_id}).first();
        
        //-------Check if modifier name already exist or not--------------------
        if (ModifierNameExist) {
            throw badRequestError(res, "Modifier name already exists.");
        }

        //-------Check if Display Name already exist or not--------------------
        // if (DisplayNameExist) {
        //     throw badRequestError(res, "Display name already exists.");
        // }

       let counter = 0, modifierAsDefault = 0;
		if (data.min_user_must_select > 0) {
			for (let i = 0; i < data.modifier_product_detail_relation.length; i++) {
				if (data.modifier_product_detail_relation[i].default == false) {
					counter++;
				}
				if (data.modifier_product_detail_relation[i].default == true) {
					modifierAsDefault++;
				}
			}

			if (data.modifier_product_detail_relation.length < data.min_user_must_select || modifierAsDefault < data.min_user_must_select) {
				throw badRequestError(
					res,
					`Add at least ${data.min_user_must_select} modifier product and select those modifier products as default.`
				);
			}

			if (data.modifier_product_detail_relation.length == counter) {
				throw badRequestError(
				res,
				"Select at least one default modifier product."
				);
			}
		}

		let modifierAsDefaultMax = 0;
		if(data.max_user_must_select > 0) {
			for (let i = 0; i < data.modifier_product_detail_relation.length; i++) {
				if (data.modifier_product_detail_relation[i].default == true) {
					modifierAsDefaultMax++;
				}
			}

			if (data.modifier_product_detail_relation.length < data.max_user_must_select) {
				throw badRequestError(
					res,
					`You have to add modifier product maximum ${data.max_user_must_select}.`
				);
			}

			// if (data.modifier_product_detail_relation.length > data.max_user_must_select || modifierAsDefaultMax > data.max_user_must_select) {
			// 	throw badRequestError(
			// 		res,
			// 		`You can add modifier product maximum ${data.max_user_must_select} and you can select maximum ${data.max_user_must_select} modifier products as default.`
			// 	);
			// }
		}
		
		for (let i = 0; i < data.modifier_product_detail_relation.length ; i++){
			if (data.modifier_product_detail_relation[i].compare_at_price > 0) {
				if (data.modifier_product_detail_relation[i].compare_at_price < data.modifier_product_detail_relation[i].selling_price) {
					throw badRequestError(res, "Compare at price can not be greater than selling price.")
				}
			}

			// if (data.modifier_product_detail_relation[i].selling_price == 0) {
			// 	throw badRequestError(res, "Selling price value has to be greater than 0")
			// }
			if (data.modifier_product_detail_relation[i].modifier_product.length > 120) {
				throw badRequestError(res, "Modier Product has exceeded the maximum limit of 120 characters.")
			}
			
		}

		let updateModifier, err;
		[err, updateModifier] = await to(
			transaction(Modifier.knex(), (trx) => {
				return Modifier.query(trx).upsertGraph(data);
			})
		);

        return okResponse(res, updateModifier, "Modifier updated successfully.");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Modifier Listing
 * @description: function is used to list all categories
 */
const ModifierList = async (req, res) => {
	try {
		let ModifierList;

		let query = Modifier.query()
			.select('*')
			.where({ merchant_id: req.user.user_id, delete_status: false })
			.orderBy('created_at', 'desc')
			.returning('*');

		if (req.query.page || req.query.search) {
			ModifierList = await query
				.eager(
					'[modifier_product_detail_relation, applied_to_group_modifiers, products_associated_with_modifiers]'
				)
				.modifyEager('modifier_product_detail_relation', builder => {
					return builder.select("modifier_product")
				})
				.modifyEager('applied_to_group_modifiers', builder => {
					return builder.select("*").eager('modifier_group_manage').modifyEager('modifier_group_manage', builder => {
						return builder.select("*").where('delete_status', false);
					})
				})
				.modifyEager('products_associated_with_modifiers', builder => {
					return builder.select("*").eager('product').modifyEager('product', builder => {
						return builder.select("product_name", "delete_status").where('delete_status', false);
					})
				})
				.where((builder) => {
					if (req.query.search !== '') {
						return builder.whereRaw(
							"LOWER(modifier.modifier_name) LIKE '%' || LOWER(?) || '%'",
							req.query.search
						);
					}
				})
				.page(req.query.page || 0, 10);

			for (let i = 0; i < ModifierList.results.length; i++) {
				//----To get groupModifierCount------------	
				let count = 0;
				if (ModifierList.results[i].applied_to_group_modifiers.length == 0) {
					ModifierList.results[i].groupModifierCount = 0;
				}
				for (let j = 0; j < ModifierList.results[i].applied_to_group_modifiers.length; j++) {
					if (ModifierList.results[i].applied_to_group_modifiers[j].modifier_group_manage != null) {
						count++
					}
				}
				ModifierList.results[i].groupModifierCount = count;

				//----To get productCount------------	
				let productCount = 0;
				for (let j = 0; j < ModifierList.results[i].products_associated_with_modifiers.length; j++) {
					if (ModifierList.results[i].products_associated_with_modifiers[j].product != null) {
						productCount++
					}
				}
				ModifierList.results[i].productCount = productCount;
			}

		} else {
			ModifierList = await query
		}

		return okResponse(
			res,
			ModifierList,
			'Modifier List fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Delete Modifier
 * @description: function is used to Delete Modifier
 */
const deleteModifier = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.body.modifier_id)
			throw badRequestError(res, 'Please enter modifier_id.');

		// if (!req.body.groupModifierCount)
		// 	throw badRequestError(res, 'Please enter groupModifierCount.');

		if (req.body.groupModifierCount > 0) {
			throw badRequestError(res, 'Modifier can not be deleted because it is associated with number of group modifiers.');
		}

		if (req.body.productCount > 0) {
			throw badRequestError(res, 'Modifier can not be deleted because it is associated with number of products.');
		}

		let deleteModifier = await Modifier.query()
			.update({ delete_status: true })
			.where({ id: req.body.modifier_id })
			.first();

		return okResponse(
			res,
			deleteModifier,
			'Modifier deleted successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================= MODIFIER Apis (END) ==================================================================

//================================================= GROUP MODIFIER Apis (START) ==================================================================
/**
 * Add Group Modifier
 * @description: function is used to add Group Modifier
 */
const AddGroupModifier = async (req, res) => {
	try {
		let data = req.body;
		data.merchant_id = req.user.user_id;
		
		/*-------------------Check Required Fields----------------------------*/
		if (!data.modifier_group_name)
			throw badRequestError(res, 'Enter Group Modifier name.');

		if (data.modifier_group_name.length > 120)
			throw badRequestError(res, 'Modifer Group Name has exceeded the maximum limit of 120 characters.');

		let ModifierNameExist = await GroupModifier.query()
			.select('id', 'modifier_group_name')
			.where({
				modifier_group_name: data.modifier_group_name,
				delete_status: false,
				merchant_id: req.user.user_id,
			})
			.first();

		//-------Check if modifier name already exist or not--------------------
		if (ModifierNameExist) {
			throw badRequestError(res, 'Group Modifier name already exists.');
		}

		let createModifier, err;
		[err, createModifier] = await to(
			transaction(GroupModifier.knex(), (trx) => {
				return GroupModifier.query(trx).insertGraph(data);
			})
		);
		
		return createdResponse(
			res,
			createModifier,
			'Group Modifier added successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Get GroupModifier Info
 * @description: function is used to Get GroupModifier Info
 */
const GetGroupModifierInfo = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.query.modifier_id)
			throw badRequestError(res, 'Please pass modifier_id in query string.');

		let GetModifierInfo = await GroupModifier.query()
			.select('*')
			.eager('modifier_group_relation')
			.modifyEager('modifier_group_relation', (builder) => {
				return builder.select('modifier_id');
			})
			.where({ id: req.query.modifier_id })
			.returning('*');

		return okResponse(
			res,
			GetModifierInfo,
			'Group Modifier details fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Update GroupModifier
 * @description: function is used to Update GroupModifier
 */
const UpdateGroupModifier = async (req, res) => {
	try {
		let data = req.body;
		
		/*-------------------Check Required Fields----------------------------*/
		if (!data.modifier_group_name)
			throw badRequestError(res, 'Enter Group Modifier name.');
		if (data.modifier_group_name.length > 120)
			throw badRequestError(res, 'Modifer Group Name has exceeded the maximum limit of 120 characters.');

		let ModifierNameExist = await GroupModifier.query()
			.select('id', 'modifier_group_name')
			.whereNot('id', data.id)
			.where({
				modifier_group_name: data.modifier_group_name,
				delete_status: false,
				merchant_id: req.user.user_id,
			})
			.first();

		//-------Check if modifier name already exist or not--------------------
		if (ModifierNameExist) {
			throw badRequestError(res, 'Group Modifier name already exists.');
		}

		let updateGroupModifier, err;
		[err, updateGroupModifier] = await to(
			transaction(GroupModifier.knex(), (trx) => {
				return GroupModifier.query(trx).upsertGraph(data);
			})
		);
		
		return okResponse(
			res,
			updateGroupModifier,
			'Group Modifier updated successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * GroupModifier Listing
 * @description: function is used to list all GroupModifier
 */
const GroupModifierList = async (req, res) => {
	try {
		let GroupModifierList;
		let query = GroupModifier.query()
			.select('*')
			.where({ merchant_id: req.user.user_id, delete_status: false })
			.orderBy('created_at', 'desc')
			.returning('*');

		if (req.query.page || req.query.search) {
      	GroupModifierList = await query
			.eager("[modifier_group_relation, product_modifier_groups]")
			.modifyEager("modifier_group_relation", (builder) => {
				return builder
					.select("modifier_id")
					.eager("group_modifier_manage")
					.modifyEager("group_modifier_manage", (builder) => {
						return builder
							.select("modifier_name")
							.where("delete_status", false);
						});
			})
			.modifyEager("product_modifier_groups", (builder) => {
				return builder
					.select("*")
					.eager("products")
					.modifyEager("products", (builder) => {
						return builder.select("*").where("delete_status", false);
					});
			})
			.where((builder) => {
			if (req.query.search !== "") {
				return builder.whereRaw(
					"LOWER(modifier_group.modifier_group_name) LIKE '%' || LOWER(?) || '%'",
					req.query.search
				);
			}
			})
			.page(req.query.page || 0, 10);

      		//---------To get productCount----------
			for (let i = 0; i < GroupModifierList.results.length; i++) {
				let count = 0;
				if (GroupModifierList.results[i].product_modifier_groups.length == 0) {
					GroupModifierList.results[i].productCount = 0;
				}
				for (let j = 0; j < GroupModifierList.results[i].product_modifier_groups.length; j++) {
					if (GroupModifierList.results[i].product_modifier_groups[j].products != null) {
						count++;
					}
				}
				GroupModifierList.results[i].productCount = count;
			}

		} else {
			GroupModifierList = await query
		}

		return okResponse(
			res,
			GroupModifierList,
			'Group Modifier List fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Delete Group Modifier
 * @description: function is used to Delete Group Modifier
 */
const deleteGroupModifier = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.body.modifier_id)
			throw badRequestError(res, 'Please enter modifier_id.');

		// if (!req.body.productCount)
		// 	throw badRequestError(res, 'Please enter productCount.');

		if (req.body.productCount > 0) {
			throw badRequestError(res, 'Group Modifier can not be deleted because it is associated with number of products.');
		}

		let deleteGroupModifier = await GroupModifier.query()
			.update({ delete_status: true })
			.where({ id: req.body.modifier_id })
			.first();

		return okResponse(
			res,
			deleteGroupModifier,
			'Group Modifier deleted successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================= GROUP MODIFIER Apis (END) ==================================================================

//================================================= PRODUCT Apis (START) ==================================================================
/**
 * Add Product
 * @description: function is used to add Product
 */
const AddProduct = async (req, res) => {
	try {
		let data = req.body;
		let arr = [],
			product_pricing_relation;
		data.merchant_id = req.user.user_id;
		
		/*-------------------Check Required Fields----------------------------*/
		if (!data.menus_of_product)
			throw badRequestError(res, 'Please enter menus_of_product.');
		if (!data.categories_of_product)
			throw badRequestError(res, 'Please enter categories_of_product.');
		if (!data.product_name)
			throw badRequestError(res, 'Please enter product_name.');

		if (data.product_name.length > 120)
			throw badRequestError(res, 'Product Name has exceeded the maximum limit of 120 characters.');
		if (data.product_description.length > 500)
			throw badRequestError(res, 'Product Description has exceeded the maximum limit of 500 characters.');		
		
		if (!data.product_quantity_counter)
			throw badRequestError(res, 'Please enter product_quantity_counter.');
		// if (!data.product_threshold_quantity)
		// 	throw badRequestError(res, 'Please enter product_threshold_quantity.');
		if (data.product_threshold_quantity == "" || data.product_threshold_quantity == null){
			data.product_threshold_quantity = 0;
		}
		// if (!data.product_replenish_days)
		// 	throw badRequestError(res, 'Please enter product_replenish_days.');
		// if (!data.products_per_order_limit)
		// 	throw badRequestError(res, 'Please enter products_per_order_limit.');
		if (data.suggestion_type_id != null && data.suggestion_type_id != "") {
			data.suggestion_type_id = data.suggestion_type_id.suggestion_id;
		}
		// if (!data.tags_of_product)
		// 	throw badRequestError(res, 'Please enter tags_of_product.');
		// if (!data.group_modifiers_of_product)
		// 	throw badRequestError(res, 'Please enter group_modifiers_of_product.');
		// if (!data.modifiers_of_product)
		// 	throw badRequestError(res, 'Please enter modifiers_of_product.');
		if (!data.product_pricing)
			throw badRequestError(res, 'Please enter product_pricing.');

		if (data.products_per_order_limit == "" || data.products_per_order_limit == null){
			data.products_per_order_limit = 0;
		}
		if (data.product_replenish_days == "" || data.product_replenish_days == null) {
			data.product_replenish_days = 0;
		}
		if (data.product_quantity == "" || data.product_quantity == null) {
			data.product_quantity = 0;
		}
		if(data.product_replenish_days > 0 && (data.product_quantity == "" || data.product_quantity == null)){
			throw badRequestError(res, "Please enter Quantity on hand because you have mentioned Product replenish days greater than 0.");
		}
		if(data.product_quantity > 0 && (data.product_replenish_days == "" || data.product_replenish_days == null)){
			throw badRequestError(res, "Please enter Product replenish days because you have mentioned Quantity on hand greater than 0.");
		}
		if (data.product_description.length > 1000) {
			throw badRequestError(res, "Product description should be less than 1000 characters.");
		}

		//-------------Formatting of product pricing relation data (START)----------------
		for (let i = 0; i < data.product_pricing.length; i++) {
			if (data.product_pricing[i].dineIn.length > 0) {
				product_pricing_relation = arr.concat(data.product_pricing[i].dineIn);
			}
			if (data.product_pricing[i].pickUp.length > 0) {
				console.log(product_pricing_relation);
				if (product_pricing_relation == undefined) {
					product_pricing_relation = arr.concat(data.product_pricing[i].pickUp);
				} else {
					product_pricing_relation = product_pricing_relation.concat(
						data.product_pricing[i].pickUp
					);
				}
			}
			if (data.product_pricing[i].Delivery.length > 0) {
				if (product_pricing_relation == undefined) {
					product_pricing_relation = arr.concat(
						data.product_pricing[i].Delivery
					);
				} else {
					product_pricing_relation = product_pricing_relation.concat(
						data.product_pricing[i].Delivery
					);
				}
			}
		}
		//-------------Formatting of product pricing relation data (END)----------------

		data.is_common_dinein = data.product_pricing[0].isCommonDineIn;
		data.is_common_pickup = data.product_pricing[0].isCommonPickUp;
		data.is_common_delivery = data.product_pricing[0].isCommonDelivery;

		delete data.product_pricing;
		if (product_pricing_relation == undefined) {
			throw badRequestError(res, "Product Pricing is required.");
		} else {
			data.product_pricing_relation = product_pricing_relation;
		}

		for (let i = 0; i < data.product_pricing_relation.length; i++){
			if (data.product_pricing_relation[i].price == "" || data.product_pricing_relation[i].price == null){
				data.product_pricing_relation[i].price = 0
			}

			if (data.product_pricing_relation[i].selling_price == "" || data.product_pricing_relation[i].selling_price == null) {
				data.product_pricing_relation[i].selling_price = 0
			}

			if (data.product_pricing_relation[i].selling_price == 0) {
				throw badRequestError(res, "Product selling price value has to be greater than 0")
			}

			if (data.product_pricing_relation[i].selling_price > 0 && data.product_pricing_relation[i].price > 0) {
				if (data.product_pricing_relation[i].selling_price > data.product_pricing_relation[i].price) {
					throw badRequestError(res, "Product selling price can not be greater than compare at price.")
				}
			}
		}

		// let ProductNameExist = await Product.query()
		// 	.select('id', 'product_name')
		// 	.where({
		// 		product_name: data.product_name,
		// 		delete_status: false,
		// 		merchant_id: req.user.user_id,
		// 	})
		// 	.first();

		// //-------Check if Product name already exist or not--------------------
		// if (ProductNameExist) {
		// 	throw badRequestError(res, 'Product name already exists.');
		// }

		//-----------------To Check data inconsistency issue (START)----------------------------
		// const menus = []
		// for(let i = 0; i<menus_of_product.length ; i++){
		// 	menus.push(menus_of_product[i].menu_id)
		// }

		// for(let j = 0; j<product_pricing_relation.length ; j++){
        //     if(menus.indexOf(product_pricing_relation[j].menu_id) !== -1){
        //         console.log("value exist. There is no data inconsistency issue here yes it is continued");
        //     }else{
		// 		console.log("value not exist. There is data inconsistency issue!")
		// 		throw badRequestError(res, "There is some data inconsistency issue please delete this product.");
        //     }
		// }
		//-----------------To Check data inconsistency issue (END)----------------------------

		let createProduct, err;
		[err, createProduct] = await to(
			transaction(Product.knex(), (trx) => {
				return Product.query(trx).insertGraph(data);
			})
		);

		for (let i = 0; i < data.product_pricing_relation.length; i++){
			let ProductsQrCodeObject = {
				merchant_id: req.user.user_id,
				category_id: 0,
				menu_id: product_pricing_relation[i].menu_id,
				product_id: createProduct.id,
				order_type_id: product_pricing_relation[i].order_type_id
			};
			//encode object using specified algorithm
			let encodedString = Buffer.from(JSON.stringify(ProductsQrCodeObject), "utf8").toString("base64")
			console.log(encodedString);
			let updateQrEncryptCode = await ProductPricing.query().update({
				qrcode_encrypt: encodedString
			}).where({menu_id: product_pricing_relation[i].menu_id, product_id: createProduct.id})
		}

		return createdResponse(
			res,
			createProduct,
			'Product added successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * get Product Details
 * @description: function is used to get Product Details
 */
const GetProduct = async (req, res) => {
	try {
		let product_pricing;
		let dineIn = [],
			pickUp = [],
			Delivery = [];

		/*-------------------Check Required Fields----------------------------*/
		if (!req.query.product_id)
			throw badRequestError(res, 'Please pass product_id in query string.');

		let IsProductExist = await Product.query()
			.select('id', 'product_name')
			.where({ id: req.query.product_id })
			.first();

		//-------Check if Product name already exist or not--------------------
		if (IsProductExist == undefined) {
			throw badRequestError(res, 'Product not exist!');
		}

		let getProduct = await Product.query()
			.select('*')
			.eager(
				'[suggestion_type, menus_of_product, categories_of_product, tags_of_product, modifiers_of_product, group_modifiers_of_product, product_pricing_relation]'
			)
			.modifyEager('suggestion_type', builder => {
				return builder.select("id as suggestion_id", "suggestion as suggestion_name");
			})
			.modifyEager('menus_of_product', builder => {
				return builder.select("*").eager("product_menu_belongsto")
					.modifyEager('product_menu_belongsto', builder => {
						return builder.select('id', 'menu_name')
					})
			})
			.modifyEager('categories_of_product', builder => {
				return builder.select("*").eager("product_category_belongsto")
					.modifyEager('product_category_belongsto', builder => {
						return builder.select('id', 'category_name')
					})
			})
			.modifyEager('tags_of_product', builder => {
				return builder.select("*").eager("tags_product_belongsto")
					.modifyEager('tags_product_belongsto', builder => {
						return builder.select('id', 'tags')
					})
			})
			.modifyEager('modifiers_of_product', builder => {
				return builder.select("*").eager("modifier")
					.modifyEager('modifier', builder => {
						return builder.select("id", "modifier_name");
					})
			})
			.modifyEager('group_modifiers_of_product', builder => {
				return builder.select("*").eager("modifier_group")
					.modifyEager('modifier_group', builder => {
						return builder.select("id", "modifier_group_name");
					})
			})
			.where('id', req.query.product_id)
			.first();

		for (let i = 0; i < getProduct.product_pricing_relation.length; i++) {
			console.log(getProduct.product_pricing_relation[i]);

			if (getProduct.product_pricing_relation[i].order_type_id == 1) {
				dineIn.push(getProduct.product_pricing_relation[i]);
			}
			if (getProduct.product_pricing_relation[i].order_type_id == 2) {
				pickUp.push(getProduct.product_pricing_relation[i]);
			}
			if (getProduct.product_pricing_relation[i].order_type_id == 4) {
				Delivery.push(getProduct.product_pricing_relation[i]);
			}
		}

		delete getProduct.product_pricing_relation;
		getProduct.suggestion_type_id = getProduct.suggestion_type
		delete getProduct.suggestion_type;;
		getProduct.product_pricing = {
			dineIn,
			pickUp,
			Delivery,
			isCommonDineIn: getProduct.is_common_dinein,
			isCommonPickUp: getProduct.is_common_pickup,
			isCommonDelivery: getProduct.is_common_delivery,
		};

		return createdResponse(
			res,
			getProduct,
			'Product details fetched successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Update Product
 * @description: function is used to Update Product
 */
const UpdateProduct = async (req, res) => {
	try {
        let data = req.body;
        let arr = [], product_pricing_relation;
		data.merchant_id = req.user.user_id;
		data.updated_at = moment().format("YYYY-MM-DD HH:mm:ss");

        /*-------------------Check Required Fields----------------------------*/
        if (!data.menus_of_product)
          throw badRequestError(res, "Please enter menus_of_product.");
        if (!data.categories_of_product)
          throw badRequestError(res, "Please enter categories_of_product.");
        if (!data.product_name)
		  throw badRequestError(res, "Please enter product_name.");
		
		if (data.product_name.length > 120)
			throw badRequestError(res, 'Product Name has exceeded the maximum limit of 120 characters.');
		if (data.product_description.length > 500)
			throw badRequestError(res, 'Product Description has exceeded the maximum limit of 500 characters.');	

        if (!data.product_quantity_counter)
          throw badRequestError(res, "Please enter product_quantity_counter.");
        // if (!data.product_threshold_quantity)
		//   throw badRequestError(res, "Please enter product_threshold_quantity.");
		if (data.product_threshold_quantity == "" || data.product_threshold_quantity == null){
			data.product_threshold_quantity = 0;
		}
        // if (!data.product_replenish_days)
        //   throw badRequestError(res, "Please enter product_replenish_days.");
        // if (!data.products_per_order_limit)
        //   throw badRequestError(res, "Please enter products_per_order_limit.");
		if (data.suggestion_type_id != null && data.suggestion_type_id != "") {
			data.suggestion_type_id = data.suggestion_type_id.suggestion_id;
		}
        // if (!data.tags_of_product)
        //   throw badRequestError(res, "Please enter tags_of_product.");
        // if (!data.group_modifiers_of_product)
        // 	throw badRequestError(res, 'Please enter group_modifiers_of_product.');
        // if (!data.modifiers_of_product)
        // 	throw badRequestError(res, 'Please enter modifiers_of_product.');
        if (!data.product_pricing)
		  throw badRequestError(res, "Please enter product_pricing.");
		  
		if (data.products_per_order_limit == "" || data.products_per_order_limit == null) {
			data.products_per_order_limit = 0;
		}
		if (data.product_replenish_days == "" || data.product_replenish_days == null) {
			data.product_replenish_days = 0;
		}
		if (data.product_quantity == "" || data.product_quantity == null) {
			data.product_quantity = 0;
		}		
		if(data.product_replenish_days > 0 && (data.product_quantity == "" || data.product_quantity == null)){
			throw badRequestError(res, "Please enter Quantity on hand because you have mentioned Product replenish days greater than 0.");
		}
		if(data.product_quantity > 0 && (data.product_replenish_days == "" || data.product_replenish_days == null)){
			throw badRequestError(res, "Please enter Product replenish days because you have mentioned Quantity on hand greater than 0.");
		}

		if (data.product_description.length > 1000) {
			throw badRequestError(res, "Product description should be less than 1000 characters.");
		}

        //-------------Formatting of product pricing relation data (START)----------------
		for (let i = 0; i < data.product_pricing.length; i++) {
			if (data.product_pricing[i].dineIn.length > 0) {
				product_pricing_relation = arr.concat(data.product_pricing[i].dineIn);
			}
			if (data.product_pricing[i].pickUp.length > 0) {
				if (product_pricing_relation == undefined) {
					product_pricing_relation = arr.concat(data.product_pricing[i].pickUp);
				} else {
					product_pricing_relation = product_pricing_relation.concat(data.product_pricing[i].pickUp);
				}
			}
			if (data.product_pricing[i].Delivery.length > 0) {
				if (product_pricing_relation == undefined) {
					product_pricing_relation = arr.concat(data.product_pricing[i].Delivery);
				} else {
					product_pricing_relation = product_pricing_relation.concat(data.product_pricing[i].Delivery);
				}
			}
		}
        //-------------Formatting of product pricing relation data (END)----------------

        data.is_common_dinein = data.product_pricing[0].isCommonDineIn;
        data.is_common_pickup = data.product_pricing[0].isCommonPickUp;
        data.is_common_delivery = data.product_pricing[0].isCommonDelivery;

		delete data.product_pricing;
		if (product_pricing_relation == undefined){
			throw badRequestError(res, "Product Pricing is required.");
		}else{
			data.product_pricing_relation = product_pricing_relation;
		}

        for (let i = 0; i < data.product_pricing_relation.length; i++) {
			if (data.product_pricing_relation[i].price == "" || data.product_pricing_relation[i].price == null) {
				data.product_pricing_relation[i].price = 0
			}

			if (data.product_pricing_relation[i].selling_price == "" || data.product_pricing_relation[i].selling_price == null) {
				data.product_pricing_relation[i].selling_price = 0
			}

			if (data.product_pricing_relation[i].selling_price == 0) {
				throw badRequestError(res, "Product selling price value has to be greater than 0")
			}

			if (data.product_pricing_relation[i].selling_price > 0 && data.product_pricing_relation[i].price > 0) {
				if (data.product_pricing_relation[i].selling_price > data.product_pricing_relation[i].price) {
					throw badRequestError(res, "Product selling price can not be greater than compare at price.")
				}
			}
        }

        // let ProductNameExist = await Product.query()
        //   .select("id", "product_name", "product_quantity_counter")
        //   .whereNot("id", data.id)
        //   .where({
        //     product_name: data.product_name,
        //     delete_status: false,
        //     merchant_id: req.user.user_id,
        //   })
		//   .first();

		// //-------Check if Product name already exist or not--------------------
        // if (ProductNameExist) {
        //   throw badRequestError(res, "Product name already exists");
        // }

		let ProductExist = await Product.query()
			.select("id", "product_name", "product_quantity_counter")
			.where("id", data.id)
			.first();
		
		if (data.product_quantity_counter > 0 && ProductExist.product_quantity_counter == 0){
			data.status = 1
		}

		//-----------------To Check data inconsistency issue (START)----------------------------
		// const menus = []
		// for(let i = 0; i<menus_of_product.length ; i++){
		// 	menus.push(menus_of_product[i].menu_id)
		// }

		// for(let j = 0; j<product_pricing_relation.length ; j++){
        //     if(menus.indexOf(product_pricing_relation[j].menu_id) !== -1){
        //         console.log("value exist. There is no data inconsistency issue here yes it is continued");
        //     }else{
		// 		console.log("value not exist. There is data inconsistency issue!")
		// 		throw badRequestError(res, "There is some data inconsistency issue please delete this product.");
        //     }
		// }
		//-----------------To Check data inconsistency issue (END)----------------------------

		let updateProduct, err;
		[err, updateProduct] = await to(
			transaction(Product.knex(), (trx) => {
				return Product.query(trx).upsertGraph(data);
			})
		);
        
        for (let i = 0; i < data.product_pricing_relation.length; i++) {
          let ProductsQrCodeObject = {
            merchant_id: req.user.user_id,
            category_id: 0,
            menu_id: product_pricing_relation[i].menu_id,
            product_id: updateProduct.id,
            order_type_id: product_pricing_relation[i].order_type_id,
          };
          console.log(ProductsQrCodeObject);

          //encode object using specified algorithm
          let encodedString = Buffer.from(
            JSON.stringify(ProductsQrCodeObject),
            "utf8"
          ).toString("base64");
          console.log(encodedString);

          let updateQrEncryptCode = await ProductPricing.query()
            .update({
              qrcode_encrypt: encodedString,
            })
            .where({
              menu_id: product_pricing_relation[i].menu_id,
              product_id: updateProduct.id,
            });
        }

        return okResponse(res, updateProduct, "Product updated successfully.");
      } catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Delete Product
 * @description: function is used to Delete Product
 */
const deleteProduct = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.body.product_id)
			throw badRequestError(res, 'Please enter product_id.');

		let deleteProduct = await Product.query()
			.update({ delete_status: true })
			.where({ id: req.body.product_id })
			.first();

		return okResponse(
			res,
			deleteProduct,
			'Product deleted successfully'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * Products Listing
 * @description: function is used to list all Products
 */
const ProductList = async (req, res) => {
    try {

        let productListing;

        if (req.query.page || req.query.search){
            productListing = await ProductMenu.query().select("*", "product.id", )
                .innerJoin('product', 'product_menu.product_id', 'product.id')
                .eager('[ product_categories_relation, menus]')
                .modifyEager('product_categories_relation', builder => {
					return builder.select('category_id', 'category_name').eager('product_category_belongsto')
                })
                .modifyEager('menus', builder => {
                    return builder.select('menu_management.menu_name')
                })
                .where({ "product.merchant_id": req.user.user_id, "product.delete_status": false })
                .orderBy("product.created_at", "desc")
                .where(builder => {
                    if (req.query.search !== '') {
                        return builder.whereRaw("LOWER(product.product_name) LIKE '%' || LOWER(?) || '%'", req.query.search);
                    }
                })
                .page(req.query.page || 0, 10)

            /*--------- Get Menu Order Types and attach to resulted query (START) -------*/
            let arr = []
            for (let i = 0; i < productListing.results.length; i++) {
				// console.log(productListing.results[i].product_id, productListing.results[i].menu_id);
				let menuOrdertypes = await ProductPricing.query().select("*").where({ product_id: productListing.results[i].product_id, menu_id: productListing.results[i].menu_id });
				// console.log(menuOrdertypes, "-----------------------------" + i);
				arr.push(menuOrdertypes);
                productListing.results[i].menu_order_types = arr[i];
            /*--------- Get Menu Order Types and attach to resulted query (END) -------*/
			}
			
        }else{
            productListing = await Product.query().select("*").where({ merchant_id: req.user.user_id, delete_status: false })
                .orderBy("product.created_at", "desc")
                .returning("*");
        }

        return okResponse(res, productListing, "Product List fetched successfully.");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Products Status
 * @description: function is used to change Products Status
 */
const changeProductStatus = async (req, res) => {
	try {
		/*-------------------Check Required Fields----------------------------*/
		if (!req.body.product_id)
			throw badRequestError(res, 'Please enter product_id.');
		if (!req.body.status) throw badRequestError(res, 'Please enter status.');

		let ChangeProductStatus = await Product.query()
			.update({ status: req.body.status })
			.where({ id: req.body.product_id })
			.first();
		
		if (req.body.status == 3){
			let date = moment().format("YYYY-MM-DD HH:mm:ss");
			await Product.query()
				.update({ today_out_of_stock: date })
				.where({ id: req.body.product_id })
				.first();
		}

		return okResponse(
			res,
			ChangeProductStatus,
			'Product Status Changed successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================= PRODUCT Apis (END) ==================================================================

//================================================= Table Management APIs (START) ========================================================

/**
 * Add Table
 * @description: function is used to Add Table
 */
const AddTable = async (req, res) => {
	try {
		let data = req.body;
		data.merchant_id = req.user.user_id;
		let addTable;

		/*-------------------Check Required Fields----------------------------*/
		// if (!req.body.table_number)
		// 	throw badRequestError(res, 'Please enter table_number.');
		if (!req.body.tableStart)
			throw badRequestError(res, 'Please enter tableStart.');
		if (!req.body.tableEnd) delete req.body.tableEnd;


		let TableExist = await Table.query()
						.select("id", "table_number")
						.where({ table_number: req.body.tableStart, merchant_id: req.user.user_id })
						.first();

		// console.log(TableExist)
		// return;

		//-------Check if Product name already exist or not--------------------
		if (TableExist) {
			throw badRequestError(res, `Table Number ${req.body.tableStart} already exists.`);
		}


		if(req.body.tableStart && req.body.tableEnd){

			if( parseInt(req.body.tableEnd) < parseInt(req.body.tableStart)){
				throw badRequestError(res, `Table Start should be less than Table End.`);
			}
			let diff = parseInt(req.body.tableEnd) - parseInt(req.body.tableStart);
			// console.log(diff)
			diff++;
			
			if(diff > 0){
				// console.log(diff, "===============");
				for (let tableNumber = req.body.tableStart; diff > 0; diff--) {

					// console.log("tableNumber", tableNumber);
					let TableExist = await Table.query()
						.select("id", "table_number")
						.where({ table_number: tableNumber, merchant_id: req.user.user_id })
						.first();

					//-------Check if Product name already exist or not--------------------
					if (TableExist) {
						throw badRequestError(res, `Table Number ${tableNumber} already exists.`);
					}

					addTable = await Table.query().insert({table_number: tableNumber, merchant_id: req.user.user_id}).returning("*");
					tableNumber++;

					//---------------qr code encryption----------------------------------
					let tableQrCodeObject = {
						table_id: addTable.id,
						table_number: addTable.table_number,
						order_type_id: 5,
						merchant_id: req.user.user_id,
					};
					//encode object using specified algorithm
					let encodedString = Buffer.from(JSON.stringify(tableQrCodeObject), "utf8").toString("base64")
					let updateTableQrEncryption = await Table.query().update({qrcode_encrypt: encodedString}).where("id", addTable.id);
					//---------------qr code encryption----------------------------------
				}
				return okResponse(res, "tables added", "Table Added successfully.");
			}else{
				addTable = await Table.query().insert({table_number: req.body.tableStart, merchant_id: req.user.user_id}).returning('*');
			}
		}else{
			addTable = await Table.query().insert({table_number: req.body.tableStart, merchant_id: req.user.user_id}).returning('*');
		}

		//---------------qr code encryption----------------------------------
		let tableQrCodeObject = {
			table_id: addTable.id,
			table_number: addTable.table_number,
			order_type_id: 5,
			merchant_id: req.user.user_id,
		};
		console.log(tableQrCodeObject);
		//encode object using specified algorithm
		let encodedString = Buffer.from(JSON.stringify(tableQrCodeObject), "utf8").toString("base64")
		console.log(encodedString);

		let updateTableQrEncryption = await Table.query().update({qrcode_encrypt: encodedString}).where("id", addTable.id);
		//---------------qr code encryption----------------------------------

		return okResponse(
			res,
			addTable,
			'Table Added successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};


/**
 * List Table
 * @description: function is used to List the Table
 */
const ListTable = async (req, res) => {
	try {
		let listTable = await Table.query()
			.select('*')
			.eager('order_relation')
			.modifyEager('order_relation', (builder) => {
				return builder.select('order_number');
			})
			.where({ merchant_id: req.user.user_id })
			.orderBy('table_number', 'asc').where((builder) => {
				if (req.query.search !== '') {
					return builder.whereRaw(
						"LOWER(table_number) LIKE '%' || LOWER(?) || '%'",
						req.query.search
					);
				}
			}).page(req.query.page || 0, 10)

		return okResponse(
			res,
			listTable,
			'Table List fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================= Table Management APIs (END) ========================================================

/**
 * List Of Product Tags
 */
const listProductTags = async (req, res) => {
	try {
		let producttags = await ProductTags.query()
			.select('*')
			.orderBy('created_at', 'desc')
			.returning('*');

		return okResponse(
			res,
			producttags,
			'Product Tags fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * List Of Suggestion Types
 */
const listSuggestions = async (req, res) => {
	try {
		let suggestionList = await Suggestions.query()
			.select('*')
			.orderBy('created_at', 'desc')
			.returning('*');

		return okResponse(
			res,
			suggestionList,
			'Suggestions fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: Menu Management Tab Dynamic Count
 */
const MenuMenagementCount = async (req, res) => {
	try {
		let menuManagementCount = {};

		let totalMenus = await Menu.query()
				.count("id as totalMenus")
				.where({ merchant_id: req.user.user_id, delete_status: false })
				.first();

		let totalCategory = await Category.query()
				.count("id as totalCategory")
				.where({ merchant_id: req.user.user_id, delete_status: false })
				.first();

		let totalModifiers = await Modifier.query()
				.count("id as totalModifiers")
				.where({ merchant_id: req.user.user_id, delete_status: false })
				.first();
		
		let totalGroupModifiers = await GroupModifier.query()
				.count("id as totalGroupModifiers")
				.where({ merchant_id: req.user.user_id, delete_status: false })
				.first();

		let totalProducts = await Product.query()
				.count("id as totalProducts")
				.where({ merchant_id: req.user.user_id, delete_status: false })
				.first();

		menuManagementCount.totalMenus = totalMenus.totalMenus;
		menuManagementCount.totalCategory = totalCategory.totalCategory;
		menuManagementCount.totalModifiers = totalModifiers.totalModifiers;
		menuManagementCount.totalGroupModifiers = totalGroupModifiers.totalGroupModifiers;
		menuManagementCount.totalProducts = totalProducts.totalProducts;

		return okResponse(
			res,
			menuManagementCount,
			'Menu Management count fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//---Export the modules here------
module.exports = {
	AddMenu,
	MenuList,
	ChangeMenuStatus,
	UpdateMenu,
	deleteMenu,
	ChangeMenuOrderTypeStatus,
	AddCategory,
	CategoryList,
	UpdateCategory,
	GetCategoryInfo,
	ChangeCategoryStatus,
	deleteCategory,
	AddModifier,
	GetModifierInfo,
	UpdateModifier,
	ModifierList,
	deleteModifier,
	AddGroupModifier,
	GetGroupModifierInfo,
	UpdateGroupModifier,
	GroupModifierList,
	deleteGroupModifier,
	AddProduct,
	GetProduct,
	UpdateProduct,
	deleteProduct,
	ProductList,
	changeProductStatus,
	AddTable,
	ListTable,
	listProductTags,
	listSuggestions,
	MenuMenagementCount
};
