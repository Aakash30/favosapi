"use strict"

/**
 * @author SHUBHAM GUPTA
 */

//----------------------------------------------RestaurantManagement Controller (Start)------------------------------------------------------------------

//Import Dependency 3rd Party Modules Here
const validator = require("validator");
const EMAIL = require("../../middlewares/email");
const bcryptjs = require('bcryptjs');
const crypto = require("crypto");

//Import Models Here
const User = require("../../models/User");
const MerchantCharacteristics = require("../../models/Merchant_Characteristics");
const OrderTypeManagement = require("../../models/Order_Type_Management");
const Menu = require('../../models/MenuManagement');
const Product = require('../../models/Product');
const Orders = require('../../models/Orders');

const Buffer = require('buffer').Buffer;
const moment = require('moment');
let knexConfig = require('../../../db/knexfile');
const Knex = require('knex')(knexConfig['development']);
const { transaction, ref } = require("objection");

//========================================================================================



/**
 * Get Merchant Detail
 * @description: function is used to get Merchant details by id
 */
const getGeneralDetailsOfMerchantInfo = async (req, res) => {
    try {
        let restaurant_id = req.user.user_id;
        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please pass restaurant_id in query string.");

        let checkMerchantExist = await User.query()
            .select("*").where({ id: restaurant_id, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkMerchantExist == undefined) {
            throw badRequestError(res, "Merchant with this restaurant_id not exist.");
        }

        let merchantDetail = await User.query().select("*").eager('user_address_relation')
            .modifyEager('user_address_relation', builder => {
                return builder.select('id', 'full_address', 'lat', 'lng', 'locality');
            }).where({ id: restaurant_id, user_type: "MERCHANT" }).first().returning("*");

        delete merchantDetail.password;

        return okResponse(res, merchantDetail, "Congratulations! Merchant details fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Update Merchant Detail
 * @description: function is used to update Merchant details by id
 */
const updateGeneralDetailsOfMerchant = async (req, res) => {
    try {
        let restaurant_id = req.user.user_id;
        let data = req.body;
        data.user_type = 'MERCHANT'

        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please enter id.");
        if (!data.first_name) throw badRequestError(res, "Please enter your first name.");
        if (data.first_name.length > 50) {
          throw badRequestError(res, "First Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.last_name) throw badRequestError(res, "Please enter your last name.");
        if (data.last_name.length > 50) {
          throw badRequestError(res, "Last Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.restaurant_name) throw badRequestError(res, "Please enter your last name.");
        if (data.restaurant_name.length > 255) {
          throw badRequestError(res, "Merchant Name has exceeded the maximum limit of 255 characters.");
        }
        if (data.restaurant_website.length > 255) {
          throw badRequestError(res, "Merchant Website has exceeded the maximum limit of 255 characters.");
        }
        if (data.foodcourt_name.length > 120) {
          throw badRequestError(res, "Foodcourt Name has exceeded the maximum limit of 120 characters.");
        }

        if (!data.email) throw badRequestError(res, "Please enter your email.");
        if (data.email.length > 120) {
          throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
        }

        if (!data.country_isd_code) throw badRequestError(res, "Please enter your country_isd_code.");
        if (data.country_isd_code.length > 5) {
            throw badRequestError(res, "Country ISD Code has exceeded the maximum limit of 120 characters..");
        }

        if (!data.country_name) throw badRequestError(res, "Please enter your country_name.");
        if (data.country_name.length > 120) {
            throw badRequestError(res, "Country Name has exceeded the maximum limit of 120 characters..");
        }

        if (!data.city_name) throw badRequestError(res, "Enter your city_name.");
        if (data.city_name.length > 50) {
            throw badRequestError(res, "City Name has exceeded the maximum limit of 50 characters..");
        }

        if (!data.mobile) throw badRequestError(res, "Enter your mobile no.");
        if (data.mobile.length > 18) {
          throw badRequestError(res, "Mobile Number has exceeded the maximum limit of 18 characters.");
        }
        
        if (!data.user_address_relation) throw badRequestError(res, "Enter your Address");

        if (!data.user_address_relation[0].locality) throw badRequestError(res, "Enter your locality");
        if (data.user_address_relation[0].locality.length > 255) throw badRequestError(res, "Locality has exceeded the maximum limit of 255 characters.");
        if (data.user_address_relation[0].full_address.length > 255) throw badRequestError(res, "Full Address has exceeded the maximum limit of 255 characters.");
        
        if (!validator.isEmail(data.email)) throw badRequestError(res, "please Enter a valid email id.");
        
        if (data.business_description.length > 1000) {
            throw badRequestError(res, "Business description has exceeded the maximum limit of 1000 characters..");
        }
        
        if (data.landmark.length > 255) {
            throw badRequestError(res, "Landmark has exceeded the maximum limit of 255 characters..");
        }


        let checkRestaurantExist = await User.query()
            .select("*").where({ id: restaurant_id, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "Merchant with this restaurant_id not exist.");
        }

        /*-------------------Check Required Fields----------------------------*/
        if (!data.first_name) throw badRequestError(res, "Enter your first name.");
        if (!data.last_name) throw badRequestError(res, "Enter your last name.");
        if (!data.email) throw badRequestError(res, "Enter your email.");
        if (!data.country_isd_code) throw badRequestError(res, "Enter your country code.");
        if (!data.country_name) throw badRequestError(res, "Enter your country name.");
        if (!data.city_name) throw badRequestError(res, "Enter your city_name.");
        if (!data.mobile) throw badRequestError(res, "Enter your mobile no.");
        if (!data.user_address_relation) throw badRequestError(res, "Enter your Address");
        if (!data.user_address_relation[0].locality) throw badRequestError(res, "Enter your locality");
        if (!validator.isEmail(data.email)) throw badRequestError(res, "Enter a valid email id.");
        if (data.business_description.length > 1000){
            throw badRequestError(res, "Business description should be less than 1000 characters.");
        }

        let checkEmailExist = await User.query()
            .select("*").where({ email: data.email, user_type: "MERCHANT" }).whereNot("id", restaurant_id).first();

        //-------Check if Email already exist or not--------------------
        if (checkEmailExist) {
            throw badRequestError(res, "Merchant with this email id already exist.");
        }

        let checkMobileExist = await User.query()
            .select("*").where({ mobile: data.mobile, user_type: "MERCHANT" }).whereNot("id", restaurant_id).first();

        //-------Check if Email already exist or not--------------------
        if (checkMobileExist) {
            throw badRequestError(res, "Merchant with this mobile already exist.");
        }

        let getEmail = await User.query()
            .select("email").where({ user_type: "MERCHANT", "id": restaurant_id }).first();


        if(getEmail.email != data.email){
            //-----------Generate Password---------------------------------------
            var length = 8,
                charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                Password = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                Password += charset.charAt(Math.floor(Math.random() * n));
            }
            let name = data.first_name// + " " + data.last_name
            data.password = Password;
            data.restaurant_password = Password;
            data.is_email_verified = false;
            data.password = await bcryptjs.hash(data.password, 10);

            let token = crypto.randomBytes(64).toString("hex");
            data.verify_email_token = token;

            let link = `https://favos.co/api/v1/user/verifyEmail?email=${token}`
            EMAIL.sendCredentialsByEmail(name, link, data.email, Password);
        }

        let updateMerchantDetail, err;
        [err, updateMerchantDetail] = await to(
			transaction(User.knex(), (trx) => {
				return User.query(trx)
                    .upsertGraph(data)
                    .where({ id: restaurant_id, user_type: "MERCHANT" });
			})
		);
        delete updateMerchantDetail.password;

        let responseData = {
            'id': updateMerchantDetail.id,
            'first_name': updateMerchantDetail.first_name,
            'last_name': updateMerchantDetail.last_name,
            'email': updateMerchantDetail.email,
            'mobile': updateMerchantDetail.mobile,
            'user_type': updateMerchantDetail.user_type,
            'profile_image': updateMerchantDetail.profile_image,
            'user_status': updateMerchantDetail.user_status,
            "restaurant_name": updateMerchantDetail.restaurant_name,
            "city_name": updateMerchantDetail.city_name,
            "country_name": updateMerchantDetail.country_name,
            "banner_image": updateMerchantDetail.banner_image
        }

        return okResponse(res, responseData, "Congratulations! Restaurant details updated successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Change Password
 * @description - function is used to Change Password
 */
const changeMerchantPassword = async (req, res) => {
    try {
        if (!req.body.oldPassword) {
            return badRequestError(res, "Enter old password.");
        }
        if (!req.body.newPassword) {
            return badRequestError(res, "Enter new password.");
        }
        if (!req.body.confirmPassword) {
            return badRequestError(res, "Please enter confirm password.");
        }

        if (!validator.isLength(req.body.newPassword, { min: 6, max: 32 })) {
            throw badRequestError(res, 'New Password needs to be between 6 - 32 characters long.');
        }

        if (!validator.isLength(req.body.confirmPassword, { min: 6, max: 32 })) {
            throw badRequestError(res, 'Confirm Password needs to be between 6 - 32 characters long.');
        }

        if (req.body.newPassword != req.body.confirmPassword) {
            throw badRequestError(res, "New and Confirm passwords do not match.")
        }

        let user = await User.query()
            .where("id", req.user.user_id)
            .first();

        const checkPassword = await user.comparePassword(req.body.oldPassword);

        if (checkPassword) {
            const newPass = await bcryptjs.hash(req.body.newPassword, 10)
            let changePassword = await User.query().patch({
                password: newPass,
                restaurant_password: req.body.newPassword
            }).findById(req.user.user_id);
            return okResponse(res, { changePassword }, "Password updated successfully.");
        } else {
            throw badRequestError(res, "Old password does not match");
        }
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Get Merchant Characteristics Detail
 * @description: function is used to get Merchant Characteristics details by id
 */
const getMerchantCharacteristics = async (req, res) => {
    try {
        let restaurant_id = req.user.user_id;

        let restaurantDetail = await MerchantCharacteristics.query().select("*")
            .eager('[merchant_amenties_relation, merchant_cuisines_relation, seating_facility_relation, merchant_tags_relation, order_type_management_relation, merchant_type_relation]')
            .modifyEager('merchant_type_relation', builder => {
                return builder.select('id as merchant_type_id', 'merchant_type')
            })
            .modifyEager('merchant_amenties_relation', builder => {
                return builder.select('amenties_id', 'amenties_name')
            })
            .modifyEager('merchant_cuisines_relation', builder => {
                return builder.select('cuisines_id', 'cuisine_name')
            })
            .modifyEager('order_type_management', builder => {
                return builder.select('id', 'order_type_id')
                    .eager('order_type_relation')
                    .modifyEager('order_type_relation', builder => {
                        return builder.select('id', 'order_type', 'parent_id')
                    });
            })
            .where({ merchant_id: restaurant_id }).first().returning("*");


        return okResponse(res, restaurantDetail, "Congratulations! Merchant characteristics get successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * update Merchant Characteristics
 * @description: function is used to update Merchant Characteristics
 */
const UpdateMerchantCharacteristics = async (req, res) => {
    try {
        let data = req.body;
        
        /*-------------------Check Required Fields----------------------------*/
        if (!data.merchant_type_id) throw badRequestError(res, "Please enter your merchant_type_id.");
        if (!data.table) throw badRequestError(res, "Enter your table facility: Shared table or Dedicated table.");
        if (!data.estimated_time_permission) throw badRequestError(res, "Enter estimated time permission.");
        if (data.price_for_two.length > 120) {
            throw badRequestError(res, "Price for Two has exceeded the maximum limit of 120 characters.");
        }
        if (data.timing.length > 255) {
            throw badRequestError(res, "Timings has exceeded the maximum limit of 255 characters.");
        }
        if (data.gst_number.length > 255) {
            throw badRequestError(res, "GST Number has exceeded the maximum limit of 255 characters.");
        }

        if (data.dineIn == false) {
            let MenusDataDineIn = await Menu.query().select("*").where({
                merchant_id: data.merchant_id, DineIn: true
            }).where('delete_status', false);

            if (MenusDataDineIn.length > 0) {
                throw badRequestError(res, "You can not update status of DineIn because currently this service is associated with multiple menus.");
            }
        }

        if (data.pickup == false) {
            let MenusDataPickUp = await Menu.query().select("*").where({
                merchant_id: data.merchant_id, PickUp: true
            }).where('delete_status', false);

            if (MenusDataPickUp.length > 0) {
                throw badRequestError(res, "You can not update status of PickUp because currently this service is associated with multiple menus.");
            }
        }

        if (data.delivery == false) {
            let MenusDataDelivery = await Menu.query().select("*").where({
                merchant_id: data.merchant_id, Delivery: true
            }).where('delete_status', false);

            if (MenusDataDelivery.length > 0) {
                throw badRequestError(res, "You can not update status of Delivery because currently this service is associated with multiple menus.");
            }
        }
        delete data.dineIn;
        delete data.pickup;
        delete data.delivery;

        if (data.order_type_management_relation.length > 0) {
            for (let i = 0; i < data.order_type_management_relation.length; i++) {
                if (data.order_type_management_relation[i].order_type_id == 3 &&
                    (data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == null || data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == "")
                    && (data.order_type_management_relation[i].max_no_of_people_per_reservation == null || data.order_type_management_relation[i].max_no_of_people_per_reservation == "")
                ) {
                    data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day = 0
                    data.order_type_management_relation[i].max_no_of_people_per_reservation = 0
                }
                else if (data.order_type_management_relation[i].order_type_id == 3 &&
                    (data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == null || data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == "")
                ) {
                    data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day = 0
                }
                else if (data.order_type_management_relation[i].order_type_id == 3 &&
                    (data.order_type_management_relation[i].max_no_of_people_per_reservation == null || data.order_type_management_relation[i].max_no_of_people_per_reservation == "")
                ) {
                    data.order_type_management_relation[i].max_no_of_people_per_reservation = 0
                }
            }
        }

        //---------------------------encrypt qr code object-----------------------------------
        for (let i = 0; i < data.order_type_management_relation.length; i++) {
            if (data.order_type_management_relation[i].order_type_id == 2) {
                let PickUpQrCodeObject = {
                merchant_id: req.user.user_id,
                order_type_id: data.order_type_management_relation[i].order_type_id,
                };

                //encode object using specified algorithm
                let encodedString = Buffer.from(JSON.stringify(PickUpQrCodeObject), "utf8").toString("base64")
                
                data.order_type_management_relation[i].qrcode_encrypt = encodedString;
            }
            if (data.order_type_management_relation[i].order_type_id == 6) {
                let DineInQrCode = {
                merchant_id: req.user.user_id,
                order_type_id: data.order_type_management_relation[i].order_type_id,
                };

                //encode object using specified algorithm
                let encodedString = Buffer.from(JSON.stringify(DineInQrCode), "utf8").toString("base64")
                
                data.order_type_management_relation[i].qrcode_encrypt = encodedString;
            }
        }
        //---------------------------encrypt qr code object-----------------------------------

        let updateCharacteristics, err;
        [err, updateCharacteristics] = await to(
			transaction(MerchantCharacteristics.knex(), (trx) => {
				return MerchantCharacteristics.query(trx)
                    .upsertGraph(data)
                    .where({ id: data.id, merchant_id: req.user.user_id })
			})
		);

        return createdResponse(res, updateCharacteristics, "Merchant characteristics updated successfully.");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * order_types for merchant
 */
const OrderTypesOfMerchant = async (req, res) => {
    try {
        let OrderTypeListing = await OrderTypeManagement.query()
            .select("*").eager('order_type_relation')
            .modifyEager('order_type_relation', builder => {
                return builder.select('id', 'order_type', 'parent_id')
            }).where(builder => {
                if (req.query.merchant_id) {
                    return builder.where("merchant_id", req.query.merchant_id);
                }else{
                    return builder.where("merchant_id", req.user.user_id);   
                }
            })
             
        return okResponse(res, OrderTypeListing, "Order Types fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(error);
    }
}


/**
 * Get Dashboard Details Of Admin
 * @description: function is used to Get Dashboard Details Of Admin
 */
const GetMerchantDashboardData = async (req, res) => {
  try {
    let totalSumRevenue = await Orders.query().sum('order_amount as totalSumRevenue').where({ order_status: 6, merchant_id: req.user.user_id }).first();

    let activeUsers = await User.query().count('id as activeUsers').where({
        user_type: "USER",
        user_status: true
    }).first();

    let activeProducts = await Product.query().count('id as activeProducts').where({
        status: 1,
        delete_status: false
    }).where({ merchant_id: req.user.user_id}).first();


    //--------last 3 days revenue----------------------------------
        let dataRevenue = [], labelRevenue = []
        for(let i = 1 ; i <= 3; i++){
            let lastThreeDayDate = moment().subtract(i, 'days').format('YYYY-MM-DD')
            let RevenueGraph = await Orders.query().sum('total_amount as totalSumRevenue').where({ order_status: 6, merchant_id: req.user.user_id }).andWhereRaw('DATE(\"created_at\") = \'' + lastThreeDayDate + '\'').first()
            dataRevenue.push((RevenueGraph.totalSumRevenue != null) ? RevenueGraph.totalSumRevenue : 0) 
            // labelRevenue.push(`Day ${i}`);
            labelRevenue.push(lastThreeDayDate);
        }

        let RevenueGraph = {
            data: dataRevenue, 
            label: labelRevenue
        }
    //--------last 3 days revenue----------------------------------


    //--------last 3 days orders----------------------------------
        let dataOrder = [], labelOrder = []
        for (let i = 1; i <= 3; i++) {
            let lastThreeDayDate = moment().subtract(i, 'days').format('YYYY-MM-DD')
            let orderDataGraph = await Orders.query().count('id as numberOfOrder').where({ order_status: 6, merchant_id: req.user.user_id }).andWhereRaw('DATE(\"created_at\") = \'' + lastThreeDayDate + '\'').first()
            dataOrder.push((orderDataGraph.numberOfOrder != null) ? orderDataGraph.numberOfOrder : 0)
            // labelOrder.push(`Day ${i}`);
            labelOrder.push(lastThreeDayDate);
        }

        let OrderGraph = {
            data: dataOrder,
            label: labelOrder
        }
    //--------last 3 days revenue----------------------------------

    let topFiveCustomersWithHighestorders = await Orders.query().count('id as totalOrders')
        .eager('orders_of_users')
        .modifyEager('orders_of_users', builder => {
            return builder.select("id", "first_name", "last_name")
        })
        .where({ order_status: 6, merchant_id: req.user.user_id })
        .groupBy('user_id').orderBy('totalOrders', 'desc').limit(5);

    let onlineOrderStatus = await MerchantCharacteristics.query()
          .select("id", "online_order_status", "opening_status")
          .where({ merchant_id: req.user.user_id })
          .first();

    return okResponse(
      res,
        { totalSumRevenue, activeProducts, activeUsers, RevenueGraph, OrderGraph, topFiveCustomersWithHighestorders, onlineOrderStatus },
      "Congratulations! All Details fetched successfully!"
    );
  } catch (error) {
    console.log(error);
    throw badRequestError(res, error);
  }
};


//---Export the modules here------
module.exports = {
    getGeneralDetailsOfMerchantInfo,
    updateGeneralDetailsOfMerchant,
    changeMerchantPassword,
    getMerchantCharacteristics,
    UpdateMerchantCharacteristics,
    OrderTypesOfMerchant,
    GetMerchantDashboardData
}