"use strict"

/**
 * @author SHUBHAM GUPTA
 */

//----------------------------------------------RestaurantManagement Controller (Start)------------------------------------------------------------------

//Import Dependency 3rd Party Modules Here
const bcryptjs = require('bcryptjs');


//Import Models Here
const User = require("../../models/User");

//========================================================================================


/**
 * Get User Detail
 * @description: function is used to get User details by id
 */
const GetUserInfo = async (req, res) => {
    try {
        let user_id = req.query.user_id;
        /*-------------------Check Required Fields----------------------------*/
        if (!user_id) throw badRequestError(res, "Please pass user_id in query string.");
        let checkRestaurantExist = await User.query()
            .select("*").where({ id: user_id, user_type: "USER" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "User with this user_id not exist.");
        }

        let userDetail = await User.query().select("*").eager('user_address_relation').where({ id: user_id, user_type: "USER" }).first().returning("*");
        delete userDetail.password;
        return okResponse(res, userDetail, "Congratulations! User detail fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}

/**
 * update user_status
 * @description: function is used to update user_status by id
 */
const changeUserStatusOfUser = async (req, res) => {
    try {
        let user_id = req.body.user_id;
        let user_status = req.body.user_status;
       /*-------------------Check Required Fields----------------------------*/
        if (!user_id) throw badRequestError(res, "Please pass user_id.");

        let checkRestaurantExist = await User.query()
            .select("*").where({ id: user_id, user_type: "USER" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "User with this user_id not exist.");
        }
        let updateUserStatus = await User.query().update({ user_status: user_status }).where({ id: user_id, user_type: "USER" }).first().returning("*");
        delete updateUserStatus.password;

        return okResponse(res, updateUserStatus, "User status updated successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * All users
 * @description: function is used to get all users
 */
const allUsers = async (req, res) => {
    try {
        let AllUsers = await User.query().select("id", "first_name", "last_name", "email", "mobile", "gender", "date_of_birth", 
            "profile_image", "user_type", "user_status", "banner_image", "created_at", "updated_at", "country_name", "country_isd_code").where({ user_type: "USER" })
            .where(builder => {
                if (req.query.search !== '') {
                    return builder.whereRaw("LOWER(users.first_name) LIKE '%' || LOWER(?) || '%'", req.query.search)
                        .orWhereRaw("LOWER(users.last_name) LIKE '%' || LOWER(?) || '%'", req.query.search)
                        .orWhereRaw("LOWER(users.email) LIKE '%' || LOWER(?) || '%'", req.query.search)
                        .orWhereRaw("LOWER(users.mobile) LIKE '%' || LOWER(?) || '%'", req.query.search)
                }
            })
            .where(builder => {
                if (req.query.country !== '') {
                    return builder.whereRaw("LOWER(users.country_name) LIKE '%' || LOWER(?) || '%'", req.query.country)
                }
            })
            .where(builder => {
                if (req.query.user_status !== '') {
                    return builder.where("user_status", req.query.user_status)
                }
            })
            .page(req.query.page || 0, 10)
            .orderBy("created_at", "desc")
            .returning("*");

        let activeUsers = await User.query().count("id as activeUsers").where({ user_type: "USER", user_status: true }).first();
        let InActiveUsers = await User.query().count("id as inActiveUsers").where({ user_type: "USER", user_status: false }).first();

        AllUsers.activeUsers = activeUsers;
        AllUsers.InActiveUsers = InActiveUsers;
        return okResponse(res, AllUsers, "All users list fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * reset user password
 * @description: function is used to reset user password
 */
const resetUserPassword = async (req, res) => {
    try {
        let user_id = req.body.id;
        let confirm_password = req.body.confirm_password;
        /*-------------------Check Required Fields----------------------------*/
        if (!user_id) throw badRequestError(res, "Please enter id.");
        if (!confirm_password) throw badRequestError(res, "Please enter confirm_password.");

        let checkRestaurantExist = await User.query()
            .select("*").where({ id: user_id, user_type: "USER" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "User with this user_id not exist.");
        }

        /*--------------bcryptjs the password-----------*/
        let password = await bcryptjs.hash(confirm_password, 10);

        let updateUserStatus = await User.query().update({ password: password }).where({ id: user_id }).first().returning("*");
        delete updateUserStatus.password;

        return okResponse(res, updateUserStatus, "Password reset successfully.");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}

//---Export the modules here------
module.exports = {
    GetUserInfo,
    changeUserStatusOfUser,
    allUsers,
    resetUserPassword
}


//----------------------------------------------RestaurantManagement Controller (End)-----------------------------------------------------------------