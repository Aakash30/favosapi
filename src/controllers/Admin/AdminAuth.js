"use strict"

/**
 * @author SHUBHAM GUPTA
*/
//----------------------------------------------UserAuthController Start------------------------------------------------------------------

//Import Dependency 3rd Party Modules
const validator = require("validator");
const bcryptjs = require("bcryptjs");
const crypto = require("crypto")
const EMAIL = require("../../middlewares/email");
const moment = require("moment");

//Import Models Here
const AuthTable = require("../../models/AuthTable");
const User = require("../../models/User");

//========================================================================================


/**
 * Admin Login API
 * @description: fuction is used to do ADMIN login
 */
const AdminLogin = async (req, res) => {
    try{
        let data = req.body;

        /** Validate for the empty fields */
        if (!data.email) throw badRequestError(res, "Enter login details.");
        if (data.email.length > 120) {
          throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
        }
        
        if (!data.password) throw badRequestError(res, "Enter your password.");
        if (data.password.length < 6) throw badRequestError(res, "Password should be at least 6 characters long.");
        if (data.password.length > 32) throw badRequestError(res, "Password should not be more than 32 characters long.");

        let user;

        /** Check user email is verified or not */
        if (validator.isEmail(data.email)) {
            user = await User.query()
                .whereRaw("LOWER(users.email) LIKE '%' || LOWER(?) || '%'", data.email)
                .whereNot("user_type", "USER")
                .first();

            if (user == undefined) {
                throw badRequestError(res, "User with this email id is not registered. Please enter a valid credentials.");
            }
        }
        /** Check user mobile is exist or not */
        else {
            throw badRequestError(res, "Enter a valid email id.")
        }

        /* Email verification */
        if (user.is_email_verified == false || user.is_email_verified == null || user.is_email_verified == "") {
            throw badRequestError(res, "Email not verified. Please verify your email first.");
        }

        /* check restaurant loggedin is non favos */
        if (user.membership == false) {
            throw badRequestError(res, "Non-Favos Merchant cannot logged in.");
        }

        /* check user is_active or not */
        if (user.user_status == false) {
            throw badRequestError(res, "Your access is blocked by admin.");
        }

        /* Check Password matched OR not */
        if (!await user.comparePassword(data.password)) {
            throw badRequestError(res, "Enter a valid password.");
        }

        /****** Generate authenticated data and its auth_token *****/
        let authToken = await user.getJWT();
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('AuthToken', authToken);
        res.setHeader('Access-Control-Expose-Headers', 'AuthToken');
        /******-----------------------------------------------*****/
        
        // let tokenExist = await AuthTable.query().select("id","user_id").where("user_id", user.id).first();
        // console.log(tokenExist)

        // if(tokenExist){
        //     await AuthTable.query().patch({
        //         auth_token: authToken,
        //         device_type: data.device_type,
        //         device_id: data.device_id,
        //         device_token: data.device_token
        //     }).where("user_id", user.id).returning("*");
        // }else{
            await AuthTable.query().insert({
                user_id: user.id,
                auth_token: authToken,
                device_type: data.device_type,
                device_id: data.device_id,
                device_token: data.device_token
            }).returning("*");
        // }

        //delete password
        delete user.password;

        let responseData = {
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'mobile': user.mobile,
            'user_type': user.user_type,
            'profile_image': user.profile_image,
            'user_status': user.user_status,
            "restaurant_name": user.restaurant_name,
            "city_name": user.city_name,
            "country_name": user.country_name,
            "banner_image": user.banner_image
        }

        return okResponse(res, responseData, "Admin logged in successfully.");
    }catch(error){
        console.log(error)
        throw badRequestError(res, error)
    }
}


/**
 * Get Profile Details By Id
 * @description: function is used to Get User Details By Id
 */
const ProfileDetails = async (req, res) => {
    try {
        let userId = req.user.user_id;
        let getUserInfo = await User.query()
            .select("first_name", "last_name", "email", "user_name", "password")
            .where("id", userId).first();

        if (!getUserInfo) {
            throw notFoundError("User Data Not Found!!");
        } else {
          return okResponse(res, getUserInfo, "User profile successfully fetched.");
        }
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
};


/**
 * Update Profile Details
 * @description: function is used to Update Profile Details
 */
const updateProfile = async (req, res) => {
    try {
        let data = req.body;

        if (!data.first_name) throw badRequestError(res, "Please enter your first name.");
        if (data.first_name.length > 50) {
          throw badRequestError(res, "First Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.last_name) throw badRequestError(res, "Please enter your last name.");
        if (data.last_name.length > 50) {
          throw badRequestError(res, "Last Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.email) throw badRequestError(res, "Please enter your email.");
        if (data.email.length > 120) {
          throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
        }

        if (data.user_name.length > 255) {
          throw badRequestError(res, "User Name has exceeded the maximum limit of 120 characters.");
        }

        let userId = req.user.user_id;

        let result_1 = await User.query()
            .select("*")
            .where({ email: data.email, "user_type": req.user.auth_user_relation.user_type })
            .whereNot("id", userId).first();

        if (result_1 || result_1 != undefined) {
            throw badRequestError(res, "User with this email id already exist.");
        }

        data.id = req.user.user_id;
        if(data.password){
            if (req.user.auth_user_relation.user_type == "MERCHANT") {
                data.restaurant_password = data.password;
            }
            const newPass = await bcryptjs.hash(data.password, 10)
            data.password = newPass;
        }

        let updateUserInfo = await User.query()
            .upsertGraph(data).first()
            .returning("*");

        delete updateUserInfo.password;
  
        return okResponse(
            res,
            updateUserInfo,
            "Profile Updated Successfully."
        );
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
};


/**
 * Forgot Password
 * @description: function is used For Forgot Password
 */
const forgotPassword = async (req, res) => {
  try{
    let email = req.body.email;
    if (!email) {
      throw badRequestError("The request expects a param: email");
    }

    if (email.length > 120) {
      throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
    }

    let user;
    user = await User.query()
      .skipUndefined()
      .whereRaw("LOWER(users.email) LIKE '%' || LOWER(?) || '%'", email)
      .first();

    if (!user) {
      throw badRequestError("Email id is not registered.");
    }
    let current_date = moment().format("YYYY-MM-DD HH:mm:ss");
    let token = crypto.randomBytes(64).toString("hex");
    let user_verification = await User.query()
      .update({ reset_password_token: token, reset_password_token_date: current_date })
      .where("email", email)
      .returning("*");

    EMAIL.sendEmail(email, "Favos App forgot password", "https://favos.co/admin/auth/reset-password?token=" + token + "&email=" + email);

    return okResponse( res, null, "Please check your email to reset your password.");
  }catch(error){
    console.log(error)
    throw badRequestError(res, error);
  }
};


/**
 * Check Token Before Reset Password
 * @description: function is used For Check Token Before Reset Password
 */
const checkToken = async (req, res) => {
  try{
    let token = req.query.token;
    let email = req.query.email;
    if (!token) {
      return badRequestError("The request expects a param: token");
    }
    if (!email) {
      return badRequestError("The request expects a param: email");
    }
    let user;
    user = await User.query()
      .skipUndefined()
      .where("email", email)
      .where("reset_password_token", token)
      .andWhere(function () {
        this.where("user_type", "Admin")
          .orWhere("user_type", "MERCHANT")
      })
      .first();

    if (user) {
      return okResponse(res, null, "Token verified successfully.");
    } else {
      const error =  linkExpiredError(res, "The password reset link is invalid or has expired");
      return error;
    }
  }catch(error){
    console.log(error);
    throw badRequestError(res, error);
  }
};


/**
 * Reset Password
 * @description: function is used For Reset Password
 */
const resetPassword = async (req, res) => {
  try{
    let body = req.body;
    let token = req.query.token;
    let email = req.query.email;

    if (!token) {
      return badRequestError("The request expects a param: token");
    }
    if (!email) {
      return badRequestError("The request expects a param: email");
    }
    if (!body.password) {
      return badRequestError("Enter your password.");
    }

    let userExist = await User.query()
      .select("id")
      .where("reset_password_token", token)
      .andWhere(function () {
        this.where("user_type", "Admin").orWhere("user_type", "MERCHANT");
      })
      .first();

    if(userExist == undefined){
      return linkExpiredError(res, "The password reset link is invalid or has expired");
    }

    let user;
    let password = body.password;
    body.password = await bcryptjs.hash(body.password, 10);
    user = await User.query()
      .skipUndefined()
      .update({
        password: body.password,
        reset_password_token: null,
        reset_password_token_date: null,
        restaurant_password: password,
      })
      .where("email", email)
      .where("reset_password_token", token)
      .andWhere(function () {
        this.where("user_type", "Admin").orWhere("user_type", "MERCHANT");
      })
      .first();

    if (user) {
      return okResponse(res, null, "Password reset successfully.");
    } else {
      return linkExpiredError(
        "The password reset link is invalid or has expired"
      );
    }
  }catch(error){
    console.log(error);
    throw badRequestError(res, error);
  }
};



module.exports = {
    AdminLogin,
    ProfileDetails,
    updateProfile,
    forgotPassword,
    checkToken,
    resetPassword
}