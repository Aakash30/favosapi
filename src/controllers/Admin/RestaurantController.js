"use strict"

/**
 * @author SHUBHAM GUPTA
 */

//----------------------------------------------RestaurantManagement Controller (Start)------------------------------------------------------------------

//Import Dependency 3rd Party Modules Here
const validator = require("validator");
const EMAIL = require("../../middlewares/email");
const bcryptjs = require('bcryptjs');
const crypto = require("crypto");


//Import Models Here
const User = require("../../models/User");
const MerchantCharacteristics = require("../../models/Merchant_Characteristics");
const Amenties = require("../../models/Amenties");
const Cuisines = require("../../models/Cuisines");
const MerchantTags = require("../../models/MerchantTags");
const MerchantType = require("../../models/MerchantType");
const Orders = require("../../models/Orders");
const { forEach } = require("lodash");
const OrderTypeManagement = require("../../models/Order_Type_Management");
const Menu = require('../../models/MenuManagement');
let knexConfig = require('../../../db/knexfile');
const Knex = require('knex')(knexConfig['development']);
const moment = require('moment');
const PaymentDetails = require("../../models/PaymentDetails");
const { transaction, ref } = require("objection");

const Buffer = require('buffer').Buffer;

//========================================================================================

/**
 * Add Merchant General Details
 * @description: function is used to Add Merchant General Details
 */
const AddMerchantGeneralDetails = async (req, res) => {
    try {
        let data = req.body;
        console.log(data)
        
        data.user_type = 'MERCHANT'
        
        /*-------------------Check Required Fields----------------------------*/
        if (!data.first_name) throw badRequestError(res, "Please enter your first name.");
        if (data.first_name.length > 50) {
          throw badRequestError(res, "First Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.last_name) throw badRequestError(res, "Please enter your last name.");
        if (data.last_name.length > 50) {
          throw badRequestError(res, "Last Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.restaurant_name) throw badRequestError(res, "Please enter your last name.");
        if (data.restaurant_name.length > 255) {
          throw badRequestError(res, "Merchant Name has exceeded the maximum limit of 255 characters.");
        }
        if (data.restaurant_website.length > 255) {
          throw badRequestError(res, "Merchant Website has exceeded the maximum limit of 255 characters.");
        }
        if (data.foodcourt_name.length > 120) {
          throw badRequestError(res, "Foodcourt Name has exceeded the maximum limit of 120 characters.");
        }

        if (!data.email) throw badRequestError(res, "Please enter your email.");
        if (data.email.length > 120) {
          throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
        }

        if (!data.country_isd_code) throw badRequestError(res, "Please enter your country_isd_code.");
        if (data.country_isd_code.length > 5) {
            throw badRequestError(res, "Country ISD Code has exceeded the maximum limit of 120 characters..");
        }

        if (!data.country_name) throw badRequestError(res, "Please enter your country_name.");
        if (data.country_name.length > 120) {
            throw badRequestError(res, "Country Name has exceeded the maximum limit of 120 characters..");
        }

        if (!data.city_name) throw badRequestError(res, "Enter your city_name.");
        if (data.city_name.length > 50) {
            throw badRequestError(res, "City Name has exceeded the maximum limit of 50 characters..");
        }

        if (!data.mobile) throw badRequestError(res, "Enter your mobile no.");
        if (data.mobile.length > 18) {
          throw badRequestError(res, "Mobile Number has exceeded the maximum limit of 18 characters.");
        }
        
        if (!data.user_address_relation) throw badRequestError(res, "Enter your Address");

        if (!data.user_address_relation[0].locality) throw badRequestError(res, "Enter your locality");
        if (data.user_address_relation[0].locality.length > 255) throw badRequestError(res, "Locality has exceeded the maximum limit of 255 characters.");
        if (data.user_address_relation[0].full_address.length > 255) throw badRequestError(res, "Full Address has exceeded the maximum limit of 255 characters.");
        
        if (!validator.isEmail(data.email)) throw badRequestError(res, "please Enter a valid email id.");
        
        if (data.business_description.length > 1000) {
            throw badRequestError(res, "Business description has exceeded the maximum limit of 1000 characters..");
        }
        
        if (data.landmark.length > 255) {
            throw badRequestError(res, "Landmark has exceeded the maximum limit of 255 characters..");
        }


        let checkEmailExist = await User.query()
            .select("*").where({ email: data.email, user_type: "MERCHANT" }).first();
        
        //-------Check if Email already exist or not--------------------
        if (checkEmailExist) {
            throw badRequestError(res, "Merchant with this email id already exist.");
        }

        let checkMobileExist = await User.query()
            .select("*").where({ mobile: data.mobile, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkMobileExist) {
            throw badRequestError(res, "Merchant with this mobile already exist.");
        }


        //-----------Generate Password---------------------------------------
        var length = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            Password = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            Password += charset.charAt(Math.floor(Math.random() * n));
        }

        let name = data.first_name //+ " " + data.last_name
        data.password = Password;
        data.restaurant_password = Password;

        let token = crypto.randomBytes(64).toString("hex");
        data.verify_email_token = token;
        
        let createMerchant, err;
        [err, createMerchant] = await to(
			transaction(User.knex(), (trx) => {
				return User.query(trx).insertGraph(data);
			})
		);

        let link = `https://favos.co/api/v1/user/verifyEmail?email=${token}`

        EMAIL.sendCredentialsByEmail(name, link ,data.email, Password);

        delete createMerchant.password;

        return createdResponse(res, createMerchant, "Congratulations! Merchant created successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Get Merchant Detail
 * @description: function is used to get Merchant details by id
 */
const GetMerchantGeneralDetailsInfo = async (req, res) => {
    try {
        let restaurant_id = req.query.restaurant_id;
        
        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please pass restaurant_id in query string.");
        
        let checkMerchantExist = await User.query()
            .select("*").where({ id: restaurant_id, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkMerchantExist == undefined) {
            throw badRequestError(res, "Merchant with this restaurant_id not exist.");
        }

        let merchantDetail = await User.query().select("*").eager('user_address_relation')
            .modifyEager('user_address_relation', builder => {
                return builder.select('id','full_address', 'lat', 'lng', 'locality');
            }).where({ id: restaurant_id, user_type: "MERCHANT" }).first().returning("*");

        delete merchantDetail.password;

        return okResponse(res, merchantDetail, "Congratulations! Merchant detail get successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Update Merchant General Detail
 * @description: function is used to update Merchant General Detail by id
 */
const UpdateMerchantGeneralDetails = async (req, res) => {
    try {
        let restaurant_id = req.body.id;
        let data = req.body;
        data.user_type = 'MERCHANT'
        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please enter id.");

        let checkRestaurantExist = await User.query()
            .select("*").where({ id: restaurant_id, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "Merchant with this restaurant_id not exist.");
        }

         /*-------------------Check Required Fields----------------------------*/
        if (!data.first_name) throw badRequestError(res, "Please enter your first name.");
        if (data.first_name.length > 50) {
          throw badRequestError(res, "First Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.last_name) throw badRequestError(res, "Please enter your last name.");
        if (data.last_name.length > 50) {
          throw badRequestError(res, "Last Name has exceeded the maximum limit of 50 characters.");
        }

        if (!data.restaurant_name) throw badRequestError(res, "Please enter your last name.");
        if (data.restaurant_name.length > 255) {
          throw badRequestError(res, "Merchant Name has exceeded the maximum limit of 255 characters.");
        }
        if (data.restaurant_website.length > 255) {
          throw badRequestError(res, "Merchant Website has exceeded the maximum limit of 255 characters.");
        }
        if (data.foodcourt_name.length > 120) {
          throw badRequestError(res, "Foodcourt Name has exceeded the maximum limit of 120 characters.");
        }

        if (!data.email) throw badRequestError(res, "Please enter your email.");
        if (data.email.length > 120) {
          throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
        }

        if (!data.country_isd_code) throw badRequestError(res, "Please enter your country_isd_code.");
        if (data.country_isd_code.length > 5) {
            throw badRequestError(res, "Country ISD Code has exceeded the maximum limit of 120 characters..");
        }

        if (!data.country_name) throw badRequestError(res, "Please enter your country_name.");
        if (data.country_name.length > 120) {
            throw badRequestError(res, "Country Name has exceeded the maximum limit of 120 characters..");
        }

        if (!data.city_name) throw badRequestError(res, "Enter your city_name.");
        if (data.city_name.length > 50) {
            throw badRequestError(res, "City Name has exceeded the maximum limit of 50 characters..");
        }

        if (!data.mobile) throw badRequestError(res, "Enter your mobile no.");
        if (data.mobile.length > 18) {
          throw badRequestError(res, "Mobile Number has exceeded the maximum limit of 18 characters.");
        }
        
        if (!data.user_address_relation) throw badRequestError(res, "Enter your Address");

        if (!data.user_address_relation[0].locality) throw badRequestError(res, "Enter your locality");
        if (data.user_address_relation[0].locality.length > 255) throw badRequestError(res, "Locality has exceeded the maximum limit of 255 characters.");
        if (data.user_address_relation[0].full_address.length > 255) throw badRequestError(res, "Full Address has exceeded the maximum limit of 255 characters.");
        
        if (!validator.isEmail(data.email)) throw badRequestError(res, "please Enter a valid email id.");
        
        if (data.business_description.length > 1000) {
            throw badRequestError(res, "Business description has exceeded the maximum limit of 1000 characters..");
        }
        
        if (data.landmark.length > 255) {
            throw badRequestError(res, "Landmark has exceeded the maximum limit of 255 characters..");
        }


        let checkEmailExist = await User.query()
            .select("*").where({ email: data.email, user_type: "MERCHANT" }).whereNot("id", restaurant_id).first();

        //-------Check if Email already exist or not--------------------
        if (checkEmailExist) {
            throw badRequestError(res, "Merchant with this email id already exist.");
        }

        let checkMobileExist = await User.query()
            .select("*").where({ mobile: data.mobile, user_type: "MERCHANT" }).whereNot("id", restaurant_id).first();

        //-------Check if Email already exist or not--------------------
        if (checkMobileExist) {
            throw badRequestError(res, "Merchant with this mobile already exist.");
        }

        let getEmail = await User.query()
            .select("email").where({ user_type: "MERCHANT", "id": restaurant_id }).first();
        

        if (getEmail.email != data.email) {
            //-----------Generate Password---------------------------------------
            var length = 8,
                charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                Password = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                Password += charset.charAt(Math.floor(Math.random() * n));
            }
            let name = data.first_name //+ " " + data.last_name
            data.password = Password;
            data.restaurant_password = Password;
            data.is_email_verified = false;
            data.password = await bcryptjs.hash(data.password, 10);

            let token = crypto.randomBytes(64).toString("hex");
            data.verify_email_token = token;
            let link = `https://favos.co/api/v1/user/verifyEmail?email=${token}`

            EMAIL.sendCredentialsByEmail(name, link, data.email, Password);
        }

        let updateMerchantDetail, err;
        [err, updateMerchantDetail] = await to(
			transaction(User.knex(), (trx) => {
                return User.query(trx)
                  .upsertGraph(data)
                  .where({ id: restaurant_id, user_type: "MERCHANT" });
			})
		);

        delete updateMerchantDetail.password;

        let responseData = {
            'id': updateMerchantDetail.id,
            'first_name': updateMerchantDetail.first_name,
            'last_name': updateMerchantDetail.last_name,
            'email': updateMerchantDetail.email,
            'mobile': updateMerchantDetail.mobile,
            'user_type': updateMerchantDetail.user_type,
            'profile_image': updateMerchantDetail.profile_image,
            'user_status': updateMerchantDetail.user_status,
            "restaurant_name": updateMerchantDetail.restaurant_name,
            "city_name": updateMerchantDetail.city_name,
            "country_name": updateMerchantDetail.country_name,
            "banner_image": updateMerchantDetail.banner_image
        }

        return okResponse(res, responseData, "Congratulations! Merchant details updated successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * change status of Merchant
 * @description: function is used to change status of Merchant by id
 */
const changeUserStatusOfRestaurant = async (req, res) => {
    try {
        let restaurant_id = req.body.restaurant_id;
        let user_status = req.body.user_status;

        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please enter restaurant_id.");
        
        let checkRestaurantExist = await User.query()
            .select("*").where({ id: restaurant_id, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "Merchant with this restaurant_id not exist.");
        }

        let changeUserStatusOfRestaurantById = await User.query().update({ user_status: user_status }).where({ id: restaurant_id, user_type: "MERCHANT" }).first().returning("*");

        delete changeUserStatusOfRestaurantById.password;
        return okResponse(res, changeUserStatusOfRestaurantById, "Merchant Status Changed successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * All Favos Merchant
 * @description: function is used to get all favos Merchants
 */
const allFavosMerchants = async (req, res) => {
    try {
        let AllRestaurants, activeRestaurants, InActiveRestaurants;
        let query =  User.query().select("id", "restaurant_name", "email", "mobile", "country_name",
            "profile_image", "user_type", "user_status", "banner_image", "created_at", "updated_at", "city_name").where({user_type: "MERCHANT" })
            .where("membership", true)
            .orderBy("created_at", "desc")
            .returning("*");

        if(req.query.page){
            AllRestaurants = await query.eager("user_address_relation")
                .where(builder => {
                    if (req.query.search !== '') {
                        return builder.whereRaw("LOWER(users.restaurant_name) LIKE '%' || LOWER(?) || '%'", req.query.search);
                    }
                })
                .where(builder => {
                    if (req.query.country !== '') {
                        return builder.whereRaw("LOWER(users.country_name) LIKE '%' || LOWER(?) || '%'", req.query.country)
                    }
                })
                .where(builder => {
                    if (req.query.city !== '') {
                        return builder.whereRaw("LOWER(users.city_name) LIKE '%' || LOWER(?) || '%'", req.query.city)
                    }
                })
                .page(req.query.page || 0, 10);
            
            activeRestaurants = await User.query().count("id as activeRestaurants").where({ user_type: "MERCHANT", user_status: true, membership: true }).first();
            InActiveRestaurants = await User.query().count("id as inActiveRestaurants").where({ user_type: "MERCHANT", user_status: false, membership: true }).first();

            AllRestaurants.activeRestaurants = activeRestaurants;
            AllRestaurants.InActiveRestaurants = InActiveRestaurants;

        }else{
            AllRestaurants = await query;
        }

        return okResponse(res, AllRestaurants, "All Favos Merchants List fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * All Non Favos Merchant
 * @description: function is used to get all non favos Merchants
 */
const allNonFavosMerchants = async (req, res) => {
    try {
        let AllRestaurants = await User.query().select("id", "restaurant_name", "email", "mobile", "country_name",
            "profile_image", "user_type", "user_status", "banner_image", "created_at", "updated_at", "city_name").where({ user_type: "MERCHANT" })
            .eager("user_address_relation")
            .where("membership", false)
            .where(builder => {
                if (req.query.search !== '') {
                    return builder.whereRaw("LOWER(users.restaurant_name) LIKE '%' || LOWER(?) || '%'", req.query.search);
                }
            })
            .where(builder => {
                if (req.query.country !== '') {
                    return builder.whereRaw("LOWER(users.country_name) LIKE '%' || LOWER(?) || '%'", req.query.country)
                }
            })
            .where(builder => {
                if (req.query.city !== '') {
                    return builder.whereRaw("LOWER(users.city_name) LIKE '%' || LOWER(?) || '%'", req.query.city)
                }
            })
            .page(req.query.page || 0, 10)
            .orderBy("created_at", "desc")
            .returning("*");

        let activeRestaurants = await User.query().count("id as activeRestaurants").where({ user_type: "MERCHANT", user_status: true, membership: false}).first();

        let InActiveRestaurants = await User.query().count("id as inActiveRestaurants").where({ user_type: "MERCHANT", user_status: false, membership: false }).first();

        AllRestaurants.activeRestaurants = activeRestaurants;
        AllRestaurants.InActiveRestaurants = InActiveRestaurants;

        return okResponse(res, AllRestaurants, "All NonFavos Merchants list fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * to upload restaurant files (images and docs)
 * @description: function is used to upload restaurant files
 */
const uploadMerchantFile = async (req, res) => {
    let file = req.file;
    let image = file.location;
    if (file.mimetype != 'image/jpeg' && file.mimetype != 'image/jpg' && file.mimetype != 'image/png') {
        throw badRequestError(res, `Image should be type of JPEG, JPG or PNG`);
    }

    if (file.size >= 1000000){
        throw badRequestError(res, `Image size should not exceed 1MB.`);
    }

    return okResponse(res, image, "file uploaded successfully!");
}


/**
 * Add Merchant Characteristics
 * @description: function is used to add Merchant Characteristics
 */
const AddMerchantCharacteristics = async (req, res) => {
    try {
          let data = req.body;

          /*-------------------Check Required Fields----------------------------*/
          if (!data.merchant_id)
            throw badRequestError(res, "Please enter your merchant_id.");
          if (!data.merchant_type_id)
            throw badRequestError(res, "Please enter your merchant_type_id.");
          if (!data.table)
            throw badRequestError(res, "Please enter your table.");
          if (!data.estimated_time_permission) throw badRequestError(res, "Please enter your estimated_time_permission.");
        //   if (data.price_for_two.length > 120) {
        //     throw badRequestError(res, "Price for Two has exceeded the maximum limit of 120 characters.");
        //   }
          if (data.timing.length > 255) {
            throw badRequestError(res, "Timings has exceeded the maximum limit of 255 characters.");
          }
          if (data.gst_number.length > 255) {
            throw badRequestError(res, "GST Number has exceeded the maximum limit of 255 characters.");
          }
          
          let checkMerchantExist = await MerchantCharacteristics.query()
            .select("*")
            .where({ merchant_id: data.merchant_id })
            .first();

          //-------Check if Email already exist or not--------------------
          if (checkMerchantExist) {
            throw badRequestError(res, "Merchant already exist.");
          }

          if (data.order_type_management_relation.length > 0) {
            for (let i = 0; i < data.order_type_management_relation.length; i++) {
              if ( data.order_type_management_relation[i].order_type_id == 3 && 
                (data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == null || data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == "") &&
                (data.order_type_management_relation[i]
                  .max_no_of_people_per_reservation == null ||
                  data.order_type_management_relation[i]
                    .max_no_of_people_per_reservation == "")
              ) {
                data.order_type_management_relation[
                  i
                ].max_no_of_reservation_per_person_per_day = 0;
                data.order_type_management_relation[
                  i
                ].max_no_of_people_per_reservation = 0;
              } else if (
                data.order_type_management_relation[i].order_type_id == 3 &&
                (data.order_type_management_relation[i]
                  .max_no_of_reservation_per_person_per_day == null ||
                  data.order_type_management_relation[i]
                    .max_no_of_reservation_per_person_per_day == "")
              ) {
                data.order_type_management_relation[
                  i
                ].max_no_of_reservation_per_person_per_day = 0;
              } else if (
                data.order_type_management_relation[i].order_type_id == 3 &&
                (data.order_type_management_relation[i]
                  .max_no_of_people_per_reservation == null ||
                  data.order_type_management_relation[i]
                    .max_no_of_people_per_reservation == "")
              ) {
                data.order_type_management_relation[
                  i
                ].max_no_of_people_per_reservation = 0;
              }
            }
          }

          delete data.dineIn;
          delete data.pickup;
          delete data.delivery;

          let addCharacteristics, err;
          [err, addCharacteristics] = await to(
            transaction(MerchantCharacteristics.knex(), (trx) => {
              return MerchantCharacteristics.query(trx).insertGraph(data)
            })
          );


        //---------------------------encrypt qr code object-----------------------------------
          for (let i = 0; i < data.order_type_management_relation.length; i++) {
            if (data.order_type_management_relation[i].order_type_id == 2) {
              let PickUpQrCodeObject = {
                merchant_id: addCharacteristics.merchant_id,
                order_type_id: 2,
              };

              //encode object using specified(BASE64) algorithm
              let encodedString = Buffer.from(JSON.stringify(PickUpQrCodeObject), "utf8").toString("base64")
              
              let updatePickUpQrCodeObject = await OrderTypeManagement.query().update({
                  qrcode_encrypt: encodedString
              }).where({merchant_id: addCharacteristics.id, order_type_id: 2})
            }
            if (data.order_type_management_relation[i].order_type_id == 6) {
              let DineInQRCode = {
                merchant_id: addCharacteristics.merchant_id,
                order_type_id: 6,
              };

              //encode object using specified algorithm
              let encodedString = Buffer.from(JSON.stringify(DineInQRCode), "utf8").toString("base64")
              
              let updateDineInQRCode = await OrderTypeManagement.query().update({
                  qrcode_encrypt: encodedString
              }).where({ merchant_id: addCharacteristics.merchant_id, order_type_id: 6})
            }
          }
          //---------------------------encrypt qr code object-----------------------------------

          return createdResponse(
            res,
            addCharacteristics,
            "Congratulations! Merchant characteristics added successfully!"
          );
        } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}

/**
 * Get Restaurant Characteristics Detail
 * @description: function is used to get restaurant Characteristics details by id
 */
const getMerchantCharacteristics = async (req, res) => {
    try {
        let restaurant_id = req.query.restaurant_id;

        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please pass restaurant_id in query string.");

        let merchantDetail = await MerchantCharacteristics.query().select("*")
            .eager('[merchant_amenties_relation, merchant_cuisines_relation, seating_facility_relation, merchant_tags_relation, order_type_management_relation, merchant_type_relation]')
            .modifyEager('merchant_type_relation', builder => {
                return builder.select('id as merchant_type_id', 'merchant_type')
            })
            .modifyEager('merchant_amenties_relation', builder => {
                return builder.select('amenties_id', 'amenties_name')
            })
            .modifyEager('merchant_cuisines_relation', builder => {
                return builder.select('cuisines_id', 'cuisine_name')
            })
            .modifyEager('order_type_management', builder => {
                return builder.select('*')
                    .eager('order_type_relation')
                    .modifyEager('order_type_relation', builder => {
                        return builder.select('id', 'order_type', 'parent_id')
                    });
            })
            .where({ merchant_id: restaurant_id }).first().returning("*");


        return okResponse(res, merchantDetail, "Congratulations! Merchant characteristics get successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * update Merchant Characteristics
 * @description: function is used to update Merchant Characteristics
 */
const UpdateMerchantCharacteristics = async (req, res) => {
    try {
            let data = req.body;

            /*-------------------Check Required Fields----------------------------*/
            if (!data.merchant_id)
                throw badRequestError(res, "Please enter your merchant_id.");
            if (!data.merchant_type_id)
                throw badRequestError(res, "Please enter your merchant_type_id.");
            if (!data.table)
                throw badRequestError(res, "Please enter your table.");
            if (!data.estimated_time_permission)
                throw badRequestError(
                res,
                "Please enter your estimated_time_permission."
            );
            // if (data.price_for_two.length > 120) {
            //     throw badRequestError(res, "Price for Two has exceeded the maximum limit of 120 characters.");
            // }
            if (data.timing.length > 255) {
                throw badRequestError(res, "Timings has exceeded the maximum limit of 255 characters.");
            }
            if (data.gst_number.length > 255) {
                throw badRequestError(res, "GST Number has exceeded the maximum limit of 255 characters.");
            }

          
            if(data.dineIn == false){
                let MenusDataDineIn = await Menu.query().select("*").where({
                    merchant_id: data.merchant_id, DineIn: true
                }).where('delete_status', false);

                if (MenusDataDineIn.length > 0) {
                    throw badRequestError(res, "You can not update status of DineIn because currently this service is associated with multiple menus.");
                }
            }

             if(data.pickup == false){
                 let MenusDataPickUp = await Menu.query().select("*").where({
                     merchant_id: data.merchant_id, PickUp: true
                 }).where('delete_status', false);

                 if (MenusDataPickUp.length > 0) {
                     throw badRequestError(res, "You can not update status of PickUp because currently this service is associated with multiple menus.");
                 }
            }

            if(data.delivery == false){
                let MenusDataDelivery = await Menu.query().select("*").where({
                    merchant_id: data.merchant_id, Delivery: true
                }).where('delete_status', false);

                if (MenusDataDelivery.length > 0) {
                    throw badRequestError(res, "You can not update status of Delivery because currently this service is associated with multiple menus.");
                }
            }
            delete data.dineIn;
            delete data.pickup;
            delete data.delivery;

            if (data.order_type_management_relation.length > 0) {
                for (let i = 0; i < data.order_type_management_relation.length; i++) {
                    if (data.order_type_management_relation[i].order_type_id == 3 &&
                    (data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == null || data.order_type_management_relation[i]
                    .max_no_of_reservation_per_person_per_day == "") && (data.order_type_management_relation[i].max_no_of_people_per_reservation == null ||
                        data.order_type_management_relation[i].max_no_of_people_per_reservation == "")) 
                    {
                        data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day = 0;
                        data.order_type_management_relation[i].max_no_of_people_per_reservation = 0;
                    } 
                    else if (data.order_type_management_relation[i].order_type_id == 3 && (data.order_type_management_relation[i]
                        .max_no_of_reservation_per_person_per_day == null || data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day == "")) 
                    {
                        data.order_type_management_relation[i].max_no_of_reservation_per_person_per_day = 0;
                    } 
                    else if (data.order_type_management_relation[i].order_type_id == 3 && (data.order_type_management_relation[i]
                        .max_no_of_people_per_reservation == null || data.order_type_management_relation[i].max_no_of_people_per_reservation == "")) 
                    {
                        data.order_type_management_relation[i].max_no_of_people_per_reservation = 0;
                    }
                }
            }


            let updateCharacteristics, err;
            [err, updateCharacteristics] = await to(
                transaction(MerchantCharacteristics.knex(), (trx) => {
                    return MerchantCharacteristics.query(trx).upsertGraph(data).where({ id: data.id, merchant_id: data.merchant_id });
                })
            );

            //---------------------------encrypt qr code object-----------------------------------
            for (let i = 0; i < data.order_type_management_relation.length; i++) {
            if (data.order_type_management_relation[i].order_type_id == 2) {
                let PickUpQrCodeObject = {
                    merchant_id: updateCharacteristics.merchant_id,
                    order_type_id: 2,
                };

                //encode object using specified(BASE64) algorithm
                let encodedString = Buffer.from(JSON.stringify(PickUpQrCodeObject), "utf8").toString("base64")
                
                let updatePickUpQrCodeObject = await OrderTypeManagement.query().update({
                    qrcode_encrypt: encodedString
                }).where({merchant_id: updateCharacteristics.merchant_id, order_type_id: 2})
            }
            if (data.order_type_management_relation[i].order_type_id == 6) {
                let DineInQrCodeObject = {
                    merchant_id: updateCharacteristics.merchant_id,
                    order_type_id: 6,
                };

                //encode object using specified(BASE64) algorithm
                let encodedString = Buffer.from(JSON.stringify(DineInQrCodeObject), "utf8").toString("base64")
                
                let updateDineInQrCodeObject = await OrderTypeManagement.query().update({
                    qrcode_encrypt: encodedString
                }).where({merchant_id: updateCharacteristics.merchant_id, order_type_id: 6})
            }
            }
            //---------------------------encrypt qr code object-----------------------------------

            return createdResponse(
                res,
                updateCharacteristics,
                "Congratulations! Merchant characteristics updated successfully!"
            );
        } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * List Of Amenties
 */
const listAmeneties = async (req, res) => {
    try {
        
        let amenties = await Amenties.query().select("*")
            .returning("*");

        return okResponse(res, amenties, "Congratulations! List of amenties fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}

/**
 * List Of cuisines
 */
const listCuisines = async (req, res) => {
    try {

        let cuisines = await Cuisines.query().select("*")
            .returning("*");

        return okResponse(res, cuisines, "Congratulations! List of Cuisines fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}

/**
 * List Of Merchant Type
 */
const listMerchantTypes = async (req, res) => {
    try {
        console.log(req.query.search)
        let merchanttype = await MerchantType.query().select("*")
            .returning("*");

        return okResponse(res, merchanttype, "Congratulations! List of merchant_type fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * List Of Merchant Tags
 */
const listMerchantTags = async (req, res) => {
    try {   
        /*-------------------Check Required Fields----------------------------*/
        if (!req.query.merchant_type_id) throw badRequestError(res, "Please pass merchant_type_id in query string.");

        let merchantTag = await MerchantTags.query().select("*")
            .where("merchant_type_id", req.query.merchant_type_id).returning("*");

        return okResponse(res, merchantTag, "Congratulations! List of merchant_type fetched successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Convert Non Favos Merchant to Favos Merchant
 * @description: function is used to Convert Non Favos Merchant to Favos Merchant
 */
const convertNonFavosToFavos = async (req, res) => {
    try {
        let restaurant_id = req.query.restaurant_id;

        /*-------------------Check Required Fields----------------------------*/
        if (!restaurant_id) throw badRequestError(res, "Please enter restaurant_id.");

        let checkRestaurantExist = await User.query()
            .select("*").where({ id: restaurant_id, user_type: "MERCHANT" }).first();

        //-------Check if Email already exist or not--------------------
        if (checkRestaurantExist == undefined) {
            throw badRequestError(res, "Merchant with this restaurant_id not exist.");
        }

        let convertFavos = await User.query().update({ membership: true }).where({ id: restaurant_id, user_type: "MERCHANT" }).first().returning("*");

        delete convertFavos.password;
        return okResponse(res, convertFavos, "Merchant Converted from Non-Favos to Favos successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 *list Orders
 * @description: function is used to listing Orders
 */
const ListOrders = async (req, res) => {
    try {

        /*-------------------Check Required Fields----------------------------*/
        if (!req.query.order_type_id) throw badRequestError(res, "Please pass order_type_id in query string.");
        if (!req.query.page) throw badRequestError(res, "Please pass page in query string.");

        let orders = await Orders.query()
            .select("orders.id", "orders.user_id", "orders.table_id", "orders.order_number", "orders.order_type_id", "orders.order_amount", 
            "orders.total_amount", "orders.comment", "orders.payment_id", "orders.transaction_id", "orders.created_at", "orders.updated_at", 
            "orders.merchant_id", "orders.order_status", "orders.order_cancellation_reason", "orders.estimated_time", "orders.accept_date_time", 
            "orders.delivery_address", "orders.table_no",
            "users.email", "users.mobile", "users.restaurant_name", "users.first_name", "users.last_name", "users.user_status", "users.restaurant_website"
            , "users.country_name", "users.city_name", "users.profile_image", "users.banner_image", "users.business_description", "users.landmark"
            , "users.opening_status", "users.membership"
            )
            .innerJoin('users', 'orders.merchant_id', 'users.id')
            .eager('[ordered_products_relation, orders_of_users]')
            .modifyEager('orders_of_users', builder => {
                return builder.select("users.first_name", "users.last_name", "users.user_status", "users.country_name", "users.city_name", "users.profile_image", "users.banner_image")
            })
            .modifyEager('ordered_products_relation', builder => {
                return builder.select('ordered_product_quantity', 'product_name', 'product_price')
                    .eager('[products_customization]')
                    .modifyEager('products_customization', builder => {
                        return builder.select('').eager('[modifier_customization, order_product_modifier_detail]')
                            .modifyEager('order_product_modifier_detail', builder => {
                                return builder.select('modifier_quantity').eager('modifier_product_detail_relations')
                                    .modifyEager('modifier_product_detail_relations', builder => {
                                        return builder.select('modifier_product', 'selling_price', 'compare_at_price')
                                    })
                            })
                            .modifyEager('modifier_customization', builder => {
                                return builder.select('modifier_name')
                            })
                    })
            })
            .where(builder => {
                if (req.query.order_type_id == 1) {
                    return builder.where("order_type_id", 1).orWhere("order_type_id", 5).orWhere("order_type_id", 6);
                }
                else {
                    return builder.where("order_type_id", req.query.order_type_id);
                }
            })
            .where(builder => {
                if (req.query.search !== '') {
                    return builder
                        .where("orders.order_number", "iLike", "%" + req.query.search + "%")
                        .orWhereRaw("LOWER(users.first_name) LIKE '%' || LOWER(?) || '%'", req.query.search)
                        .orWhereRaw("LOWER(users.last_name) LIKE '%' || LOWER(?) || '%'", req.query.search);
                }
            })
            .where(builder => {
                if (req.query.country !== '') {
                    return builder.whereRaw("LOWER(users.country_name) LIKE '%' || LOWER(?) || '%'", req.query.country);
                }
            })
            .where(builder => {
                if (req.query.city !== '') {
                    return builder.whereRaw("LOWER(users.city_name) LIKE '%' || LOWER(?) || '%'", req.query.city);
                }
            })
            .where(builder => {
                if (req.query.restaurant_name !== '') {
                    return builder.whereRaw("LOWER(users.restaurant_name) LIKE '%' || LOWER(?) || '%'", req.query.restaurant_name);
                }
            })
            .where(builder => {
                if (req.query.order_status != '') {
                    return builder.where("orders.order_status", req.query.order_status);
                }
            })
            .orderBy("orders.created_at", "desc")
            .page(req.query.page, 10)
            .returning("*");

        return okResponse(res, orders, "Congratulations! Orders list fetched successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 *Get Order Details
 * @description: function is used to get Order Details
 */
const getOrderDetails = async (req, res) => {
    try {

        /*-------------------Check Required Fields----------------------------*/
        if (!req.query.order_id) throw badRequestError(res, "Please pass order_id in query string.");

        let orders = await Orders.query()
            .select("*")
            .eager('[orders_of_users, orders_of_merchant, merchant_address_relation, ordered_products_relation.[products_customization]]')
            .modifyEager('orders_of_users', builder => {
                return builder.select('users.first_name', 'users.last_name', 'users.mobile', 'users.country_isd_code', 'users.profile_image')
            })
            .modifyEager('orders_of_merchant', builder => {
                return builder.select('users.first_name', 'users.last_name', 'users.mobile', 'users.country_isd_code', 'users.restaurant_name')
                        .eager('merchant_characteristic_relation')
                        .modifyEager('merchant_characteristic_relation', builder => {
                            return builder.select('gst_number')
                        })
            })
            .modifyEager('merchant_address_relation', (builder) => {
                return builder.select('full_address');
            })
            .modifyEager('ordered_products_relation', (builder) => {
                return (
                    builder
                        .select('product_name', 'ordered_product_quantity', 'product_price')
                        //.eager('[products_relation]')
                        .modifyEager('products_customization', (builder) => {
                            return builder
                                .select(
                                    Knex.raw(
                                        '(select "modifier_name" from modifier where "modifier"."id" = "order_customization"."modifier_id")'
                                    ),
                                    Knex.raw(
                                        '(select "display_name" from modifier where "modifier"."id" = "order_customization"."modifier_id")'
                                    )
                                )
                                .eager('order_product_modifier_detail')
                                .modifyEager('order_product_modifier_detail', (builder) => {
                                    return builder.select(
                                        '*'
                                        // Knex.raw(
                                        //     '(select modifier_product from "modifier_product_details" where "id" = "order_product_modifier_detail"."modifier_product_detail_id" )'
                                        // ),
                                        // Knex.raw(
                                        //     '(select selling_price from "modifier_product_details" where "id" = "order_product_modifier_detail"."modifier_product_detail_id" )'
                                        // )
                                    )
                                    .eager('modifier_product_detail_relations')
                                    .modifyEager('modifier_product_detail_relations', builder => {
                                        return builder.select("modifier_product")
                                    })
                                });
                        })
                );
            })
            .where({ id: req.query.order_id })
            .first();
        
        if (orders.accept_date_time != null) {
            let accept_at = moment(orders.accept_date_time);
            accept_at.minutes((accept_at.minutes() + orders.estimated_time));
            orders.estimated_date_with_time = accept_at
        }


        let price;
        for (let i = 0; i < orders.ordered_products_relation.length; i++) {
            price = 0;
            for (let j = 0; j < orders.ordered_products_relation[i].products_customization.length; j++) {
                for (let k = 0; k < orders.ordered_products_relation[i].products_customization[j].order_product_modifier_detail.length; k++) {
                    price = price + (orders.ordered_products_relation[i].products_customization[j].order_product_modifier_detail[k].modifier_quantity * orders.ordered_products_relation[i].products_customization[j].order_product_modifier_detail[k].selling_price);
                }
            }
            orders.ordered_products_relation[i].productPriceWithModifier = ((orders.ordered_products_relation[i].product_price * orders.ordered_products_relation[i].ordered_product_quantity) + price).toFixed(2);
        }

        return okResponse(res, orders, "Congratulations! Order Details fetched successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Add Payment details with delivery charges
 * @description: function is used to Add Payment details with delivery charges
 */
const AddPaymentDetailsAndDeliveryCharges = async (req, res) => {
    try{

        let data = req.body;
        if (data.delivery_note.length > 500) {
            throw badRequestError(res, "Delivery Note has exceeded the maximum limit of 500 characters.");
        }
        
        let checkExistingInfo = await PaymentDetails.query()
            .select("*")
            .where("merchant_id", data.merchant_id).first();

        if (checkExistingInfo){
            throw badRequestError(res, "Details already added for this merchant.");  
        }

        let addInfo, err;
        [err, addInfo] = await to(
            transaction(PaymentDetails.knex(), (trx) => {
                return PaymentDetails.query(trx).insertGraph(data)
            })
        );

        return createdResponse(
            res,
            addInfo,
            "Congratulations! Details added successfully!"
        );

    } catch (error){
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Update Payment details with delivery charges
 * @description: function is used to Update Payment details with delivery charges
 */
const UpdatePaymentDetailsAndDeliveryCharges = async (req, res) => {
    try {
        let data = req.body;

        if (data.delivery_note.length > 500) {
            throw badRequestError(res, "Delivery Note has exceeded the maximum limit of 500 characters.");
        }

        let updateInfo, err;
        [err, updateInfo] = await to(
            transaction(PaymentDetails.knex(), (trx) => {
                return PaymentDetails.query(trx).upsertGraph(data);
            })
        );

        return okResponse(res, updateInfo, "Congratulations! All Details updated successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Update Payment details with delivery charges
 * @description: function is used to Update Payment details with delivery charges
 */
const GetPaymentDetailsAndDeliveryCharges = async (req, res) => {
    try {
        /*-------------------Check Required Fields----------------------------*/
        if (!req.query.restaurant_id) throw badRequestError(res, "Please pass restaurant_id in query string.");        

        let getInfo = await PaymentDetails.query()
            .select("*").eager('delivery_charges_relation')
            .where("merchant_id", req.query.restaurant_id)
            .first()
            .returning("*");

        return okResponse(res, getInfo, "Congratulations! All Details fetched successfully!");

    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}

/**
 * Get Dashboard Details Of Admin
 * @description: function is used to Get Dashboard Details Of Admin
 */
const GetAdminDashboardData = async (req, res) => {
  try {

        let todayDate = moment();
        let lastSevenDayDate = moment().subtract(7, 'days')

        let newMerchants = await User.query().count('id as newMerchants').where({ user_type: "MERCHANT" })
            .whereBetween('created_at', [lastSevenDayDate, todayDate]).first();

        let newUsers = await User.query().count('id as newUsers').where({ user_type: "USER" }).whereBetween('created_at', [lastSevenDayDate, todayDate]).first();

        let totalOrders = await Orders.query().count('id as totalOrders').where({ order_status: 6 }).first();

        let totalSumRevenue = await Orders.query().sum('total_amount as totalSumRevenue').whereBetween('created_at', [lastSevenDayDate, todayDate]).andWhere({ order_status: 6 }).first();

        let activeUsers = await User.query().count('id as activeUsers').where({ user_type: "USER", user_status: true }).first();

      let topFiveCustomersWithHighestorders = await Orders.query().count('id as totalOrders')
          .eager('orders_of_users')
          .modifyEager('orders_of_users', builder => {
              return builder.select("id", "first_name", "last_name")
          })
          .where({ order_status: 6 })
          .groupBy('user_id').orderBy('totalOrders', 'desc').limit(5);     

      let topFiveCustomersWithHighestRevenue = await Orders.query().sum('order_amount as totalRevenue')
          .eager('orders_of_users')
          .modifyEager('orders_of_users', builder => {
              return builder.select("id", "first_name", "last_name")
          })
          .where({ order_status: 6 })
          .groupBy('user_id').orderBy('totalRevenue', 'desc').limit(5);

        let topFiveMerchantsWithHighestRevenue = await Orders.query().sum('total_amount as totalRevenue')
          .eager('orders_of_merchant')
          .modifyEager('orders_of_merchant', builder => {
              return builder.select("id", "first_name", "last_name", "restaurant_name")
          })
          .where({ order_status: 6 })
          .groupBy('merchant_id').orderBy('totalRevenue', 'desc').limit(5);   
        
        let topFiveMerchantsWithLowestRevenue = await Orders.query().sum('total_amount as totalRevenue')
          .eager('orders_of_merchant')
          .modifyEager('orders_of_merchant', builder => {
              return builder.select("id", "first_name", "last_name", "restaurant_name")
          })
          .where({order_status: 6})
          .groupBy('merchant_id').orderBy('totalRevenue', 'asc').limit(5);
        
          
        //--------last 3 days revenue----------------------------------
        let dataRevenue = [], labelRevenue = []
        for(let i = 1 ; i <= 3; i++){
            let lastThreeDayDate = moment().subtract(i, 'days').format('YYYY-MM-DD')
            let RevenueGraph = await Orders.query().sum('total_amount as totalSumRevenue').where({ order_status: 6 }).andWhereRaw('DATE(\"created_at\") = \'' + lastThreeDayDate + '\'').first()
            dataRevenue.push((RevenueGraph.totalSumRevenue != null) ? RevenueGraph.totalSumRevenue : 0) 
            // labelRevenue.push(`Day ${i}`);
            labelRevenue.push(lastThreeDayDate);
        }

        let RevenueGraph = {
            data: dataRevenue, 
            label: labelRevenue
        }
        //--------last 3 days revenue----------------------------------

        //--------last 3 days orders----------------------------------
        let dataOrder = [], labelOrder = []
        for(let i = 1 ; i <= 3; i++){
            let lastThreeDayDate = moment().subtract(i, 'days').format('YYYY-MM-DD')
            let orderDataGraph = await Orders.query().count('id as numberOfOrder').where({ order_status: 6 }).andWhereRaw('DATE(\"created_at\") = \'' + lastThreeDayDate + '\'').first()
            dataOrder.push((orderDataGraph.numberOfOrder != null) ? orderDataGraph.numberOfOrder : 0) 
            // labelOrder.push(`Day ${i}`);
            labelOrder.push(lastThreeDayDate);
        }

        let OrderGraph = {
            data: dataOrder, 
            label: labelOrder
        }
        //--------last 3 days revenue----------------------------------

        return okResponse(
        res,
            { 
                newMerchants, 
                newUsers, 
                totalOrders, 
                activeUsers, 
                topFiveCustomersWithHighestorders, 
                topFiveCustomersWithHighestRevenue,
                topFiveMerchantsWithHighestRevenue,
                topFiveMerchantsWithLowestRevenue,
                totalSumRevenue,
                RevenueGraph,
                OrderGraph 
            },
        "Congratulations! All Details fetched successfully!"
        );
  } catch (error) {
    console.log(error);
    throw badRequestError(res, error);
  }
};




//---Export the modules here------
module.exports = {
    AddMerchantGeneralDetails,
    GetMerchantGeneralDetailsInfo,
    UpdateMerchantGeneralDetails,
    changeUserStatusOfRestaurant,
    allFavosMerchants,
    allNonFavosMerchants,
    uploadMerchantFile,
    AddMerchantCharacteristics,
    getMerchantCharacteristics,
    listAmeneties,
    listCuisines,
    listMerchantTypes,
    listMerchantTags,
    UpdateMerchantCharacteristics,
    convertNonFavosToFavos,
    ListOrders,
    getOrderDetails,
    AddPaymentDetailsAndDeliveryCharges,
    UpdatePaymentDetailsAndDeliveryCharges,
    GetPaymentDetailsAndDeliveryCharges,
    GetAdminDashboardData
}


//----------------------------------------------RestaurantManagement Controller (End)-----------------------------------------------------------------