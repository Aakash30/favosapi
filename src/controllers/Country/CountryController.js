"use strict";

/**
 * @author 'SHUBHAM GUPTA'
 */

//----------------------------------------------CountryController Start------------------------------------------------------------------

//Import Model Here
const Country = require("../../models/Country");
const User = require("../../models/User");

//=====================================================================================

/**
 * Get List Of All Countries
 * @description: function is used to Get List Of All Countries
 */
const getAllCountries = async (req, res) => {
    try{
        let allCountries = await User.query()
            .distinct("country_name")
            .whereNot({country_name: ""})
            .orderBy("country_name", "asc");
        // await Country.query()
        //     .select("*")
        //     .orderBy("country_name", "asc");
        return okResponse(res, allCountries, "All Countries fetched successfully.");
    }catch(error){
        console.log(error);
        throw badRequestError(res, error);
    }
};


/**
 * Get List Of ISD_CODE Of All Countries
 * @description: function is used to Get List Of ISD_CODE Of All Countries
 */
const getAllCountryISDcode = async (req, res) => {
    try {
        let getAllISDcodes = await Country.query()
            .select("id", "country_isd_code")
            .orderBy("country_isd_code", "desc");
        return okResponse(res, getAllISDcodes, "Country Get Successfully.");
    } catch(error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Get Country Name By ISD_CODE
 * @description: function is used to Get Country Name By ISD_CODE
 */
const getCountryByISDcode = async (req, res) => {
    try{
        let isd_country_code = req.query.isd_code;
        let getCountry = await Country.query()
            .select("id", "country_name")
            .orderBy("country_name", "desc")
            .where("country_isd_code", isd_country_code).first();
        return okResponse(res, getCountry, "Country Get Successfully.");
    }catch(error){
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Get List Of All Cities According to country
 * @description: function is used to Get List Of All Cities According to country
 */
const getAllCityAccordingToCountry = async (req, res) => {
    try {

        let allCity = await User.query()
            .distinct("city_name", "country_name")
            .whereNot({ city_name: null })
            .andWhereRaw("LOWER(country_name) LIKE '%' || LOWER(?) || '%'", req.query.country_name)
            .orderBy("city_name", "asc");
        // await Country.query()
        //     .select("*")
        //     .orderBy("country_name", "asc");
        return okResponse(res, allCity, "All Cities fetched Successfully.");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
};


//Exports the functions
module.exports = {
    getAllCountries,
    getAllCountryISDcode,
    getCountryByISDcode,
    getAllCityAccordingToCountry
};

//----------------------------------------------CountryController End------------------------------------------------------------------
