'use strict';

/**
 * @author SHUBHAM GUPTA
 */
//----------------------------------------------UserAuthController Start------------------------------------------------------------------

//Import Dependency 3rd Party Modules
const validator = require('validator');
const bcryptjs = require('bcryptjs');
const EMAIL = require('../../middlewares/email');
const sms = require('../../middlewares/sms');
const crypto = require('crypto');
const moment = require('moment');

//Import Models Here
const AuthTable = require('../../models/AuthTable');
const User = require('../../models/User');
const stripe = require('./../../middlewares/stripe');
const Stripe = require('./../../middlewares/StripeIOS');
let knexConfig = require('../../../db/knexfile');
const Knex = require('knex')(knexConfig['development']);

//========================================================================================

/**
 * Create User
 * @description: function is used to create user
 */
const registerUser = async (req, res) => {
	try {
		let data = req.body;
		data.user_type = 'USER';
		if (!data.is_email_verified) {
			data.is_email_verified = false;
		}
		let result_1, result_2;

		// console.log(data);

		/*-------------------Check Required Fields----------------------------*/
		if (!data.first_name) throw badRequestError(res, 'Enter first name.');
		if (data.first_name.length > 50) {
			throw badRequestError(
				res,
				'First Name has exceeded the maximum limit of 50 characters.'
			);
		}
		if (!data.last_name) throw badRequestError(res, 'Enter last name.');
		if (data.last_name.length > 50) {
			throw badRequestError(
				res,
				'Last Name has exceeded the maximum limit of 50 characters.'
			);
		}

		if (!data.email) throw badRequestError(res, 'Enter email.');
		if (data.email.length > 120) {
			throw badRequestError(
				res,
				'Email has exceeded the maximum limit of 120 characters.'
			);
		}

		if (!data.password) throw badRequestError(res, 'Enter password.');
		if (!data.country_isd_code)
			throw badRequestError(res, 'Enter country_isd_code.');
		// if (!data.country_name)
		// 	throw badRequestError(res, 'Enter country_name.');
		if (!data.country_iso_code)
			throw badRequestError(res, 'Enter country_iso_code.');
		if (data.country_isd_code.length > 5) {
			throw badRequestError(
				res,
				'Country ISD Code has exceeded the maximum limit of 120 characters..'
			);
		}
		if (!data.mobile) throw badRequestError(res, 'Enter mobile.');
		if (data.mobile.length > 18) {
			throw badRequestError(
				res,
				'Mobile Number has exceeded the maximum limit of 18 characters.'
			);
		}

		if (!data.device_type) throw badRequestError(res, 'Enter device type.');
		if (!data.date_of_birth) throw badRequestError(res, 'Enter date of birth.');
		if (!validator.isEmail(data.email))
			throw badRequestError(res, 'Enter a valid email id.');

		if (!validator.isLength(data.password, { min: 6, max: 32 })) {
			throw badRequestError(
				res,
				'Password needs to be between 6 to 32 characters long.'
			);
		}

		let result_11 = await User.query()
			.select('*')
			.where({ email: data.email, user_type: 'USER' })
			.andWhere({ deleteRequest: 1 })
			.first();

		let result_12 = await User.query()
			.select('*')
			.where({ email: data.email, user_type: 'USER' })
			.andWhere({ deleteRequest: 2 })
			.first();

		if (result_11) {
			result_1 = result_11;
		} else {
			result_1 = result_12;
		}

		//-------Check if Email already exist or not--------------------
		if (result_1 || result_1 != undefined) {
			throw badRequestError(res, 'User with this email id already exist.');
		}

		let result_21 = await User.query()
			.select('*')
			.where({ mobile: data.mobile, user_type: 'USER' })
			.andWhere({ deleteRequest: 1 })
			.first();

		let result_22 = await User.query()
			.select('*')
			.where({ mobile: data.mobile, user_type: 'USER' })
			.andWhere({ deleteRequest: 2 })
			.first();

		// console.log(result_1);
		// return;

		if (result_21) {
			result_2 = result_21;
		} else {
			result_2 = result_22;
		}

		//-------Check if Mobile Number already exist or not--------------------
		if (result_2 || result_2 != undefined) {
			throw badRequestError(res, 'User with this mobile no. already exist.');
		}

		/*-------------------If a valid referral_code Entered----------------------------*/
		if (data.referral_code) {
			let userWithThisReferrCodeExist = await User.query()
				.select('*')
				.where('referr_code', data.referral_code)
				.first();
			if (userWithThisReferrCodeExist == undefined)
				throw badRequestError(res, 'Referral Code is Invalid!');
		}

		//-------------Otp Generate Code------------------------------------------
		let otp = Math.floor(1000 + Math.random() * 9000);
		// let otp = 1234;
		data.otpGenerateTime = new Date().getTime();
		let otpGenerateTime = data.otpGenerateTime;

		let msg;
		if (data.device_type == 'android') {
			msg =
				'<#>' +
				' ' +
				'Your Favos verification code (OTP) is ' +
				otp +
				' ' +
				'WcZv666/V1R';
		} else {
			msg = 'Your Favos verification code (OTP) is ' + otp;
		}

		delete data.device_type;

		let smsData = {
			to: req.body.country_isd_code + req.body.mobile,
			message: msg,
		};
		sms.sendSms(smsData);

		//----Insert OTP-----
		data.otp = otp;
		//-----------Generate referral code---------------------------------------
		var length = 6,
			charset =
				'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
			referral_code = '';
		for (var i = 0, n = charset.length; i < length; ++i) {
			referral_code += charset.charAt(Math.floor(Math.random() * n));
		}
		data.referr_code = referral_code;
		let name = data.first_name; //+ ' ' + data.last_name;

		if (!data.is_email_verified) {
			let date = new Date().getTime();
			let token = crypto.randomBytes(64).toString('hex');
			data.verify_email_token = token;
			data.email_sentout_date = moment().format('YYYY-MM-DD HH:mm:ss');

			//-------------Send Mail for email verification----------------------------
			EMAIL.sendEmailToVerifyEmail(
				name,
				data.email,
				`https://favos.co/api/v1/user/verifyEmail?email=${token}&valid=${date}`
			);
		}

		data.stripecustomerId = await stripe.GetCustomerID({
			email: data.email,
		});

		let registerUser = await User.query().insert(data).returning('*');

		let responseData = {
			id: registerUser.id,
			first_name: registerUser.first_name,
			last_name: registerUser.last_name,
			email: registerUser.email,
			mobile: registerUser.mobile,
			country_isd_code: registerUser.country_isd_code,
			country_iso_code: registerUser.country_iso_code,
			country_name: registerUser.country_name,
			user_type: registerUser.user_type,
			date_of_birth: registerUser.date_of_birth,
			profile_image: registerUser.profile_image,
			gender: registerUser.gender,
			banner_image: registerUser.banner_image,
			stripecustomerId: registerUser.stripecustomerId,
		};

		return createdResponse(
			res,
			responseData,
			'Congratulations! User Registered and Otp and Email sent successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Login API
 * @description: fuction is used to do login
 */
const UserLogin = async (req, res) => {
	try {
		let data = req.body;

		/** Validate for the empty fields */
		if (!data.login_Id) throw badRequestError(res, 'Enter user login details.');

		// if (!data.device_type)
		// 	throw badRequestError(res, 'device_type is required.');
		// if (!data.device_token)
		// 	throw badRequestError(res, 'device_token is required.');

		if (!data.password) throw badRequestError(res, 'Enter password.');
		// if (data.password.length < 6)
		// 	throw badRequestError(res, 'Password should be at least 6 characters long.');
		// if (data.password.length > 32)
		// 	throw badRequestError(res, 'Password should not be more than 32 characters long.');

		let user;

		/** Check user email is verified or not */
		if (validator.isEmail(data.login_Id)) {
			if (data.login_Id.length > 120) {
				throw badRequestError(
					res,
					'Email has exceeded the maximum limit of 120 characters.'
				);
			}
			user = await User.query()
				.whereNot('deleteRequest', 3)
				.where({ user_type: 'USER' }) //email: data.login_Id,
				.andWhereRaw(
					"LOWER(users.email) LIKE '%' || LOWER(?) || '%'",
					data.login_Id
				)
				.first();

			if (user == undefined) {
				throw badRequestError(
					res,
					'user with this email id is not registered. Please enter valid credentials.'
				);
			}

			// if (user.is_email_verified == false) {
			//     throw badRequestError(res, "Email not verified. Please enter valid credentials.");
			// }
		} else {
			/** Check user mobile is exist or not */
			if (data.login_Id.length > 18) {
				throw badRequestError(
					res,
					'Mobile Number has exceeded the maximum limit of 18 characters.'
				);
			}

			user = await User.query()
				.whereNot('deleteRequest', 3)
				.where(function () {
					this.where({ mobile: data.login_Id })
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							data.login_Id
						)
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							'+' + data.login_Id
						);
				})
				.andWhere({ user_type: 'USER' }) //, country_isd_code: data.country_isd_code
				.first();

			console.log(user);

			if (user == undefined) {
				throw badRequestError(
					res,
					'User with this mobile no. is not registered. Please enter valid credentials.'
				);
			}
		}

		if (user.is_mobile_verified == null) {
			// if ((user.is_mobile_verified == null || user.is_mobile_verified == false) && (user.is_email_verified == false || user.is_email_verified == null)) {
			//-------------Otp Generate Code------------------------------------------
			let otp = Math.floor(1000 + Math.random() * 9000);
			// let otp = 1234;
			let otpGenerateTime = new Date().getTime();

			let msg;
			if (data.device_type == 'android') {
				msg =
					'<#>' +
					' ' +
					'Your Favos verification code (OTP) is ' +
					otp +
					' ' +
					'WcZv666/V1R';
			} else {
				msg = 'Your Favos verification code (OTP) is ' + otp;
			}

			let smsData = {
				to: user.country_isd_code + user.mobile,
				message: msg,
			};
			sms.sendSms(smsData);

			console.log('user======', user);

			//Status send to apllication to move scrren from login to OTP screen
			user.status = 2;

			//----Insert OTP-----
			await User.query()
				.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where('id', user.id);
			throw okResponse(
				res,
				user,
				'This mobile no. is not yet verified. Please verify your mobile no. with the OTP sent to it.'
			);
		}

		/* check user is_active or not */
		if (user.user_status == false) {
			throw badRequestError(res, 'You are blocked by admin.');
		}

		/* Check Password matched OR not */
		if (!(await user.comparePassword(data.password))) {
			throw badRequestError(res, 'Enter a valid password.');
		}

		/****** Generate authenticated data and its auth_token *****/
		let authToken = await user.getJWT();
		res.setHeader('Content-Type', 'application/json');
		res.setHeader('AuthToken', authToken);
		res.setHeader('Access-Control-Expose-Headers', 'AuthToken');
		/******-----------------------------------------------*****/

		// let tokenExist = await AuthTable.query().select("id","user_id").where("user_id", user.id).first();
		// console.log(tokenExist)

		// if(tokenExist){
		//     await AuthTable.query().patch({
		//         auth_token: authToken,
		//         device_type: data.device_type,
		//         device_id: data.device_id,
		//         device_token: data.device_token
		//     }).where("user_id", user.id).returning("*");
		// }else{
		await AuthTable.query()
			.insert({
				user_id: user.id,
				auth_token: authToken,
				device_type: data.device_type,
				device_id: data.device_id,
				device_token: data.device_token,
			})
			.returning('*');
		// }

		//account reactivation done
		if (user.deleteRequest == 2 && user.delete_request_date != null) {
			await User.query()
				.update({ delete_request_date: null, deleteRequest: 1 })
				.where('id', user.id);
		}

		//delete password
		delete user.password;

		let responseData = {
			id: user.id,
			first_name: user.first_name,
			last_name: user.last_name,
			email: user.email,
			mobile: user.mobile,
			country_isd_code: user.country_isd_code,
			country_iso_code: user.country_iso_code,
			country_name: user.country_name,
			user_type: 'USER',
			profile_image: user.profile_image,
			gender: user.gender,
			date_of_birth: user.date_of_birth,
			user_status: user.user_status,
			is_email_verified: user.is_email_verified,
			is_mobile_verified: user.is_mobile_verified,
			stripecustomerId: user.stripecustomerId,
		};

		return okResponse(res, responseData, 'Loggedin Successfully.');
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Verify OTP API
 * @description: fuction is used to do Verify OTP
 */
const verifyOtpController = async (req, res) => {
	try {
		let data = req.body;

		if (!req.body.otp) {
			throw badRequestError(res, 'Invalid Otp!');
		}

		let otp_data;
		otp_data = await User.query()
			.select(
				'id',
				'email',
				'mobile',
				'otp',
				'is_email_verified',
				'is_mobile_verified',
				'otpGenerateTime',
				'user_type',
				'referral_code'
			)
			.where({ otp: data.otp, user_type: 'USER', id: data.id })
			.first();

		if (otp_data == undefined || otp_data.otpGenerateTime == null) {
			throw badRequestError(
				res,
				'OTP (verification code) entered is incorrect. Enter correct OTP to verify your mobile no.'
			);
		}

		//------------ Check OTP Expired OR Not (START) ------------//
		let otpGeneratedTime = parseInt(otp_data.otpGenerateTime);
		let todayDateTime = new Date().getTime();
		/*-------- If otpGeneratedTime is less than 15 minutes OTP is VALID -------*/
		/*-------- If otpGeneratedTime is greater than 15 minutes OTP is EXPIRED -------*/
		let difference = (todayDateTime - otpGeneratedTime) / 1000;
		let diff = difference / 60;
		diff = Math.abs(Math.round(diff));
		//---------------------------- If difference is above 15 minutes, then OTP expires ----------------------------//
		if (diff > 15) {
			await User.query()
				.patch({ otp: null, otpGenerateTime: null })
				.where({ id: data.id })
				.first();

			throw badRequestError(res, 'Otp Expired');
		}
		//------------ Check OTP Expired OR Not (END) ------------//

		//-----------condition to update mobile-------------
		if (req.body.mobile && req.body.country_isd_code) {
			let updateMobile = await User.query()
				.patch({
					otp: null,
					is_mobile_verified: true,
					mobile: req.body.mobile,
					country_isd_code: data.country_isd_code,
					otpGenerateTime: null,
				})
				.where({
					id: data.id,
				})
				.first()
				.returning('*');

			delete updateMobile.password;

			return okResponse(
				res,
				updateMobile,
				'Mobile no. is verified and updated successfully!'
			);
		}
		//-----------condition to update email-------------
		else if (req.body.email) {
			let updateEmail = await User.query()
				.patch({
					otp: null,
					is_email_verified: true,
					email: req.body.email,
					otpGenerateTime: null,
				})
				.where({
					id: data.id,
				})
				.first()
				.returning('*');

			delete updateEmail.password;

			return okResponse(
				res,
				updateEmail,
				'Email Verified and updated successfully!'
			);
		}
		//-----------condition to Verify OTP-------------
		else if (
			otp_data.is_mobile_verified == false ||
			otp_data.is_mobile_verified == null
		) {
			/****** Generate authenticated data and its auth_token *****/
			let authToken = await otp_data.getJWT();
			res.setHeader('Content-Type', 'application/json');
			res.setHeader('AuthToken', authToken);
			res.setHeader('Access-Control-Expose-Headers', 'AuthToken');

			await AuthTable.query()
				.insert({
					user_id: otp_data.id,
					auth_token: authToken,
					device_type: data.device_type,
					device_id: data.device_id,
					device_token: data.device_token,
				})
				.returning('*');

			let verifyMobileNumber;
			if (otp_data.referral_code == null || otp_data.referral_code == '') {
				verifyMobileNumber = await User.query()
					.patch({ otp: null, is_mobile_verified: true, otpGenerateTime: null })
					.where({
						id: data.id,
					})
					.first()
					.returning('*');
			} else {
				/*-------Harcode the earning point value for user if he/she eneters referr code value-------*/
				let earning_point = 500;

				verifyMobileNumber = await User.query()
					.patch({
						otp: null,
						is_mobile_verified: true,
						otpGenerateTime: null,
						earning_point: earning_point,
					})
					.where({
						id: data.id,
					})
					.first()
					.returning('*');

				let userWithThisReferrCodeExist = await User.query()
					.select('*')
					.where('referr_code', otp_data.referral_code)
					.first();

				/*---------------Update Earning Point of user whiich have entered referral code (Add 100 points)--------------------*/
				let totalEarningOfReferredOne =
					parseInt(userWithThisReferrCodeExist.earning_point) + 100;
				let updateEarningPointOfReferredOne = await User.query()
					.patch({ earning_point: totalEarningOfReferredOne })
					.where('id', userWithThisReferrCodeExist.id);
				if (!updateEarningPointOfReferredOne)
					throw badRequestError(
						res,
						'Update Earning point failed of user which have entered referral code.'
					);
			}
			delete verifyMobileNumber.password;

			return okResponse(
				res,
				verifyMobileNumber,
				'Otp is verified successfully and now you are logged in.'
			);
		}
		//Otp Verification during forgot password
		else {
			let verifyMobileDuringForgotPassword = await User.query()
				.patch({ otp: null, is_mobile_verified: true, otpGenerateTime: null })
				.where({
					id: data.id,
				})
				.first()
				.returning('*');

			delete verifyMobileDuringForgotPassword.password;

			return okResponse(
				res,
				verifyMobileDuringForgotPassword,
				'Otp Verified Successfully!'
			);
		}
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Social Login API
 * @description: fuction is used to do Social-Login
 */
const userSocialLogin = async (req, res) => {
	try {
		const data = req.body;
		console.log(data);
		if (!data.email || data.email == '') {
			return badRequestError(res, 'Enter email id.');
		}
		if (data.email.length > 120) {
			throw badRequestError(
				res,
				'Email has exceeded the maximum limit of 120 characters.'
			);
		}

		if (
			!data.login_type ||
			data.login_type == '' ||
			data.login_type == null ||
			data.login_type == undefined
		) {
			return badRequestError(res, 'login_type is required!');
		}

		if (data.login_type == 'Normal') {
			return badRequestError(
				res,
				"login_type can't be Normal, because it is social login! Either it is facebook or google."
			);
		}

		let userData1 = await User.query()
			.where({ email: data.email, user_type: 'USER' })
			.where({ deleteRequest: 1 })
			.first();

		let userData2 = await User.query()
			.where({ email: data.email, user_type: 'USER' })
			.where({ deleteRequest: 2 })
			.first();

		let userData;
		if (userData1) {
			userData = userData1;
		} else {
			userData = userData2;
		}

		//If User Exist
		if (userData) {
			//Return Status 3 (It means Before Login Verify your Email Id)
			if (userData.is_email_verified == false && userData) {
				try {
					// userData.status = 3;
					// delete userData.password;
					// return okResponse(res, userData, "Email Address is not verified, Please verify your email first.");

					userData.status = 4;
					// console.log("users===========")
					const auth_token = await userData.getJWT();

					console.log(auth_token);
					res.setHeader('Content-Type', 'application/json');
					res.setHeader('AuthToken', auth_token);
					res.setHeader('Access-Control-Expose-Headers', 'AuthToken');
					//if they don't exist than insert them in "auth_table" table
					let authTableData = {};
					authTableData.user_id = userData.id;
					authTableData.auth_token = auth_token;
					authTableData.device_type = data.device_type;
					authTableData.device_id = data.device_id;
					authTableData.device_token = data.device_token;

					//------Check If User data exist in auth_table then it is updated, If NOT then it is Inserted------------------
					// let tokenExist = await AuthTable.query().select("id", "user_id").where("user_id", userData.id).first();
					// console.log(tokenExist)

					// if (tokenExist) {
					//     await AuthTable.query().patch(authTableData).where("user_id", userData.id).returning("*");
					// } else {
					await AuthTable.query().insert(authTableData).returning('*');
					await User.query()
						.update({ is_email_verified: true })
						.where('id', userData.id)
						.returning('*');

					// }

					delete userData.password;
					return okResponse(res, userData, 'Login successful.');
				} catch (error) {
					throw badRequestError(res, error);
				}
			}
			//Return Status 2 (It means Before Login Verify your Mobile Number)
			else if (userData.is_mobile_verified == null && userData) {
				try {
					userData.status = 2;
					//-------------Otp Generate Code------------------------------------------
					let otp = Math.floor(1000 + Math.random() * 9000);
					// let otp = 1234;
					let otpGenerateTime = new Date().getTime();

					let msg;
					if (data.device_type == 'android') {
						msg =
							'<#>' +
							' ' +
							'Your Favos verification code (OTP) is ' +
							otp +
							' ' +
							'WcZv666/V1R';
					} else {
						msg = 'Your Favos verification code (OTP) is ' + otp;
					}

					let smsData = {
						to: req.body.country_isd_code + req.body.mobile,
						message: msg,
					};
					sms.sendSms(smsData);

					let insertOtp = await User.query()
						.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
						.where({ email: userData.email, mobile: userData.mobile })
						.returning('*');

					if (!insertOtp) throw badRequestError(res, 'Something Went Wrong!');

					delete userData.password;

					return okResponse(
						res,
						userData,
						'OTP (Verification Code) sent successfully. Please Verify your mobile number.'
					);
				} catch (error) {
					throw badRequestError(res, error);
				}
			}
			//Return Status 4 (It means Login Existing User)
			else if (
				userData.is_mobile_verified &&
				userData.is_email_verified &&
				userData.user_status &&
				(userData.mobile != null || userData != '')
			) {
				try {
					userData.status = 4;
					// console.log("users===========")
					const auth_token = await userData.getJWT();

					console.log(auth_token);
					res.setHeader('Content-Type', 'application/json');
					res.setHeader('AuthToken', auth_token);
					res.setHeader('Access-Control-Expose-Headers', 'AuthToken');
					//if they don't exist than insert them in "auth_table" table
					let authTableData = {};
					authTableData.user_id = userData.id;
					authTableData.auth_token = auth_token;
					authTableData.device_type = data.device_type;
					authTableData.device_id = data.device_id;
					authTableData.device_token = data.device_token;

					//------Check If User data exist in auth_table then it is updated, If NOT then it is Inserted------------------
					// let tokenExist = await AuthTable.query().select("id", "user_id").where("user_id", userData.id).first();
					// console.log(tokenExist)

					// if (tokenExist) {
					//     await AuthTable.query().patch(authTableData).where("user_id", userData.id).returning("*");
					// } else {
					await AuthTable.query().insert(authTableData).returning('*');
					// }

					delete userData.password;
					return okResponse(res, userData, 'Login successful.');
				} catch (e) {
					return badRequestError(res, 'something went wrong');
				}
			}
		}
		//Return Status 1 (It means Social-Login by user first time User)
		else {
			try {
				const status = 1;
				return okResponse(res, { status }, 'first time Login successful.l!');
			} catch (error) {
				console.log('sociallogin authcontroller patient ' + err);
				return badRequestError(res, 'Something went wrong');
			}
		}
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Forgot Password API
 * @description: fuction is used for Forgot Password
 */
const forgotPassword = async (req, res) => {
	try {
		let data = req.body;
		console.log(data);

		if (!data.device_type)
			throw badRequestError(res, 'device_type is required!');

		//-------------Otp Generate Code------------------------------------------
		let otp = Math.floor(1000 + Math.random() * 9000);
		// let otp = 1234;
		let otpGenerateTime = new Date().getTime();

		if (data.status == 'email') {
			if (!validator.isEmail(data.login_Id)) {
				throw badRequestError(res, 'Enter a valid email id.');
			}
			if (data.login_Id.length > 120) {
				throw badRequestError(
					res,
					'Email has exceeded the maximum limit of 120 characters.'
				);
			}

			let email = data.login_Id;
			let result;
			let result1 = await User.query()
				.select(
					'id',
					'first_name',
					'last_name',
					'email',
					'country_isd_code',
					'mobile',
					'user_type'
				)
				.whereRaw("LOWER(users.email) LIKE '%' || LOWER(?) || '%'", email)
				.andWhere({ user_type: 'USER', deleteRequest: 1 })
				.first();

			let result2 = await User.query()
				.select(
					'id',
					'first_name',
					'last_name',
					'email',
					'country_isd_code',
					'mobile',
					'user_type'
				)
				.whereRaw("LOWER(users.email) LIKE '%' || LOWER(?) || '%'", email)
				.andWhere({ user_type: 'USER', deleteRequest: 2 })
				.first();

			if (result1) {
				result = result1;
			} else {
				result = result2;
			}

			if (result == undefined) {
				throw badRequestError(
					res,
					'Account with this email Id is not registered.'
				);
			}

			if (result.is_email_verified == false)
				throw badRequestError(res, 'Verify your email id first.');

			let name = result.first_name; //+ ' ' + result.last_name;

			//----------Send mail for OTP---------------------
			EMAIL.sendOtpByEmail(name, email, otp);
			//------------------------------------------------

			let insert_otp = await User.query()
				.update({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where('email', email);
			if (!insert_otp) {
				throw badRequestError(res, 'Something Went Wrong.');
			}
			return okResponse(
				res,
				result,
				'Please check your email to reset your password.'
			);
		} else if (data.status == 'mobile') {
			let mobile = data.login_Id;
			let result;

			if (data.login_Id.length > 18) {
				throw badRequestError(
					res,
					'Mobile Number has exceeded the maximum limit of 18 characters.'
				);
			}

			let result1 = await User.query()
				.select(
					'id',
					'first_name',
					'last_name',
					'email',
					'country_isd_code',
					'mobile',
					'user_type'
				)
				.where(function () {
					this.where({ mobile: mobile })
						.orWhere(Knex.raw('concat(country_isd_code, mobile)'), mobile)
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							'+' + mobile
						);
				})
				.andWhere({ deleteRequest: 1, user_type: 'USER' })
				.first();

			let result2 = await User.query()
				.select(
					'id',
					'first_name',
					'last_name',
					'email',
					'country_isd_code',
					'mobile',
					'user_type'
				)
				.where(function () {
					this.where({ mobile: mobile })
						.orWhere(Knex.raw('concat(country_isd_code, mobile)'), mobile)
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							'+' + mobile
						);
				})
				.andWhere({ deleteRequest: 2, user_type: 'USER' })
				.first();

			if (result1) {
				result = result1;
			} else {
				result = result2;
			}

			if (result == undefined) {
				throw badRequestError(
					res,
					'The mobile no. is not registered with any account.'
				);
			}

			if (result.is_mobile_verified == false)
				throw badRequestError(res, 'Please First verify your mobile number');

			let msg;
			if (data.device_type == 'android') {
				msg =
					'<#>' +
					' ' +
					'Your Favos verification code (OTP) is ' +
					otp +
					' ' +
					'WcZv666/V1R';
			} else {
				msg = 'Your Favos verification code (OTP) is ' + otp;
			}

			// let new_mobile;
			// if ((result.country_isd_code + result.mobile) == mobile){
			// 	new_mobile = mobile;
			// }else{
			// 	new_mobile = result.country_isd_code + mobile
			// }

			let smsData = {
				to: result.country_isd_code + result.mobile,
				message: msg,
			};
			sms.sendSms(smsData);

			let insert_otp = await User.query()
				.update({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where(function () {
					this.where({ mobile: mobile })
						.orWhere(Knex.raw('concat(country_isd_code, mobile)'), mobile)
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							'+' + mobile
						);
				});

			if (!insert_otp) {
				throw badRequestError(res, 'Data not inserted. Something Went Wrong.');
			}

			return okResponse(res, result, 'OTP sent successfully.');
		}
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Reset Password
 * @description: fuction is used to Reset Password
 */
const resetPassword = async (req, res) => {
	try {
		let otp = req.body.otp;
		let data = req.body;

		if (!data.id) throw badRequestError(res, 'Please enter id!');

		if (!otp) {
			return badRequestError(res, 'Please enter OTP.');
		}
		if (!data.password) {
			return badRequestError(res, 'Enter password.');
		}
		if (!data.confirm_password) {
			return badRequestError(res, 'Please enter Confirm Password');
		}

		if (data.password != data.confirm_password) {
			throw badRequestError(res, 'New and Confirm Passwoord do not match.');
		}

		//---------- bcryptjs the password before insert-----------------------
		data.password = await bcryptjs.hash(data.password, 10);

		let user = await User.query()
			.where({ otp: otp, id: data.id })
			.andWhere('user_type', 'USER')
			.first()
			.returning('*');

		if (user == undefined || user.otpGenerateTime == null)
			throw badRequestError(res, 'Invalid OTP!');

		delete user.password;

		//------------ Check OTP Expired OR Not (START) ------------//
		let otpGeneratedTime = parseInt(user.otpGenerateTime);
		let todayDateTime = new Date().getTime();
		/*-------- If otpGeneratedTime is less than 15 minutes OTP is VALID -------*/
		/*-------- If otpGeneratedTime is greater than 15 minutes OTP is EXPIRED -------*/
		let difference = (todayDateTime - otpGeneratedTime) / 1000;
		let diff = difference / 60;
		diff = Math.abs(Math.round(diff));
		//---------------------------- If difference is above 15 minutes, then OTP expires ----------------------------//
		if (diff > 15) {
			console.log('Error');
			await User.query()
				.patch({ otp: null, otpGenerateTime: null })
				.where({
					id: data.id,
				})
				.first();
			throw badRequestError(res, 'Otp Expired');
		}
		//------------ Check OTP Expired OR Not (END) ------------//

		if (user) {
			let resetPass = await User.query()
				.update({ password: data.password, otp: null, otpGenerateTime: null })
				.where({ otp: otp, id: data.id })
				.andWhere('user_type', 'USER')
				.first()
				.returning('*');

			if (resetPass == undefined)
				throw badRequestError(res, "reset password can't done!");
			return okResponse(res, resetPass, 'Password reset successfully.');
		}
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Logout API
 * @description: function is used to do Logout
 */
const UserLogout = async (req, res) => {
	try {
		let logout = await AuthTable.query()
			.delete()
			.where({ user_id: req.user.user_id, id: req.user.id })
			.returning('*');

		return okResponse(
			res,
			{
				logout,
			},
			'Logged out successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Resent Mail API
 * @description: function is used to resent mail
 */
const resentEmailToVerify = async (req, res) => {
	try {
		let email = req.query.email;
		let date = new Date().getTime();

		if (!email)
			throw badRequestError(res, 'Please pass email in query string.');

		if (email.length > 120) {
			throw badRequestError(
				res,
				'Email has exceeded the maximum limit of 120 characters.'
			);
		}

		let result = await User.query()
			.select('*')
			.where({ email: email, user_type: 'USER' })
			.first();

		if (result == undefined || result.deleteRequest == 3)
			throw badRequestError(res, 'User does not exist with this email id.');

		if (result.is_email_verified == true)
			throw badRequestError(
				res,
				'User with this email Id already exist and verified.'
			);

		let name = result.first_name; //+ ' ' + result.last_name;

		//-------email verification token----------------
		let token = crypto.randomBytes(64).toString('hex');
		let todayDate = moment().format('YYYY-MM-DD HH:mm:ss');
		await User.query()
			.patch({ verify_email_token: token, email_sentout_date: todayDate })
			.where({ email: email, user_type: 'USER', id: result.id })
			.first();

		//-------------Send Mail for email verification----------------------------
		EMAIL.sendEmailToVerifyEmail(
			name,
			email,
			`https://favos.co/api/v1/user/verifyEmail?email=${token}&valid=${date}`
		);

		delete result.password;
		return okResponse(
			res,
			result,
			'Email resent successfully. Please verify your email id.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

const getephemeralKey = async (req, res) => {
	let api_version = req.body.api_version;
	let customerid = req.body.customerid;
	let key = await stripe.GetephemeralKey(api_version, customerid);
	if (!key) {
		throw badRequestError(res, 'Oops, some error has occured');
	}
	return okResponse(res, key, 'Operation successfull');
};

const getephemeralKey_v2 = async (req, res) => {
	let api_version = req.body.api_version;
	let customerid = req.body.customerid;
	if (api_version == '' || customerid == '') {
		throw badRequestError(res, 'Required Parameter not found');
	}
	let key = await stripe.GetephemeralKey(api_version, customerid);
	if (!key) {
		throw badRequestError(res, 'Oops, some error has occured');
	}
	return okResponse(res, key, 'Operation successfull');
};

/**
 * Resent OTP API
 * @description: function is used to resent OTP
 */

const resentOTPtoVerify = async (req, res) => {
	try {
		let data = req.body;
		let otpGenerateTime = new Date().getTime();

		if (!data.mobile) throw badRequestError(res, 'Enter your mobile number.');
		if (data.mobile.length > 18) {
			throw badRequestError(
				res,
				'Mobile Number has exceeded the maximum limit of 18 characters.'
			);
		}

		let result = await User.query()
			.select('*')
			.where({ mobile: data.mobile, user_type: 'USER' })
			.first();

		if (result == undefined || result.deleteRequest == 3)
			throw badRequestError(res, 'User With this mobile number not exist!');

		let otp;
		if (
			result.otp == null &&
			(result.otpGenerateTime == null || result.otpGenerateTime == '')
		) {
			//-------------Otp Generate Code------------------------------------------
			otp = Math.floor(1000 + Math.random() * 9000);
			// otp = 1234;

			//----Insert OTP-----
			let resentOTP = await User.query()
				.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where({ mobile: data.mobile }) //, country_isd_code: data.country_isd_code
				.first();
		} else {
			let otpGeneratedTime = parseInt(result.otpGenerateTime);
			let todayDateTime = new Date().getTime();
			/*-------- If otpGeneratedTime is less than 15 minutes OTP is VALID -------*/
			/*-------- If otpGeneratedTime is greater than 15 minutes OTP is EXPIRED -------*/
			let difference = (todayDateTime - otpGeneratedTime) / 1000;
			let diff = difference / 60;
			diff = Math.abs(Math.round(diff));
			//---------------------------- If difference is above 15 minutes, then OTP expires ----------------------------//
			if (diff > 15) {
				otp = Math.floor(1000 + Math.random() * 9000);
				// otp = 1234;

				//----Insert OTP-----
				let resentOTP = await User.query()
					.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
					.where({ mobile: data.mobile }) //, country_isd_code: data.country_isd_code
					.first();
			} else {
				//------send exist otp----------------
				otp = result.otp;
			}
		}
		// else {
		// 	otp = result.otp;
		// }

		let msg;
		if (data.device_type == 'android') {
			msg =
				'<#>' +
				' ' +
				'Your Favos verification code (OTP) is ' +
				otp +
				' ' +
				'WcZv666/V1R';
		} else {
			msg = 'Your Favos verification code (OTP) is ' + otp;
		}
		let smsData = {
			to: result.country_isd_code + req.body.mobile,
			message: msg,
		};
		sms.sendSms(smsData);

		delete result.password;

		return okResponse(
			res,
			result,
			'OTP (Verification Code) resent successfully. Please Verify your mobile number.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Verify Email
 * @description: function is used to verify email
 */
const verifyEmail = async (req, res) => {
	try {
		const verification_token = req.query.email;
		let userData, userType;

		if (req.query.valid) {
			let validDate = req.query.valid;
			let todayDate = new Date();
			userType = 'USER';

			//-------------- Calculate the days difference (START) ------------------//
			/*-------- If link validDate is less than or equal to 7, Link is VALID -------*/
			/*-------- If link validDate is greater than 7, Link is EXPIRED -------*/
			// To calculate the time difference of two dates
			let Difference_In_Time = todayDate.getTime() - validDate;
			// To calculate the no. of days between two dates
			let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			//-------------- Calculate the days difference (END) ------------------//

			if (!verification_token)
				throw badRequestError(
					res,
					'Please pass verification_token in query string.'
				);

			if (Difference_In_Days > 7) {
				// throw badRequestError(res, "Link Expired!");
				await User.query()
					.patch({
						email: '',
						email_sentout_date: null,
						verify_email_token: null,
					})
					.where({
						verify_email_token: verification_token,
						user_type: userType,
					});
				return res.render('unsuccess');
			}
		} else {
			userType = 'MERCHANT';
		}

		userData = await User.query()
			.select(
				'first_name',
				'last_name',
				'email',
				'mobile',
				'date_of_birth',
				'is_mobile_verified',
				'is_email_verified',
				'user_status',
				'user_type',
				'verify_email_token'
			)
			.where('verify_email_token', verification_token)
			.andWhere('user_type', userType)
			.first();

		if (userData == undefined) {
			// return linkExpiredError("Email verification link is invalid or has expired");
			return res.render('unsuccess');
		}

		if (userData.is_email_verified) {
			return res.render('unsuccess');
		}

		if (!userData.user_status)
			throw badRequestError(res, 'Admin has blocked your account.');

		let updateVerification = await User.query()
			.patch({
				is_email_verified: true,
				verify_email_token: null,
				email_sentout_date: null,
			})
			.where({ verify_email_token: verification_token, user_type: userType });

		if (!updateVerification)
			throw badRequestError(res, 'Something Went Wrong.');

		return res.render('success');
	} catch (error) {
		throw badRequestError(res, error);
	}
};

/**
 * Login API V2
 * @description: fuction is used to do login V2
 */
const UserLogin_V2 = async (req, res) => {
	try {
		let data = req.body;

		/** Validate for the empty fields */
		if (!data.login_Id) throw badRequestError(res, 'Enter user login details.');

		// if (!data.device_type)
		// 	throw badRequestError(res, 'device_type is required.');
		// if (!data.device_token)
		// 	throw badRequestError(res, 'device_token is required.');

		if (!data.password) throw badRequestError(res, 'Enter password.');
		// if (data.password.length < 6)
		// 	throw badRequestError(res, 'Password should be at least 6 characters long.');
		// if (data.password.length > 32)
		// 	throw badRequestError(res, 'Password should not be more than 32 characters long.');

		let user;

		/** Check user email is verified or not */
		if (validator.isEmail(data.login_Id)) {
			if (data.login_Id.length > 120) {
				throw badRequestError(
					res,
					'Email has exceeded the maximum limit of 120 characters.'
				);
			}
			user = await User.query()
				.whereNot('deleteRequest', 3)
				.where({ user_type: 'USER' }) //email: data.login_Id,
				.andWhereRaw(
					"LOWER(users.email) LIKE '%' || LOWER(?) || '%'",
					data.login_Id
				)
				.first();

			if (user == undefined) {
				throw badRequestError(
					res,
					'user with this email id is not registered. Please enter valid credentials.'
				);
			}

			// if (user.is_email_verified == false) {
			//     throw badRequestError(res, "Email not verified. Please enter valid credentials.");
			// }
		} else {
			/** Check user mobile is exist or not */
			if (data.login_Id.length > 18) {
				throw badRequestError(
					res,
					'Mobile Number has exceeded the maximum limit of 18 characters.'
				);
			}

			user = await User.query()
				.whereNot('deleteRequest', 3)
				.where(function () {
					this.where({ mobile: data.login_Id })
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							data.login_Id
						)
						.orWhere(
							Knex.raw('concat(country_isd_code, mobile)'),
							'+' + data.login_Id
						);
				})
				.andWhere({ user_type: 'USER' }) //, country_isd_code: data.country_isd_code
				.first();

			console.log(user);

			if (user == undefined) {
				throw badRequestError(
					res,
					'User with this mobile no. is not registered. Please enter valid credentials.'
				);
			}
		}

		// if (user.is_mobile_verified == null) {
		if (
			(user.is_mobile_verified == null || user.is_mobile_verified == false) &&
			(user.is_email_verified == false || user.is_email_verified == null)
		) {
			//-------------Otp Generate Code------------------------------------------
			let otp = Math.floor(1000 + Math.random() * 9000);
			// let otp = 1234;
			let otpGenerateTime = new Date().getTime();

			let msg;
			if (data.device_type == 'android') {
				msg =
					'<#>' +
					' ' +
					'Your Favos verification code (OTP) is ' +
					otp +
					' ' +
					'WcZv666/V1R';
			} else {
				msg = 'Your Favos verification code (OTP) is ' + otp;
			}

			let smsData = {
				to: user.country_isd_code + user.mobile,
				message: msg,
			};
			sms.sendSms(smsData);

			console.log('user======', user);

			//Status send to apllication to move scrren from login to OTP screen
			user.status = 2;

			//----Insert OTP-----
			await User.query()
				.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where('id', user.id);
			throw okResponse(
				res,
				user,
				'This mobile no. is not yet verified. Please verify your mobile no. with the OTP sent to it.'
			);
		}

		/* check user is_active or not */
		if (user.user_status == false) {
			throw badRequestError(res, 'You are blocked by admin.');
		}

		/* Check Password matched OR not */
		if (!(await user.comparePassword(data.password))) {
			throw badRequestError(res, 'Enter a valid password.');
		}

		/****** Generate authenticated data and its auth_token *****/
		let authToken = await user.getJWT();
		res.setHeader('Content-Type', 'application/json');
		res.setHeader('AuthToken', authToken);
		res.setHeader('Access-Control-Expose-Headers', 'AuthToken');
		/******-----------------------------------------------*****/

		// let tokenExist = await AuthTable.query().select("id","user_id").where("user_id", user.id).first();
		// console.log(tokenExist)

		// if(tokenExist){
		//     await AuthTable.query().patch({
		//         auth_token: authToken,
		//         device_type: data.device_type,
		//         device_id: data.device_id,
		//         device_token: data.device_token
		//     }).where("user_id", user.id).returning("*");
		// }else{
		await AuthTable.query()
			.insert({
				user_id: user.id,
				auth_token: authToken,
				device_type: data.device_type,
				device_id: data.device_id,
				device_token: data.device_token,
			})
			.returning('*');
		// }

		//account reactivation done
		if (user.deleteRequest == 2 && user.delete_request_date != null) {
			await User.query()
				.update({ delete_request_date: null, deleteRequest: 1 })
				.where('id', user.id);
		}

		//delete password
		delete user.password;

		let responseData = {
			id: user.id,
			first_name: user.first_name,
			last_name: user.last_name,
			email: user.email,
			mobile: user.mobile,
			country_isd_code: user.country_isd_code,
			country_iso_code: user.country_iso_code,
			country_name: user.country_name,
			user_type: 'USER',
			profile_image: user.profile_image,
			gender: user.gender,
			date_of_birth: user.date_of_birth,
			user_status: user.user_status,
			is_email_verified: user.is_email_verified,
			is_mobile_verified: user.is_mobile_verified,
			stripecustomerId: user.stripecustomerId,
		};

		return okResponse(res, responseData, 'Loggedin Successfully.');
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

const registerUser_v3 = async (req, res) => {
	try {
		let data = req.body;
		data.user_type = 'USER';
		if (!data.is_email_verified) {
			data.is_email_verified = false;
		}
		let result_1, result_2;

		// console.log(data);

		/*-------------------Check Required Fields----------------------------*/
		if (!data.first_name) throw badRequestError(res, 'Enter first name.');
		if (data.first_name.length > 50) {
			throw badRequestError(
				res,
				'First Name has exceeded the maximum limit of 50 characters.'
			);
		}
		if (!data.last_name) throw badRequestError(res, 'Enter last name.');
		if (data.last_name.length > 50) {
			throw badRequestError(
				res,
				'Last Name has exceeded the maximum limit of 50 characters.'
			);
		}

		if (!data.email) throw badRequestError(res, 'Enter email.');
		if (data.email.length > 120) {
			throw badRequestError(
				res,
				'Email has exceeded the maximum limit of 120 characters.'
			);
		}

		if (!data.password) throw badRequestError(res, 'Enter password.');
		if (!data.country_isd_code)
			throw badRequestError(res, 'Enter country_isd_code.');
		// if (!data.country_name)
		// 	throw badRequestError(res, 'Enter country_name.');
		if (!data.country_iso_code)
			throw badRequestError(res, 'Enter country_iso_code.');
		if (data.country_isd_code.length > 5) {
			throw badRequestError(
				res,
				'Country ISD Code has exceeded the maximum limit of 120 characters..'
			);
		}
		if (!data.mobile) throw badRequestError(res, 'Enter mobile.');
		if (data.mobile.length > 18) {
			throw badRequestError(
				res,
				'Mobile Number has exceeded the maximum limit of 18 characters.'
			);
		}

		if (!data.device_type) throw badRequestError(res, 'Enter device type.');
		if (!data.date_of_birth) throw badRequestError(res, 'Enter date of birth.');
		if (!validator.isEmail(data.email))
			throw badRequestError(res, 'Enter a valid email id.');

		if (!validator.isLength(data.password, { min: 6, max: 32 })) {
			throw badRequestError(
				res,
				'Password needs to be between 6 to 32 characters long.'
			);
		}

		let result_11 = await User.query()
			.select('*')
			.where({ email: data.email, user_type: 'USER' })
			.andWhere({ deleteRequest: 1 })
			.first();

		let result_12 = await User.query()
			.select('*')
			.where({ email: data.email, user_type: 'USER' })
			.andWhere({ deleteRequest: 2 })
			.first();

		if (result_11) {
			result_1 = result_11;
		} else {
			result_1 = result_12;
		}

		//-------Check if Email already exist or not--------------------
		if (result_1 || result_1 != undefined) {
			throw badRequestError(res, 'User with this email id already exist.');
		}

		let result_21 = await User.query()
			.select('*')
			.where({ mobile: data.mobile, user_type: 'USER' })
			.andWhere({ deleteRequest: 1 })
			.first();

		let result_22 = await User.query()
			.select('*')
			.where({ mobile: data.mobile, user_type: 'USER' })
			.andWhere({ deleteRequest: 2 })
			.first();

		// console.log(result_1);
		// return;

		if (result_21) {
			result_2 = result_21;
		} else {
			result_2 = result_22;
		}

		//-------Check if Mobile Number already exist or not--------------------
		if (result_2 || result_2 != undefined) {
			throw badRequestError(res, 'User with this mobile no. already exist.');
		}

		/*-------------------If a valid referral_code Entered----------------------------*/
		if (data.referral_code) {
			let userWithThisReferrCodeExist = await User.query()
				.select('*')
				.where('referr_code', data.referral_code)
				.first();
			if (userWithThisReferrCodeExist == undefined)
				throw badRequestError(res, 'Referral Code is Invalid!');
		}

		//-------------Otp Generate Code------------------------------------------
		let otp = Math.floor(1000 + Math.random() * 9000);
		// let otp = 1234;
		data.otpGenerateTime = new Date().getTime();
		let otpGenerateTime = data.otpGenerateTime;

		let msg;
		if (data.device_type == 'android') {
			msg =
				'<#>' +
				' ' +
				'Your Favos verification code (OTP) is ' +
				otp +
				' ' +
				'WcZv666/V1R';
		} else {
			msg = 'Your Favos verification code (OTP) is ' + otp;
		}
		if (data.device_type == 'ios') {
			data.stripecustomerId = await stripe.GetCustomerID({
				email: data.email,
			});
		} else {
			data.stripecustomerId = await stripe.GetCustomerID({
				email: data.email,
			});
		}

		// if (data.isProduction!=undefined){
		// 	if(data.isProduction == true) {
		// 		data.stripecustomerId = await stripe.GetCustomerID({
		// 			email: data.email,
		// 		});
		// 	}else {
		// 		data.stripecustomerId = await Stripe.GetCustomerID({
		// 			email: data.email,
		// 		});	
		// 	}
		// }

		delete data.device_type;

		let smsData = {
			to: req.body.country_isd_code + req.body.mobile,
			message: msg,
		};
		sms.sendSms(smsData);

		//----Insert OTP-----
		data.otp = otp;
		//-----------Generate referral code---------------------------------------
		var length = 6,
			charset =
				'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
			referral_code = '';
		for (var i = 0, n = charset.length; i < length; ++i) {
			referral_code += charset.charAt(Math.floor(Math.random() * n));
		}
		data.referr_code = referral_code;
		let name = data.first_name; //+ ' ' + data.last_name;

		if (!data.is_email_verified) {
			let date = new Date().getTime();
			let token = crypto.randomBytes(64).toString('hex');
			data.verify_email_token = token;
			data.email_sentout_date = moment().format('YYYY-MM-DD HH:mm:ss');

			//-------------Send Mail for email verification----------------------------
			EMAIL.sendEmailToVerifyEmail(
				name,
				data.email,
				`https://favos.co/api/v1/user/verifyEmail?email=${token}&valid=${date}`
			);
		}

		let registerUser = await User.query().insert(data).returning('*');

		let responseData = {
			id: registerUser.id,
			first_name: registerUser.first_name,
			last_name: registerUser.last_name,
			email: registerUser.email,
			mobile: registerUser.mobile,
			country_isd_code: registerUser.country_isd_code,
			country_iso_code: registerUser.country_iso_code,
			country_name: registerUser.country_name,
			user_type: registerUser.user_type,
			date_of_birth: registerUser.date_of_birth,
			profile_image: registerUser.profile_image,
			gender: registerUser.gender,
			banner_image: registerUser.banner_image,
			stripecustomerId: registerUser.stripecustomerId,
		};

		return createdResponse(
			res,
			responseData,
			'Congratulations! User Registered and Otp and Email sent successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

const getephemeralKey_v3 = async (req, res) => {
	let api_version = req.body.api_version;
	let customerid = req.body.customerid;
	let device_type = req.body.device_type;
	let key;
	if (api_version == '' || customerid == '') {
		throw badRequestError(res, 'Required Parameter not found');
	}
	if (device_type) {
		if (device_type == 'ios') {
			key = await Stripe.GetephemeralKey(api_version, customerid);
		} else {
			key = await stripe.GetephemeralKey(api_version, customerid);
		}
	} else {
		key = await stripe.GetephemeralKey(api_version, customerid);
	}

	//let key = await stripe.GetephemeralKey(api_version, customerid);
	if (!key) {
		throw badRequestError(res, 'Oops, some error has occured');
	}
	return okResponse(res, key, 'Operation successfull');
};

//Exports the functions
module.exports = {
	registerUser,
	UserLogin,
	userSocialLogin,
	forgotPassword,
	getephemeralKey,
	verifyOtpController,
	resetPassword,
	UserLogout,
	resentEmailToVerify,
	resentOTPtoVerify,
	verifyEmail,
	getephemeralKey_v2,
	UserLogin_V2,
	getephemeralKey_v3,
	registerUser_v3,
};

//----------------------------------------------UserAuthController End------------------------------------------------------------------
