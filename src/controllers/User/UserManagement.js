'use strict';

/**
 * @author SHUBHAM GUPTA
 */
//----------------------------------------------UserManagement Controller Start------------------------------------------------------------------

//Import Dependency 3rd Party Modules
const validator = require('validator');
const bcryptjs = require('bcryptjs');
var fetchUrl = require("fetch").fetchUrl;
const EMAIL = require('../../middlewares/email');
const sms = require('../../middlewares/sms');
const moment = require('moment');
const Cart = require('../../models/Cart');
const CartProductModifierDetail = require('../../models/CartProductModifierDetail');

//Import Models Here
const User = require('../../models/User');
const AuthTable = require('../../models/AuthTable');
const Place = require('../../models/PlaceAdded');
const NonEaseRestaurants = require('../../models/NonEaseRestaurants');
const Feedback = require('../../models/Feedback');
const Review = require('../../models/ReviewAndRating');
let knexConfig = require('../../../db/knexfile');
const Knex = require('knex')(knexConfig['development']);
const Notification = require('../../models/Notification');
const ContactUs = require('../../models/ContactUs');

//========================================================================================

/**
 * Get User Details By Id
 * @description: function is used to Get User Details By Id
 */
const userProfileDetails = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let getUserInfo = await User.query()
			.select(
				'first_name',
				'last_name',
				'email',
				'mobile',
				'gender',
				'country_name',
				'country_isd_code',
				'country_iso_code',
				'date_of_birth',
				'profile_image',
				'banner_image'
			)
			.eager('[user_address_relation]')
			.where('id', userId)
			.first();

		if (!getUserInfo) {
			throw notFoundError('User Data Not Found!!');
		} else {
			return okResponse(res, getUserInfo, 'User Data Get Successfully!!');
		}
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Update User Profile
 * @description: function is used to update user profile
 */
const updateUserProfile = async (req, res) => {
	try {
		let file = req.files;
		let data = req.body;

		if (file.profile_image == undefined) {
			let result = await User.query()
				.select('profile_image')
				.where('id', req.user.user_id)
				.first();
			data.profile_image = result.profile_image;
		} else {
			if (
				file.profile_image[0].mimetype != 'image/jpeg' &&
				file.profile_image[0].mimetype != 'image/jpg' &&
				file.profile_image[0].mimetype != 'image/png'
			) {
				throw badRequestError(
					res,
					`Profile Image should be type of JPEG, JPG or PNG.`
				);
			}

			/*----------Check image size should be less than 1MB-----------------*/
			if (file.profile_image[0].size >= 1000000)
				throw badRequestError(res, `Image size should not exceed 1MB.`);

			data.profile_image = file.profile_image[0].location;
		}

		if (file.banner_image == undefined) {
			let result = await User.query()
				.select('banner_image')
				.where('id', req.user.user_id)
				.first();
			data.banner_image = result.banner_image;
		} else {
			if (
				file.banner_image[0].mimetype != 'image/jpeg' &&
				file.banner_image[0].mimetype != 'image/jpg' &&
				file.banner_image[0].mimetype != 'image/png'
			) {
				throw badRequestError(
					res,
					`Banner Image should be type of JPEG, JPG or PNG`
				);
			}

			/*----------Check image size should be less than 1MB-----------------*/
			if (file.banner_image[0].size >= 1000000)
				throw badRequestError(res, `Image size should not exceed 1MB.`);

			data.banner_image = file.banner_image[0].location;
		}

		let userId = req.user.user_id;

		if (data.first_name) {
			if (data.first_name.length > 50) {
				throw badRequestError(
					res,
					'First Name has exceeded the maximum limit of 50 characters.'
				);
			}
		}
		if (data.last_name) {
			if (data.last_name.length > 50) {
				throw badRequestError(
					res,
					'Last Name has exceeded the maximum limit of 50 characters.'
				);
			}
		}

		// if (!data.first_name) throw badRequestError(res, "Please enter your first name.");
		// if (!data.last_name) throw badRequestError(res, "Please enter your last name.");
		// if (!data.email) throw badRequestError(res, "Please enter your email.");
		// if (!data.country_isd_code) throw badRequestError(res, "Please enter your country isd code.");
		// if (!data.country_iso_code) throw badRequestError(res, "Please enter your country iso code.");
		// if (!data.country_name) throw badRequestError(res, "Please enter your country name.");
		// if (!data.gender) throw badRequestError(res, "Please enter your gender.");
		// if (!data.date_of_birth) throw badRequestError(res, "Please enter your date of birth.");
		// if (!data.mobile) throw badRequestError(res, "Please enter your mobile number")

		if (data.email) {
			if (!validator.isEmail(data.email)) throw badRequestError(res, "please Enter a valid email id.");

			if (data.email.length > 120) {
				throw badRequestError(res, "Email has exceeded the maximum limit of 120 characters.");
			}
			let result_1 = await User.query()
				.select('*')
				.where({ email: data.email, user_type: 'USER' })
				.whereNot({ id: userId, deleteRequest: 3 })
				.first();

			if (result_1 || result_1 != undefined) {
				throw badRequestError(res, 'User With This EmailId Already Exist.');
			}
		}

		if (data.mobile) {
			if (data.mobile.length > 18) {
				throw badRequestError(res, "Mobile Number has exceeded the maximum limit of 18 characters.");
			}
			let result_2 = await User.query()
				.select('*')
				.where({ mobile: data.mobile, user_type: 'USER' })
				.whereNot({ id: userId, deleteRequest: 3 })
				.first();

			if (result_2 || result_2 != undefined) {
				throw badRequestError(res, 'User with this mobile no. already exist.');
			}
		}

		data.id = req.user.user_id;
		let updateUserInfo = await User.query()
			.upsertGraph(data)
			.first()
			.returning('*');

		delete updateUserInfo.password;
		
		return okResponse(
			res,
			updateUserInfo,
			'Congratulations! User Profile Updated Successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Delete User Account
 * @description: function is used to Delete User Account
 */
const deleteUserAccount = async (req, res) => {
	try {
		// if (!req.body.account_delete_reason) throw badRequestError(res, "account_delete_reason required.")

		let userId = req.user.user_id;
		let deleted_datetime = moment().format('YYYY-MM-DD HH:mm:ss');

		let delete_auth = await AuthTable.query().delete().where('user_id', userId);
		let getUser = await User.query()
			.update({
				account_delete_reason: req.body.account_delete_reason,
				delete_request_date: deleted_datetime,
				deleteRequest: 2,
			})
			.where('id', userId);

		if (!getUser) {
			throw notFoundError('User Data Not Found!!');
		}
		return okResponse(
			res,
			{ deleted: getUser },
			'User Account deleted successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Change User's Password
 * @description - function is used to Change User's Password
 */
const changeUserPassword = async (req, res) => {
	try {
		if (!req.body.oldPassword) {
			return badRequestError(res, 'Enter old password.');
		}
		if (!req.body.newPassword) {
			return badRequestError(res, 'Enter new password.');
		}
		if (!req.body.confirmPassword) {
			return badRequestError(res, 'Please enter confirm password.');
		}

		if (!validator.isLength(req.body.newPassword, { min: 6, max: 32 })) {
			throw badRequestError(
				res,
				'New Password needs to be between 6 - 32 characters long.'
			);
		}

		if (!validator.isLength(req.body.confirmPassword, { min: 6, max: 32 })) {
			throw badRequestError(
				res,
				'Confirm Password needs to be between 6 - 32 characters long.'
			);
		}

		if (req.body.newPassword != req.body.confirmPassword) {
			throw badRequestError(res, 'New and Confirm passwords do not match.');
		}

		let user = await User.query().where('id', req.user.user_id).first();

		const checkPassword = await user.comparePassword(req.body.oldPassword);
		console.log(checkPassword);

		if (checkPassword) {
			const newPass = await bcryptjs.hash(req.body.newPassword, 10);
			let changePassword = await User.query()
				.patch({
					password: newPass,
				})
				.findById(req.user.user_id);
			return okResponse(
				res,
				{ changePassword },
				'Password updated successfully.'
			);
		} else {
			throw badRequestError(res, 'Old password did not match');
		}
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Change Notification Status ON / OFF
 * @description - function is used to Change Notification Status ON / OFF
 */
const updateNotificationStatus = async (req, res) => {
	try {
		let updateNotification = await User.query()
			.patch({ notification_toggle: req.body.notification_status })
			.where('id', req.user.user_id);

		return okResponse(
			res,
			{ updateNotification },
			'Notification status updated successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to update mobile number
 */
const updateUserMobile = async (req, res) => {
	try {
		let data = req.body;
		if (!data.country_isd_code)
			throw badRequestError(res, 'Enter country code.');
		if (data.country_isd_code.length > 5) {
			throw badRequestError(
				res,
				'Country ISD Code has exceeded the maximum limit of 120 characters..'
			);
		}

		if (!data.mobile) throw badRequestError(res, 'Enter mobile no.');
		if (data.mobile.length > 18) {
			throw badRequestError(
				res,
				'Mobile Number has exceeded the maximum limit of 18 characters.'
			);
		}

		let mobileAlreadyExist1 = await User.query()
			.select('*')
			.where({
				country_isd_code: data.country_isd_code,
				mobile: data.mobile,
				user_type: 'USER',
			})
			.andWhere({ deleteRequest: 1 })
			.whereNot({ id: req.user.user_id })
			.first();

		let mobileAlreadyExist2 = await User.query()
			.select('*')
			.where({
				country_isd_code: data.country_isd_code,
				mobile: data.mobile,
				user_type: 'USER',
			})
			.andWhere({ deleteRequest: 2 })
			.whereNot({ id: req.user.user_id })
			.first();

		let mobileAlreadyExist;
		if (mobileAlreadyExist1) {
			mobileAlreadyExist = mobileAlreadyExist1;
		} else {
			mobileAlreadyExist = mobileAlreadyExist2;
		}

		console.log(mobileAlreadyExist);

		if (mobileAlreadyExist) {
			throw badRequestError(
				res,
				"Mobile Number already exist. It can't be update."
			);
		}

		let sendExistOtp = await User.query()
			.select('*')
			.where({ id: req.user.user_id })
			.returning('*')
			.first();

		let sentOTP, otp, responseData;
		if (sendExistOtp.otp == null && (sendExistOtp.otpGenerateTime == null || sendExistOtp.otpGenerateTime == '')) {
			//-------------Otp Generate Code------------------------------------------
			otp = Math.floor(1000 + Math.random() * 9000);
			// otp = 1234;
			let otpGenerateTime = new Date().getTime();

			//----Insert OTP-----
			sentOTP = await User.query()
				.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where({ id: req.user.user_id })
				.returning('*')
				.first();

			responseData = {
				id: sentOTP.id,
				first_name: sentOTP.first_name,
				last_name: sentOTP.last_name,
				email: sentOTP.email,
				country_isd_code: sentOTP.country_isd_code,
				mobile: sentOTP.mobile,
			};
		} else {

			let otpGeneratedTime = parseInt(sendExistOtp.otpGenerateTime);
			let todayDateTime = new Date().getTime();
			/*-------- If otpGeneratedTime is less than 15 minutes OTP is VALID -------*/
			/*-------- If otpGeneratedTime is greater than 15 minutes OTP is EXPIRED -------*/
			let difference = (todayDateTime - otpGeneratedTime) / 1000;
			let diff = difference / 60;
			diff = Math.abs(Math.round(diff));
			//---------------------------- If difference is above 15 minutes, then OTP expires ----------------------------//
			if (diff > 15) {
				otp = Math.floor(1000 + Math.random() * 9000);
				let otpGenerateTime = new Date().getTime();
				//----Insert OTP-----
				sentOTP = await User.query()
					.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
					.where({ id: req.user.user_id })
					.returning('*')
					.first();
				
				responseData = {
					id: sentOTP.id,
					first_name: sentOTP.first_name,
					last_name: sentOTP.last_name,
					email: sentOTP.email,
					country_isd_code: sentOTP.country_isd_code,
					mobile: sentOTP.mobile,
				};

			}else{
				//------send exist otp----------------
				otp = sendExistOtp.otp;
				responseData = {
					id: sendExistOtp.id,
					first_name: sendExistOtp.first_name,
					last_name: sendExistOtp.last_name,
					email: sendExistOtp.email,
					country_isd_code: sendExistOtp.country_isd_code,
					mobile: sendExistOtp.mobile,
				};
			}
		}

		let msg;
		if (req.user.device_type == 'android') {
			msg =
				'<#>' +
				' ' +
				'Your Favos verification code (OTP) is ' +
				otp +
				' ' +
				'fYc56cjQ9HY';
		} else {
			msg = 'Your Favos verification code (OTP) is ' + otp;
		}

		let smsData = {
			to: req.body.country_isd_code + req.body.mobile,
			message: msg,
		};
		sms.sendSms(smsData);

		return okResponse(
			res,
			responseData,
			'OTP (Verification Code) sent successfully. Please Verify your mobile number.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to update email
 */
const updateUserEmail = async (req, res) => {
	try {
		let emailId = req.body.email;

		if (!validator.isEmail(emailId)) {
			throw badRequestError(res, 'Enter a valid email id.');
		}

		if (emailId.length > 120) {
			throw badRequestError(
				res,
				'Email has exceeded the maximum limit of 120 characters.'
			);
		}

		let emailAlreadyExist1 = await User.query()
			.select('*')
			.where({ email: emailId, user_type: 'USER' })
			.andWhere({ deleteRequest: 1 })
			.whereNot({ id: req.user.user_id })
			.first();

		let emailAlreadyExist2 = await User.query()
			.select('*')
			.where({ email: emailId, user_type: 'USER' })
			.andWhere({ deleteRequest: 2 })
			.whereNot({ id: req.user.user_id })
			.first();

		let emailAlreadyExist;
		if (emailAlreadyExist1) {
			emailAlreadyExist = emailAlreadyExist1;
		} else {
			emailAlreadyExist = emailAlreadyExist2;
		}

		if (emailAlreadyExist) {
			throw badRequestError(res, 'This email id already exist.');
		}

		let name = req.user.auth_user_relation.first_name //+ ' ' + req.user.auth_user_relation.last_name;

		let sendExistOtp = await User.query()
			.select('*')
			.where({ id: req.user.user_id })
			.returning('*')
			.first();

		let sentOTP, otp, responseData;
		if ( sendExistOtp.otp == null && (sendExistOtp.otpGenerateTime == null || sendExistOtp.otpGenerateTime == '')) {
			//-------------Otp Generate Code------------------------------------------
			otp = Math.floor(1000 + Math.random() * 9000);
			// otp = 1234;
			let otpGenerateTime = new Date().getTime();

			//----Insert OTP-----
			sentOTP = await User.query()
				.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
				.where({ id: req.user.user_id })
				.returning('*')
				.first();

			responseData = {
				id: sentOTP.id,
				first_name: sentOTP.first_name,
				last_name: sentOTP.last_name,
				email: sentOTP.email,
				country_isd_code: sentOTP.country_isd_code,
				mobile: sentOTP.mobile,
			};
		} else {

			let otpGeneratedTime = parseInt(sendExistOtp.otpGenerateTime);
			let todayDateTime = new Date().getTime();
			/*-------- If otpGeneratedTime is less than 15 minutes OTP is VALID -------*/
			/*-------- If otpGeneratedTime is greater than 15 minutes OTP is EXPIRED -------*/
			let difference = (todayDateTime - otpGeneratedTime) / 1000;
			let diff = difference / 60;
			diff = Math.abs(Math.round(diff));
			//---------------------------- If difference is above 15 minutes, then OTP expires ----------------------------//
			if (diff > 15) {
				otp = Math.floor(1000 + Math.random() * 9000);
				let otpGenerateTime = new Date().getTime();
				//----Insert OTP-----
				sentOTP = await User.query()
					.patch({ otp: otp, otpGenerateTime: otpGenerateTime })
					.where({ id: req.user.user_id })
					.returning('*')
					.first();
				
				responseData = {
					id: sentOTP.id,
					first_name: sentOTP.first_name,
					last_name: sentOTP.last_name,
					email: sentOTP.email,
					country_isd_code: sentOTP.country_isd_code,
					mobile: sentOTP.mobile,
				};

			}else{
				//------send exist otp----------------
				otp = sendExistOtp.otp;
				responseData = {
					id: sendExistOtp.id,
					first_name: sendExistOtp.first_name,
					last_name: sendExistOtp.last_name,
					email: sendExistOtp.email,
					country_isd_code: sendExistOtp.country_isd_code,
					mobile: sendExistOtp.mobile,
				};
			}
		}
		// else {
		// 	//------send exist otp----------------
		// 	otp = sendExistOtp.otp;

		// 	responseData = {
		// 		id: sendExistOtp.id,
		// 		first_name: sendExistOtp.first_name,
		// 		last_name: sendExistOtp.last_name,
		// 		email: sendExistOtp.email,
		// 		country_isd_code: sendExistOtp.country_isd_code,
		// 		mobile: sendExistOtp.mobile,
		// 	};
		// }

		//----------Send mail for OTP---------------------
		EMAIL.sendOtpByEmail(name, emailId, otp);
		//------------------------------------------------

		return okResponse(
			res,
			responseData,
			'OTP (Verification Code) sent successfully. Please Verify your email id.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================================ Place Added Or Requested Module APIS (START) ================================================================//

/**
 * @description: function is used to search non-ease restaurants
 */
const searchNonFavosMerchants = async (req, res) => {
	try {
		let search = req.query.search;

		let searchResult = await User.query()
			.select('id', 'restaurant_name')
			.where('membership', false)
			.where((builder) => {
				if (req.query.search != '') {
					return builder.whereRaw(
						"LOWER(restaurant_name) LIKE '%' || LOWER(?) || '%'",
						search
					);
				}
			});

		return okResponse(
			res,
			searchResult,
			'Non Favos Merchant search successful.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to register bussiness
 */
const registerBussiness = async (req, res) => {
	try {
		let data = req.body;
		let docs = [],
			image;
		let uploads = req.files;

		let pdf_files = uploads.document;

		if (uploads.restaurant_image == undefined) {
			data.restaurant_image = null;
		} else {
			image = uploads.restaurant_image[0];
			/*-------------------------Check image type---------------------------------*/
			if (
				image.mimetype != 'image/jpeg' &&
				image.mimetype != 'image/jpg' &&
				image.mimetype != 'image/png'
			) {
				throw badRequestError(
					res,
					`Merchant image should be type of JPEG, JPG or PNG`
				);
			}

			/*----------Check image size should be less than 500kb-----------------*/
			if (image.size >= 500000)
				throw badRequestError(res, `Image size should not exceed 500 kb.`);

			data.restaurant_image = image.location;
		}

		if (uploads.document == undefined) {
			data.ownership_documents = [];
		} else {
			/*-------------------------Check Docs Can't be upload more than 5---------------------------------*/
			if (pdf_files.length > 5)
				throw badRequestError(res, "You can't upload more than 5 documents.");

			for (let i = 0; i < pdf_files.length; i++) {
				docs.push({ documents: pdf_files[i].location });
			}
			data.ownership_documents = docs;
		}

		if (!data.bussiness_relation)
			throw badRequestError(
				res,
				'Please Select your relationship with business.'
			);
		if (!data.bussiness_name)
			throw badRequestError(res, 'Enter business name.');
		if (!data.restaurant_address)
			throw badRequestError(res, 'Enter merchant address.');
		if (!data.lat)
			throw badRequestError(res, 'Provide latitude of the address.');
		if (!data.lng) throw badRequestError(res, 'Provide longitude of address.');
		if (!data.restaurant_mobile)
			throw badRequestError(res, 'Enter merchant contact number.');

		//----------Register a business---------------------------
		data.business_request = 'Registered';
		data.user_id = req.user.user_id;
		data.request_approval = 'PENDING';

		let registerPlace = await Place.query().insertGraph(data).returning('*');

		if (!registerPlace) throw badRequestError(res, 'Something Went Wrong!');

		return okResponse(
			res,
			registerPlace,
			'Register place request sent to Favos Successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to request bussiness
 */
const requestBusiness = async (req, res) => {
	try {
		let data = req.body;

		//--------------------Request Business to Become FAVOS Partner--------------------
		if (!data.bussiness_request_id)
			throw badRequestError(res, 'Please enter bussiness_request_id');
		if (!data.bussiness_name)
			throw badRequestError(res, 'Enter the merchant name');

		data.business_request = 'Requested';
		data.user_id = req.user.user_id;
		data.request_approval = 'PENDING';

		let requestExist = await Place.query()
			.select('id', 'bussiness_name', 'user_id')
			.where({
				bussiness_request_id: data.bussiness_request_id,
				request_approval: 'PENDING',
				user_id: req.user.user_id,
			})
			.first();

		if (requestExist) throw badRequestError(res, 'Request already Submitted!');

		let requestPlace = await Place.query().insert(data).returning('*');
		if (!requestPlace) throw badRequestError(res, 'Something Went Wrong!');

		return okResponse(
			res,
			requestPlace,
			'Partner with Favos request is sent to Merchant successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get all register bussiness
 */
const AllregisteredBusiness = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;

		let AllregisterBusiness = await Place.query()
			.select('*')
			.where({ user_id: req.user.user_id, business_request: 'Registered' })
			.eager('ownership_documents')
			.where((builder) => {
				if (req.query.createdAt != '') {
					return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy('created_at', 'desc')
			.limit(limit)
			.returning('*');
		if (!AllregisterBusiness)
			throw badRequestError(res, 'Something Went Wrong!');
		return okResponse(
			res,
			AllregisterBusiness,
			'All Registered Places fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get all request of businesses
 */
const AllrequestedBusiness = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;

		let AllrequestBusiness = await Place.query()
			.select('*')
			.where({ user_id: req.user.user_id, business_request: 'Requested' })
			.where((builder) => {
				if (req.query.createdAt != '') {
					return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy('created_at', 'desc')
			.limit(limit)
			.returning('*');

		if (!AllrequestBusiness)
			throw badRequestError(res, 'Something Went Wrong!');
		return okResponse(
			res,
			AllrequestBusiness,
			'All Requested Places fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================================ Place Added Or Requested Module APIS (END) ================================================================//

//================================================================ Feedback Module APIS (START) ================================================================//

/**
 * @description: function is used to give feedback
 */
const CreateFeedback = async (req, res) => {
	try {
		let data = req.body;
		data.user_id = req.user.user_id;

		if (!data.subject) throw badRequestError(res, 'Please enter subject!');
		if (!data.description)
			throw badRequestError(res, 'Please enter description!');

		let insertFeedbck = await Feedback.query().insert(data).returning('*');

		return createdResponse(
			res,
			insertFeedbck,
			'Congratulations! Your feedback submitted successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get all feedbacks for Merchant
 */
const AllFeedbackForMerchant = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;

		let AllFeedbacks = await Feedback.query()
			.select('*')
			.eager('feedback_merchant_relation')
			.modifyEager('feedback_merchant_relation', (builder) => {
				return builder.select('id', 'restaurant_name');
			})
			.where({ user_id: req.user.user_id, to: 'merchant' })
			.where((builder) => {
				if (req.query.createdAt != '') {
					return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy('created_at', 'desc')
			.limit(limit)
			.returning('*');

		return okResponse(
			res,
			AllFeedbacks,
			'All FeedbackForMerchant Get Successfully!.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get all feedbacks for Merchant
 */
const AllFeedbackForFavos = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;

		let AllFeedbacks = await Feedback.query()
			.select('*')
			.where({ user_id: req.user.user_id, to: 'favos' })
			.eager('feedback_merchant_relation')
			.modifyEager('feedback_merchant_relation', (builder) => {
				return builder.select('id', 'restaurant_name');
			})
			.where((builder) => {
				if (req.query.createdAt != '') {
					return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy('created_at', 'desc')
			.limit(limit)
			.returning('*');

		return okResponse(
			res,
			AllFeedbacks,
			'All FeedbackForFavos Get Successfully!.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to deletel feedbacks for Merchant
 */
const deleteFeedback = async (req, res) => {
	try {
		if (!req.query.feedbackId)
			throw badRequestError(res, 'Please pass feedbackId in to query string!');

		let result = await Feedback.query()
			.select('*')
			.where('id', req.query.feedbackId)
			.first()
			.returning('*');
		if (result == undefined)
			throw badRequestError(res, "Feedback with this id doesn't exist!");

		let deleteFeedback = await Feedback.query()
			.deleteById(req.query.feedbackId)
			.returning('*');

		return okResponse(res, deleteFeedback, 'Feedback deleted successfully!.');
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================================ Feedback Module APIS (END) ================================================================//

//================================================================ Review And Rating Module APIS (START) ================================================================//

/**
 * @description: function is used to give review and rating
 */
const ReviewAndRating = async (req, res) => {
	try {
		let data = req.body;
		let image = req.file;

		//-------Check If Image Uploaded then Value sets otherWise it store value as NULL------------
		if (image == undefined) {
			data.review_image = null;
		} else {
			if (
				image.mimetype != 'image/jpeg' &&
				image.mimetype != 'image/jpg' &&
				image.mimetype != 'image/png'
			) {
				throw badRequestError(
					res,
					`Review Image should be type of 'JPEG', 'JPG' or 'PNG`
				);
			}
			/*----------Check image size should be less than 500kb-----------------*/
			if (image.size > 50000)
				throw badRequestError(res, `Image size should not exceed 500 kb.`);

			data.review_image = image.location;
		}

		// console.log(data)

		if (!data.merchant) throw badRequestError(res, 'Please enter merchant!');
		if (!data.review) throw badRequestError(res, 'Please enter reviews!');

		data.user_id = req.user.user_id;
		let insertReview = await Review.query().insert(data).returning('*');

		return createdResponse(
			res,
			insertReview,
			'Congratulations! Your review submitted successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get review and rating
 */
const GetReviewAndRating = async (req, res) => {
	try {
		if (!req.query.review_id)
			throw badRequestError(res, 'Please pass review_id in to query string!');

		let getReview = await Review.query()
			.select('*')
			.where('id', req.query.review_id)
			.first()
			.returning('*');

		if (getReview == undefined)
			throw badRequestError(res, "Review with this id doesn't exist!");

		return okResponse(
			res,
			getReview,
			'Congratulations! Your review get successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to update review and rating
 */
const UpdateReviewAndRating = async (req, res) => {
	try {
		let review_id = req.query.review_id;
		let data = req.body;
		let image = req.file;

		//-------Check If Image Uploaded then Value sets otherWise it store value as NULL------------
		if (image == undefined) {
			let result = Review.query()
				.select('review_image')
				.where('id', review_id)
				.first();
			data.review_image = result.review_image;
		} else {
			if (
				image.mimetype != 'image/jpeg' &&
				image.mimetype != 'image/jpg' &&
				image.mimetype != 'image/png'
			) {
				throw badRequestError(
					res,
					`Review Image should be type of 'JPEG', 'JPG' or 'PNG`
				);
			}
			/*----------Check image size should be less than 500kb-----------------*/
			if (image.size > 500000)
				throw badRequestError(res, `Image size should not exceed 500 kb.`);

			data.review_image = image.location;
		}

		if (!review_id)
			throw badRequestError(res, 'Please pass review_id in queruy string!');

		if (!data.merchant) throw badRequestError(res, 'Please enter merchant!');
		if (!data.review) throw badRequestError(res, 'Please enter reviews!');

		data.user_id = req.user.user_id;
		let updateReview = await Review.query()
			.update(data)
			.where('id', review_id)
			.first()
			.returning('*');

		return okResponse(
			res,
			updateReview,
			'Congratulations! Your review updated successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to delete review and rating
 */
const DeleteReviewAndRating = async (req, res) => {
	try {
		let review_id = req.query.review_id;

		if (!review_id)
			throw badRequestError(res, 'Please pass review_id into query string!');

		let result = await Review.query()
			.select('*')
			.where('id', review_id)
			.first()
			.returning('*');
		if (result == undefined)
			throw badRequestError(res, "Review with this id doesn't exist!");

		let deleteReview = await Review.query()
			.deleteById(review_id)
			.returning('*');

		return okResponse(res, deleteReview, 'Review Deleted Successfully!');
	} catch (error) {
		console.log(error);
		badRequestError(res, error);
	}
};

/**
 * @description: function is used to get all reviews for MERCHANT
 */
const AllReviewsForMerchant = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;

		let allReviews = await Review.query()
			.select('*')
			.eager('[review_merchant_relation]')
			.modifyEager('review_merchant_relation', (builder) => {
				return builder.select('id', 'restaurant_name');
			})
			.where({ user_id: req.user.user_id, item_id: null })
			.where((builder) => {
				if (req.query.createdAt != '') {
					return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy('created_at', 'desc')
			.limit(limit)
			.returning('*');

		return okResponse(
			res,
			allReviews,
			'All Reviews for Merchant get successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get all reviews for MERCHANT
 */
const AllReviewsForItem = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;

		let allReviews = await Review.query()
			.select('*')
			.eager('[review_merchant_relation]')
			.modifyEager('review_merchant_relation', (builder) => {
				return builder.select('id', 'restaurant_name');
			})
			.where({ user_id: req.user.user_id })
			.whereNot('item_id', null)
			.where((builder) => {
				if (req.query.createdAt != '') {
					return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy('created_at', 'desc')
			.limit(limit)
			.returning('*');

		return okResponse(
			res,
			allReviews,
			'All Reviews for Item get successfully!'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//================================================================ Review And Rating Module APIS (END) ================================================================//

/**
 * @description: function is used to get all notification of user
 */
const AllNotification = async (req, res) => {
	try {
		let limit = req.query.limit ? req.query.limit : 10;
		// let page = req.query.page ? req.query.page : 1;
		// let limit = 10;
		// let offset = (page - 1) * limit;

		// console.log(req.query.createdAt);
		let allNotifications = await Notification.query()
			.select(
				"*",
				Knex.raw(
				'to_char("notification"."created_at", \'YYYY-MM-DD HH24:MI:SS\') as created_at'
				)
			)
			.eager("[order_notification]")
			.modifyEager("order_notification", (builder) => {
				return builder
				.select(
					"id",
					"order_number",
					"merchant_id",
					"order_type_id",
					"user_id",
					"created_at"
				)
				.eager("orders_of_merchant")
				.modifyEager("orders_of_merchant", (builder) => {
					return builder.select("id", "restaurant_name");
				});
			})
			.where({ user_id: req.user.user_id })
			.where((builder) => {
				if (req.query.createdAt != "") {
				return builder.where('created_at', '<', req.query.createdAt);
				}
			})
			.orderBy("notification.created_at", "desc")
			// .offset(offset)
			.limit(limit)
			.returning("*");

		return okResponse(
			res,
			allNotifications,
			'All notifications fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * @description: function is used to get notification count and cart items count
 */
const NotificationCount = async (req, res) => {
	try {
		let allNotifications = await Notification.query()
			.count('id as notificationCount')
			.where({ user_id: req.user.user_id, read_status: false })
			.first();

		let cart_data = await Cart.query()
			.select(
				'id',
				'merchant_id',
				'quantity',
				Knex.raw(
					'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: req.user.user_id });
		var totalsum = 0;
		var sum = 0;
		var totalquantity = 0;
		for (var i = 0; i < cart_data.length; i++) {
			totalquantity = totalquantity + cart_data[i].quantity;
			sum = parseFloat(cart_data[i].sub_total);
			totalsum = totalsum + sum;
		}

		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', req.user.user_id)
			.innerJoinRelation('cart_modifier_products_detail_relation');

		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}
		return okResponse(
			res,
			{
				allNotifications,
				total_amount: totalsum + totalmodifiersum,
				totalquantity,
			},
			'Notifications fetched successfully.'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

/**
 * Add Contact Form Details
 */
const AddContactUsDetails = async (req, res) => {
	try {
		// ----------JSON Input for this Api
		// email: ""
		// how_can_help: ""
		// merchant_name: ""
		// message: ""
		// name: ""
		// phone: ""
		// user_type: ""

		let data = req.body;
		console.log(data, req.connection.remoteAddress)

		if (!data.email) throw badRequestError(res, "Please enter your email.");
		if (data.email.length > 120) {
			throw badRequestError(res,"Email has exceeded the maximum limit of 120 characters.");
		}
		if (data.name.length > 255) {
			throw badRequestError(res, "Name has exceeded the maximum limit of 255 characters.");
		}
		if (data.message.length > 1000) {
			throw badRequestError(res, "Message has exceeded the maximum limit of 1000 characters.");
		}

		// g-recaptcha-response is the key that browser will generate upon form submit.
		// if its blank or null means user has not selected the captcha, so return the error.
		if (req.body.recaptcha === undefined || req.body.recaptcha === "" || req.body.recaptcha === null) {
			throw badRequestError(res, "Please select captcha.");
		}
		// Put your secret key here.
		const secretKey = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
		const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body.recaptcha + "&remoteip=" + req.connection.remoteAddress;
		
		fetchUrl(verificationURL, async function (error, meta, body) {
			body = JSON.parse(body);
			console.log(body);
			if (body.success !== undefined && !body.success) {
				throw badRequestError(res, "Failed captcha verification");
			}
			console.log("recaptcha Verified")

			delete data.recaptcha;
			if (data.user_type == "CUSTOMER") {
				let checkUserEmailExist = await User.query()
					.select("id", "email", "deleteRequest", "user_type", "user_status")
					.whereNot({ deleteRequest: 3 })
					.andWhere({ user_type: "USER", user_status: true })
					.first();

				if (checkUserEmailExist != undefined) {
					data.user_id = checkUserEmailExist.id;
				}

				EMAIL.sendContactUsDetailToCustomer(data.name, data.email);
			} else if (data.user_type == "MERCHANT") {
				let checkMerchantEmailExist = await User.query()
					.select("id", "email", "user_type", "user_status")
					.where({ user_type: "MERCHANT", user_status: true })
					.first();

				if (checkMerchantEmailExist != undefined) {
					data.user_id = checkMerchantEmailExist.id;
				}

				EMAIL.sendContactUsDetailToMerchant(data.name, data.email);
			}

			let addContactUsDetails = await ContactUs.query()
			.insert(data)
			.returning("*");

			return okResponse(res, addContactUsDetails, "Contact form details submitted successfully.");
	});
	
  } catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//Exports the functions
module.exports = {
	userProfileDetails,
	updateUserProfile,
	deleteUserAccount,
	changeUserPassword,
	updateNotificationStatus,
	updateUserMobile,
	updateUserEmail,
	searchNonFavosMerchants,
	registerBussiness,
	requestBusiness,
	AllregisteredBusiness,
	AllrequestedBusiness,
	CreateFeedback,
	AllFeedbackForMerchant,
	AllFeedbackForFavos,
	deleteFeedback,
	ReviewAndRating,
	GetReviewAndRating,
	UpdateReviewAndRating,
	DeleteReviewAndRating,
	AllReviewsForMerchant,
	AllReviewsForItem,
	AllNotification,
	NotificationCount,
	AddContactUsDetails,
};

//----------------------------------------------UserManagement Controller End------------------------------------------------------------------
