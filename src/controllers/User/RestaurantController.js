'use strict';
const validator = require('validator');
const moment = require('moment');

const order = require('../../models/Orders');
var lodash = require('lodash');
const stripe = require('./../../middlewares/stripe');
const Stripe = require('./../../middlewares/StripeIOS');

//Import Models Here
const Distance = require('../../middlewares/distance');
const DeliveryCharges = require('../../models/DeliveryCharges');
const User = require('../../models/User');
const Menu = require('../../models/MenuManagement');
const Product = require('../../models/Product');
const OrderedProducts = require('../../models/OrderedProducts');
const Cart = require('../../models/Cart');
const Category = require('../../models/Categories');
const CartProductModifier = require('../../models/CartProductModifier');
const CartProductModifierDetail = require('../../models/CartProductModifierDetail');
const ProductMenu = require('../../models/ProductMenu');
const ProductCategory = require('../../models/ProductCategory');
const ProductModifiers = require('../../models/ProductModifiers');
const AuthTable = require('../../models/AuthTable');
const Place = require('../../models/PlaceAdded');
const NonEaseRestaurants = require('../../models/NonEaseRestaurants');
const Feedback = require('../../models/Feedback');
const Review = require('../../models/ReviewAndRating');
let knexConfig = require('../../../db/knexfile');
const { transaction, ref } = require('objection');
const Knex = require('knex')(knexConfig['development']);
const _ = require('lodash');
const Modifier = require('../../models/Modifier');
const ModifierProductDetails = require('../../models/ModifierProductDetails');
const MerchantCharacteristic = require('../../models/Merchant_Characteristics');
const Notification = require('../../models/Notification');
const ModifiersProduct = require('../../models/ModifierProductDetails');
const OrderType = require('../../models/Order_Type_Management');
const OrderStatusFlowManagement = require('../../models/OrderStatusManagement');
const Table = require('../../models/Table');

var Buffer = require('buffer').Buffer;
const objCodec = require('object-encode');
const { update } = require('lodash');
const { KMS } = require('aws-sdk');
const salt = ')*myNewAWESOME-salt254@%^&%';
const EMAIL = require('../../middlewares/email');

//================Restaurant list start=================
const restaurantListWithFilter = async (req, res) => {
	try {
		let body = req.body;

		if (_.isEmpty(body)) {
			throw badRequestError(res, 'Required parameter not found!');
		}
		let page = body.page ? body.page : 1;
		let limit = 10;
		let offset = (page - 1) * 10;
		let err, restaurantDetail;
		//-------Check if Email already exist or not--------------------
		let query = User.query()
			.select(
				'users.id',
				'restaurant_name',
				'restaurant_website',
				'mobile',
				'profile_image',
				'banner_image',
				'membership',
				'city_name',
				'country_name',
				'country_isd_code',
				Knex.raw(
					'acos(sin(lat * 0.0175) * sin(' +
						body.lat +
						' * 0.0175) + cos(lat * 0.0175) * cos(' +
						body.lat +
						' * 0.0175) * cos((' +
						body.lng +
						' * 0.0175) - (lng * 0.0175))) * 3959 as distance'
				),
				Knex.raw(
					'(select count("id") from "review_and_rating" where "merchant" = "users"."id") as reviewcount'
				),
				Knex.raw(
					'(select COALESCE(avg("rating"), 0) as avgRating from "review_and_rating" where "merchant" = "users"."id")'
				)
				// 'merchant_cuisine.cuisine_name'
			)
			.innerJoinRelation('merchant_cuisine')
			.where({ 'users.user_type': 'MERCHANT', 'users.user_status': true })
			.eager(
				'[user_address_relation, merchant_cuisine, merchant_characteristic_relation]'
			)
			.modifyEager('user_address_relation', (builder) => {
				return builder.select('id', 'full_address', 'lat', 'lng', 'locality');
			})
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.modifyEager('merchant_characteristic_relation', (builder) => {
				return builder.select(
					'price_for_two',
					'opening_status',
					'online_order_status'
				);
			})
			.groupBy('users.id')
			.where((builder) => {
				if (body.search)
					return builder
						.whereRaw('users.restaurant_name ilike ?', '%' + body.search + '%')
						.orWhereRaw(
							'merchant_cuisine.cuisine_name ilike ?',
							'%' + body.search + '%'
						);
			});

		if (body.city && body.type == 1) {
			query.andWhereRaw(
				"LOWER(users.city_name) LIKE '%' || LOWER(?) || '%'",
				body.city
			);
		}

		if (body.type == 2) {
			query.where(
				Knex.raw(
					'acos(sin(lat * 0.0175) * sin(' +
						body.lat +
						' * 0.0175) + cos(lat * 0.0175) * cos(' +
						body.lat +
						' * 0.0175) * cos((' +
						body.lng +
						' * 0.0175) - (lng * 0.0175))) * 3959'
				),
				'<=',
				30
			);
		}

		if (body.isNearBy) {
			query.orderBy('distance', 'ASC');
		} else {
			query.orderBy('distance', 'ASC');
			//query.orderBy('id', 'DESC');
		}

		query.offset(offset).limit(limit);
		restaurantDetail = await query;
		//console.log(restaurantDetail, 'resdetail');
		if (restaurantDetail == undefined || restaurantDetail == '') {
			throw badRequestError(res, 'Merchants list not found');
		}
		delete restaurantDetail.password;
		return okResponse(
			res,
			restaurantDetail,
			'Merchants list successfully fetched'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};
//================Restaurant list end=================

//================Restaurant detail=================
const restaurantDetail = async (req, res) => {
	try {
		let id = req.params.id;
		let err, restaurantDetail;
		let query = User.query()
			.select(
				'id',
				'restaurant_name',
				'restaurant_website',
				'mobile',
				'profile_image',
				'banner_image',
				'lat',
				'lng',
				'business_description',
				'membership',
				'country_isd_code',
				Knex.raw(
					'(select count("id") from "review_and_rating" where "merchant" = "users"."id") as reviewcount'
				),
				Knex.raw(
					'(select COALESCE(avg("rating"), 0) as avgRating from "review_and_rating" where "merchant" = "users"."id")'
				)
			)
			.where({ id: id, user_type: 'MERCHANT', user_status: true })
			.eager(
				'[user_address_relation, merchant_cuisine, merchant_amenties, merchant_characteristic_relation, merchant_order_type,delivery_details,merchants_delivery_charges]'
			)
			.modifyEager('user_address_relation', (builder) => {
				return builder.select('id', 'full_address', 'lat', 'lng');
			})
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.modifyEager('merchant_amenties', (builder) => {
				return builder.select(
					'merchant_amenties.amenties_name',
					Knex.raw(' true as flag')
				);
			})
			.modifyEager('merchant_characteristic_relation', (builder) => {
				return builder.select(
					'isAlcohol',
					'seating',
					'table',
					'food_certificate',
					'food_grade',
					'issue_date',
					'timing',
					'opening_status',
					'expiry_date'
				);
			})
			.modifyEager('delivery_details', (builder) => {
				return builder.select('delivery_note');
			})
			.modifyEager('merchant_order_type', (builder) => {
				return builder.select(
					'order_type.id as order_type_id',
					'order_type.order_type'
				);
			})
			.modifyEager('merchants_delivery_charges', (builder) => {
				return builder.select(
					'to',
					'from',
					'delivery_fee',
					'free_delivery_order',
					'minimum_order'
				);
			});
		restaurantDetail = await query.catch((err) => {});
		// console.log(restaurantDetail, 'resdetail');
		if (restaurantDetail == undefined || restaurantDetail == '') {
			throw badRequestError(res, 'Merchant detail not found');
		}

		if (restaurantDetail[0].delivery_details == null) {
			restaurantDetail[0].delivery_details = {
				delivery_note: '',
			};
		}

		//delete restaurantDetail.password;
		//Price Calculation for the Cart
		/*
		let subTotal = await Cart.query()
			.select(
				'quantity',
				Knex.raw(
					'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid")'
				)
			)
			.where({ user_id: req.user.user_id });
		var totalsum = 0,
			sum = 0;
		var totalquantity = 0;
		for (var i = 0; i < subTotal.length; i++) {
			totalquantity = totalquantity + subTotal[i].quantity;
			sum = parseFloat(subTotal[i].sum);
			totalsum = totalsum + sum;
		}
		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', req.user.user_id)
			.innerJoinRelation('cart_modifier_products_detail_relation');
		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}
		let user_details = {
			totalquantity,
			totalitems: subTotal.length,
			total_amount: +(totalsum + totalmodifiersum).toFixed(2),
		};
		*/
		// console.log(restaurantDetail[0].merchant_characteristic_relation);
		let user_details = await calculateCartPrice(req.user.user_id);

		// for (let i = 0; i < restaurantDetail[0].merchant_amenties.length; i++) {
		//   restaurantDetail[0].merchant_amenties[i].flag = true;
		// }

		if (restaurantDetail[0].merchant_characteristic_relation != undefined) {
			if (restaurantDetail[0].merchant_characteristic_relation.isAlcohol)
				restaurantDetail[0].merchant_amenties.push({
					amenties_name: 'Alcohol Served',
					flag: true,
				});
			if (restaurantDetail[0].merchant_characteristic_relation.seating)
				restaurantDetail[0].merchant_amenties.push({
					amenties_name: 'Seating Available',
					flag: true,
				});
			if (restaurantDetail[0].merchant_characteristic_relation.table == 1)
				restaurantDetail[0].merchant_amenties.push({
					amenties_name: 'Dedicated Table',
					flag: true,
				});
			if (restaurantDetail[0].merchant_characteristic_relation.table == 2)
				restaurantDetail[0].merchant_amenties.push({
					amenties_name: 'Shared Table',
					flag: true,
				});
			if (
				restaurantDetail[0].merchant_characteristic_relation.isAlcohol == false
			)
				restaurantDetail[0].merchant_amenties.push({
					amenties_name: 'No Alcohol Served ',
					flag: false,
				});
			if (restaurantDetail[0].merchant_characteristic_relation.seating == false)
				restaurantDetail[0].merchant_amenties.push({
					amenties_name: 'No Seating Available',
					flag: false,
				});
		}
		return okResponse(
			res,
			{ restaurantDetail, user_details },
			'Merchant detail successfully fetched'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};
//================Restaurant detail end=================

//================Menu List by restaurant id=================
const menuList = async (req, res) => {
	try {
		let id = +req.params.id;
		let userId = req.user.user_id;
		let err, menuList;
		let order_type_id;
		if (req.query.id == 5 || req.query.id == 6) {
			order_type_id = 1;
		} else {
			order_type_id = req.query.id;
		}

		let query = Menu.query()
			.select('menu_management.id', 'menu_name')
			.where({ merchant_id: id, status: true, delete_status: false })
			.andWhere(function () {
				this.whereRaw('"start_date"::date <= ?', [
					moment().format('YYYY-MM-DD'),
				]).orWhere('start_date', null);
			})
			.andWhere(function () {
				this.whereRaw('"end_date"::date >= ?', [
					moment().format('YYYY-MM-DD'),
				]).orWhere('end_date', null);
			})
			.innerJoinRelation('menu_order_type_relation')
			.eager('[menu_product, menu_order_type_relation]')
			.modifyEager('menu_order_type_relation', (builder) => {
				builder
					.select('order_type_id')
					.where('menu_order_type.order_type_id', order_type_id);
			})
			.modifyEager('menu_product', (builder) => {
				//return builder.select('product.id', 'product_name', 'product_image', 'product_description', 'products_per_order_limit', Knex.raw('(select count(\"id\") from \"review_and_rating\" where \"item_id\" = \"product\".\"id\") as reviewcount'), Knex.raw('(select COALESCE(avg(\"rating\"), 0) as avgRating from \"review_and_rating\" where \"item_id\" = \"product\".\"id\")'), Knex.raw('(select count(\"id\") from \"cart\" where \"product_id\" = \"product\".\"id\" AND \"user_id\" = '+userId+') as "cartId"')).where('product_menu.status', true).where('product.delete_status', false).where('product.status', 1)
				return (
					builder
						.select('product.id')
						.where('product_menu.status', true)
						.where('product.delete_status', false)
						.where('product.status', 1)
						.eager('[product_category]')
						// .modifyEager('product_tag_relation', builder => {
						//     return builder.select('product_tags.tags');
						// })
						// .modifyEager('product_menu_price_relation', builder => {
						//     return builder.select('price', 'selling_price');
						// })
						.modifyEager('product_category', (builder) => {
							return builder
								.select(
									'categories.id as catid',
									'product_category.category_name'
								)
								.where('product_category.status', true);
						})
				);
			})
			.where('menu_order_type_relation.order_type_id', order_type_id)
			.groupBy('menu_management.id');

		menuList = await query;

		// if (menuList == undefined || menuList == '') {
		// 	throw badRequestError(res, 'Menu list not found');
		// }
		let categoryArr = [];
		let isExistsId = [0];
		menuList = menuList.map(function (value, index) {
			categoryArr = [];
			isExistsId = [0];

			value.menu_product.map((item) => {
				if (item.product_category != undefined) {
					item.product_category.forEach((element) => {
						if (!isExistsId.includes(element.catid)) {
							isExistsId.push(element.catid);
							categoryArr.push(element);
						}
					});
				}
				delete item.product_category;
			});
			delete value.menu_product;
			value.category = categoryArr;
			return value;
		});

		let merchangeQuery = await User.query()
			.select(
				'id',
				'restaurant_name',
				'profile_image',
				'banner_image',
				'business_description',
				'membership',
				Knex.raw(
					'(select count("id") from "review_and_rating" where "merchant" = "users"."id") as reviewcount'
				),
				Knex.raw(
					'(select COALESCE(avg("rating"), 0) as avgRating from "review_and_rating" where "merchant" = "users"."id")'
				)
			)
			.where({ id: id, user_type: 'MERCHANT', user_status: true })
			.eager(
				'[merchant_cuisine, merchant_order_type, merchant_characteristic_relation]'
			)
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.modifyEager('merchant_characteristic_relation', (builder) => {
				return builder.select(
					'timing',
					'opening_status',
					'online_order_status'
				);
			})
			.modifyEager('merchant_order_type', (builder) => {
				return builder.select(
					'order_type.id as order_type_id',
					'order_type.order_type'
				);
			});
		if (merchangeQuery == '' || merchangeQuery == undefined) {
			merchangeQuery = {};
		}
		return okResponse(
			res,
			{ restaurantDetail: merchangeQuery, menuList: menuList },
			'Menu list successfully fetched'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};
//================Menu List end=================

//================Menu List By ID=================
const menuListbyId = async (req, res) => {
	try {
		let body = req.query;

		//If order_Type id is 5,6 it should be consider as 1(Dine-In)
		if (body.order_type_id == 5 || body.order_type_id == 6) {
			body.order_type_id = 1;
		}

		let err, menuList;
		let userId = req.user.user_id;
		let page = body.page ? body.page : 1;
		let limit = 10;
		let offset = (page - 1) * limit;
		let query = Menu.query().select(
			'menu_management.id',
			'menu_name',
			'merchant_id'
		);
		let eagerRelation =
			'[product_tag_relation, product_menu_price_relation, cart_product, modifiers_of_product, group_modifiers_of_product,cart_product_order_type, suggestion_type]';
		let productIds;
		if (body.categoryId != 0 && body.categoryId != '') {
			let categoryProductId = await ProductCategory.query()
				.select('product_id')
				.where('status', true)
				.where('category_id', body.categoryId);
			if (categoryProductId) {
				productIds = Array.prototype.map.call(categoryProductId, function (
					item
				) {
					return item.product_id;
				});
			}
		}
		query
			.where({
				'menu_management.id': body.id,
				'menu_management.status': true,
				'menu_management.delete_status': false,
			})
			.andWhere(function () {
				this.whereRaw('"start_date"::date <= ?', [
					moment().format('YYYY-MM-DD'),
				]).orWhere('start_date', null);
			})
			.andWhere(function () {
				this.whereRaw('"end_date"::date >= ?', [
					moment().format('YYYY-MM-DD'),
				]).orWhere('end_date', null);
			})
			.eager('[menu_product]')
			.modifyEager('menu_product', (builder) => {
				return builder
					.select(
						'product.id',
						'product_name',
						'product_image',
						'requires_age_verification',
						'product.status',
						'product_description',
						'products_per_order_limit',
						'product_quantity_counter',
						'product.suggestion_type_id',
						'requires_age_verification',
						Knex.raw(
							'(select count("id") from "review_and_rating" where "item_id" = "product"."id") as reviewcount'
						),
						Knex.raw(
							'(select COALESCE(avg("rating"), 0) as avgRating from "review_and_rating" where "item_id" = "product"."id")'
						)
					)
					.where('product_menu.status', true)
					.where('product.delete_status', false)
					.andWhere((builder) => {
						return builder
							.where('product.status', 1)
							.orWhere('product.status', 2)
							.orWhere('product.status', 3);
					})
					.where((builder) => {
						if (productIds) {
							builder.whereIn('product.id', productIds);
						}
					})
					.eager(`${eagerRelation}`)
					.modifyEager('product_tag_relation', (builder) => {
						return builder.select('product_tags.tags');
					})
					.modifyEager('suggestion_type', (builder) => {
						return builder.select('suggestion');
					})
					.modifyEager('product_menu_price_relation', (builder) => {
						return builder
							.select('price', 'selling_price')
							.where('product_pricing.order_type_id', body.order_type_id)
							.where('product_pricing.menu_id', body.id);
					})
					.modifyEager('cart_product', (builder) => {
						return builder
							.select('cart.id as cartId', 'quantity')
							.where('menu_id', body.id)
							.where('user_id', userId)
							.where('temp_order_typeid', body.order_type_id);
					})
					.modifyEager('cart_product_order_type', (builder) => {
						return builder
							.select('cart.id as cartId', 'temp_order_typeid as order_type_id')
							.where('menu_id', body.id)
							.where('user_id', userId)
							.first();
					});
			})
			.orderBy('menu_management.id', 'desc')
			.limit(limit)
			.offset(offset)
			.catch((err) => {});
		menuList = await query;

		let cart_product = [];
		let cart_product_order_type = [];
		let quantity = 0;
		let cart_id_Array;
		let menuListing = [],
			data = {};
		for (let i = 0; i < menuList.length; i++) {
			const Characteristics = await MerchantCharacteristic.query()
				.select('opening_status', 'online_order_status')
				.where('merchant_id', menuList[i].merchant_id)
				.first();

			const generalDetailOfMerchant = await User.query()
				.select('opening_status')
				.where('id', menuList[i].merchant_id)
				.first();

			menuList[i].current_opening_status = Characteristics.opening_status;
			menuList[i].online_order_status = Characteristics.online_order_status;
			menuList[i].opening_status = generalDetailOfMerchant.opening_status;

			for (let j = 0; j < menuList[i].menu_product.length; j++) {
				quantity = 0;
				cart_id_Array = [];
				cart_product = [];
				cart_product_order_type = [];
				//---------------Filtering Menus whose menu prodict pricing list is not empty (START)----------------
				if (
					menuList[i].menu_product[j].product_menu_price_relation.length > 0
				) {
					menuListing.push(menuList[i].menu_product[j]);
				}
				//---------------Filtering Menus whose menu prodict pricing list is not empty (END)----------------

				for (
					let k = 0;
					k < menuList[i].menu_product[j].cart_product.length;
					k++
				) {
					quantity =
						quantity + menuList[i].menu_product[j].cart_product[k].quantity;

					cart_id_Array.push(
						menuList[i].menu_product[j].cart_product[k].cartId
					);

					//delete menuList[i].cart_productclear
				}
				//console.log(cart_id_Array, quantity);
				if (cart_id_Array.length > 0) {
					let product = {
						quantity,
						cart_id: cart_id_Array,
					};
					let product_order_type = {
						cart_id: cart_id_Array,
						order_type_id:
							menuList[i].menu_product[j].cart_product_order_type[0]
								.order_type_id,
					};

					cart_product.push(product);
					cart_product_order_type.push(product_order_type);
				}

				delete menuList[i].menu_product[j].cart_product;
				delete menuList[i].menu_product[j].cart_product_order_type;
				menuList[i].menu_product[j].cart_product = cart_product;
				menuList[i].menu_product[
					j
				].cart_product_order_type = cart_product_order_type;

				if (
					menuList[i].menu_product[j].modifiers_of_product.length > 0 ||
					menuList[i].menu_product[j].group_modifiers_of_product.length > 0
				) {
					menuList[i].menu_product[j].isExistModifier = true;
				} else {
					menuList[i].menu_product[j].isExistModifier = false;
				}
				delete menuList[i].menu_product[j].modifiers_of_product;
				delete menuList[i].menu_product[j].group_modifiers_of_product;
			}
			data.id = menuList[i].id;
			data.menu_name = menuList[i].menu_name;
			data.merchant_id = menuList[i].merchant_id;
			data.current_opening_status = menuList[i].current_opening_status;
			data.online_order_status = menuList[i].online_order_status;
			data.opening_status = menuList[i].opening_status;
		}

		//---------------Filtering Menus whose menu prodict pricing list is not empty (START)----------------
		// let menuListing = [],
		// 	data = {};
		// for (let i = 0; i < menuList.length; i++) {
		// 	for (let j = 0; j < menuList[i].menu_product.length; j++) {
		// 		// console.log(menuList[i].menu_product[j].product_menu_price_relation.length);
		// 		if (
		// 			menuList[i].menu_product[j].product_menu_price_relation.length > 0
		// 		) {
		// 			menuListing.push(menuList[i].menu_product[j]);
		// 		}
		// 	}
		// 	data.id = menuList[i].id;
		// 	data.menu_name = menuList[i].menu_name;
		// 	data.merchant_id = menuList[i].merchant_id;
		// 	data.current_opening_status = menuList[i].current_opening_status;
		// 	data.online_order_status = menuList[i].online_order_status;
		// 	data.opening_status = menuList[i].opening_status;
		// }
		//---------------Filtering Menus whose menu prodict pricing list is not empty (END)----------------

		//Cart Price Calculations
		let user_details = await calculateCartPrice(req.user.user_id);
		/*
		let subTotal = await Cart.query()
			.select(
				'quantity',
				Knex.raw(
					'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid")'
				)
			)
			.where({ user_id: req.user.user_id });

		var totalsum = 0,
			sum = 0;
		var totalquantity = 0;

		for (var i = 0; i < subTotal.length; i++) {
			totalquantity = totalquantity + subTotal[i].quantity;
			sum = parseFloat(subTotal[i].sum);
			totalsum = totalsum + sum;
		}
		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', req.user.user_id)
			.innerJoinRelation('cart_modifier_products_detail_relation');

		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}

		let user_details = {
			totalquantity,
			totalitems: subTotal.length,
			total_amount: totalsum + totalmodifiersum,
		};
		*/

		let current_opening_status_message = '',
			online_order_status_message = '';
		if (data.current_opening_status == false) {
			current_opening_status_message =
				'Merchant is currently closed. Please order once it’s open.';
		}
		if (data.online_order_status == false) {
			online_order_status_message =
				'Merchant is not accepting online orders at this moment. Please try again later.';
		}

		if (data.opening_status == 1) {
			data.opening_status_message = 'Merchant will be opening soon.';
		} else if (data.opening_status == 2) {
			data.opening_status_message = 'Merchant Opened';
		} else if (data.opening_status == 3) {
			data.opening_status_message =
				'Currently Merchant is temprorily closed. Please order after some time.';
		} else if (data.opening_status == 4) {
			data.opening_status_message = 'Merchant is permanently closed.';
		}

		let responsData = [
			{
				id: data.id,
				menu_name: data.menu_name,
				merchant_id: data.merchant_id,
				current_opening_status: data.current_opening_status,
				current_opening_status_message: current_opening_status_message,
				online_order_status: data.online_order_status,
				online_order_status_message: online_order_status_message,
				opening_status: data.opening_status,
				opening_status_message: data.opening_status_message,
				menu_product: menuListing,
				user_details,
			},
		];

		return okResponse(res, responsData, 'Menu list successfully fetched');
	} catch (error) {
		throw badRequestError(res, error);
	}
};
//================Menu List end=================

//================Group Modifier=================
const productGroupModifier = async (req, res) => {
	try {
		let id = +req.params.id;
		//let cartId = +req.params.cartId;
		let product_group_modifier;
		let err, menuList;
		let userId = req.user.user_id;
		let query = Product.query()
			.select('id', 'product_name', 'product_description')
			.where({ id: id, delete_status: false, status: 1 })
			.eager(
				'[product_modifier_get_relation.[modifier_product_detail_relation],product_modifier_group_get_relation]'
			)
			.modifyEager('product_modifier_group_get_relation', (builder) => {
				return builder
					.select('modifier_group_name')
					.where('modifier_group.status', true)
					.eager('group_modifier_management')
					.modifyEager('group_modifier_management', (builder) => {
						return builder
							.select('modifier.*')
							.eager('modifier_product_detail_relation')
							.modifyEager('modifier_product_detail_relation', (builder) => {
								return builder.select(
									'modifier_product_details.*',
									Knex.raw(
										'case when (select "id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
											userId +
											'order by id desc limit 1' +
											') IS NULL THEN false else true END as is_exists'
									)
								);
							});
					});
			})
			.modifyEager('product_modifier_get_relation', (builder) => {
				return builder
					.select('modifier.*')
					.modifyEager('modifier_product_detail_relation', (builder) => {
						return builder.select(
							'modifier_product_details.*',
							Knex.raw(
								'case when (select "id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
									userId +
									'order by id desc limit 1' +
									') IS NULL THEN false else true END as is_exists'
							)
						);
					});
			})
			.first();

		menuList = await query;

		if (menuList.product_modifier_group_get_relation.length > 0) {
			for (
				var i = 0;
				i < menuList.product_modifier_group_get_relation.length;
				i++
			) {
				product_group_modifier =
					menuList.product_modifier_group_get_relation[i]
						.group_modifier_management;

				delete menuList.product_modifier_group_get_relation[i]
					.group_modifier_management;
			}

			menuList.product_modifier_get_relation = [
				...new Set([
					...menuList.product_modifier_get_relation,
					...product_group_modifier,
				]),
			];

			menuList.product_modifier_get_relation = Array.from(
				new Set(
					menuList.product_modifier_get_relation.map((a) => a.modifier_name)
				)
			).map((modifier_name) => {
				return menuList.product_modifier_get_relation.find(
					(a) => a.modifier_name === modifier_name
				);
			});
		}
		delete menuList.product_modifier_group_get_relation;

		return okResponse(
			res,
			menuList,
			'Group modifier list successfully fetched'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};
//================Group Modifier=================

//================Add to Cart=================

//API not in use
const addToCartWithModifier = async (req, res) => {
	//addToCartWithModifier
	try {
		let body = req.body;
		let userId = req.user.user_id;

		let err, checkCartProductStatus, addItem;

		if (body.cartId != 0) {
			if (body.quantity == 0) {
				let deleteItem = await Cart.query().del().where('id', body.cartId);
				return okResponse(
					res,
					{ cartId: 0, quantity: 0, subtotal: 0 },
					'Item removed from cart'
				);
			} else {
				addItem = await (await Cart.query().findOne({ id: body.cartId }))
					.$query()
					.updateAndFetch({ quantity: body.quantity });
			}
		} else {
			let getCartDetail = await Cart.query()
				.select('merchant_id', 'order_type_id', 'product_id')
				.where('user_id', userId)
				.where((builder) => {
					builder
						.whereNot('merchant_id', body.merchant_id)
						.orWhereNot('order_type_id', body.order_type_id);
				})
				.first()
				.catch((err) => {});
			if (getCartDetail) {
				if (getCartDetail.merchant_id != body.merchant_id) {
					throw badRequestError(
						res,
						'You have already added an item from another merchant'
					);
				}
				if (getCartDetail.order_type_id != body.order_type_id) {
					throw badRequestError(
						res,
						'You have already added an item from another order type'
					);
				}
			}
			let checkitem = await Cart.query().findOne({
				product_id: body.product_id,
				menu_id: body.menu_id,
				order_type_id: body.order_type_id,
				user_id: userId,
			});
			if (checkitem)
				throw badRequestError(res, 'Item already added to the cart');
			delete body.cartId;
			body.user_id = userId;
			//console.log(userId, 'ddddd');

			addItem = await Cart.query()
				.insert(body)
				.catch((err) => {
					console.log(err);
				});

			// let relation_data = body.cart_product_modifiers_relation;
			// relation_data.forEach((data) => {
			// 	console.log(data.modifier_id);
			// });
		}
		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		//Check detail status
		// let query = User.query().select('users.id')
		// .where({ 'users.id': body.merchant_id, user_type: "MERCHANT", user_status: true })
		// .eager('[merchant_product_relation, merchant_menu_relation, merchant_order_type, merchant_category_relation]')
		//         .modifyEager('merchant_product_relation', builder => {
		//             return builder.select('product.id').where('product.id', body.product_id).where('product.delete_status', false)
		//             .eager('product_category')
		//             .modifyEager('product_category', builder => {
		//                 return builder.select('product_category.id').where('product_id', body.product_id).where('category_id', body.category_id).where('categories.delete_status', false).where('categories.status', true).where('product_category.status', true);
		//             })
		//         })
		//         .modifyEager('merchant_menu_relation', builder => {
		//             return builder.select('menu_management.id').where('menu_management.id', body.menu_id).where('menu_management.delete_status', false).where('menu_management.status', true);
		//         })
		//         .modifyEager('merchant_order_type', builder => {
		//             return builder.select('order_type.id').where('order_type.id', body.order_type_id).where('order_type_merchant_characteristics_management.order_type_id', body.order_type_id);
		//         }).firsr()
		//         checkCartProductStatus = await query.catch(err => {
		//             console.log(err);
		//         });
		// console.log(checkCartProductStatus)
		let subTotal = await Cart.query()
			.select(
				'quantity',
				Knex.raw(
					'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId, id: addItem.id })
			.eager('[cart_product_modifiers_relation]')
			.first();
		console.log(subTotal);
		var totalsum = 0,
			sum = 0;
		//var totalquantity = 0;
		//for (var i = 0; i < subTotal.length; i++) {
		//totalquantity = totalquantity + subTotal.quantity;
		sum = parseFloat(subTotal.sub_total);
		totalsum = totalsum + subTotal.quantity * sum;
		//}

		let query = await Cart.query()
			.select(
				'merchant_id',
				'quantity',
				Knex.raw(
					'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId });

		var total = 0,
			sum = 0;
		var totalitems = 0;

		for (var i = 0; i < query.length; i++) {
			totalitems = totalitems + query[i].quantity;
			sum = parseFloat(query[i].sub_total);
			total = total + query[i].quantity * sum;
		}

		return okResponse(
			res,
			{
				cartId: addItem.id,
				quantity: subTotal.quantity,
				total_cart_item: query.length,
				total_amount: total,
				subtotal: totalsum,
				cart_product_modifiers_relation:
					addItem.cart_product_modifiers_relation,
			},
			'Successfully added to the cart'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};
//================Add to Cart end=================
const addToCart = async (req, res) => {
	try {
		let body = req.body;
		let userId = req.user.user_id;
		let err, checkCartProductStatus, addItem;
		let cart_value = body.cartId;
		let km = undefined;
		let Delivery = undefined;
		if (body.distance != undefined) {
			km = body.distance;
			delete body.distance;
		}

		if (body.category_id == 0) {
			body.category_id = null;
		}
		if (body.cartId != 0) {
			if (body.quantity == 0) {
				let deleteCart = await Cart.query()
					.del()
					.where({ user_id: userId, id: body.cartId });

				let query = await Cart.query()
					.select(
						'quantity',
						Knex.raw(
							'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
						)
					)
					.where({ user_id: userId });

				var total = 0;
				var sum = 0;
				var totalitems = 0;

				for (var i = 0; i < query.length; i++) {
					totalitems = totalitems + query[i].quantity;
					sum = parseFloat(query[i].sub_total);
					total = total + query[i].quantity * sum;
				}
				let modifier_data = await CartProductModifierDetail.query()
					.select('selling_price', 'modifier_quantity')
					.where('user_id', userId)
					.innerJoinRelation('cart_modifier_products_detail_relation');

				var totalmodifiersum = 0;
				if (modifier_data != undefined) {
					for (var i = 0; i < modifier_data.length; i++) {
						//totalquantity = totalquantity + subTotal[i].quantity;
						totalmodifiersum =
							totalmodifiersum +
							modifier_data[i].modifier_quantity *
								modifier_data[i].selling_price;
					}
				}
				if (km != undefined) {
					Delivery = await DeliveryCharges.query()
						.select('delivery_fee', 'minimum_order', 'free_delivery_order')
						.where((builder) => {
							return builder.where('from', '<=', km).andWhere('to', '>=', km);
						})
						.where('merchant_id', body.merchant_id);
					if (Delivery.length > 0) {
						Delivery[0].distance = km;
						if (Delivery[0].free_delivery_order == 0) {
							Delivery[0].is_free_delivery = false;
						} else {
							if (Delivery[0].free_delivery_order <= total + totalmodifiersum) {
								Delivery[0].is_free_delivery = true;
								Delivery[0].delivery_fee = 0;
							} else {
								Delivery[0].is_free_delivery = false;
							}
						}
					} else Delivery = [];
				} else {
					Delivery = [];
				}
				return okResponse(
					res,
					{
						cartId: 0,
						quantity: 0,
						totalquantity: totalitems,
						total_cart_item: query.length,
						total_amount: total + totalmodifiersum,
						cart_product_modifiers_relation: [],
						Delivery,
					},
					'Item deleted from cart'
				);

				//return okResponse(res, {}, 'Item removed From Cart');
			}
			if (
				body.cart_product_modifiers_relation != undefined &&
				body.cart_product_modifiers_relation.length == 0
			) {
				addItem = await Cart.query().updateAndFetchById(body.cartId, {
					quantity: body.quantity,
				});
				let cart_modifier = await CartProductModifier.query()
					.select('id')
					.where('cart_id', body.cartId);

				if (cart_modifier) {
					let id_data = [];
					// let id = Array.prototype.map(cart_modifier, function (item) {
					// 	return item[0].id;
					// });
					for (let i = 0; i < cart_modifier.length; i++) {
						id_data.push(cart_modifier[i].id);
					}

					let cart_product_detail = await CartProductModifierDetail.query()
						.update({ modifier_quantity: body.quantity })
						.whereIn('cart_modifier_id', id_data)
						.catch((err) => {});
				}
			} else {
				let deleteCart = await Cart.query()
					.del()
					.where({ user_id: userId, id: body.cartId });
				delete body.cartId;
				body.user_id = userId;

				if (body.cart_product_modifiers_relation.length > 0) {
					let relation_data = body.cart_product_modifiers_relation;
					for (let i = 0; i < relation_data.length; i++) {
						let modifierData = await Modifier.query()
							.select('min_user_must_select', 'max_user_must_select')
							.where('id', relation_data[i].modifier_id)
							.first();
						let count = relation_data[i].cart_modifier_detail_relation.length;

						if (modifierData.max_user_must_select == 0) {
						} else if (modifierData.min_user_must_select > count) {
							throw badRequestError(res, 'Please select modifier');
						} else if (modifierData.max_user_must_select < count) {
							throw badRequestError(
								res,
								'You cannot select multiple modifiers'
							);
						}
					}
				}
			}
		} else {
			let getCartDetail = await Cart.query()
				.select('merchant_id', 'order_type_id', 'product_id')
				.where('user_id', userId)
				.where((builder) => {
					builder
						.whereNot('merchant_id', body.merchant_id)
						.orWhereNot('order_type_id', body.order_type_id);
				})
				.first()
				.catch((err) => {
					//console.log(err, 'aaaa');
				});

			if (getCartDetail) {
				if (getCartDetail.merchant_id != body.merchant_id) {
					throw badRequestError(
						res,
						'You have already added an item from another Merchant'
					);
				}
				if (getCartDetail.order_type_id != body.order_type_id) {
					throw badRequestError(
						res,
						'You have already added an item from another order type'
					);
				}
			}
			/*
			let checkitem = await Cart.query().findOne({
				product_id: body.product_id,
				menu_id: body.menu_id,
				//category_id: body.category_id,
				user_id: userId,
			});
			if (checkitem) throw badRequestError(res, 'Item is already present');
			*/

			delete body.cartId;
			body.user_id = userId;
			let relation_data = body.cart_product_modifiers_relation;

			if (relation_data != undefined && relation_data.length > 0) {
				for (var i = 0; i < relation_data.length; i++) {
					let modifierData = await Modifier.query()
						.select('min_user_must_select', 'max_user_must_select')
						.where('id', relation_data[i].modifier_id)
						.first();
					let count = relation_data[i].cart_modifier_detail_relation.length;
					if (modifierData.max_user_must_select == 0) {
					} else if (modifierData.min_user_must_select > count) {
						throw badRequestError(res, 'Please select modifier');
					} else if (modifierData.max_user_must_select < count) {
						throw badRequestError(res, 'You cannot select multiple modifiers');
					}
				}
			} else {
				let query = await Product.query()
					.select('id')
					.where({ id: body.product_id })
					.eager(
						'[product_modifier_get_relation,product_modifier_group_get_relation]'
					)
					.first();

				if (
					query.product_modifier_get_relation.length > 0 ||
					query.product_modifier_group_get_relation.length > 0
				) {
					throw badRequestError(res, 'Please add modifier(s)');
				}
			}
		}

		if (
			body.cart_product_modifiers_relation != undefined &&
			body.cart_product_modifiers_relation.length == 0 &&
			cart_value != 0
		) {
		} else {
			[err, addItem] = await to(
				transaction(Cart.knex(), (trx) => {
					return Cart.query(trx).insertGraph(body);
				})
			);
		}

		//console.log('error', addItem);
		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		/*
		let subTotal = await Cart.query()
			.select(
				'quantity',
				Knex.raw(
					'(select "selling_price" from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."order_type_id") as sub_total'
				)
			)
			.where({ user_id: userId, id: addItem.id })
			.eager('[cart_product_modifiers_relation]')
			.first();
		console.log('total', subTotal);
		var totalsum = 0,
			sum = 0;
		//var totalquantity = 0;
		//for (var i = 0; i < subTotal.length; i++) {
		//	totalquantity = totalquantity + subTotal[i].quantity;
		sum = parseInt(subTotal.sub_total);
		totalsum = totalsum + subTotal.quantity * sum;
		//}
		*/
		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', userId)
			.innerJoinRelation('cart_modifier_products_detail_relation');

		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}
		let query = await Cart.query()
			.select(
				'merchant_id',
				'quantity',
				Knex.raw(
					'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId });

		var total = 0,
			sum = 0;
		var totalitems = 0;

		for (var i = 0; i < query.length; i++) {
			totalitems = totalitems + query[i].quantity;

			sum = parseFloat(query[i].sub_total);
			total = total + query[i].quantity * sum;
		}
		if (km != undefined) {
			Delivery = await DeliveryCharges.query()
				.select('delivery_fee', 'minimum_order', 'free_delivery_order')
				.where((builder) => {
					return builder.where('from', '<=', km).andWhere('to', '>=', km);
				})
				.where('merchant_id', body.merchant_id);
			if (Delivery.length > 0) {
				Delivery[0].distance = km;
				if (Delivery[0].free_delivery_order == 0) {
					Delivery[0].is_free_delivery = false;
				} else {
					if (Delivery[0].free_delivery_order <= total + totalmodifiersum) {
						Delivery[0].is_free_delivery = true;
						Delivery[0].delivery_fee = 0;
					} else {
						Delivery[0].is_free_delivery = false;
					}
				}
			} else Delivery = [];
		} else {
			Delivery = [];
		}

		return okResponse(
			res,
			{
				cartId: addItem.id,
				quantity: body.quantity,
				total_cart_item: query.length,
				totalquantity: totalitems,
				total_amount: total + totalmodifiersum,
				cart_product_modifiers_relation: [],
				Delivery,
			},
			'Successfully added to the cart'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};

const addToCartFinal = async (req, res) => {
	try {
		let body = req.body;
		let userId = req.user.user_id;
		let err,
			checkCartProductStatus,
			addItem = undefined;
		let cart_value = body.cartId;
		let update_flag = false;
		let update_cart_id = 0;
		let km = undefined;
		let Delivery = undefined;
		//Retreive distance Key from body
		if (body.distance != undefined) {
			km = body.distance;
			delete body.distance;
		}

		let product = await Product.query()
			.select(
				'product_name',
				'product_threshold_quantity',
				'product_quantity_counter',
				'products_per_order_limit',
				'online_order_status',
				'opening_status',
				Knex.raw(
					'(SELECT CASE WHEN sum(quantity) IS NULL THEN 0 ELSE sum(quantity) END AS quantity from cart where cart.product_id=product.id and cart.user_id=' +
						req.user.user_id +
						')'
				)
			)
			.innerJoin(
				'merchant_characteristics',
				'merchant_characteristics.merchant_id',
				'product.merchant_id'
			)
			.eager('merchant_detail')
			.modifyEager('merchant_detail', (builder) => {
				return builder.select('id', 'opening_status');
			})
			.where({ delete_status: false })
			.where('product.id', body.product_id)
			.andWhere((builder) => {
				return builder.where('status', 1);
			})
			.first();
		console.log(product);
		if (product) {
			if (product.merchant_detail.opening_status == 1) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant will be opening soon.',
					400
				);
			} else if (product.merchant_detail.opening_status == 3) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Currently Merchant is temprorily closed. Please order after some time.',
					400
				);
			} else if (product.merchant_detail.opening_status == 4) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is permanently closed.',
					400
				);
			}

			if (!product.opening_status) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is currently closed. Please order once it’s open.',
					400
				);
			}
			if (!product.online_order_status) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is not accepting online orders at this moment. Please try again later.',
					400
				);
			}

			//If CartID is present,this case is Product Without Modifier

			if (body.cartId != 0) {
				if (product.product_quantity_counter < body.quantity) {
					throw errorresponse(
						res,
						{ code: 300 },
						'Unfortunately, the item you are trying to order is now out-of-stock.',
						400
					);
				}
				if (product.products_per_order_limit == 0) {
				}
				//Condition for Limit for a product to order
				else if (product.products_per_order_limit < body.quantity) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Sorry, you have already reached maximum allowed items limit per order.',
						400
					);
				}
			} else {
				//If cartID present case with the product along with modifier
				//Out of Stock condition
				if (
					product.product_quantity_counter <
					parseInt(product.quantity) + body.quantity
				) {
					throw errorresponse(
						res,
						{ code: 300 },
						'Unfortunately, the item you are trying to order is now out-of-stock.',
						400
					);
				}
				if (product.products_per_order_limit == 0) {
				}
				//Condition for Limit for a product to order
				else if (
					product.products_per_order_limit <
					parseInt(product.quantity) + body.quantity
				) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Sorry, you have already reached maximum allowed items limit per order.',
						400
					);
				}
			}
			if (!product.opening_status) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is currently closed. Please order once it’s open.',
					400
				);
			}
			if (!product.online_order_status) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is not accepting online orders at this moment. Please try again later.',
					400
				);
			}
		} else {
			throw badRequestError(
				res,
				'Item you are looking for is Out Of Stock. Please come back later'
			);
		}
		//Condition Category_id is not mandatory in case of 0 it should be store as null in DB
		if (body.category_id == 0) {
			body.category_id = null;
		}

		//If Product is already present in the Cart
		if (body.cartId != 0) {
			let flag = body.flag;
			delete body.flag; //delete flag
			//If product quantity becomes 0,delete product
			if (body.quantity == 0) {
				let deleteCart = await Cart.query()
					.del()
					.where({ user_id: userId, id: body.cartId });
				//Price Calculation of products in cart
				let query = await Cart.query()
					.select(
						'quantity',
						Knex.raw(
							'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
						)
					)
					.where({ user_id: userId });

				var total = 0;
				var sum = 0;
				var totalitems = 0; //totalitems in cart

				for (var i = 0; i < query.length; i++) {
					totalitems = totalitems + query[i].quantity;
					sum = parseFloat(query[i].sub_total);
					total = total + query[i].quantity * sum;
				}
				let modifier_data = await CartProductModifierDetail.query()
					.select('selling_price', 'modifier_quantity')
					.where('user_id', userId)
					.innerJoinRelation('cart_modifier_products_detail_relation');
				//Modifier price Calculation
				var totalmodifiersum = 0;
				if (modifier_data != undefined) {
					for (var i = 0; i < modifier_data.length; i++) {
						//totalquantity = totalquantity + subTotal[i].quantity;
						totalmodifiersum =
							totalmodifiersum +
							modifier_data[i].modifier_quantity *
								modifier_data[i].selling_price;
					}
				}
				if (km != undefined) {
					//Calculate Delivery Charges
					Delivery = await DeliveryCharges.query()
						.select('delivery_fee', 'minimum_order', 'free_delivery_order')
						.where((builder) => {
							return builder.where('from', '<=', km).andWhere('to', '>=', km);
						})
						.where('merchant_id', body.merchant_id);
					if (Delivery.length > 0) {
						Delivery[0].distance = km;
						//If products amount is greater than merchant free_delivery_order in a range
						if (Delivery[0].free_delivery_order == 0) {
							Delivery[0].is_free_delivery = false;
						} else {
							if (Delivery[0].free_delivery_order <= total + totalmodifiersum) {
								Delivery[0].is_free_delivery = true; //Flag set to true
								Delivery[0].delivery_fee = 0;
							} else {
								Delivery[0].is_free_delivery = false;
							}
						}
					} else Delivery = [];
				} else {
					Delivery = [];
				}
				let item_details = {};
				return okResponse(
					res,
					{
						cartId: 0,
						quantity: 0,
						totalquantity: totalitems,
						total_cart_item: query.length,
						total_amount: total + totalmodifiersum,
						cart_product_modifiers_relation: [],
						Delivery,
						item_details,
					},
					'Item deleted from cart'
				);

				//return okResponse(res, {}, 'Item removed From Cart');
			}
			//If product present in Cart and quantiy changes both product with Modifier and without modifier
			if (
				body.cart_product_modifiers_relation != undefined &&
				body.cart_product_modifiers_relation.length == 0
			) {
				addItem = await Cart.query().updateAndFetchById(body.cartId, {
					quantity: body.quantity,
				});
				//Fetch Modifier For a Product of Same Cart ID
				if (flag) {
					let cart_modifier = await CartProductModifier.query()
						.select('id')
						.where('cart_id', body.cartId);

					if (cart_modifier) {
						let id_data = [];
						// let id = Array.prototype.map(cart_modifier, function (item) {
						// 	return item[0].id;
						// });
						for (let i = 0; i < cart_modifier.length; i++) {
							id_data.push(cart_modifier[i].id);
						}
						// Update Product Modifier Details Quantity
						let cart_product_detail = await CartProductModifierDetail.query()
							.update({ modifier_quantity: body.quantity })
							.whereIn('cart_modifier_id', id_data)
							.catch((err) => {
								console.log(err);
							});
					}
				} else {
					console.log('Aakash');
					let deleteCart = await CartProductModifier.query()
						.del()
						.where({ user_id: userId, cart_id: body.cartId });
				}
			} else {
				//Edit Case For Product with Modifier present in the cart,in case of product edit,we just deleting the product and adding it back in the cart
				let deleteCart = await Cart.query()
					.del()
					.where({ user_id: userId, id: body.cartId });
				delete body.cartId;
				body.user_id = userId;

				//Check Modifier Condition For a product
				if (body.cart_product_modifiers_relation.length > 0) {
					let relation_data = body.cart_product_modifiers_relation;
					for (let i = 0; i < relation_data.length; i++) {
						let modifierData = await Modifier.query()
							.select('min_user_must_select', 'max_user_must_select')
							.where('id', relation_data[i].modifier_id)
							.first();
						let count = relation_data[i].cart_modifier_detail_relation.length;

						if (modifierData.max_user_must_select == 0) {
						} else if (modifierData.min_user_must_select > count) {
							throw badRequestError(res, 'Please select modifier');
						} else if (modifierData.max_user_must_select < count) {
							throw badRequestError(
								res,
								'You cannot select multiple modifiers'
							);
						}
						//Modifier Quantity should be same as product quantity
						for (
							let j = 0;
							j < relation_data[i].cart_modifier_detail_relation.length;
							j++
						) {
							relation_data[i].cart_modifier_detail_relation[
								j
							].modifier_quantity = body.quantity;
						}
					}
				} else {
					let modifier = await ProductModifiers.query()
						.select(
							Knex.raw(
								'(Select min_user_must_select where min_user_must_select>0)'
							)
						)
						.innerJoinRelation('modifier')
						.where('product_id', body.product_id)
						.first();
					if (modifier != undefined && modifier.min_user_must_select != null) {
						throw badRequestError(res, 'Please add modifier(s)');
					}
				}
			}
		} else {
			//Get Cart Detail
			let getCartDetail = await Cart.query()
				.select('merchant_id', 'order_type_id', 'product_id')
				.where('user_id', userId)
				.where((builder) => {
					builder
						.whereNot('merchant_id', body.merchant_id)
						.orWhereNot('order_type_id', body.order_type_id);
				})
				.first();

			if (getCartDetail) {
				//If some merchant item is already present in Cart
				if (getCartDetail.merchant_id != body.merchant_id) {
					throw badRequestError(
						res,
						'You have already added an item from another Merchant'
					);
				}
				//If cart item has different order_type of same merchant
				if (getCartDetail.order_type_id != body.order_type_id) {
					throw badRequestError(
						res,
						'You have already added an item from another order type'
					);
				}
			}
			let product = await Product.query()
				.select('requires_age_verification', 'product_quantity_counter')
				.where('id', body.product_id)
				.eager('cart_product')
				.modifyEager('cart_product', (builder) => {
					return builder.select();
				})
				.first();

			if (product) {
				//If product requires age_verification
				if (product.requires_age_verification) {
					//check years>18
					let dob = req.user.auth_user_relation.date_of_birth;

					let years = dob.getTime() - moment().valueOf();

					years = Math.abs(Math.floor(years / 31536000000)); //Age Calculations

					if (years < 18) {
						throw badRequestError(
							res,
							'You cannot Order this item due to age restrictions'
						);
					}
				}
			}

			delete body.cartId; //delete cartID as in case of insertion for the first time ,it is 0
			body.user_id = userId;
			let relation_data = body.cart_product_modifiers_relation;
			//Flag to now that user is adding a product which has modifier
			//Flag true in case of adding an item for the first time ,than false,in case of product without modifier it is always false
			//On adding a same item with modifier for the second time ,the flag is false
			if (body.flag) {
				//Check product modifier conditions
				if (relation_data != undefined && relation_data.length > 0) {
					for (var i = 0; i < relation_data.length; i++) {
						let modifierData = await Modifier.query()
							.select('min_user_must_select', 'max_user_must_select')
							.where('id', relation_data[i].modifier_id)
							.first();
						let count = relation_data[i].cart_modifier_detail_relation.length;
						if (modifierData.max_user_must_select == 0) {
						} else if (modifierData.min_user_must_select > count) {
							throw badRequestError(res, 'Please select modifier');
						} else if (modifierData.max_user_must_select < count) {
							throw badRequestError(
								res,
								'You cannot select multiple modifiers'
							);
						}
					}
				} else {
					let modifier = await ProductModifiers.query()
						.select(
							Knex.raw(
								'(Select min_user_must_select where min_user_must_select>0)'
							)
						)
						.innerJoinRelation('modifier')
						.where('product_id', body.product_id)
						.first();
					if (modifier != undefined && modifier.min_user_must_select != null) {
						throw badRequestError(res, 'Please add modifier(s)');
					}
				}
			}
			// Updating a product with Modifier already present in cart
			//In case of increasing quantity for a product with modifier ,body.quantity=1,in case of decreasing the quantity,body.quantity =-1
			if (relation_data != undefined && relation_data.length > 0) {
				let cart_modifier_data = await Cart.query()
					.select('id')
					.eager(
						'cart_product_modifiers_relation.cart_modifier_detail_relation'
					)
					.where('product_id', body.product_id)
					.where('user_id', req.user.user_id);
				//return okResponse(res, cart_modifier_data, 'msg');

				if (cart_modifier_data) {
					//get Cart ID of product to updated
					//Note:Product can be added multiple times,as in case of modifier
					//Find the CartID of the product
					for (let j = 0; j < cart_modifier_data.length; j++) {
						if (
							cart_modifier_data[j].cart_product_modifiers_relation.length !=
							body.cart_product_modifiers_relation.length
						) {
							update_flag = false;
						} else {
							for (
								let i = 0;
								i <
								cart_modifier_data[j].cart_product_modifiers_relation.length;
								i++
							) {
								if (
									body.cart_product_modifiers_relation[i].modifier_id ==
									cart_modifier_data[j].cart_product_modifiers_relation[i]
										.modifier_id
								) {
									if (
										body.cart_product_modifiers_relation[j] != undefined &&
										cart_modifier_data[j].cart_product_modifiers_relation[i]
											.cart_modifier_detail_relation.length ==
											body.cart_product_modifiers_relation[j]
												.cart_modifier_detail_relation.length
									) {
										for (
											let k = 0;
											k <
											cart_modifier_data[j].cart_product_modifiers_relation[i]
												.cart_modifier_detail_relation.length;
											k++
										) {
											if (
												body.cart_product_modifiers_relation[i]
													.cart_modifier_detail_relation[k]
													.modifier_product_detail_id ==
												cart_modifier_data[j].cart_product_modifiers_relation[i]
													.cart_modifier_detail_relation[k]
													.modifier_product_detail_id
											)
												update_flag = true;
											else {
												update_flag = false;
												break;
											}
										}
									}
									if (update_flag == false) {
										break;
									}
								}
							}
						}
						if (update_flag) {
							update_cart_id = cart_modifier_data[j].id; //Get Cart ID
							break;
						}
					}
				}
			} else {
				let cart_modifier_data = await Cart.query()
					.select('id')
					.where('product_id', body.product_id)
					.where('user_id', req.user.user_id)
					.orderBy('id', 'desc')
					.limit(1)
					.first();

				if (cart_modifier_data != undefined) {
					update_cart_id = cart_modifier_data.id;
					update_flag = true;
				} else update_flag = false;
			}
			delete body.flag;
		}
		//Update quantity for the cart
		if (update_flag) {
			//Fetch Cart
			let getItem = await Cart.query()
				.select('quantity')
				.where('id', update_cart_id)
				.first();
			//Delete Cart
			if (getItem.quantity + body.quantity == 0) {
				let deleteCart = await Cart.query()
					.del()
					.where({ user_id: userId, id: update_cart_id });
				if (addItem == undefined) {
					addItem = {
						id: 0,
						quantity: 0,
					};
				}
			} else {
				//Update Cart
				addItem = await Cart.query().updateAndFetchById(update_cart_id, {
					quantity: getItem.quantity + body.quantity,
				});
				let cart_modifier = await CartProductModifier.query()
					.select('id')
					.where('cart_id', update_cart_id);

				if (cart_modifier) {
					let id_data = [];
					// let id = Array.prototype.map(cart_modifier, function (item) {
					// 	return item[0].id;
					// });
					for (let i = 0; i < cart_modifier.length; i++) {
						id_data.push(cart_modifier[i].id);
					}

					let cart_product_detail = await CartProductModifierDetail.query()
						.update({ modifier_quantity: getItem.quantity + body.quantity })
						.whereIn('cart_modifier_id', id_data)
						.catch((err) => {});
				}
			}
		} else if (
			//Update condition fullfilled before just the condition not to add item again in the cart
			body.cart_product_modifiers_relation != undefined &&
			body.cart_product_modifiers_relation.length == 0 &&
			cart_value != 0
		) {
		} else {
			//Add ITem to the cart
			[err, addItem] = await to(
				transaction(Cart.knex(), (trx) => {
					return Cart.query(trx).insertGraph(body);
				})
			);
		}

		//console.log('error', addItem);
		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		//Price Calculation of the product added in the Cart
		let cartdata = await Cart.query()
			.select(
				Knex.raw(
					'(select ("selling_price") from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as product_price'
				)
			)
			.where({ user_id: userId, id: addItem.id })
			.eager('[cart_product_modifiers_relation]')
			.modifyEager('cart_product_modifiers_relation', (builder) => {
				return builder
					.eager('[cart_modifier_detail_relation]')
					.modifyEager('cart_modifier_detail_relation', (builder) => {
						return builder
							.select('selling_price')
							.innerJoinRelation('cart_modifier_product_detail_relation');
					});
			})
			.first();
		let item_details = undefined;
		if (cartdata != undefined) {
			let current_modifier_sum = 0;
			item_details = { product_price: cartdata.product_price }; //Particular product added in cart
			if (cartdata.cart_product_modifiers_relation.length > 0) {
				for (
					let i = 0;
					i < cartdata.cart_product_modifiers_relation.length;
					i++
				) {
					for (
						let j = 0;
						j <
						cartdata.cart_product_modifiers_relation[i]
							.cart_modifier_detail_relation.length;
						j++
					) {
						current_modifier_sum =
							current_modifier_sum +
							cartdata.cart_product_modifiers_relation[i]
								.cart_modifier_detail_relation[j].selling_price;
					}
				}
				item_details.modifier_price = current_modifier_sum;
			} else {
				item_details.modifier_price = current_modifier_sum;
			}
		} else {
			item_details = {};
		}

		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', userId)
			.innerJoinRelation('cart_modifier_products_detail_relation');

		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}
		//Total price Calculation in the cart
		let query = await Cart.query()
			.select(
				'merchant_id',
				'product_id',
				'quantity',
				Knex.raw(
					'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId });

		var total = 0,
			sum = 0;
		var totalitems = 0;
		addItem.quantity = 0;

		for (var i = 0; i < query.length; i++) {
			if (body.product_id == query[i].product_id) {
				addItem.quantity = addItem.quantity + query[i].quantity;
			}
			totalitems = totalitems + query[i].quantity;
			sum = parseFloat(query[i].sub_total);
			total = total + query[i].quantity * sum;
		}
		if (km != undefined) {
			//Delivery Charges calculation
			Delivery = await DeliveryCharges.query()
				.select('delivery_fee', 'minimum_order', 'free_delivery_order')
				.where((builder) => {
					return builder.where('from', '<=', km).andWhere('to', '>=', km);
				})
				.where('merchant_id', body.merchant_id);
			if (Delivery.length > 0) {
				Delivery[0].distance = km;
				if (Delivery[0].free_delivery_order == 0) {
					Delivery[0].is_free_delivery = false;
				} else {
					if (Delivery[0].free_delivery_order <= total + totalmodifiersum) {
						Delivery[0].is_free_delivery = true;
						Delivery[0].delivery_fee = 0;
					} else {
						Delivery[0].is_free_delivery = false;
					}
				}
			} else Delivery = [];
		} else {
			Delivery = [];
		}

		return okResponse(
			res,
			{
				cartId: addItem.id,
				quantity: addItem.quantity,
				total_cart_item: query.length,
				totalquantity: totalitems,
				total_amount: total + totalmodifiersum,
				cart_product_modifiers_relation: [],
				Delivery,
				item_details,
			},
			'Successfully added to the cart'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//API not in use
const addtoCart = async (req, res) => {
	try {
		let body = req.body;
		let userId = req.user.user_id;
		let err, checkCartProductStatus, addItem;
		let cart_value = body.cartId;
		if (body.category_id == 0) {
			body.category_id = null;
		}
		let getCartDetail = await Cart.query()
			.select('merchant_id', 'order_type_id', 'product_id')
			.where('user_id', userId)
			.where((builder) => {
				builder
					.whereNot('merchant_id', body.merchant_id)
					.orWhereNot('order_type_id', body.order_type_id);
			})
			.first()
			.catch((err) => {
				//console.log(err, 'aaaa');
			});

		if (getCartDetail) {
			if (getCartDetail.merchant_id != body.merchant_id) {
				throw badRequestError(
					res,
					'You have already added an item from another Merchant'
				);
			}
			if (getCartDetail.order_type_id != body.order_type_id) {
				throw badRequestError(
					res,
					'You have already added an item from another order type'
				);
			}
		}
		delete body.cartId;
		body.user_id = userId;
		let relation_data = body.cart_product_modifiers_relation;

		if (relation_data != undefined && relation_data.length > 0) {
			for (var i = 0; i < relation_data.length; i++) {
				let modifierData = await Modifier.query()
					.select('min_user_must_select', 'max_user_must_select')
					.where('id', relation_data[i].modifier_id)
					.first();
				let count = relation_data[i].cart_modifier_detail_relation.length;
				if (modifierData.max_user_must_select == 0) {
				} else if (modifierData.min_user_must_select > count) {
					throw badRequestError(res, 'Please select modifier');
				} else if (modifierData.max_user_must_select < count) {
					throw badRequestError(res, 'You cannot select multiple modifiers');
				}
			}
		} else {
			let modifier = await ProductModifiers.query()
				.select(
					Knex.raw('(Select min_user_must_select where min_user_must_select>0)')
				)
				.innerJoinRelation('modifier')
				.where('product_id', body.product_id);
			if (modifier.length > 0) {
				throw badRequestError(res, 'Please add modifier(s)');
			}
		}
		[err, addItem] = await to(
			transaction(Cart.knex(), (trx) => {
				return Cart.query(trx).insertGraph(body);
			})
		);

		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', userId)
			.innerJoinRelation('cart_modifier_products_detail_relation');

		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}
		let query = await Cart.query()
			.select(
				'merchant_id',
				'quantity',
				Knex.raw(
					'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId });

		var total = 0,
			sum = 0;
		var totalitems = 0;

		for (var i = 0; i < query.length; i++) {
			totalitems = totalitems + query[i].quantity;

			sum = parseInt(query[i].sub_total);
			total = total + query[i].quantity * sum;
		}

		return okResponse(
			res,
			{
				cartId: addItem.id,
				quantity: body.quantity,
				total_cart_item: query.length,
				totalquantity: totalitems,
				total_amount: total + totalmodifiersum,
				cart_product_modifiers_relation: [],
			},
			'Successfully added to the cart'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};

//================Add to Cart with modifier end=================

//================view to Cart=================
const viewCart = async (req, res) => {
	try {
		//let body = req.body;
		let userId = req.user.user_id;
		let body = req.body;
		//console.log(req.user);
		let err, checkCartProductStatus, addItem;
		//Get CartDetails and its relation
		let query = Cart.query()
			.select(
				'cart.id as cartId',
				'quantity',
				'cart.merchant_id as merchant_id',
				'menu_id',
				'table_no',
				'products_per_order_limit',
				'product_id',
				'category_id',
				'product_name',
				'status',
				'product_quantity_counter',
				'product_description',
				'delete_status',
				'order_type_id',
				'cart_delivery_address',
				'cart_delivery_lat',
				'cart_delivery_lng',
				Knex.raw(
					'(select ("selling_price") from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as product_price'
				)
			)
			.where({ user_id: userId })
			.innerJoin('product', 'cart.product_id', 'product.id')
			.eager(
				'[cart_product_modifiers_relation,modifiers_of_product,group_modifiers_of_product]'
			)
			.modifyEager('cart_product_modifiers_relation', (builder) => {
				return builder
					.select('modifier_id', 'modifier_name', 'user_id', 'display_name')
					.innerJoinRelation('cart_modifier_relation')
					.eager('[cart_modifier_detail_relation]')
					.modifyEager('cart_modifier_detail_relation', (builder) => {
						return builder
							.select(
								'cart_product_modifier_detail.modifier_id as modifier_id',
								'user_id',
								'modifier_quantity',
								'modifier_product_detail_id',
								'modifier_product',
								'selling_price'
							)
							.innerJoinRelation('cart_modifier_product_detail_relation');
					});
			});
		//comment modifier condition
		// .modifyEager('cart_product_modifiers_relation', (builder) => {
		// 	return builder
		// 		.select('modifier_id')
		// 		.eager('[cart_modifier_relation,cart_modifier_detail_relation]')
		// 		.modifyEager('cart_modifier_relation', (builder) => {
		// 			return builder.select(
		// 				'modifier_name',
		// 				'min_user_must_select',
		// 				'max_user_must_select'
		// 			);
		// 		})
		// 		.modifyEager('cart_modifier_detail_relation', (builder) => {
		// 			return builder
		// 				.select('modifier_product_detail_id')
		// 				.eager('cart_modifier_product_detail_relation')
		// 				.modifyEager(
		// 					'cart_modifier_product_detail_relation',
		// 					(builder) => {
		// 						return builder.select('modifier_product');
		// 					}
		// 				);
		// 		});
		// });

		checkCartProductStatus = await query.catch((err) => {});
		//console.log(checkCartProductStatus);

		if (checkCartProductStatus == undefined || checkCartProductStatus == '') {
			throw badRequestError(res, 'Your cart is empty');
		}
		//Adding isexist modifier key on a product,for handling on a App End
		for (let i = 0; i < checkCartProductStatus.length; i++) {
			if (
				checkCartProductStatus[i].modifiers_of_product.length > 0 ||
				checkCartProductStatus[i].group_modifiers_of_product.length > 0
			) {
				checkCartProductStatus[i].isExistModifier = true; //Add a key if product has modifier
			} else {
				checkCartProductStatus[i].isExistModifier = false;
			}
			delete checkCartProductStatus[i].modifiers_of_product;
			delete checkCartProductStatus[i].group_modifiers_of_product;
			if (
				checkCartProductStatus[i].product_quantity_counter == 0 ||
				checkCartProductStatus[i].status != 1 ||
				checkCartProductStatus[i] == true
			) {
				checkCartProductStatus[i].current_message =
					'Item you are looking for is Out Of Stock. Please come back later';
			} else {
				checkCartProductStatus[i].current_message = '';
			}
		}
		//Product Price Calculations
		/*
		let subTotal = await Cart.query()
			.select(
				'quantity',

				Knex.raw(
					'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId });

		var totalsum = 0,
			sum = 0;
		var totalquantity = 0;
		for (var i = 0; i < subTotal.length; i++) {
			totalquantity = totalquantity + subTotal[i].quantity;
			sum = parseFloat(subTotal[i].sub_total);
			totalsum = totalsum + sum;
		}
		// Modifier price Calculations
		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', req.user.user_id)
			.innerJoinRelation('cart_modifier_products_detail_relation');
		var totalmodifiersum = 0;
		if (modifier_data != undefined) {
			for (var i = 0; i < modifier_data.length; i++) {
				//totalquantity = totalquantity + subTotal[i].quantity;
				totalmodifiersum =
					totalmodifiersum +
					modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
			}
		}
		*/
		//Get merchant details of Cart
		let user_details = await calculateCartPrice(req.user.user_id);
		let merchantDetail = await User.query()
			.select(
				'id',
				'restaurant_name',
				'user_status',
				'lat',
				'lng',
				'opening_status'
			)
			.where('id', checkCartProductStatus[0].merchant_id)
			.eager(
				'[merchant_cuisine,user_address_relation,merchant_characteristic_relation,merchants_delivery_charges, merchant_order_type, delivery_details]'
			)
			.modifyEager('delivery_details', (builder) => {
				return builder.select('delivery_note');
			})
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.modifyEager('user_address_relation', (builder) => {
				return builder.select('full_address', 'locality');
			})
			.modifyEager('merchant_characteristic_relation', (builder) => {
				return builder.select('opening_status', 'online_order_status');
			})
			.modifyEager('merchants_delivery_charges', (builder) => {
				return builder.select(
					'to',
					'from',
					'delivery_fee',
					'free_delivery_order',
					'minimum_order'
				);
			})
			.first()
			.catch((err) => {});
		//console.log(merchantDetail);
		//

		let Delivery = [];
		if (!merchantDetail || merchantDetail == undefined) {
			throw badRequestError(res, 'Your cart is empty');
		}

		if (merchantDetail.opening_status == 1) {
			merchantDetail.merchant_characteristic_relation.opening_status_message =
				'Merchant will be opening soon.';
		} else if (merchantDetail.opening_status == 2) {
			merchantDetail.merchant_characteristic_relation.opening_status_message =
				'Merchant Opened';
		} else if (merchantDetail.opening_status == 3) {
			merchantDetail.merchant_characteristic_relation.opening_status_message =
				'Currently Merchant is temprorily closed. Please order after some time.';
		} else if (merchantDetail.opening_status == 4) {
			merchantDetail.merchant_characteristic_relation.opening_status_message =
				'Merchant is permanently closed.';
		}

		if (merchantDetail.delivery_details == null) {
			merchantDetail.delivery_details = {
				delivery_note: '',
			};
		}

		merchantDetail.merchant_characteristic_relation.current_opening_status_message =
			'';
		merchantDetail.merchant_characteristic_relation.online_order_status_message =
			'';
		if (
			merchantDetail.merchant_characteristic_relation.opening_status == false
		) {
			merchantDetail.merchant_characteristic_relation.current_opening_status_message =
				'Merchant is currently closed. Please order once it’s open.';
		}
		if (
			merchantDetail.merchant_characteristic_relation.online_order_status ==
			false
		) {
			merchantDetail.merchant_characteristic_relation.online_order_status_message =
				'Merchant is not accepting online orders at this moment. Please try again later.';
		}

		if (
			checkCartProductStatus[0].order_type_id == 4 &&
			checkCartProductStatus != undefined
		) {
			if (body.lat == 0 && body.lng == 0) {
				Delivery = [];
			} else {
				//Calculate delivery charges on the basis of lat,lng
				//Distance Matrix Api

				let pickUp = merchantDetail.lat + ',' + merchantDetail.lng;
				console.log(merchantDetail.id);
				let dropOff = body.lat + ',' + body.lng;
				console.log(dropOff);
				let dist = await Distance.GetDistanceByLatLng(pickUp, dropOff);

				let km = Math.round(dist.data.distanceValue / 1000);
				//Calculate Delivery Charges For Delivery Type Order
				Delivery = await DeliveryCharges.query()
					.select('delivery_fee', 'minimum_order', 'free_delivery_order')
					.where((builder) => {
						return builder.where('from', '<=', km).andWhere('to', '>=', km);
					})
					.where('merchant_id', checkCartProductStatus[0].merchant_id);
				if (Delivery.length > 0) {
					Delivery[0].distance = km;
					if (Delivery[0].free_delivery_order == 0) {
						Delivery[0].is_free_delivery = false;
					} else {
						if (Delivery[0].free_delivery_order <= user_details.total_amount) {
							Delivery[0].is_free_delivery = true;
							Delivery[0].delivery_fee = 0;
						} else {
							Delivery[0].is_free_delivery = false;
						}
					}
				} else Delivery = [];
			}
		}
		merchantDetail.delivery = Delivery;
		merchantDetail.table_no =
			checkCartProductStatus[checkCartProductStatus.length - 1].table_no; //Adding Table Number

		return okResponse(
			res,
			{
				restaurantDetail: merchantDetail,
				cartDetail: checkCartProductStatus,
				total_amount: user_details.total_amount,
				totalquantity: user_details.totalquantity,
				total_cart_item: user_details.totalitems,
			},
			'Successfully fetched'
		);
	} catch (error) {
		console.log(error);
		if (error.code != undefined && error.code == 312) {
			throw errorresponse(
				res,
				{ code: 312 },
				'Restaurant doesn’t serves at your selected location.Choosing a different location will clear the items from the cart. Do you want to continue?',
				400
			);
		}

		throw badRequestError(res, error);
	}
};

//API not in use
const viewCartwithModifier = async (req, res) => {
	try {
		//let body = req.body;
		let userId = req.user.user_id;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		let query = Cart.query()
			.select(
				'id',
				'quantity',
				'product_id',
				Knex.raw(
					'(select ("selling_price") from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as product_price'
				)
			)
			.where({ user_id: userId })
			.eager('[cart_product_relation,cart_product_modifiers_relation]')
			.modifyEager('cart_product_relation', (builder) => {
				return builder.select(
					'product.product_name',
					'product.status',
					'product.delete_status'
				);
				// .eager('product_pricing_relation')
				// .modifyEager('product_pricing_relation', (builder) => {
				// 	return builder.select('selling_price');
				// });
			})
			//comment modifier condition
			.modifyEager('cart_product_modifiers_relation', (builder) => {
				return builder
					.select('modifier_id')
					.eager('[cart_modifier_relation,cart_modifier_detail_relation]')
					.modifyEager('cart_modifier_relation', (builder) => {
						return builder.select(
							'modifier_name',
							'min_user_must_select',
							'max_user_must_select'
						);
					})
					.modifyEager('cart_modifier_detail_relation', (builder) => {
						return builder
							.select('modifier_product_detail_id')
							.eager('cart_modifier_product_detail_relation')
							.modifyEager(
								'cart_modifier_product_detail_relation',
								(builder) => {
									return builder.select('modifier_product');
								}
							);
					});
			});
		checkCartProductStatus = await query.catch((err) => {});

		if (checkCartProductStatus == undefined || checkCartProductStatus == '') {
			throw badRequestError(res, 'Your cart is empty');
		}
		let subTotal = await Cart.query()
			.select(
				'merchant_id',
				Knex.raw(
					'(select sum("selling_price") from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId })
			.first();
		let merchantDetail = await User.query()
			.select('id', 'restaurant_name', 'user_status')
			.where('id', subTotal.merchant_id)
			.eager('[merchant_cuisine]')
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.first()
			.catch((err) => {});
		if (!merchantDetail || merchantDetail == undefined) {
			throw badRequestError(res, 'Your cart is empty');
		}

		return okResponse(
			res,
			{
				restaurantDetail: merchantDetail,
				cartDetail: checkCartProductStatus,
				subTotal: subTotal.sub_total,
			},
			'Successfully fetched'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};

//================view to Cart=================

// const removeItemFromCart = async (req, res) => {
// 	try {
// 		let id = +req.params.id;
// 		let userId = req.user.user_id;
// 		//console.log(req.user);return;
// 		let err, checkCartProductStatus, addItem;

// 		let query = await Cart.query()
// 			.select('id')
// 			.where({ user_id: userId, id: id })
// 			.first()
// 			.catch((err) => {
// 				console.log(err);
// 			});
// 		if (query) {
// 			let deleteCart = await Cart.query()
// 				.del()
// 				.where({ user_id: userId, id: id });
// 			if (deleteCart) {
// 				return okResponse(res, {}, 'Item is successfully removed from the cart');
// 			}
// 			throw badRequestError(
// 				res,
// 				'Something went wrong, please try again later'
// 			);
// 		}
// 		throw badRequestError(res, "Item not available in cart's item list");
// 	} catch (error) {
// 		throw badRequestError(res, error);
// 	}
// };

const removeItemFromCart = async (req, res) => {
	try {
		//let id = +req.params.id;
		let userId = req.user.user_id;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		let id = Array.from(req.body.id);

		//Delete Cart on the basis of Cart ID
		let query = await Cart.query()
			.select('id')
			.where({ user_id: userId })
			.whereIn('id', id)
			.catch((err) => {});

		if (query) {
			let deleteCart = await Cart.query()
				.del()
				.where({ user_id: userId })
				.whereIn('id', id);
			if (deleteCart) {
				let user_details = await calculateCartPrice(req.user.user_id);
				//Modifier Sum
				/*
				let modifier_data = await CartProductModifierDetail.query()
					.select('selling_price', 'modifier_quantity')
					.where('user_id', userId)
					.innerJoinRelation('cart_modifier_products_detail_relation');

				var totalmodifiersum = 0;
				if (modifier_data != undefined) {
					for (var i = 0; i < modifier_data.length; i++) {
						//totalquantity = totalquantity + subTotal[i].quantity;
						totalmodifiersum =
							totalmodifiersum +
							modifier_data[i].modifier_quantity *
								modifier_data[i].selling_price;
					}
				}
				//Product Price Sum
				let cart_data = await Cart.query()
					.select(
						'merchant_id',
						'quantity',
						Knex.raw(
							'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
						)
					)
					.where({ user_id: userId });

				var total = 0,
					sum = 0;
				var totalitems = 0;

				for (var i = 0; i < cart_data.length; i++) {
					totalitems = totalitems + cart_data[i].quantity;
					sum = parseFloat(cart_data[i].sub_total);
					total = total + cart_data[i].quantity * sum;
				}
				*/

				return okResponse(
					res,
					{
						totalquantity: user_details.totalquantity,
						total_cart_item: user_details.totalitems,
						total_amount: user_details.total_amount,
					},
					'Item is successfully removed from the cart'
				);
			}
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		throw badRequestError(res, "Item not available in cart's item list");
	} catch (error) {
		throw badRequestError(res, error);
	}
};
//================Remove item from Cart end=================
//API not in use
const removeCartAndAddScanItem = async (req, res) => {
	let body = req.body;
	let addItem, err;
	let userId = req.user.user_id;
	try {
		let deleteCart = await Cart.query()
			.del()
			.where('user_id', userId)
			.catch((err) => {});
		delete body.update_flag;
		if (body) {
			if (body.category_id == 0) {
				body.category_id = null;
			}
			let query = await Product.query()
				.select('id')
				.where({ id: body.product_id, delete_status: false, status: 1 });
			if (query == undefined) {
				//res, data, message, statusCode, code
				throw errorresponse(
					res,
					{ code: 401 },
					'Item Not available at the moment',
					400
				);
			}

			body.quantity = 1;
			body.user_id = userId;
			let relation_data = body.cart_product_modifiers_relation;

			if (relation_data != undefined && relation_data.length > 0) {
				for (var i = 0; i < relation_data.length; i++) {
					let modifierData = await Modifier.query()
						.select('min_user_must_select', 'max_user_must_select')
						.where('id', relation_data[i].modifier_id)
						.first();
					let count = relation_data[i].cart_modifier_detail_relation.length;
					if (modifierData.max_user_must_select == 0) {
					} else if (modifierData.min_user_must_select > count) {
						throw badRequestError(res, 'Please select modifier');
					} else if (modifierData.max_user_must_select < count) {
						throw badRequestError(res, 'You cannot select multiple modifiers');
					}
				}
			} else {
				let query = await Product.query()
					.select('id')
					.where({ id: body.product_id })
					.eager(
						'[product_modifier_get_relation,product_modifier_group_get_relation]'
					)
					.first();

				if (
					query.product_modifier_get_relation.length > 0 ||
					query.product_modifier_group_get_relation.length > 0
				) {
					throw badRequestError(res, 'Please add modifier(s)');
				}
			}

			body.user_id = userId;

			[err, addItem] = await to(
				transaction(Cart.knex(), (trx) => {
					return Cart.query(trx).insertGraph(body);
				})
			);

			if (addItem == undefined || addItem == '') {
				throw badRequestError(
					res,
					'Something went wrong, please try again later'
				);
			}

			let subTotal = await Cart.query()
				.select(
					'merchant_id',
					'quantity',

					Knex.raw(
						'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: req.user.user_id });

			var totalsum = 0,
				sum = 0;
			var totalquantity = 0;
			for (var i = 0; i < subTotal.length; i++) {
				totalquantity = totalquantity + subTotal[i].quantity;
				sum = parseFloat(subTotal[i].sub_total);
				totalsum = totalsum + sum;
			}

			let modifier_data = await CartProductModifierDetail.query()
				.select('selling_price', 'modifier_quantity')
				.where('user_id', req.user.user_id)
				.innerJoinRelation('cart_modifier_products_detail_relation');

			var totalmodifiersum = 0;
			if (modifier_data != undefined) {
				for (var i = 0; i < modifier_data.length; i++) {
					//totalquantity = totalquantity + subTotal[i].quantity;
					totalmodifiersum =
						totalmodifiersum +
						modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
				}
			}
			return okResponse(
				res,
				{
					totalquantity,
					total_amount: totalsum + totalmodifiersum,
					total_car_items: subTotal.length,
				},
				'Successfully added to the cart'
			);
		}
	} catch (error) {
		throw badRequestError(res, error);
	}
};
//Follow add to Cart ,runs First time if item present in the cart
const removeCartAndAddItem = async (req, res) => {
	try {
		let body = req.body;
		let addItem;
		let userId = req.user.user_id;
		let deleteCart = await Cart.query()
			.del()
			.where('user_id', userId)
			.catch((err) => {});

		if (body) {
			if (body.category_id == 0) {
				body.category_id = null;
			}
			let product = await Product.query()
				.select('requires_age_verification')
				.where('id', body.product_id)
				.first();

			if (product) {
				if (product.requires_age_verification) {
					//check years>18
					let dob = req.user.auth_user_relation.date_of_birth;

					let years = dob.getTime() - moment().valueOf();

					years = Math.abs(Math.floor(years / 31536000000));

					if (years < 18) {
						throw badRequestError(
							res,
							'You cannot Order this item due to age restrictions'
						);
					}
				}
			}
			delete body.cartId;
			delete body.flag;
			body.user_id = userId;
			// let relation_data = body.cart_product_modifiers_relation;
			// if (relation_data.length > 0) {
			// 	relation_data.forEach(async (data) => {
			// 		let modifierData = await Modifier.query()
			// 			.select('min_user_must_select', 'max_user_must_select')
			// 			.where('id', data.modifier_id)
			// 			.first();
			// 		let count = data.cart_modifier_detail_relation.length;
			// 		if (modifierData.min_user_must_select > count) {
			// 			throw badRequestError(res, 'Please select modifier');
			// 		}
			// 		if (modifierData.max_user_must_select < count) {
			// 			throw badRequestError(
			// 				res,
			// 				'You cannot select multiple modifiers'
			// 			);
			// 		}
			// 	});
			// } else {
			// 	let modifier = await ProductModifiers.query()
			// 		.select(
			// 			Knex.raw(
			// 				'(Select min_user_must_select where min_user_must_select>0)'
			// 			)
			// 		)
			// 		.innerJoinRelation('modifier')
			// 		.where('product_id', body.product_id);
			// 	if (modifier.length > 0) {
			// 		throw badRequestError(res, 'Please add modifier(s)');
			// 	}
			// }

			let relation_data = body.cart_product_modifiers_relation;

			if (relation_data != undefined && relation_data.length > 0) {
				for (var i = 0; i < relation_data.length; i++) {
					let modifierData = await Modifier.query()
						.select('min_user_must_select', 'max_user_must_select')
						.where('id', relation_data[i].modifier_id)
						.first();
					let count = relation_data[i].cart_modifier_detail_relation.length;
					if (modifierData.max_user_must_select == 0) {
					} else if (modifierData.min_user_must_select > count) {
						throw badRequestError(res, 'Please select modifier');
					} else if (modifierData.max_user_must_select < count) {
						throw badRequestError(res, 'You cannot select multiple modifiers');
					}
				}
			} else {
				let modifier = await ProductModifiers.query()
					.select(
						Knex.raw(
							'(Select min_user_must_select where min_user_must_select>0)'
						)
					)
					.innerJoinRelation('modifier')
					.where('product_id', body.product_id)
					.first();
				if (modifier != undefined && modifier.min_user_must_select != null) {
					throw badRequestError(res, 'Please add modifier(s)');
				}
			}

			addItem = await transaction(Cart.knex(), (trx) => {
				return Cart.query(trx).insertGraph(body);
			});
			/*
			let subTotal = await Cart.query()
				.select(
					'merchant_id',
					'quantity',

					Knex.raw(
						'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: req.user.user_id });

			var totalsum = 0,
				sum = 0;
			var totalquantity = 0;
			for (var i = 0; i < subTotal.length; i++) {
				totalquantity = totalquantity + subTotal[i].quantity;
				sum = parseFloat(subTotal[i].sub_total);
				totalsum = totalsum + sum;
			}

			let modifier_data = await CartProductModifierDetail.query()
				.select('selling_price', 'modifier_quantity')
				.where('user_id', req.user.user_id)
				.innerJoinRelation('cart_modifier_products_detail_relation');

			var totalmodifiersum = 0;
			if (modifier_data != undefined) {
				for (var i = 0; i < modifier_data.length; i++) {
					//totalquantity = totalquantity + subTotal[i].quantity;
					totalmodifiersum =
						totalmodifiersum +
						modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
				}
			}
			*/

			// return;
			let user_details = await calculateCartPrice(req.user.user_id);
			if (addItem) {
				return okResponse(
					res,
					{
						totalquantity: user_details.totalquantity,
						total_cart_item: user_details.totalitems,
						total_amount: user_details.total_amount,
						cartId: addItem.id,
						quantity: body.quantity,
						cart_product_modifiers_relation: [],
					},
					'Item Successfully added to the cart'
				);
			}
		}
		return okResponse(res, {}, 'Items successfully removed from cart');
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};
//================Your cart is empty and add new item=================

// const checkCartDetail = async (req, res) => {
//     try {
//         let body = req.body;
//         let userId = req.user.user_id;
//        //console.log(req.user);return;

//             if(body){
//                 body.user_id = userId;
//                 addItem =  await transaction(Cart.knex(), trx => {
//                     return (
//                         Cart.query(trx)
//                             .insertGraph(body)
//                     );
//                 });
//                 if(addItem){
//                     return okResponse(res, {}, "Item Successfully added to the cart");
//                 }
//             }
//             return okResponse(res, {}, "Items successfully removed from cart");
//     } catch (error) {
//         throw badRequestError(res, error);
//     }
// }

const CategoryList = async (req, res) => {
	//Demo api not in use
	try {
		// Knex.raw('(select count(\"product_id\") from \"product_category\" where \"category_id\" = \"categories\".\"id\" GROUP BY \"product_id\") as reviewcount')
		let CategoryList = await Category.query()
			.select('*')
			.eager('[category_product]')
			.modifyEager('category_product', (builder) => {
				return builder
					.select()
					.count('product_category.product_id')
					.groupBy(
						'product_category.product_id',
						'product_category.category_id',
						'product.id'
					);
			})
			.where({ merchant_id: 305, delete_status: false })
			// .where(builder => {
			//     if (req.query.search !== '') {
			//         return builder.whereRaw("LOWER(categories.category_name) LIKE '%' || LOWER(?) || '%'", req.query.search);
			//     }
			// })
			// .page(1 || 0, 10)
			.orderBy('created_at', 'desc');
		if (CategoryList) {
			CategoryList.map(function (value) {
				value.productCount = value.category_product.length;
			});
		}
		// console.log(CategoryList.toString()); return;
		return okResponse(
			res,
			CategoryList,
			'Congratulations! Category List fetched successfully!'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};
//================Test api for product count=================

//API not in use
const checkCartDetail = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;

		let ordered = body.ordered;
		//console.log(req.user);return;
		let sub_total = 0;
		let err, checkCartProductStatus, addItem;
		if (!body.merchant_id)
			throw badRequestError(res, 'Please pass the merchant id');
		if (!body.lat) throw badRequestError(res, 'Please pass the latitude');
		if (!body.lng) throw badRequestError(res, 'Please pass the longitude');
		if (body.distance == 0) {
			let minOrderValue = await DeliveryCharges.query()
				.min('minimum_order as minvalue')
				.where('merchant_id', body.merchant_id)
				.first();
			if (minOrderValue.minvalue > body.order_amount) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Minimum order for delivery is $' +
						minOrderValue.minvalue +
						'. Please add more items.',
					400
				);
			}
		} else {
			let Delivery = await DeliveryCharges.query()
				.select('minimum_order')
				.where((builder) => {
					return builder
						.where('from', '<=', body.distance)
						.andWhere('to', '>=', body.distance);
				})
				.where('merchant_id', body.merchant_id)
				.first();
			if (Delivery.minimum_order > body.order_amount) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Minimum order for delivery is $' +
						Delivery.minimum_order +
						'. Please add more items.',
					400
				);
			}
		}
		if (body.lat == 0 && body.lng == 0) {
		} else {
			let merchantDetail = await User.query()
				.select('lat', 'lng')
				.where('id', body.merchant_id)
				.first();
			let pickUp = merchantDetail.lat + ',' + merchantDetail.lng;
			let dropOff = body.lat + ',' + body.lng;
			let dist = await Distance.GetDistanceByLatLng(pickUp, dropOff);

			let km = Math.round(dist.data.distanceValue / 1000);

			Delivery = await DeliveryCharges.query()
				.select('delivery_fee', 'minimum_order', 'free_delivery_order')
				.where((builder) => {
					return builder.where('from', '<=', km).andWhere('to', '>=', km);
				})
				.where('merchant_id', body.merchant_id);
			if (Delivery.length > 0) {
			} else {
				Delivery = await DeliveryCharges.query()
					.select('to')
					.where('merchant_id', body.merchant_id)
					.orderBy('to', 'desc')
					.first();

				throw badRequestError(
					res,
					'Your delivery address is not within the service radius.Please choose another address within ' +
						Delivery.to +
						' km.'
				);
			}
		}

		for (let i = 0; i < ordered.length; i++) {
			let product = await Product.query()
				.select('status')
				.where({ id: ordered[i].product_id })
				.first();
			//console.log(product);
			if (product) {
				if (product.status != 1 || product.delete_status == true) {
					throw badRequestError(res, 'Item is unavailable');
				}
			} else {
				throw badRequestError(res, 'Oops, some error has occured');
			}
			sub_total = +ordered[i].price;
		}
		let query = await Cart.query()
			.select(
				Knex.raw(
					'(select sum("selling_price") from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId })
			.first();
		if (sub_total != query.sub_total) {
			throw badRequestError(
				res,
				'There is some modification in product please check cart details'
			);
		}

		return okResponse(res, {}, 'Item is available');
	} catch (err) {
		throw badRequestError(res, err);
	}
};
//================Check cart detail=================

//API not in use
const checkout_without_modifier = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		//let cart_id = body.cart_id;
		let total_price = body.totalprice;
		//console.log(body);
		let ordered = body.ordered;
		let price = 0;
		//let sub_total = 0;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		let amountdetail = 0;
		for (let i = 0; i < ordered.length; i++) {
			let product = await Product.query()
				.select('status', 'delete_status')
				.where({ id: ordered[i].product_id })
				.first();

			if (product) {
				if (product.status != 1 || product.delete_status == true) {
					throw badRequestError(
						res,
						'Some products are unavailable, please check the cart details.'
					);
				}
			} else {
				throw badRequestError(res, 'Oops, some error has occured');
			}
			//sub_total =+ ordered[i].price;

			let query = await Cart.query()
				.select(
					'quantity',
					Knex.raw(
						'(select "selling_price" from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: userId, id: ordered[i].cart_id })
				.first();
			if (ordered[i].price != query.sub_total) {
				throw badRequestError(res, 'Oops, price error has occured');
			}
			amountdetail = amountdetail + parseInt(query.sub_total) * query.quantity;
		}
		// let amountDetail = await Cart.query()
		// 	.select(
		// 		Knex.raw(
		// 			'(select sum("selling_price") from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."order_type_id") as sub_total'
		// 		)
		// 	)
		// 	.where({ user_id: userId })
		// 	.first();

		if (total_price != amountdetail) {
			throw badRequestError(
				res,
				'There is some modification in product please check cart details'
			);
		}

		return okResponse(res, { amountdetail }, 'Operation Successfull');
	} catch (err) {
		throw badRequestError(res, err);
	}
};

//CheckCart Detail Api
const checkout_with_modifier = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		//let cart_id = body.cart_id;
		let total_price = body.subTotal;
		//console.log(body);
		let ordered = body.ordered;
		let price = 0;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		console.log(body.lat);
		let Delivery = undefined;
		let amountdetail = 0;
		if (!body.merchant_id)
			throw badRequestError(res, 'Please pass the merchant id');

		//---------Check Merchant opening_status and Online Order Status-----------------
		let MerchantChar = await MerchantCharacteristic.query()
			.select('id', 'merchant_id', 'opening_status', 'online_order_status')
			.where({ merchant_id: body.merchant_id })
			.first();

		let MerchantGeneralDetails = await User.query()
			.select('id', 'opening_status')
			.where({ id: body.merchant_id })
			.first();

		if (MerchantGeneralDetails.opening_status == 1) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant will be opening soon.',
				400
			);
		} else if (MerchantGeneralDetails.opening_status == 3) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Currently Merchant is temprorily closed. Please order after some time.',
				400
			);
		} else if (MerchantGeneralDetails.opening_status == 4) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is permanently closed.',
				400
			);
		}

		if (MerchantChar.opening_status == false) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is currently closed. Please order once it’s open.',
				400
			);
		}

		if (MerchantChar.online_order_status == false) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is not accepting online orders at this moment. Please try again later.',
				400
			);
		}

		//---------Table Exist or not And It is availaible or not-------------------------
		let TableExist = null,
			TableOccupiedByOrNot = null;
		if (body.order_type_id == 5 && body.table_no) {
			// console.log(body.table_no, body.merchant_id)
			// if (!body.table_no)
			// 	throw badRequestError(res, 'Please pass the table no');

			TableExist = await Table.query()
				.select('id', 'table_number', 'merchant_id')
				.where({ table_number: body.table_no, merchant_id: body.merchant_id })
				.first();

			if (TableExist == undefined) {
				// throw badRequestError(res, "Table number not exist !");
				throw errorresponse(
					res,
					{ code: 300 },
					'Incorrect table number entered. Please double check and re-enter your table number or check with Wait staff.',
					400
				);
			}
			/*	else {
				// console.log(TableExist);
				TableOccupiedByOrNot = await order
					.query()
					.select('id', 'table_id', 'order_number', 'table_no')
					.where({ table_id: TableExist.id })
					.first();
				console.log(TableOccupiedByOrNot);
				if (TableOccupiedByOrNot) {
					// throw badRequestError(res, "Table is already occupied by some one else. Please choose different table.");
					throw errorresponse(
						res,
						{ code: 300 },
						'Entered Table is occupied by the Customer. Please enter a different table number.',
						400
					);
				}
			}
			*/
		}

		//If Order Type is DineIn(5,6) 1 will be present for sure
		if (body.order_type_id == 5 || body.order_type_id == 6) {
			let order_type_id = [1];
			order_type_id.push(body.order_type_id);

			let Order_type = await OrderType.query()
				.select('order_type_id')
				.where('merchant_id', body.merchant_id)
				.whereIn('order_type_id', order_type_id);

			if (Order_type.length == 0 || Order_type.length == 1) {
				throw errorresponse(
					res,
					{ code: 301 },
					'Merchant Currently Not Accepting Dine-In Orders',
					400
				);
			}
		} else {
			let Order_type = await OrderType.query()
				.select('order_type_id')
				.where('merchant_id', body.merchant_id)
				.where('order_type_id', body.order_type_id);

			if (Order_type.length == 0) {
				//Pick-Up Orders
				if (body.order_type_id == 2) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Merchant Currently Not Accepting PickUp Orders',
						400
					);
				}
				// delivery Orders
				if (body.order_type_id == 4) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Merchant Currently Not Accepting Delivery Orders',
						400
					);
				}
			}
		}
		//Check Delivery Object in case of Delivery Orders
		if (body.order_type_id == 4) {
			if (body.lat == 0 && body.lng == 0) {
			} else {
				let merchantDetail = await User.query()
					.select('lat', 'lng')
					.where('id', body.merchant_id)
					.first();
				let pickUp = merchantDetail.lat + ',' + merchantDetail.lng;
				let dropOff = body.lat + ',' + body.lng;
				let dist = await Distance.GetDistanceByLatLng(pickUp, dropOff);

				let km = Math.round(dist.data.distanceValue / 1000);

				Delivery = await DeliveryCharges.query()
					.select('delivery_fee', 'minimum_order', 'free_delivery_order')
					.where((builder) => {
						return builder.where('from', '<=', km).andWhere('to', '>=', km);
					})
					.where('merchant_id', body.merchant_id);
				if (Delivery.length > 0) {
				} else {
					Delivery = await DeliveryCharges.query()
						.select('to')
						.where('merchant_id', body.merchant_id)
						.orderBy('to', 'desc')
						.first();

					throw errorresponse(
						res,
						{ code: 300 },
						'Your delivery address is not within the service radius.Please choose another address within ' +
							Delivery.to +
							' km.',
						400
					);
				}
			}
			console.log(body.distance);
			if (body.distance == 0) {
				/*
			let minOrderValue = await DeliveryCharges.query()
				.min('minimum_order as minvalue')
				.where('merchant_id', body.merchant_id)
				.first();
			if (
				minOrderValue != undefined &&
				minOrderValue.minvalue > body.subTotal
			) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Minimum order for delivery is $' + minOrderValue.minvalue + '. Please add more items.',
					400
				);
			}
			*/
			} else {
				Delivery = await DeliveryCharges.query()
					.select('minimum_order')
					.where((builder) => {
						return builder
							.where('from', '<=', body.distance)
							.andWhere('to', '>=', body.distance);
					})
					.where('merchant_id', body.merchant_id)
					.first();
				if (Delivery != undefined && Delivery.minimum_order > body.subTotal) {
					throw errorresponse(
						res,
						{ code: 300 },
						'Minimum order for delivery is $' +
							Delivery.minimum_order +
							'. Please add more items.',
						400
					);
				}
			}
		}

		for (let i = 0; i < ordered.length; i++) {
			let product = await Product.query()
				.select('status')
				.where({ id: ordered[i].product_id, delete_status: false })
				.first();

			if (product) {
				if (product.status != 1) {
					throw badRequestError(res, 'Item is not available at the moment');
				}
			} else {
				throw badRequestError(res, 'Item is not available at the moment');
			}
			let query = await Cart.query()
				.select(
					'quantity',
					Knex.raw(
						'(select "selling_price" from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: userId, id: ordered[i].cart_id })
				.first();
			if (ordered[i].price != query.sub_total)
				throw badRequestError(
					res,
					'There is some modification in product please check cart details'
				);

			amountdetail = amountdetail + parseInt(query.sub_total) * query.quantity;
			/*
			let modifier_length = ordered[i].cart_product_modifiers_relation.length;
			if (modifier_length > 0) {
				for (var j = 0; j < modifier_length; j++) {
					let modifier = await ModifierProductDetails.query()
						.select('selling_price')
						.where(
							'id',
							ordered[i].cart_product_modifiers_relation[j]
								.modifier_product_detail_id
						)
						.first();
					console.log(modifier);
					if (modifier) {
						if (
							modifier.selling_price !=
							ordered[i].cart_product_modifiers_relation[j].price
						)
							throw badRequestError(
								res,
								'Modifier Oops, price error has occured'
							);

						price = price + query.quantity * modifier.selling_price;
					}
				}
			}
			amountdetail = amountdetail + price;
			*/
		}
		/*
		if (amountdetail != total_price) {
			throw badRequestError(
				res,
				'There is some modification in product please check cart details'
			);
		}
		*/

		let responseData;
		if (TableExist == null && TableOccupiedByOrNot == null) {
			responseData = {
				table_id: null,
				table_no: null,
			};
		} else {
			responseData = {
				table_id: TableExist.id,
				table_no: TableExist.table_number,
			};
		}

		return okResponse(res, responseData, 'Operation Successfull');
	} catch (err) {
		//console.log(err);
		if (err.code != undefined && err.code == 312) {
			throw errorresponse(
				res,
				{ code: 312 },
				'Restaurant doesn’t serves at your selected location.Choosing a different location will clear the items from the cart. Do you want to continue?',
				400
			);
		}

		throw badRequestError(res, err);
	}
};

const checkout_with_modifier_v2 = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		//let cart_id = body.cart_id;
		let total_price = body.total_amount;
		//console.log(body);
		let ordered = body.ordered;
		let price = 0;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		//	console.log(body.lat);
		let Delivery = undefined;
		let amountdetail = 0;
		if (!body.merchant_id)
			throw badRequestError(res, 'Please pass the merchant id');

		//---------Check Merchant opening_status and Online Order Status-----------------
		let MerchantChar = await MerchantCharacteristic.query()
			.select('id', 'merchant_id', 'opening_status', 'online_order_status')
			.where({ merchant_id: body.merchant_id })
			.first();

		let MerchantGeneralDetails = await User.query()
			.select('id', 'opening_status')
			.where({ id: body.merchant_id })
			.first();

		if (MerchantGeneralDetails.opening_status == 1) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant will be opening soon.',
				400
			);
		} else if (MerchantGeneralDetails.opening_status == 3) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Currently Merchant is temprorily closed. Please order after some time.',
				400
			);
		} else if (MerchantGeneralDetails.opening_status == 4) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is permanently closed.',
				400
			);
		}

		if (MerchantChar.opening_status == false) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is currently closed. Please order once it’s open.',
				400
			);
		}

		if (MerchantChar.online_order_status == false) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is not accepting online orders at this moment. Please try again later.',
				400
			);
		}

		//---------Table Exist or not And It is availaible or not-------------------------
		let TableExist = null,
			TableOccupiedByOrNot = null;
		if (body.order_type_id == 5 && body.table_no) {
			// console.log(body.table_no, body.merchant_id)
			// if (!body.table_no)
			// 	throw badRequestError(res, 'Please pass the table no');

			TableExist = await Table.query()
				.select('id', 'table_number', 'merchant_id')
				.where({ table_number: body.table_no, merchant_id: body.merchant_id })
				.first();

			if (TableExist == undefined) {
				// throw badRequestError(res, "Table number not exist !");
				throw errorresponse(
					res,
					{ code: 300 },
					'Incorrect table number entered. Please double check and re-enter your table number or check with Wait staff.',
					400
				);
			}
			/*
			else {
				// console.log(TableExist);
				TableOccupiedByOrNot = await order
					.query()
					.select('id', 'table_id', 'order_number', 'table_no')
					.where({ table_id: TableExist.id })
					.first();
				console.log(TableOccupiedByOrNot);
				if (TableOccupiedByOrNot) {
					// throw badRequestError(res, "Table is already occupied by some one else. Please choose different table.");
					throw errorresponse(
						res,
						{ code: 300 },
						'Entered Table is occupied by the Customer. Please enter a different table number.',
						400
					);
				}
			}
			*/
		}

		//If Order Type is DineIn(5,6) 1 will be present for sure
		if (body.order_type_id == 5 || body.order_type_id == 6) {
			let order_type_id = [1];
			order_type_id.push(body.order_type_id);

			let Order_type = await OrderType.query()
				.select('order_type_id')
				.where('merchant_id', body.merchant_id)
				.whereIn('order_type_id', order_type_id);

			if (Order_type.length == 0 || Order_type.length == 1) {
				throw errorresponse(
					res,
					{ code: 301 },
					'Merchant Currently Not Accepting Dine-In Orders',
					400
				);
			}
		} else {
			let Order_type = await OrderType.query()
				.select('order_type_id')
				.where('merchant_id', body.merchant_id)
				.where('order_type_id', body.order_type_id);

			if (Order_type.length == 0) {
				//Pick-Up Orders
				if (body.order_type_id == 2) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Merchant Currently Not Accepting PickUp Orders',
						400
					);
				}
				// delivery Orders
				if (body.order_type_id == 4) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Merchant Currently Not Accepting Delivery Orders',
						400
					);
				}
			}
		}
		//Check Delivery Object in case of Delivery Orders
		if (body.order_type_id == 4) {
			if (body.lat == 0 && body.lng == 0) {
			} else {
				let merchantDetail = await User.query()
					.select('lat', 'lng')
					.where('id', body.merchant_id)
					.first();
				let pickUp = merchantDetail.lat + ',' + merchantDetail.lng;
				let dropOff = body.lat + ',' + body.lng;
				let dist = await Distance.GetDistanceByLatLng(pickUp, dropOff);
				console.log(dist, 'dist');
				let km = Math.round(dist.data.distanceValue / 1000);

				Delivery = await DeliveryCharges.query()
					.select('delivery_fee', 'minimum_order', 'free_delivery_order')
					.where((builder) => {
						return builder.where('from', '<=', km).andWhere('to', '>=', km);
					})
					.where('merchant_id', body.merchant_id);
				if (Delivery.length > 0) {
				} else {
					Delivery = await DeliveryCharges.query()
						.select('to')
						.where('merchant_id', body.merchant_id)
						.orderBy('to', 'desc')
						.first();

					throw errorresponse(
						res,
						{ code: 300 },
						'Your delivery address is not within the service radius.Please choose another address within ' +
							Delivery.to +
							' km.',
						400
					);
				}
			}
			console.log(body.distance);
			if (body.distance != undefined) {
				Delivery = await DeliveryCharges.query()
					.select('minimum_order')
					.where((builder) => {
						return builder
							.where('from', '<=', body.distance)
							.andWhere('to', '>=', body.distance);
					})
					.where('merchant_id', body.merchant_id)
					.first();
				console.log(Delivery.minimum_order);
				if (
					Delivery != undefined &&
					Delivery.minimum_order > parseFloat(body.subTotal)
				) {
					console.log('DeliveryError');
					throw errorresponse(
						res,
						{ code: 300 },
						'Miniumum order for delivery is $' +
							parseFloat(Delivery.minimum_order) +
							'.Please add more items.',
						400
					);
				}
			}
		}
		for (let i = 0; i < ordered.length; i++) {
			let product = await Product.query()
				.select('status', 'product_quantity_counter')
				.where({ id: ordered[i].product_id, delete_status: false })
				.first();

			if (product) {
				if (product.status != 1) {
					throw errorresponse(
						res,
						{ code: 300 },
						'Item you are looking for is out of Stock.Please Come back latter',
						400
					);
				}
			} else {
				throw errorresponse(
					res,
					{ code: 300 },
					'Item you are looking for is out of Stock.Please Come back latter',
					400
				);
			}

			let query = await Cart.query()
				.select(
					'quantity',
					Knex.raw(
						'(select "selling_price" from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: userId, id: ordered[i].cart_id })
				.first();
			console.log(query);
			//

			console.log(typeof query.quantity, query.quantity);
			if (
				parseInt(product.product_quantity_counter) < parseInt(query.quantity)
			) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Item you are looking for is out of Stock.Please Come back latter',
					400
				);
			}
			if (ordered[i].price != query.sub_total) {
				throw errorresponse(
					res,
					{ code: 300 },
					'There is some modification in product please check cart details',
					400
				);
			}

			amountdetail = amountdetail + parseInt(query.sub_total) * query.quantity;
			console.log(amountdetail, 'amountdetail');
			/*
			let modifier_length = ordered[i].cart_product_modifiers_relation.length;
			if (modifier_length > 0) {
				for (var j = 0; j < modifier_length; j++) {
					let modifier = await ModifierProductDetails.query()
						.select('selling_price')
						.where(
							'id',
							ordered[i].cart_product_modifiers_relation[j]
								.modifier_product_detail_id
						)
						.first();
					console.log(modifier);
					if (modifier) {
						if (
							modifier.selling_price !=
							ordered[i].cart_product_modifiers_relation[j].price
						)
							throw badRequestError(
								res,
								'Modifier Oops, price error has occured'
							);

						price = price + query.quantity * modifier.selling_price;
					}
				}
			}
			amountdetail = amountdetail + price;
			*/
		}
		/*
		if (amountdetail != total_price) {
			throw badRequestError(
				res,
				'There is some modification in product please check cart details'
			);
		}
		*/
		const stripe = require('./../../middlewares/stripe');
		let sercretKey = await stripe.createPaymentForSecretKey(
			body.total_amount,
			'nzd',
			body.stripeCustomerId,
			'card'
		);

		let responseData;
		if (TableExist == null && TableOccupiedByOrNot == null) {
			responseData = {
				secretKey: sercretKey,
				table_id: null,
				table_no: null,
			};
		} else {
			responseData = {
				secretKey: sercretKey,
				table_id: TableExist.id,
				table_no: TableExist.table_number,
			};
		}

		return okResponse(res, responseData, 'Operation Successfull');
	} catch (err) {
		console.log(err);
		if (err.code != undefined && err.code == 312) {
			throw errorresponse(
				res,
				{ code: 312 },
				'Restaurant doesn’t serves at your selected location.Choosing a different location will clear the items from the cart. Do you want to continue?',
				400
			);
		}

		throw badRequestError(res, err);
	}
};

const placeOrder = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		let err, stripeData;
		if (!body.transaction_id) {
			throw badRequestError(res, 'Required parameter not found');
		}

		for (let i = 0; i < body.ordered_products_relation.length; i++) {
			//Get Ordered Product Relation
			let product_data = body.ordered_products_relation[i];
			//Get Product if Avaliable
			let product = await Product.query()
				.select(
					'product_name',
					'product_threshold_quantity',
					'product_quantity_counter',
					'products_per_order_limit'
				)
				.where({
					id: product_data.product_id,
					delete_status: false,
					status: 1,
				})
				.first();

			if (product) {
				//Item is Out of Stock if available Quantity is Less Than Order Quantity
				if (
					product.product_quantity_counter <
					product_data.ordered_product_quantity
				) {
					throw badRequestError(
						res,
						'Item you are looking for is Out Of Stock. Please come back later'
					);
				}
			}
			//If Merchant off the status of the product
			else {
				throw badRequestError(
					res,
					'Item you are looking for is Out Of Stock. Please come back later'
				);
			}

			//	console.log(typeof new Date().toISOString);

			/*		let ordered_product = await OrderedProducts.query()
				.sum('ordered_product_quantity')
				.where(function () {
					this.whereRaw('"created_at"::date = ?', [
						moment().format('YYYY-MM-DD'),
					]);
				})
				.andWhere('product_id', product_data.product_id)
				.first();

			if (ordered_product.sum == null) {
				ordered_product.sum = 0;
			}

			if (product) {
				if (
					product.product_quantity_counter - ordered_product.sum ==
					product.product_threshold_quantity
				) {
					let date = new Date().toISOString;
					await Product.query()
						.update({
							product_threshold_date: date,
						})
						.where({
							id: product_data.product_id,
							delete_status: false,
							status: 1,
						})
						.first();
				}
				if (
					product.product_quantity_counter - ordered_product.sum <
					product_data.ordered_product_quantity
				) {
					throw badRequestError(res, 'Out of Stock');
				}
				if (
					product.products_per_order_limit != 0 &&
					product.products_per_order_limit <
						product_data.ordered_product_quantity
				) {
					throw badRequestError(
						res,
						'You cannot Order product ' +
							product.product_name +
							' more than ' +
							product.products_per_order_limit
					);
				}
			} else {
				throw badRequestError(res, 'Item not available at the moment');
			}
			*/
		}
		//stripe
		const stripe = require('./../../middlewares/stripe');
		body.total_amount = (+body.total_amount).toFixed(2);
		let stripeAmount = body.total_amount * 100;
		stripeAmount = parseInt((+stripeAmount).toFixed(2));
		[err, stripeData] = await to(
			stripe.createPayment({
				amount: stripeAmount,
				tknId: body.transaction_id,
				currency: 'usd',
			})
		);
		//console.log(stripeData);

		if (err) {
			throw badRequestError(res, err.message);
		}
		body.transaction_id = stripeData.data.txnId;
		body.user_id = userId;
		body.order_status = 1;

		const Order = await order
			.query()
			.select('id')
			.orderBy('id', 'desc')
			.first();
		//console.log(Order);
		if (Order) {
			body.order_number = 'FAV' + Order.id;
		} else {
			body.order_number = 'FAV1';
		}
		//console.log(body.order_number);

		// return;

		//Order Entry in table
		const addItem = await transaction(order.knex(), (trx) => {
			return order.query(trx).insertGraph(body);
		});
		if (addItem) {
			const delete_cart = await Cart.query().delete().where('user_id', userId);

			//--------------------------(START) Code to send order details on mail-----------------------------
			let orderDetails = body;
			let MerchantInfo = await User.query()
				.select('restaurant_name', 'mobile', 'country_isd_code')
				.eager('[user_address_relation, merchant_characteristic_relation]')
				.modifyEager('user_address_relation', (builder) => {
					return builder.select('full_address');
				})
				.modifyEager('merchant_characteristic_relation', (builder) => {
					return builder.select('gst_number');
				})
				.modifyEager('merchant_characteristic_relation', (builder) => {
					return builder.select('gst_number');
				})
				.where('id', orderDetails.merchant_id)
				.first();

			let CustomerInfo = await User.query()
				.select(
					'first_name',
					'last_name',
					'mobile',
					'country_isd_code',
					'email'
				)
				.eager('user_address_relation')
				.modifyEager('user_address_relation', (builder) => {
					return builder.select('full_address');
				})
				.where('id', userId)
				.first();

			let orderType;
			if (
				orderDetails.order_type_id == 1 ||
				orderDetails.order_type_id == 5 ||
				orderDetails.order_type_id == 6
			) {
				orderType = 'DineIn';
			} else if (orderDetails.order_type_id == 2) {
				orderType = 'Pickup';
			} else if (orderDetails.order_type_id == 4) {
				orderType = 'Delivery';
			}

			let table;
			if (orderDetails.table_no && orderDetails.table_no != '') {
				table = orderDetails.table_no;
			} else {
				table = 'N/A';
			}

			let sum;
			for (let i = 0; i < orderDetails.ordered_products_relation.length; i++) {
				sum = 0;
				let product_id = orderDetails.ordered_products_relation[i].product_id;
				let quantity =
					orderDetails.ordered_products_relation[i].ordered_product_quantity;

				let product = await Product.query()
					.select('id', 'product_quantity_counter')
					.where({ id: product_id, delete_status: false, status: 1 })
					.first();

				if (product) {
					if (product.product_quantity_counter - quantity <= 0) {
						//Updating Product Quantity when order placed
						let update_product = await Product.query()
							.update({
								product_quantity_counter: 0,
								status: 2,
							})
							.where({ id: product.id, delete_status: false, status: 1 });
					} else {
						let update_product = await Product.query()
							.update({
								product_quantity_counter:
									product.product_quantity_counter - quantity,
							})
							.where({ id: product.id, delete_status: false, status: 1 });
					}
				}

				for (
					let j = 0;
					j <
					orderDetails.ordered_products_relation[i].products_customization
						.length;
					j++
				) {
					let modifiers = await Modifier.query()
						.select('modifier_name', 'display_name')
						.where(
							'id',
							orderDetails.ordered_products_relation[i].products_customization[
								j
							].modifier_id
						)
						.first();
					// console.log(modifiers)
					if (modifiers.display_name != '') {
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].modifier_name = modifiers.display_name;
					} else {
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].modifier_name = modifiers.modifier_name;
					}
					for (
						let k = 0;
						k <
						orderDetails.ordered_products_relation[i].products_customization[j]
							.order_product_modifier_detail.length;
						k++
					) {
						let modifierPrices = await ModifiersProduct.query()
							.select('modifier_product', 'selling_price')
							.where(
								'id',
								orderDetails.ordered_products_relation[i]
									.products_customization[j].order_product_modifier_detail[k]
									.modifier_product_detail_id
							)
							.first();
						// console.log(modifierPrices)
						sum =
							sum +
							modifierPrices.selling_price *
								orderDetails.ordered_products_relation[i]
									.products_customization[j].order_product_modifier_detail[k]
									.modifier_quantity;
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].order_product_modifier_detail[k].modifier_product =
							modifierPrices.modifier_product;
					}
				}
				// orderDetails.ordered_products_relation[i].PriceWithModifiers = sum + orderDetails.ordered_products_relation[i].product_price;
				//orderDetails.ordered_products_relation[i].PriceWithModifiers = sum + (orderDetails.ordered_products_relation[i].product_price * orderDetails.ordered_products_relation[i].ordered_product_quantity);
				orderDetails.ordered_products_relation[i].PriceWithModifiers =
					sum +
					orderDetails.ordered_products_relation[i].product_price *
						parseInt(
							orderDetails.ordered_products_relation[i].ordered_product_quantity
						);
				console.log(
					orderDetails.ordered_products_relation[i].PriceWithModifiers,
					orderDetails.ordered_products_relation[i].product_price,
					orderDetails.ordered_products_relation[i].ordered_product_quantity
				);
			}

			let orderDetailsToSendEmail = {
				email: CustomerInfo.email,
				merchant_name: MerchantInfo.restaurant_name,
				merchant_mobile: MerchantInfo.country_isd_code + MerchantInfo.mobile,
				merchant_address: MerchantInfo.user_address_relation[0].full_address,
				customer_name:
					CustomerInfo.first_name + ' ' + CustomerInfo.last_name.charAt(0),
				customer_mobile: CustomerInfo.country_isd_code + CustomerInfo.mobile,
				customer_address: body.delivery_address,
				order_number: body.order_number,
				table: table,
				orderType: orderType,
				order_type_id: body.order_type_id,
				orderStatus: 'Placed',
				paymentStatus: 'Paid',
				order_date: moment().format('YYYY-MM-DD HH:mm'),
				comment: body.comment,
				order_amount: body.order_amount,
				delivery_fee: body.deliver_charges,
				total_amount: body.total_amount,
				gst_number: MerchantInfo.merchant_characteristic_relation.gst_number,
				ordered_products: orderDetails.ordered_products_relation,
			};
			//console.log(body);

			let orderStatusFlowMaintain = await OrderStatusFlowManagement.query()
				.insert({ order_id: addItem.id, order_status: 1 })
				.returning('*');
			if (!orderStatusFlowMaintain)
				throw badRequestError(res, 'Something Went Wrong.');
			if (orderDetailsToSendEmail.email != '') {
				EMAIL.sendOrderDetails(orderDetailsToSendEmail);
			}
			//--------------------------(END) Code to send order details on mail-----------------------------
			console.log(`OrderSocketGet${body.merchant_id}`, 'evnetName');
			req.io.emit(`OrderSocketGet${body.merchant_id}`, {
				order_type_id: body.order_type_id,
			}); //event emit for socket notification admin
			return okResponse(res, {}, 'Ordered successfully placed');
		}
	} catch (err) {
		throw badRequestError(res, err);
	}
};

const placeOrder_v2 = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		let err, stripeData;
		// if (!body.transaction_id) {
		// 	throw badRequestError(res, 'Required parameter not found');
		// }

		for (let i = 0; i < body.ordered_products_relation.length; i++) {
			//Get Ordered Product Relation
			let product_data = body.ordered_products_relation[i];
			//Get Product if Avaliable
			let product = await Product.query()
				.select(
					'product_name',
					'product_threshold_quantity',
					'product_quantity_counter',
					'products_per_order_limit'
				)
				.where({
					id: product_data.product_id,
					delete_status: false,
					status: 1,
				})
				.first();

			if (product) {
				//Item is Out of Stock if available Quantity is Less Than Order Quantity
				if (
					product.product_quantity_counter <
					product_data.ordered_product_quantity
				) {
					throw badRequestError(
						res,
						'looking for is Out Of Stock. Please come back later'
					);
				}
			}
			//If Merchant off the status of the product
			else {
				throw badRequestError(
					res,
					'Item you are looking for is Out Of Stock. Please come back later'
				);
			}

			//	console.log(typeof new Date().toISOString);

			/*		let ordered_product = await OrderedProducts.query()
				.sum('ordered_product_quantity')
				.where(function () {
					this.whereRaw('"created_at"::date = ?', [
						moment().format('YYYY-MM-DD'),
					]);
				})
				.andWhere('product_id', product_data.product_id)
				.first();

			if (ordered_product.sum == null) {
				ordered_product.sum = 0;
			}

			if (product) {
				if (
					product.product_quantity_counter - ordered_product.sum ==
					product.product_threshold_quantity
				) {
					let date = new Date().toISOString;
					await Product.query()
						.update({
							product_threshold_date: date,
						})
						.where({
							id: product_data.product_id,
							delete_status: false,
							status: 1,
						})
						.first();
				}
				if (
					product.product_quantity_counter - ordered_product.sum <
					product_data.ordered_product_quantity
				) {
					throw badRequestError(res, 'Out of Stock');
				}
				if (
					product.products_per_order_limit != 0 &&
					product.products_per_order_limit <
						product_data.ordered_product_quantity
				) {
					throw badRequestError(
						res,
						'You cannot Order product ' +
							product.product_name +
							' more than ' +
							product.products_per_order_limit
					);
				}
			} else {
				throw badRequestError(res, 'Item not available at the moment');
			}
			*/
		}
		//stripe
		const stripe = require('./../../middlewares/stripe');
		body.total_amount = (+body.total_amount).toFixed(2);
		// let stripeAmount = body.total_amount * 100;
		// stripeAmount = parseInt((+stripeAmount).toFixed(2));
		let payment_confirm;
		console.log(body.payment_method);
		payment_confirm = await stripe.paymentConfirm(body.payment_method);
		console.log(payment_confirm);
		body.transaction_id = payment_confirm;
		body.user_id = userId;
		body.order_status = 1;
		const Order = await order
			.query()
			.select('id')
			.orderBy('id', 'desc')
			.first();
		//console.log(Order);
		if (Order) {
			body.order_number = 'FAV' + Order.id;
		} else {
			body.order_number = 'FAV1';
		}
		//console.log(body.order_number);

		// return;

		//Order Entry in table
		delete body.payment_method;
		const addItem = await transaction(order.knex(), (trx) => {
			return order
				.query(trx)
				.insertGraph(body)
				.catch((err) => {
					console.log(err);
				});
		});
		if (addItem) {
			const delete_cart = await Cart.query().delete().where('user_id', userId);

			//--------------------------(START) Code to send order details on mail-----------------------------
			let orderDetails = body;
			let MerchantInfo = await User.query()
				.select('restaurant_name', 'mobile', 'country_isd_code')
				.eager('[user_address_relation, merchant_characteristic_relation]')
				.modifyEager('user_address_relation', (builder) => {
					return builder.select('full_address');
				})
				.modifyEager('merchant_characteristic_relation', (builder) => {
					return builder.select('gst_number');
				})
				.modifyEager('merchant_characteristic_relation', (builder) => {
					return builder.select('gst_number');
				})
				.where('id', orderDetails.merchant_id)
				.first();

			let CustomerInfo = await User.query()
				.select(
					'first_name',
					'last_name',
					'mobile',
					'country_isd_code',
					'email'
				)
				.eager('user_address_relation')
				.modifyEager('user_address_relation', (builder) => {
					return builder.select('full_address');
				})
				.where('id', userId)
				.first();

			let orderType;
			if (
				orderDetails.order_type_id == 1 ||
				orderDetails.order_type_id == 5 ||
				orderDetails.order_type_id == 6
			) {
				orderType = 'DineIn';
			} else if (orderDetails.order_type_id == 2) {
				orderType = 'Pickup';
			} else if (orderDetails.order_type_id == 4) {
				orderType = 'Delivery';
			}

			let table;
			if (orderDetails.table_no && orderDetails.table_no != '') {
				table = orderDetails.table_no;
			} else {
				table = 'N/A';
			}

			let sum;
			for (let i = 0; i < orderDetails.ordered_products_relation.length; i++) {
				sum = 0;
				let product_id = orderDetails.ordered_products_relation[i].product_id;
				let quantity =
					orderDetails.ordered_products_relation[i].ordered_product_quantity;

				let product = await Product.query()
					.select('id', 'product_quantity_counter')
					.where({ id: product_id, delete_status: false, status: 1 })
					.first();

				if (product) {
					if (product.product_quantity_counter - quantity <= 0) {
						//Updating Product Quantity when order placed
						let update_product = await Product.query()
							.update({
								product_quantity_counter: 0,
								status: 2,
							})
							.where({ id: product.id, delete_status: false, status: 1 });
					} else {
						let update_product = await Product.query()
							.update({
								product_quantity_counter:
									product.product_quantity_counter - quantity,
							})
							.where({ id: product.id, delete_status: false, status: 1 });
					}
				}

				for (
					let j = 0;
					j <
					orderDetails.ordered_products_relation[i].products_customization
						.length;
					j++
				) {
					let modifiers = await Modifier.query()
						.select('modifier_name', 'display_name')
						.where(
							'id',
							orderDetails.ordered_products_relation[i].products_customization[
								j
							].modifier_id
						)
						.first();
					// console.log(modifiers)
					if (modifiers.display_name != '') {
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].modifier_name = modifiers.display_name;
					} else {
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].modifier_name = modifiers.modifier_name;
					}
					for (
						let k = 0;
						k <
						orderDetails.ordered_products_relation[i].products_customization[j]
							.order_product_modifier_detail.length;
						k++
					) {
						let modifierPrices = await ModifiersProduct.query()
							.select('modifier_product', 'selling_price')
							.where(
								'id',
								orderDetails.ordered_products_relation[i]
									.products_customization[j].order_product_modifier_detail[k]
									.modifier_product_detail_id
							)
							.first();
						// console.log(modifierPrices)
						sum =
							sum +
							modifierPrices.selling_price *
								orderDetails.ordered_products_relation[i]
									.products_customization[j].order_product_modifier_detail[k]
									.modifier_quantity;
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].order_product_modifier_detail[k].modifier_product =
							modifierPrices.modifier_product;
					}
				}
				// orderDetails.ordered_products_relation[i].PriceWithModifiers = sum + orderDetails.ordered_products_relation[i].product_price;
				//orderDetails.ordered_products_relation[i].PriceWithModifiers = sum + (orderDetails.ordered_products_relation[i].product_price * orderDetails.ordered_products_relation[i].ordered_product_quantity);
				orderDetails.ordered_products_relation[i].PriceWithModifiers =
					sum +
					orderDetails.ordered_products_relation[i].product_price *
						parseInt(
							orderDetails.ordered_products_relation[i].ordered_product_quantity
						);
			}

			let orderDetailsToSendEmail = {
				email: CustomerInfo.email,
				merchant_name: MerchantInfo.restaurant_name,
				merchant_mobile: MerchantInfo.country_isd_code + MerchantInfo.mobile,
				merchant_address: MerchantInfo.user_address_relation[0].full_address,
				customer_name:
					CustomerInfo.first_name + ' ' + CustomerInfo.last_name.charAt(0),
				customer_mobile: CustomerInfo.country_isd_code + CustomerInfo.mobile,
				customer_address: body.delivery_address,
				order_number: body.order_number,
				table: table,
				orderType: orderType,
				order_type_id: body.order_type_id,
				orderStatus: 'Placed',
				paymentStatus: 'Paid',
				order_date: moment().format('YYYY-MM-DD HH:mm'),
				comment: body.comment,
				order_amount: body.order_amount,
				delivery_fee: body.deliver_charges,
				total_amount: body.total_amount,
				gst_number: MerchantInfo.merchant_characteristic_relation.gst_number,
				ordered_products: orderDetails.ordered_products_relation,
			};
			//console.log(body);

			let orderStatusFlowMaintain = await OrderStatusFlowManagement.query()
				.insert({ order_id: addItem.id, order_status: 1 })
				.returning('*');
			if (!orderStatusFlowMaintain)
				throw badRequestError(res, 'Something Went Wrong.');
			if (orderDetailsToSendEmail.email != '') {
				EMAIL.sendOrderDetails(orderDetailsToSendEmail);
			}
			//--------------------------(END) Code to send order details on mail-----------------------------
			console.log(`OrderSocketGet${body.merchant_id}`, 'evnetName');
			req.io.emit(`OrderSocketGet${body.merchant_id}`, {
				order_type_id: body.order_type_id,
			}); //event emit for socket notification admin
			return okResponse(res, {}, 'Order successfully placed');
		}
	} catch (err) {
		console.log(err);
		throw badRequestError(res, err);
	}
};

//API not in use
const placeOrderwithModifier = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		if (!body.trans_id) {
			throw ErrorResponse('Required parameter not found');
		}
		const stripe = require('./../../middlewares/stripe');
		let stripeAmount = data.total_amount * 100;
		[err, stripeData] = await to(
			stripe.createPayment({
				amount: stripeAmount,
				tknId: data.trans_id,
				currency: 'usd',
			})
		);
		if (err) {
			throw ErrorResponse(err.message);
		}
		body.transaction_id = stripeData.txnId;
		body.user_id = userId;
		body.order_status = 1;
		body.order_number = 1;
		const addItem = await transaction(order.knex(), (trx) => {
			return order.query(trx).insertGraph(body);
		});
		if (addItem) {
			const delete_cart_product_modifier_detail = await CartProductModifierDetail.query()
				.delete()
				.where('user_id', userId);
			const delete_cart_product_modifier = await CartProductModifier.query()
				.delete()
				.where('user_id', userId);

			const delete_cart = await Cart.query().delete().where('user_id', userId);
			return okResponse(res, {}, 'Ordered successfully placed');
		}
	} catch (err) {
		throw badRequestError(res, err);
	}
};
const placeOrderHistory = async (req, res) => {
	let status = req.query.status;
	let Order;
	let page = req.body.page ? req.body.page : 1;
	let limit = 10;
	let offset = (page - 1) * 10;

	//'1, means new order, 2 means order accept, 3 means order preparing, 4 means order ready, 5 means order served, 6 means order complete, 7 means order cancel, 8 means payment refunded'
	try {
		if (status) {
			Order = await order
				.query()
				.select(
					'orders.id',
					'orders.order_type_id',
					'order_status',
					'estimated_time',
					'is_free_delivery',
					'deliver_charges',
					'table_id',
					'order_cancellation_reason',
					'table_no',
					'transaction_id',
					'merchant_id',
					'order_number',
					'payment_id',
					'restaurant_name',
					'profile_image',
					'banner_image',
					'order_amount',
					'orders.created_at',
					'country_isd_code',
					'mobile',
					'delivery_lat',
					'delivery_lng',
					'delivery_address'
				)
				.where((builder) => {
					builder.where('user_id', req.user.user_id);
					if (req.body.merchant_id) {
						builder.where('merchant_id', '=', req.body.merchant_id);
					}
				})
				.andWhere((builder) => {
					if (status == 0) {
					} else if (status == 1) {
						builder.whereNotIn('order_status', [6, 7]);
					} else {
						builder.where({ order_status: status });
					}
				})
				.innerJoinRelation('orders_of_merchant')
				.eager(
					'[orders_of_users, merchant_address_relation, merchant_cuisine, characteristics_of_merchant]'
				)
				.modifyEager('characteristics_of_merchant', (builder) => {
					return builder.select(
						'opening_status as current_opening_status',
						'online_order_status'
					);
				})
				.modifyEager('merchant_cuisine', (builder) => {
					return builder.select('cuisines.cuisine_name');
				})
				.modifyEager('orders_of_users', (builder) => {
					return builder.select('mobile', 'country_isd_code');
				})
				.modifyEager('merchant_address_relation', (builder) => {
					return builder
						.select(
							'full_address',
							'address_management.lat',
							'address_management.lng',
							'locality',
							'city_name'
						)
						.innerJoin('users', 'address_management.user_id', 'users.id');
					// .where('active_address');
				})
				.orderBy('orders.id', 'desc')
				.offset(offset)
				.limit(limit);
		}

		let checkitem = await Cart.query()
			.where({
				user_id: req.user.user_id,
			})
			.eager('[cart_merchant_relation]')
			.modifyEager('cart_merchant_relation', (builder) => {
				return builder
					.select(
						'users.id',
						'users.restaurant_name',
						'merchant_characteristics.opening_status as current_opening_status',
						'merchant_characteristics.online_order_status'
					)
					.innerJoin(
						'merchant_characteristics',
						'users.id',
						'merchant_characteristics.merchant_id'
					);
			})
			.first();

		let restaurantDetails = {};

		if (checkitem) {
			restaurantDetails = checkitem.cart_merchant_relation;
			if (restaurantDetails == null) {
				restaurantDetails = {};
			} else {
				restaurantDetails.current_opening_status_message = '';
				restaurantDetails.online_order_status_message = '';
				if (restaurantDetails.current_opening_status == false) {
					restaurantDetails.current_opening_status_message =
						'Merchant is currently closed. Please order once it’s open.';
				}
				if (restaurantDetails.online_order_status == false) {
					restaurantDetails.online_order_status_message =
						'Merchant is not accepting online orders at this moment. Please try again later.';
				}
			}
		}

		//---------Loop To Append message keys of current_opening_status and online_order_status
		for (let i = 0; i < Order.length; i++) {
			Order[i].characteristics_of_merchant.current_opening_status_message = '';
			Order[i].characteristics_of_merchant.online_order_status_message = '';
			if (
				Order[i].characteristics_of_merchant.current_opening_status == false
			) {
				Order[i].characteristics_of_merchant.current_opening_status_message =
					'Merchant is currently closed. Please order once it’s open.';
			}
			if (Order[i].characteristics_of_merchant.online_order_status == false) {
				Order[i].characteristics_of_merchant.online_order_status_message =
					'Merchant is not accepting online orders at this moment. Please try again later.';
			}

			if (Object.keys(restaurantDetails).length == 0) {
				Order[i].characteristics_of_merchant.message = '';
			} else {
				Order[
					i
				].characteristics_of_merchant.message = `Your cart contains dishes form ${restaurantDetails.restaurant_name}. Do you want to discard the selection and add dishes from ${Order[i].restaurant_name}`;
			}
		}

		return okResponse(
			res,
			{ Order, restaurantDetails },
			'Order history fetched successfully'
		);
	} catch (error) {}
};

const placeOrderHistoryById = async (req, res) => {
	//'1, means new order, 2 means order accept, 3 means order preparing, 4 means order ready, 5 means order served, 6 means order complete, 7 means order cancel, 8 means payment refunded'
	let orderid = req.params.order_id;

	if (req.query.notification_id != 0) {
		await Notification.query()
			.update({ read_status: true })
			.where('id', req.query.notification_id);
	}
	delete req.query.notification_id;

	let Order;
	try {
		Order = await order
			.query()
			.select(
				'orders.id',
				'orders.order_type_id',
				'order_status',
				'deliver_charges',
				'order_number',
				'is_free_delivery',
				'transaction_id',
				'merchant_id',
				'table_id',
				'table_no',
				'estimated_time',
				'order_cancellation_reason',
				'payment_id',
				'restaurant_name',
				'profile_image',
				'banner_image',
				'mobile',
				'delivery_address',
				'country_isd_code',
				'lat',
				'lng',
				'order_amount',
				'total_amount',
				'orders.created_at',
				'delivery_lat',
				'delivery_lng'
			)
			.where({ user_id: req.user.user_id })
			.andWhere('orders.id', orderid)
			.innerJoinRelation('orders_of_merchant')
			.eager(
				'[merchant_cuisine ,merchant_address_relation,ordered_products_relation.[products_customization], order_status_management, characteristics_of_merchant]'
			)
			.modifyEager('characteristics_of_merchant', (builder) => {
				return builder.select(
					'opening_status as current_opening_status',
					'online_order_status'
				);
			})
			.modifyEager('orders_of_users', (builder) => {
				return builder.select('id', 'mobile', 'country_isd_code');
			})
			.modifyEager('order_status_management', (builder) => {
				return builder.select('order_status');
			})
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.modifyEager('merchant_address_relation', (builder) => {
				return builder
					.select('full_address', 'locality', 'city_name')
					.innerJoin('users', 'address_management.user_id', 'users.id');
				// .where('active_address');
			})
			.modifyEager('ordered_products_relation', (builder) => {
				return (
					builder
						.select('product_name', 'ordered_product_quantity', 'product_price')
						//.eager('[products_relation]')
						.modifyEager('products_customization', (builder) => {
							return builder
								.select(
									Knex.raw(
										'(select "modifier_name" from modifier where "modifier"."id" = "order_customization"."modifier_id")'
									),
									Knex.raw(
										'(select "display_name" from modifier where "modifier"."id" = "order_customization"."modifier_id")'
									)
								)
								.eager('order_product_modifier_detail')
								.modifyEager('order_product_modifier_detail', (builder) => {
									return builder.select(
										'*',
										'modifier_product_name as modifier_product'
										// Knex.raw(
										// 	'(select modifier_product from "modifier_product_details" where "id" = "order_product_modifier_detail"."modifier_product_detail_id" )'
										// ),
										// Knex.raw(
										// 	'(select selling_price from "modifier_product_details" where "id" = "order_product_modifier_detail"."modifier_product_detail_id" )'
										// )
									);
									//.eager('modifier_product_detail_relation');
								});
						})
				);
			});

		//---------Loop To Append message keys of current_opening_status and online_order_status
		for (let i = 0; i < Order.length; i++) {
			Order[i].characteristics_of_merchant.current_opening_status_message = '';
			Order[i].characteristics_of_merchant.online_order_status_message = '';
			if (
				Order[i].characteristics_of_merchant.current_opening_status == false
			) {
				Order[i].characteristics_of_merchant.current_opening_status_message =
					'Merchant is currently closed. Please order once it’s open.';
			}
			if (Order[i].characteristics_of_merchant.online_order_status == false) {
				Order[i].characteristics_of_merchant.online_order_status_message =
					'Merchant is not accepting online orders at this moment. Please try again later.';
			}
		}

		let checkitem = await Cart.query()
			.findOne({
				user_id: req.user.user_id,
			})
			.eager('[cart_merchant_relation]')
			.modifyEager('cart_merchant_relation', (builder) => {
				return builder
					.select(
						'users.id',
						'users.restaurant_name',
						'merchant_characteristics.opening_status as current_opening_status',
						'merchant_characteristics.online_order_status'
					)
					.innerJoin(
						'merchant_characteristics',
						'users.id',
						'merchant_characteristics.merchant_id'
					);
			});

		let restaurantDetails = {};

		if (!Order) {
			throw badRequestError(res, 'Oops, some error has occured');
		}

		if (checkitem) {
			restaurantDetails = checkitem.cart_merchant_relation;
			restaurantDetails.current_opening_status_message = '';
			restaurantDetails.online_order_status_message = '';
			if (restaurantDetails.current_opening_status == false) {
				restaurantDetails.current_opening_status_message =
					'Merchant is currently closed. Please order once it’s open.';
			}
			if (restaurantDetails.online_order_status == false) {
				restaurantDetails.online_order_status_message =
					'Merchant is not accepting online orders at this moment. Please try again later.';
			}
			restaurantDetails.message = `Your cart contains dishes from ${restaurantDetails.restaurant_name}. Do you want to discard the selection and add dishes from ${Order[0].restaurant_name}`;
		}

		return okResponse(
			res,
			{ Order, restaurantDetails },
			'Order history fetched successfully'
		);
	} catch (error) {}
};

//API not in use
const scanProductAndAddToCart = async (req, res) => {
	try {
		let body = req.body;
		let userId = req.user.user_id;
		let cart_id = body.cartId;
		//delete body.category_id;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		if (body.category_id == 0) {
			body.category_id = null;
		}
		//console.log(query); return;

		let checkitem = await Cart.query()
			.where({
				product_id: body.product_id,
				menu_id: body.menu_id,
				order_type_id: body.order_type_id,
				user_id: userId,
			})
			.orderBy('id', 'desc')
			.first();

		/*	if(cart_id.length>0)
		{
			for(let i=0;i<cart_id.length;i++)
			{
				let cart_modifier = await CartProductModifier.query()
				.select('id',"modifier_id")
				.where('cart_id', cart_id[i]);
				if(cart_modifier.length==body.cart_product_modifiers_relation.length)
				{
					for(let j=0;j<cart_modifier.length;j++)
					{
					  if(cart_product_modifiers_relation[i].modifier_id==cart_modifier[j].modifier_id)
					  {
						let cart_product_detail = await CartProductModifierDetail.query()
						.select("modifier_product_detail_id")
						.whereIn('cart_modifier_id', cart_modifier[i].id)
						if(cart_product_detail.length==cart_product_modifiers_relation[i].cart_modifier_detail_relation.length)
						{
							for(let k=0;k<cart_product_detail.length;k++)
							{
								if(cart_product_modifiers_relation[i].cart_modifier_detail_relation[k].modifier_product_detail_id==cart_product_detail[k].modifier_product_detail_id)
								{

								}
							}
						}

					  }
					}
				}
			}
		}	
		*/

		if (checkitem && body.update_flag === true) {
			let addItem = await Cart.query().updateAndFetchById(checkitem.id, {
				quantity: checkitem.quantity + 1,
			});
			let cart_modifier = await CartProductModifier.query()
				.select('id')
				.where('cart_id', checkitem.id);

			if (cart_modifier) {
				let id_data = [];

				for (let i = 0; i < cart_modifier.length; i++) {
					id_data.push(cart_modifier[i].id);
				}

				let cart_product_detail = await CartProductModifierDetail.query()
					.update({ modifier_quantity: addItem.quantity })
					.whereIn('cart_modifier_id', id_data)
					.catch((err) => {});
			}
			return okResponse(res, {}, 'Added to Cart');
		} else {
			delete body.update_flag;
			//delete body.item_present;
			let getCartDetail = await Cart.query()
				.select('merchant_id', 'order_type_id', 'product_id')
				.where('user_id', userId)
				.where((builder) => {
					builder
						.whereNot('merchant_id', body.merchant_id)
						.orWhereNot('order_type_id', body.order_type_id);
				})
				.first()
				.catch((err) => {});
			if (getCartDetail) {
				if (getCartDetail.merchant_id != body.merchant_id) {
					throw badRequestError(
						res,
						'You have already added an item from another merchant'
					);
				}
				if (getCartDetail.order_type_id != body.order_type_id) {
					throw badRequestError(
						res,
						'You have already added an item from another order type'
					);
				}
			}
			let query = await Product.query()
				.select('id')
				.where({ id: body.product_id, delete_status: false, status: 1 });
			if (query == undefined) {
				//res, data, message, statusCode, code
				throw errorresponse(
					res,
					{ code: 401 },
					'Item Not available at the moment',
					400
				);
			}

			body.quantity = 1;
			body.user_id = userId;
			let relation_data = body.cart_product_modifiers_relation;

			if (relation_data != undefined && relation_data.length > 0) {
				for (var i = 0; i < relation_data.length; i++) {
					let modifierData = await Modifier.query()
						.select('min_user_must_select', 'max_user_must_select')
						.where('id', relation_data[i].modifier_id)
						.first();
					let count = relation_data[i].cart_modifier_detail_relation.length;
					if (modifierData.max_user_must_select == 0) {
					} else if (modifierData.min_user_must_select > count) {
						throw badRequestError(res, 'Please select modifier');
					} else if (modifierData.max_user_must_select < count) {
						throw badRequestError(res, 'You cannot select multiple modifiers');
					}
				}
			} else {
				let query = await Product.query()
					.select('id')
					.where({ id: body.product_id })
					.eager(
						'[product_modifier_get_relation,product_modifier_group_get_relation]'
					)
					.first();

				if (
					query.product_modifier_get_relation.length > 0 ||
					query.product_modifier_group_get_relation.length > 0
				) {
					throw badRequestError(res, 'Please add modifier(s)');
				}
			}
			[err, addItem] = await to(
				transaction(Cart.knex(), (trx) => {
					return Cart.query(trx).insertGraph(body);
				})
			);

			if (addItem == undefined || addItem == '') {
				throw badRequestError(
					res,
					'Something went wrong, please try again later'
				);
			}
			return okResponse(res, {}, 'Added to Cart');
		}
	} catch (error) {
		throw badRequestError(res, error);
	}
};

//API not in use
const Reorder_old = async (req, res) => {
	try {
		let order_id = req.params.order_id;
		//console.log(order_id);
		const reorder = await order
			.query()
			.select()
			.where('id', order_id)
			.eager(
				'[ordered_products_relation.products_customization.order_product_modifier_detail]'
			)
			.first();

		//	console.log(reorder, 'reorder');
		//{
		// 	"merchant_id":317,
		// 	"category_id": 1,
		// 	"menu_id": 8,
		// 	"product_id":3,
		// 	"order_type_id": 1,
		// 	"quantity": 1,
		// 	"cartId": 0
		// }
		if (!reorder) {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		// let checkitem = await Cart.query().findOne({
		// 	product_id: reorder.ordered_products_relation[0].product_id,
		// 	menu_id: reorder.ordered_products_relation[0].menu_id,
		// 	//category_id: body.category_id,
		// 	user_id: req.user.user_id,
		// });
		// console.log(checkitem);

		let deleteItem = await Cart.query()
			.del()
			.where('user_id', req.user.user_id);

		if (reorder.ordered_products_relation[0].category_id == 0) {
			reorder.ordered_products_relation[0].category_id = null;
		}

		let data = {
			user_id: req.user.user_id,
			category_id: reorder.ordered_products_relation[0].category_id,
			menu_id: reorder.ordered_products_relation[0].menu_id,
			product_id: reorder.ordered_products_relation[0].product_id,
			order_type_id: reorder.order_type_id,
			temp_order_typeid: reorder.temp_order_typeid,
			quantity: reorder.ordered_products_relation[0].ordered_product_quantity,
			merchant_id: reorder.merchant_id,
		};
		if (
			reorder.ordered_products_relation[0].products_customization !=
				undefined &&
			reorder.ordered_products_relation[0].products_customization.length > 0
		) {
			let inner_relation = [];
			for (
				let i = 0;
				i < reorder.ordered_products_relation[0].products_customization.length;
				i++
			) {
				let inner_data = {};

				inner_data.modifier_id =
					reorder.ordered_products_relation[0].products_customization[
						i
					].modifier_id;
				let cart_modifier_detail_relation = [];
				for (
					let j = 0;
					j <
					reorder.ordered_products_relation[0].products_customization[i]
						.order_product_modifier_detail.length;
					j++
				) {
					cart_modifier_detail_relation.push({
						modifier_id:
							reorder.ordered_products_relation[0].products_customization[i]
								.modifier_id,
						modifier_quantity:
							reorder.ordered_products_relation[0].products_customization[i]
								.order_product_modifier_detail[j].modifier_quantity,
						user_id:
							reorder.ordered_products_relation[0].products_customization[i]
								.order_product_modifier_detail[j].user_id,
						modifier_product_detail_id:
							reorder.ordered_products_relation[0].products_customization[i]
								.order_product_modifier_detail[j].modifier_product_detail_id,
					});
				}
				inner_data.cart_modifier_detail_relation = cart_modifier_detail_relation;
				inner_relation.push(inner_data);
			}
			data.cart_product_modifiers_relation = inner_relation;
		}
		let err, addItem;
		[err, addItem] = await to(
			transaction(Cart.knex(), (trx) => {
				return Cart.query(trx).insertGraph(data);
			})
		);

		//console.log(addItem);
		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		// let userId = req.user.user_id;
		// let subTotal = await Cart.query()
		// 	.select(
		// 		'quantity',
		// 		Knex.raw(
		// 			'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."order_type_id") as sub_total'
		// 		)
		// 	)
		// 	.where({ user_id: userId, id: addItem.id })
		// 	.first();

		// var totalsum = 0,
		// 	sum = 0;
		// //var totalquantity = 0;
		// //for (var i = 0; i < subTotal.length; i++) {
		// //totalquantity = totalquantity + subTotal.quantity;
		// sum = parseInt(subTotal.sub_total);
		// totalsum = totalsum + subTotal.quantity * sum;
		// //}

		return okResponse(
			res,
			{
				cartId: addItem.id,
			},
			'Successfully added to the cart'
		);

		//return okResponse(res, reorder, 'Operation Successfull');
	} catch (err) {
		//console.log(err);
		throw badRequestError(res, err);
	}
};

//Reorder a product from orderID
const Reorder = async (req, res) => {
	try {
		let order_id = req.params.order_id;
		let userId = req.user.user_id;
		const reorder = await order
			.query()
			.select('*')
			.where('id', order_id)
			.where('user_id', req.user.user_id)
			.eager(
				'[ordered_products_relation.products_customization.order_product_modifier_detail]'
			)
			.first();

		//	console.log(reorder, 'reorder');
		//{
		// 	"merchant_id":317,
		// 	"category_id": 1,
		// 	"menu_id": 8,
		// 	"product_id":3,
		// 	"order_type_id": 1,
		// 	"quantity": 1,
		// 	"cartId": 0
		// }
		if (!reorder) {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		// let checkitem = await Cart.query().findOne({
		// 	product_id: reorder.ordered_products_relation[0].product_id,
		// 	menu_id: reorder.ordered_products_relation[0].menu_id,
		// 	//category_id: body.category_id,
		// 	user_id: req.user.user_id,
		// });
		// console.log(checkitem);

		// if (reorder.ordered_products_relation[0].category_id == 0) {
		// 	reorder.ordered_products_relation[0].category_id = null;
		// }
		let data;
		let cartArr = [];

		let merchant_Details = await MerchantCharacteristic.query()
			.select('online_order_status', 'opening_status')
			.where('merchant_id', reorder.merchant_id)
			.first();
		if (merchant_Details) {
			if (!merchant_Details.opening_status) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is currently closed. Please order once it’s open.',
					400
				);
			}
			if (!merchant_Details.online_order_status) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Merchant is not accepting online orders at this moment. Please try again later.',
					400
				);
			}
		} else {
			throw badRequestError(res, 'Merchant Details Not Found');
		}

		for (let k = 0; k < reorder.ordered_products_relation.length; k++) {
			data = {
				user_id: userId,
				category_id:
					reorder.ordered_products_relation[k].category_id == 0
						? null
						: reorder.ordered_products_relation[0].category_id,
				menu_id: reorder.ordered_products_relation[k].menu_id,
				product_id: reorder.ordered_products_relation[k].product_id,
				order_type_id: reorder.order_type_id,
				temp_order_typeid: reorder.temp_order_typeid,
				quantity: reorder.ordered_products_relation[k].ordered_product_quantity,
				merchant_id: reorder.merchant_id,
				cart_delivery_address: reorder.delivery_address,
				cart_delivery_lat: reorder.delivery_lat,
				cart_delivery_lng: reorder.delivery_lng,
			};
			//cartArr.push(data)
			//console.log(data, 'bbbb')
			let inner_relation = [];
			if (
				reorder.ordered_products_relation[k].products_customization !=
					undefined &&
				reorder.ordered_products_relation[k].products_customization.length > 0
			) {
				for (
					let i = 0;
					i <
					reorder.ordered_products_relation[k].products_customization.length;
					i++
				) {
					let inner_data = {};

					inner_data.modifier_id =
						reorder.ordered_products_relation[k].products_customization[
							i
						].modifier_id;
					inner_data.user_id = userId;
					let cart_modifier_detail_relation = [];
					for (
						let j = 0;
						j <
						reorder.ordered_products_relation[k].products_customization[i]
							.order_product_modifier_detail.length;
						j++
					) {
						cart_modifier_detail_relation.push({
							modifier_id:
								reorder.ordered_products_relation[k].products_customization[i]
									.modifier_id,
							modifier_quantity:
								reorder.ordered_products_relation[k].products_customization[i]
									.order_product_modifier_detail[j].modifier_quantity,
							user_id:
								reorder.ordered_products_relation[k].products_customization[i]
									.order_product_modifier_detail[j].user_id,
							modifier_product_detail_id:
								reorder.ordered_products_relation[k].products_customization[i]
									.order_product_modifier_detail[j].modifier_product_detail_id,
						});
					}
					inner_data.cart_modifier_detail_relation = cart_modifier_detail_relation;
					inner_relation.push(inner_data);
				}
				//console.log(data,'dddddd');
				data.cart_product_modifiers_relation = inner_relation;
			}
			cartArr.push(data);
		}
		//console.log(data, 'new data');
		//console.log(cartArr, 'new arr'); return;
		let err, addItem;
		[err, addItem] = await to(
			transaction(Cart.knex(), (trx) => {
				return Cart.query(trx).insertGraph(cartArr);
			})
		);
		if (addItem) {
			console.log(addItem);
			let idArray = [];
			for (let i = 0; i < addItem.length; i++) {
				idArray.push(addItem[i].id);
			}
			console.log(idArray);
			let deleteCart = await Cart.query()
				.del()
				.where({ user_id: userId })
				.whereNotIn('id', idArray);
		}

		//console.log(addItem);
		//console.log(err, 'err');
		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		// let userId = req.user.user_id;
		// let subTotal = await Cart.query()
		// 	.select(
		// 		'quantity',
		// 		Knex.raw(
		// 			'(select selling_price from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."order_type_id") as sub_total'
		// 		)
		// 	)
		// 	.where({ user_id: userId, id: addItem.id })
		// 	.first();

		// var totalsum = 0,
		// 	sum = 0;
		// //var totalquantity = 0;
		// //for (var i = 0; i < subTotal.length; i++) {
		// //totalquantity = totalquantity + subTotal.quantity;
		// sum = parseInt(subTotal.sub_total);
		// totalsum = totalsum + subTotal.quantity * sum;
		// //}

		return okResponse(
			res,
			{
				cartId: addItem.id,
			},
			'Successfully added to the cart'
		);

		//return okResponse(res, reorder, 'Operation Successfull');
	} catch (err) {
		//console.log(err);
		throw badRequestError(res, err);
	}
};

//API not in use
const ReorderwithModifier = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let order_id = req.params.order_id;

		const reorder = await order
			.query()
			.select()
			.where('id', order_id)
			.eager(
				'[ordered_products_relation.products_customization.order_product_modifier_detail]'
			)
			.first();

		if (!reorder) {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		let data = {
			user_id: req.user.user_id,
			category_id: reorder.ordered_products_relation[0].category_id,
			menu_id: reorder.ordered_products_relation[0].menu_id,
			product_id: reorder.ordered_products_relation[0].product_id,
			order_type_id: reorder.order_type_id,
			quantity: reorder.ordered_products_relation[0].ordered_product_quantity,
			merchant_id: reorder.merchant_id,
		};
		let inner_relation = [];
		for (
			let i = 0;
			i < reorder.ordered_products_relation[0].products_customization.length;
			i++
		) {
			let inner_data = {};

			inner_data.modifier_id =
				reorder.ordered_products_relation[0].products_customization[
					i
				].modifier_id;
			let cart_modifier_detail_relation = [];
			for (
				let j = 0;
				j <
				reorder.ordered_products_relation[0].products_customization[0]
					.order_product_modifier_detail.length;
				j++
			) {
				cart_modifier_detail_relation.push({
					modifier_id:
						reorder.ordered_products_relation[0].products_customization[0]
							.order_product_modifier_detail[j].modifier_id,
					modifier_quantity:
						reorder.ordered_products_relation[0].products_customization[0]
							.order_product_modifier_detail[j].modifier_quantity,
					user_id:
						reorder.ordered_products_relation[0].products_customization[0]
							.order_product_modifier_detail[j].user_id,
					modifier_product_detail_id:
						reorder.ordered_products_relation[0].products_customization[0]
							.order_product_modifier_detail[j].modifier_product_detail_id,
				});
			}
			inner_data.cart_modifier_detail_relation = cart_modifier_detail_relation;
			inner_relation.push(inner_data);
		}
		data.cart_product_modifiers_relation = inner_relation;
		//console.log(data);
		let err, addItem;
		[err, addItem] = await to(
			transaction(Cart.knex(), (trx) => {
				return Cart.query(trx).insertGraph(data);
			})
		);
		//console.log('Additem', addItem.id);
		if (addItem == undefined || addItem == '') {
			throw badRequestError(
				res,
				'Something went wrong, please try again later'
			);
		}
		//console.log(userId);
		let subTotal = await Cart.query()
			.select(
				'quantity',
				Knex.raw(
					'(select "selling_price" from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
				)
			)
			.where({ user_id: userId, id: addItem.id })
			.first();

		var totalsum = 0,
			sum = 0;
		//var totalquantity = 0;
		//for (var i = 0; i < subTotal.length; i++) {
		//	totalquantity = totalquantity + subTotal[i].quantity;
		sum = parseInt(subTotal.sub_total);
		totalsum = totalsum + subTotal.quantity * sum;
		//}
		let modifier_data = await CartProductModifierDetail.query()
			.select('selling_price', 'modifier_quantity')
			.where('user_id', userId)
			.innerJoinRelation('cart_modifier_products_detail_relation');

		var totalmodifiersum = 0;
		for (var i = 0; i < modifier_data.length; i++) {
			//totalquantity = totalquantity + subTotal[i].quantity;
			totalmodifiersum =
				totalmodifiersum +
				modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
		}
		return okResponse(
			res,
			{
				cartId: addItem.id,
				quantity: subTotal.quantity,
				subtotal: totalsum + totalmodifiersum,
			},
			'Successfully added to cart'
		);
		//return okResponse(res, data, 'Operation Successfull');
	} catch (error) {
		throw badRequestError(res, error);
	}
};

//Delete Cart
const deleteCart = async (req, res) => {
	let userId = req.user.user_id;
	try {
		let deleteCart = await Cart.query()
			.del()
			.where('user_id', userId)
			.catch((err) => {});
		if (deleteCart) {
			return okResponse(res, {}, 'Cart Item deleted Successfully');
		}

		throw badRequestError(res, 'No item present in the cart');
	} catch (error) {
		throw badRequestError(res, error);
	}
};

//--------------QR code Versions API (START)------------------------------------------
//API not in use
const scanProductAndAddToCartVersionQrCode = async (req, res) => {
	try {
		let requestedData = req.body.qrCode;

		//decode string back to the object
		var decodedObject = Buffer.from(requestedData, 'base64').toString('utf8');
		if (JSON.parse(JSON.stringify(decodedObject)) == null) {
			throw badRequestError(
				res,
				'Required encrypted string does not contain a valid JSON.'
			);
		}

		let body = JSON.parse(decodedObject);

		let userId = req.user.user_id;
		//delete body.category_id;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		if (body.category_id == 0) {
			body.category_id = null;
		}
		//console.log(query); return;
		let checkitem = await Cart.query().findOne({
			product_id: body.product_id,
			menu_id: body.menu_id,
			order_type_id: body.order_type_id,
			user_id: userId,
		});
		if (checkitem) {
			let getitem = await Cart.query()
				.select('quantity')
				.where('id', checkitem.id)
				.eager(
					'[cart_product_modifiers_relation.cart_modifier_detail_relation]'
				)
				.first();

			let addItem = await Cart.query()
				.update({ quantity: getitem.quantity + 1 })
				.where('id', checkitem.id);
			if (
				getitem.cart_product_modifiers_relation != undefined &&
				getitem.cart_product_modifiers_relation.length > 0
			) {
				for (
					var i = 0;
					i <
					getitem.cart_product_modifiers_relation[0]
						.cart_modifier_detail_relation.length;
					i++
				) {
					let modifier_item = await CartProductModifierDetail.query()
						.update({
							modifier_quantity:
								getitem.cart_product_modifiers_relation[0]
									.cart_modifier_detail_relation[i].modifier_quantity + 1,
						})
						.where(
							'cart_modifier_id',
							getitem.cart_product_modifiers_relation[0].id
						);
				}
			}
			// let item = await Cart.query()
			// 	.select('quantity')
			// 	.where('id', checkitem.id)
			// 	.eager(
			// 		'[cart_product_modifiers_relation.cart_modifier_detail_relation]'
			// 	)
			// 	.first();
			//okresponse

			return okResponse(res, {}, 'Added to Cart');
		} else {
			let getCartDetail = await Cart.query()
				.select('merchant_id', 'order_type_id', 'product_id')
				.where('user_id', userId)
				.where((builder) => {
					builder
						.whereNot('merchant_id', body.merchant_id)
						.orWhereNot('order_type_id', body.order_type_id);
				})
				.first()
				.catch((err) => {});
			if (getCartDetail) {
				if (getCartDetail.merchant_id != body.merchant_id) {
					throw badRequestError(
						res,
						'You have already added an item from another merchant'
					);
				}
				if (getCartDetail.order_type_id != body.order_type_id) {
					throw badRequestError(
						res,
						'You have already added an item from another order type'
					);
				}
			}

			let query = await Product.query()
				.select('id')
				.where({ id: body.product_id, delete_status: false, status: 1 })
				.eager(
					'[product_modifier_get_relation as cart_product_modifiers_relation.[modifier_product_detail_relation], product_modifier_group_get_relation]'
				)
				.modifyEager('product_modifier_group_get_relation', (builder) => {
					return builder
						.select('')
						.where('modifier_group.status', true)
						.eager(
							'group_modifier_management as cart_product_modifiers_relation'
						)
						.modifyEager('cart_product_modifiers_relation', (builder) => {
							return builder
								.select('modifier.id as modifier_id')
								.where(
									'modifier.id',
									Knex.raw(
										'(select "modifier_id" from "modifier_product_details" where "modifier_id" = "modifier"."id" AND "default" = true limit 1)'
									)
								)
								.limit(1)
								.eager(
									'modifier_product_detail_relation as cart_modifier_detail_relation'
								)
								.modifyEager('cart_modifier_detail_relation', (builder) => {
									return builder
										.select(
											'modifier_product_details.modifier_id',
											'modifier_product_details.id as modifier_product_detail_id'
										)
										.orderBy('modifier_product_details.id', 'ASC')
										.limit(1);
								});
						});
				})
				.modifyEager('cart_product_modifiers_relation', (builder) => {
					return builder
						.select('modifier.id as modifier_id')
						.where(
							'modifier.id',
							Knex.raw(
								'(select "modifier_id" from "modifier_product_details" where "modifier_id" = "modifier"."id" AND "default" = true limit 1)'
							)
						)
						.limit(1)
						.eager(
							'modifier_product_detail_relation as cart_modifier_detail_relation'
						)
						.modifyEager('cart_modifier_detail_relation', (builder) => {
							return builder
								.select(
									'modifier_product_details.modifier_id',
									'modifier_product_details.id as modifier_product_detail_id'
								)
								.orderBy('modifier_product_details.id', 'ASC')
								.limit(1);
						});
				})
				.first()
				.catch((err) => {});
			if (query == undefined) {
				throw createStatusCodeError(
					res,
					401,
					'Item Not available at the moment'
				);
			}

			body.quantity = 1;
			if (query != '' && query != undefined) {
				if (query.cart_product_modifiers_relation.length > 0) {
					body.cart_product_modifiers_relation =
						query.cart_product_modifiers_relation;
				} else if (query.product_modifier_group_get_relation) {
					body.cart_product_modifiers_relation =
						query.product_modifier_group_get_relation[0].cart_product_modifiers_relation;
				}
				if (body.cart_product_modifiers_relation.length > 0) {
					for (
						let i = 0;
						i < body.cart_product_modifiers_relation.length;
						i++
					) {
						body.cart_product_modifiers_relation[i].user_id = req.user.user_id;

						for (
							let j = 0;
							j <
							body.cart_product_modifiers_relation[i]
								.cart_modifier_detail_relation.length;
							j++
						) {
							body.cart_product_modifiers_relation[
								i
							].cart_modifier_detail_relation[j].modifier_quantity = 1;
							body.cart_product_modifiers_relation[
								i
							].cart_modifier_detail_relation[j].user_id = req.user.user_id;
						}
					}
				}
				//console.log('Aakash', body.cart_product_modifiers_relation);
				body.user_id = userId;

				//console.log(userId, 'ddddd');
				[err, addItem] = await to(
					transaction(Cart.knex(), (trx) => {
						return Cart.query(trx).insertGraph(body);
					})
				);

				if (addItem == undefined || addItem == '') {
					throw badRequestError(
						res,
						'Something went wrong, please try again later'
					);
				}
				return okResponse(res, {}, 'Successfully added to the cart');
			}
		}
	} catch (error) {
		throw badRequestError(res, error);
	}
};

//================Menu List by restaurant id=================

const menuListVersionQrCode = async (req, res) => {
	try {
		let requestedData = req.query.qrCode;

		//decode string back to the object
		var decodedObject = Buffer.from(requestedData, 'base64').toString('utf8');
		if (JSON.parse(JSON.stringify(decodedObject)) == null) {
			throw badRequestError(
				res,
				'Required encrypted string does not contain a valid JSON.'
			);
		}

		let body = JSON.parse(decodedObject);

		// return;

		let id = +body.merchant_id;
		let userId = req.user.user_id;
		let err, menuList;
		let order_type_id = body.order_type_id;
		let query = Menu.query()
			.select('menu_management.id', 'menu_name')
			.where({ merchant_id: id, status: true, delete_status: false })
			.innerJoinRelation('menu_order_type_relation')
			.eager('[menu_product, menu_order_type_relation]')
			.modifyEager('menu_order_type_relation', (builder) => {
				builder
					.select('order_type_id')
					.where('menu_order_type.order_type_id', order_type_id);
			})
			.modifyEager('menu_product', (builder) => {
				//return builder.select('product.id', 'product_name', 'product_image', 'product_description', 'products_per_order_limit', Knex.raw('(select count(\"id\") from \"review_and_rating\" where \"item_id\" = \"product\".\"id\") as reviewcount'), Knex.raw('(select COALESCE(avg(\"rating\"), 0) as avgRating from \"review_and_rating\" where \"item_id\" = \"product\".\"id\")'), Knex.raw('(select count(\"id\") from \"cart\" where \"product_id\" = \"product\".\"id\" AND \"user_id\" = '+userId+') as "cartId"')).where('product_menu.status', true).where('product.delete_status', false).where('product.status', 1)
				return (
					builder
						.select('product.id')
						.where('product_menu.status', true)
						.where('product.delete_status', false)
						.where('product.status', 1)
						.eager('[product_category]')
						// .modifyEager('product_tag_relation', builder => {
						//     return builder.select('product_tags.tags');
						// })
						// .modifyEager('product_menu_price_relation', builder => {
						//     return builder.select('price', 'selling_price');
						// })
						.modifyEager('product_category', (builder) => {
							return builder
								.select(
									'categories.id as catid',
									'product_category.category_name'
								)
								.where('product_category.status', true);
						})
				);
			})
			.where('menu_order_type_relation.order_type_id', order_type_id)
			.groupBy('menu_management.id');

		menuList = await query;

		// if (menuList == undefined || menuList == '') {
		// 	throw badRequestError(res, 'Menu list not found');
		// }
		let categoryArr = [];
		let isExistsId = [0];
		menuList = menuList.map(function (value, index) {
			categoryArr = [];
			isExistsId = [0];

			value.menu_product.map((item) => {
				if (item.product_category != undefined) {
					item.product_category.forEach((element) => {
						if (!isExistsId.includes(element.catid)) {
							isExistsId.push(element.catid);
							categoryArr.push(element);
						}
					});
				}
				delete item.product_category;
			});
			delete value.menu_product;
			value.category = categoryArr;
			return value;
		});

		let merchangeQuery = await User.query()
			.select(
				'id',
				'restaurant_name',
				'profile_image',
				'banner_image',
				'business_description',
				'membership',
				Knex.raw(
					'(select count("id") from "review_and_rating" where "merchant" = "users"."id") as reviewcount'
				),
				Knex.raw(
					'(select COALESCE(avg("rating"), 0) as avgRating from "review_and_rating" where "merchant" = "users"."id")'
				)
			)
			.where({ id: id, user_type: 'MERCHANT', user_status: true })
			.eager(
				'[merchant_cuisine, merchant_order_type, merchant_characteristic_relation]'
			)
			.modifyEager('merchant_cuisine', (builder) => {
				return builder.select('cuisines.cuisine_name');
			})
			.modifyEager('merchant_characteristic_relation', (builder) => {
				return builder.select(
					'timing',
					'opening_status',
					'online_order_status'
				);
			})
			.modifyEager('merchant_order_type', (builder) => {
				return builder.select(
					'order_type.id as order_type_id',
					'order_type.order_type'
				);
			});
		if (merchangeQuery == '' || merchangeQuery == undefined) {
			merchangeQuery = {};
		}
		return okResponse(
			res,
			{ restaurantDetail: merchangeQuery, menuList: menuList },
			'Menu list successfully fetched'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

//API not in use
const CheckItemInCart = async (req, res) => {
	try {
		let exist_in_cart = false;
		let cart_id = 0;
		let exist_modifier = false;
		let item_available = true;
		let body = req.body;
		const checkitem = await Cart.query()
			.select('id')
			.where({
				product_id: body.product_id,
				menu_id: body.menu_id,
				order_type_id: body.order_type_id,
				merchant_id: body.merchant_id,
			})
			.where('user_id', req.user.user_id)
			.orderBy('id', 'desc')
			.first();

		if (checkitem) {
			cart_id = checkitem.id;

			exist_in_cart = true;
		}
		let query = await Product.query()
			.select('id')
			.where({ id: body.product_id, delete_status: false, status: 1 })
			.eager(
				'[product_modifier_get_relation,product_modifier_group_get_relation]'
			)
			.first();

		if (query == undefined) {
			//res, data, message, statusCode, code
			item_available = false;
		}

		if (
			query.product_modifier_get_relation.length > 0 ||
			query.product_modifier_group_get_relation.length > 0
		) {
			exist_modifier = true;
		}
		return okResponse(
			res,
			{ exist_in_cart, exist_modifier, item_available, cart_id },
			'Menu list successfully fetched'
		);
	} catch (error) {
		throw badRequestError(res, error);
	}
};

const calculatedelivery_charges = async (req, res) => {
	let data = req.body;
	let delivery = {};
	try {
		if (!data.merchant_id)
			throw badRequestError(res, 'Please pass the merchant id');
		if (!data.lat) throw badRequestError(res, 'Please pass the latitude');
		if (!data.lng) throw badRequestError(res, 'Please pass the longitude');
		if (data.lat == 0 && data.lng == 0) {
			delivery = [];
		} else {
			/*
			let subTotal = await Cart.query()
				.select(
					'id',
					'merchant_id',
					'quantity',

					Knex.raw(
						'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: req.user.user_id });

			var totalsum = 0,
				sum = 0;
			var totalquantity = 0;
			for (var i = 0; i < subTotal.length; i++) {
				totalquantity = totalquantity + subTotal[i].quantity;
				sum = parseFloat(subTotal[i].sub_total);
				totalsum = totalsum + sum;
			}
			let modifier_data = await CartProductModifierDetail.query()
				.select('selling_price', 'modifier_quantity')
				.where('user_id', req.user.user_id)
				.innerJoinRelation('cart_modifier_products_detail_relation');
			var totalmodifiersum = 0;
			if (modifier_data != undefined) {
				for (var i = 0; i < modifier_data.length; i++) {
					//totalquantity = totalquantity + subTotal[i].quantity;
					totalmodifiersum =
						totalmodifiersum +
						modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
				}
			}
			*/
			let user_details = await calculateCartPrice(req.user.user_id);
			let maxDistance = await DeliveryCharges.query()
				.max('to as maxvalue')
				.where('merchant_id', data.merchant_id)
				.first();
			let merchantDetail = await User.query()
				.select('lat', 'lng')
				.where('id', data.merchant_id)
				.first();

			if (!merchantDetail) {
				//maxDistance if not exist in db ->> need to discuss
				throw badRequestError(res, 'No merchant Info Present');
			}

			let pickUp = merchantDetail.lat + ',' + merchantDetail.lng;
			let dropOff = data.lat + ',' + data.lng;
			//let dist = await Distance.GetDistanceByLatLng(pickUp, dropOff);
			let err, dist;
			[err, dist] = await to(Distance.GetDistanceByLatLng(pickUp, dropOff));
			if (dist == undefined) {
				throw badRequestError(
					res,
					'Your delivery address is not within the service radius. Please choose another address within ' +
						maxDistance.maxvalue +
						' km'
				);
			}
			let km = Math.round(dist.data.distanceValue / 1000);
			delivery = await DeliveryCharges.query()
				.select('delivery_fee', 'minimum_order', 'free_delivery_order')
				.where((builder) => {
					return builder.where('from', '<=', km).andWhere('to', '>=', km);
				})
				.where('merchant_id', data.merchant_id);

			if (delivery.length == 0) {
				throw badRequestError(
					res,
					'Your delivery address is not within the service radius. Please choose another address within ' +
						maxDistance.maxvalue +
						' km'
				);
				0;
			}
			if (delivery.length > 0) {
				delivery[0].distance = km;
				if (delivery[0].free_delivery_order == 0) {
					delivery[0].is_free_delivery = false;
				} else {
					if (delivery[0].free_delivery_order <= user_details.total_amount) {
						delivery[0].is_free_delivery = true;
						delivery[0].delivery_fee = 0;
					} else {
						delivery[0].is_free_delivery = false;
					}
				}
			}
		}
		return okResponse(res, { delivery }, 'Delivery details send successfully');
	} catch (error) {
		console.log(error);
		if (error.code != undefined && error.code == 312) {
			throw errorresponse(
				res,
				{ code: 312 },
				'Restaurant doesn’t serves at your selected location.Choosing a different location will clear the items from the cart. Do you want to continue?',
				400
			);
		}
		throw badRequestError(res, error);
	}
};

const test_socket_notification = async (req, res) => {
	//console.log(req.io);
	req.io.emit('OrderSocketGet');
	return okResponse(res, {}, 'Order placed');
};

const calculateCartPrice = async (userid) => {
	//	console.log(userid);
	let subTotal = await Cart.query()
		.select(
			'quantity',
			Knex.raw(
				'(select sum(selling_price*quantity) from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid")'
			)
		)
		.where({ user_id: userid });

	var totalsum = 0,
		sum = 0;
	var totalquantity = 0;

	for (var i = 0; i < subTotal.length; i++) {
		totalquantity = totalquantity + subTotal[i].quantity;
		sum = parseFloat(subTotal[i].sum);
		totalsum = totalsum + sum;
	}
	console.log(totalsum);
	let modifier_data = await CartProductModifierDetail.query()
		.select('selling_price', 'modifier_quantity')
		.where('user_id', userid)
		.innerJoinRelation('cart_modifier_products_detail_relation');

	console.log(modifier_data);
	var totalmodifiersum = 0;
	if (modifier_data != undefined) {
		for (var i = 0; i < modifier_data.length; i++) {
			//totalquantity = totalquantity + subTotal[i].quantity;
			totalmodifiersum =
				totalmodifiersum +
				modifier_data[i].modifier_quantity * modifier_data[i].selling_price;
		}
	}
	console.log(totalmodifiersum);

	let user_details = {
		totalquantity,
		totalitems: subTotal.length,
		total_amount: +(totalsum + totalmodifiersum).toFixed(2),
	};
	return user_details;
};

//--------------QR code Versions API (END)------------------------------------------
const ModifierEditApi_old = async (req, res) => {
	try {
		let cart_id = +req.params.cart_id;
		//let cartId = +req.params.cartId;
		let product_group_modifier;
		let err, menuList;
		let userId = req.user.user_id;

		menuList = await Cart.query()
			.select('product_id', 'product_name', 'product_description')
			.where('cart.id', cart_id)
			.innerJoin('product', 'cart.product_id', 'product.id')
			.eager(
				'[cart_product_modifiers_relation.[cart_modifier_detail_relation],product_modifier_get_relation.[modifier_product_detail_relation],product_modifier_group_get_relation]'
			)
			.modifyEager('cart_product_modifiers_relation', (builder) => {
				return builder
					.select('modifier_id')
					.modifyEager('cart_modifier_detail_relation', (builder) => {
						return builder.select('modifier_product_detail_id');
					});
			})
			.modifyEager('product_modifier_group_get_relation', (builder) => {
				return builder
					.select('modifier_group_name')
					.where('modifier_group.status', true)
					.eager('group_modifier_management')
					.modifyEager('group_modifier_management', (builder) => {
						return builder
							.select('modifier.*')
							.eager('modifier_product_detail_relation')
							.modifyEager('modifier_product_detail_relation', (builder) => {
								return builder.select('modifier_product_details.*');
							});
					});
			})
			.modifyEager('product_modifier_get_relation', (builder) => {
				return builder
					.select('modifier.*')
					.modifyEager('modifier_product_detail_relation', (builder) => {
						return builder.select('modifier_product_details.*');
					});
			})
			.first();

		if (menuList.product_modifier_group_get_relation.length > 0) {
			for (
				var i = 0;
				i < menuList.product_modifier_group_get_relation.length;
				i++
			) {
				product_group_modifier =
					menuList.product_modifier_group_get_relation[i]
						.group_modifier_management;

				delete menuList.product_modifier_group_get_relation[i]
					.group_modifier_management;
			}

			menuList.product_modifier_get_relation = [
				...new Set([
					...menuList.product_modifier_get_relation,
					...product_group_modifier,
				]),
			];

			menuList.product_modifier_get_relation = Array.from(
				new Set(
					menuList.product_modifier_get_relation.map((a) => a.modifier_name)
				)
			).map((modifier_name) => {
				return menuList.product_modifier_get_relation.find(
					(a) => a.modifier_name === modifier_name
				);
			});
		}
		delete menuList.product_modifier_group_get_relation;
		let modifier_detail_id = [];
		for (let i = 0; i < menuList.cart_product_modifiers_relation.length; i++) {
			for (
				let j = 0;
				j <
				menuList.cart_product_modifiers_relation[i]
					.cart_modifier_detail_relation.length;
				j++
			) {
				modifier_detail_id.push(
					menuList.cart_product_modifiers_relation[i]
						.cart_modifier_detail_relation[j].modifier_product_detail_id
				);
			}
		}

		for (let i = 0; i < menuList.product_modifier_get_relation.length; i++) {
			for (
				let j = 0;
				j <
				menuList.product_modifier_get_relation[i]
					.modifier_product_detail_relation.length;
				j++
			) {
				console.log(
					menuList.product_modifier_get_relation[i]
						.modifier_product_detail_relation[j]
				);
				if (
					modifier_detail_id.includes(
						menuList.product_modifier_get_relation[i]
							.modifier_product_detail_relation[j].id
					)
				) {
					menuList.product_modifier_get_relation[
						i
					].modifier_product_detail_relation[j].is_exists = true;
				} else {
					menuList.product_modifier_get_relation[
						i
					].modifier_product_detail_relation[j].is_exists = false;
				}
			}
		}
		delete menuList.cart_product_modifiers_relation;
		return okResponse(
			res,
			menuList,
			'Group modifier list successfully fetched'
		);
		/*
		let query = Cart.query()
			.select('id')
			.where({ id: cart_id })
			.eager(
				'[cart_product_modifiers_relation.[cart_modifier_detail_relation]]'
			)
			.modifyEager('cart_product_modifiers_relation', (builder) => {
				return builder
					.select('modifier_id')
					.modifyEager('cart_modifier_detail_relation', (builder) => {
						return builder.select('modifier_product_detail_id');
					});
			})
			*/
		/*
			.modifyEager('product_modifier_group_get_relation', (builder) => {
				return builder
					.select('modifier_group_name')
					.where('modifier_group.status', true)
					.eager('group_modifier_management')
					.modifyEager('group_modifier_management', (builder) => {
						return builder
							.select('modifier.*')
							.eager('modifier_product_detail_relation')
							.modifyEager('modifier_product_detail_relation', (builder) => {
								return builder.select(
									'modifier_product_details.*',
									Knex.raw(
										'(select "id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
											userId +
											'order by id desc limit 1' +
											') as is_exists'
									)
								);
							});
					});
			})
			.modifyEager('product_modifier_get_relation', (builder) => {
				return builder
					.select('modifier.*')
					.modifyEager('modifier_product_detail_relation', (builder) => {
						return builder.select(
							'modifier_product_details.*',
							Knex.raw(
								'(select "id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
									userId +
									'order by id desc limit 1' +
									') as is_exists'
							)
						);
					});
			})
			*/

		/*	let query = Cart.query()
			.select('id')
			.where({ id: cart_id })
			.eager(
				'[cart_product_relation.[product_modifier_get_relation.[modifier_product_detail_relation]]]'
			)
			.modifyEager('cart_product_relation', (builder) => {
				return builder
					.select('product_name')
					.modifyEager('product_modifier_get_relation', (builder) => {
						return builder
							.select('modifier_name')
							.modifyEager('modifier_product_detail_relation', (builder) => {
								return builder.select(
									'modifier_product',
									Knex.raw(
										'(select "id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
											userId +
											'order by id desc limit 1' +
											') as is_exists'
									)
								);
							});
					});
			});
			*/

		/*
		if (menuList.product_modifier_group_get_relation.length > 0) {
			for (
				var i = 0;
				i < menuList.product_modifier_group_get_relation.length;
				i++
			) {
				product_group_modifier =
					menuList.product_modifier_group_get_relation[i]
						.group_modifier_management;

				delete menuList.product_modifier_group_get_relation[i]
					.group_modifier_management;
			}

			menuList.product_modifier_get_relation = [
				...new Set([
					...menuList.product_modifier_get_relation,
					...product_group_modifier,
				]),
			];

			menuList.product_modifier_get_relation = Array.from(
				new Set(
					menuList.product_modifier_get_relation.map((a) => a.modifier_name)
				)
			).map((modifier_name) => {
				return menuList.product_modifier_get_relation.find(
					(a) => a.modifier_name === modifier_name
				);
			});
		}
		delete menuList.product_modifier_group_get_relation;
		*/
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

const ModifierEditApi = async (req, res) => {
	try {
		let cartId = +req.params.cart_id;
		let id = +req.params.id;
		let product_group_modifier;
		let err, menuList;
		let userId = req.user.user_id;
		let checkCart = await Cart.query().select('id').where('id', cartId).first();
		if (checkCart == '' || checkCart == undefined) {
			return badRequestError(res, 'Cart Detail Not Found');
		}
		let query = Product.query()
			.select('product.id', 'product_name', 'product_description')
			.where({ id: id, delete_status: false, status: 1 })
			.eager(
				'[product_modifier_get_relation.[modifier_product_detail_relation],product_modifier_group_get_relation]'
			)
			.modifyEager('product_modifier_group_get_relation', (builder) => {
				return builder
					.select('modifier_group_name')
					.where('modifier_group.status', true)
					.eager('group_modifier_management')
					.modifyEager('group_modifier_management', (builder) => {
						return builder
							.select('modifier.*')
							.eager('modifier_product_detail_relation')
							.modifyEager('modifier_product_detail_relation', (builder) => {
								return builder.select(
									'modifier_product_details.*',
									Knex.raw(
										'case when (select "cart_product_modifier_detail"."id" from  "cart_product_modifier_detail" INNER JOIN "cart_product_modifier" ON "cart_product_modifier"."id" = "cart_product_modifier_detail"."cart_modifier_id" AND "cart_product_modifier"."cart_id" = ' +
											cartId +
											'  WHERE "cart_product_modifier_detail"."modifier_product_detail_id" = "modifier_product_details"."id" AND "cart_product_modifier_detail"."user_id" = ' +
											userId +
											'order by id desc limit 1' +
											') IS NULL THEN false else true END as is_exists'
									),
									Knex.raw(
										'case when (select "cart_product_modifier_detail"."id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
											userId +
											'order by id desc limit 1' +
											') IS NULL THEN false else true END as is_exists_new'
									)
								);
							});
					});
			})
			.modifyEager('product_modifier_get_relation', (builder) => {
				return builder
					.select('modifier.*')
					.modifyEager('modifier_product_detail_relation', (builder) => {
						return builder.select(
							'modifier_product_details.*',
							Knex.raw(
								'case when (select "cart_product_modifier_detail"."id" from  "cart_product_modifier_detail" INNER JOIN "cart_product_modifier" ON "cart_product_modifier"."id" = "cart_product_modifier_detail"."cart_modifier_id" AND "cart_product_modifier"."cart_id" = ' +
									cartId +
									'   WHERE "cart_product_modifier_detail"."modifier_product_detail_id" = "modifier_product_details"."id" AND "cart_product_modifier_detail"."user_id" = ' +
									userId +
									'order by id desc limit 1' +
									') IS NULL THEN false else true END as is_exists'
							),
							Knex.raw(
								'case when (select "cart_product_modifier_detail"."id" from  "cart_product_modifier_detail" WHERE "modifier_product_details"."id" = "cart_product_modifier_detail"."modifier_product_detail_id" AND "cart_product_modifier_detail"."user_id" = ' +
									userId +
									'order by id desc limit 1' +
									') IS NULL THEN false else true END as is_exists_new'
							)
						);
					});
			})
			.first();

		menuList = await query;
		if (menuList == undefined) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Item you are looking for is Out Of Stock. Please come back later',
				400
			);
		}

		if (menuList.product_modifier_group_get_relation.length > 0) {
			//merge Product modifier group array  and remove for only showing group modifier without group
			for (
				var i = 0;
				i < menuList.product_modifier_group_get_relation.length;
				i++
			) {
				product_group_modifier =
					menuList.product_modifier_group_get_relation[i]
						.group_modifier_management;

				delete menuList.product_modifier_group_get_relation[i]
					.group_modifier_management;
			}

			menuList.product_modifier_get_relation = [
				...new Set([
					...menuList.product_modifier_get_relation,
					...product_group_modifier,
				]),
			];

			menuList.product_modifier_get_relation = Array.from(
				new Set(
					menuList.product_modifier_get_relation.map((a) => a.modifier_name)
				)
			).map((modifier_name) => {
				return menuList.product_modifier_get_relation.find(
					(a) => a.modifier_name === modifier_name
				);
			});
		}
		delete menuList.product_modifier_group_get_relation;

		// menuList = await Cart.query()
		// 	.select('product_id', 'product_name', 'product_description')
		// 	.where('cart.id', cart_id)
		// 	.innerJoin('product', 'cart.product_id', 'product.id')
		// 	.eager(
		// 		'[cart_product_modifiers_relation.[cart_modifier_detail_relation],product_modifier_get_relation.[modifier_product_detail_relation],product_modifier_group_get_relation]'
		// 	)
		// 	.modifyEager('cart_product_modifiers_relation', (builder) => {
		// 		return builder
		// 			.select('modifier_id')
		// 			.modifyEager('cart_modifier_detail_relation', (builder) => {
		// 				return builder.select('modifier_product_detail_id');
		// 			});
		// 	})
		// 	.modifyEager('product_modifier_group_get_relation', (builder) => {
		// 		return builder
		// 			.select('modifier_group_name')
		// 			.where('modifier_group.status', true)
		// 			.eager('group_modifier_management')
		// 			.modifyEager('group_modifier_management', (builder) => {
		// 				return builder
		// 					.select('modifier.*')
		// 					.eager('modifier_product_detail_relation')
		// 					.modifyEager('modifier_product_detail_relation', (builder) => {
		// 						return builder.select('modifier_product_details.*');
		// 					});
		// 			});
		// 	})
		// 	.modifyEager('product_modifier_get_relation', (builder) => {
		// 		return builder
		// 			.select('modifier.*')
		// 			.modifyEager('modifier_product_detail_relation', (builder) => {
		// 				return builder.select('modifier_product_details.*');
		// 			});
		// 	})
		// 	.first();

		// if (menuList.product_modifier_group_get_relation.length > 0) {
		// 	for (
		// 		var i = 0;
		// 		i < menuList.product_modifier_group_get_relation.length;
		// 		i++
		// 	) {
		// 		product_group_modifier =
		// 			menuList.product_modifier_group_get_relation[i]
		// 				.group_modifier_management;

		// 		delete menuList.product_modifier_group_get_relation[i]
		// 			.group_modifier_management;
		// 	}

		// 	menuList.product_modifier_get_relation = [
		// 		...new Set([
		// 			...menuList.product_modifier_get_relation,
		// 			...product_group_modifier,
		// 		]),
		// 	];

		// 	menuList.product_modifier_get_relation = Array.from(
		// 		new Set(
		// 			menuList.product_modifier_get_relation.map((a) => a.modifier_name)
		// 		)
		// 	).map((modifier_name) => {
		// 		return menuList.product_modifier_get_relation.find(
		// 			(a) => a.modifier_name === modifier_name
		// 		);
		// 	});
		// }
		// delete menuList.product_modifier_group_get_relation;
		// let modifier_detail_id = [];
		// for (let i = 0; i < menuList.cart_product_modifiers_relation.length; i++) {
		// 	for (
		// 		let j = 0;
		// 		j <
		// 		menuList.cart_product_modifiers_relation[i]
		// 			.cart_modifier_detail_relation.length;
		// 		j++
		// 	) {
		// 		modifier_detail_id.push(
		// 			menuList.cart_product_modifiers_relation[i]
		// 				.cart_modifier_detail_relation[j].modifier_product_detail_id
		// 		);
		// 	}
		// }
		// for (let i = 0; i < menuList.product_modifier_get_relation.length; i++) {
		// 	for (
		// 		let j = 0;
		// 		j <
		// 		menuList.product_modifier_get_relation[i]
		// 			.modifier_product_detail_relation.length;
		// 		j++
		// 	) {
		// 		console.log(
		// 			menuList.product_modifier_get_relation[i]
		// 				.modifier_product_detail_relation[j]
		// 		);
		// 		if (
		// 			modifier_detail_id.includes(
		// 				menuList.product_modifier_get_relation[i]
		// 					.modifier_product_detail_relation[j].id
		// 			)
		// 		) {
		// 			menuList.product_modifier_get_relation[
		// 				i
		// 			].modifier_product_detail_relation[j].is_exists = true;
		// 		} else {
		// 			menuList.product_modifier_get_relation[
		// 				i
		// 			].modifier_product_detail_relation[j].is_exists = false;
		// 		}
		// 	}
		// }
		//delete menuList.cart_product_modifiers_relation;
		return okResponse(
			res,
			menuList,
			'Group modifier list successfully fetched'
		);
	} catch (error) {
		console.log(error);
		throw badRequestError(res, error);
	}
};

const checkout_with_modifier_v3 = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		//let cart_id = body.cart_id;
		let total_price = body.total_amount;
		let device_type = body.device_type;
		//console.log(body);
		let ordered = body.ordered;
		let price = 0;
		//console.log(req.user);return;
		let err, checkCartProductStatus, addItem;
		//	console.log(body.lat);
		let Delivery = undefined;
		let amountdetail = 0;
		if (!body.merchant_id)
			throw badRequestError(res, 'Please pass the merchant id');

		//---------Check Merchant opening_status and Online Order Status-----------------
		let MerchantChar = await MerchantCharacteristic.query()
			.select('id', 'merchant_id', 'opening_status', 'online_order_status')
			.where({ merchant_id: body.merchant_id })
			.first();

		if (MerchantChar.opening_status == false) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is currently closed. Please order once it’s open.',
				400
			);
		}

		if (MerchantChar.online_order_status == false) {
			throw errorresponse(
				res,
				{ code: 300 },
				'Merchant is not accepting online orders at this moment. Please try again later.',
				400
			);
		}

		//---------Table Exist or not And It is availaible or not-------------------------
		let TableExist = null,
			TableOccupiedByOrNot = null;
		if (body.order_type_id == 5 && body.table_no) {
			// console.log(body.table_no, body.merchant_id)
			// if (!body.table_no)
			// 	throw badRequestError(res, 'Please pass the table no');

			TableExist = await Table.query()
				.select('id', 'table_number', 'merchant_id')
				.where({ table_number: body.table_no, merchant_id: body.merchant_id })
				.first();

			if (TableExist == undefined) {
				// throw badRequestError(res, "Table number not exist !");
				throw errorresponse(
					res,
					{ code: 300 },
					'Incorrect table number entered. Please double check and re-enter your table number or check with Wait staff.',
					400
				);
			}
			/*
			else {
				// console.log(TableExist);
				TableOccupiedByOrNot = await order
					.query()
					.select('id', 'table_id', 'order_number', 'table_no')
					.where({ table_id: TableExist.id })
					.first();
				console.log(TableOccupiedByOrNot);
				if (TableOccupiedByOrNot) {
					// throw badRequestError(res, "Table is already occupied by some one else. Please choose different table.");
					throw errorresponse(
						res,
						{ code: 300 },
						'Entered Table is occupied by the Customer. Please enter a different table number.',
						400
					);
				}
			}
			*/
		}

		//If Order Type is DineIn(5,6) 1 will be present for sure
		if (body.order_type_id == 5 || body.order_type_id == 6) {
			let order_type_id = [1];
			order_type_id.push(body.order_type_id);

			let Order_type = await OrderType.query()
				.select('order_type_id')
				.where('merchant_id', body.merchant_id)
				.whereIn('order_type_id', order_type_id);

			if (Order_type.length == 0 || Order_type.length == 1) {
				throw errorresponse(
					res,
					{ code: 301 },
					'Merchant Currently Not Accepting Dine-In Orders',
					400
				);
			}
		} else {
			let Order_type = await OrderType.query()
				.select('order_type_id')
				.where('merchant_id', body.merchant_id)
				.where('order_type_id', body.order_type_id);

			if (Order_type.length == 0) {
				//Pick-Up Orders
				if (body.order_type_id == 2) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Merchant Currently Not Accepting PickUp Orders',
						400
					);
				}
				// delivery Orders
				if (body.order_type_id == 4) {
					throw errorresponse(
						res,
						{ code: 301 },
						'Merchant Currently Not Accepting Delivery Orders',
						400
					);
				}
			}
		}
		//Check Delivery Object in case of Delivery Orders
		if (body.order_type_id == 4) {
			if (body.lat == 0 && body.lng == 0) {
			} else {
				let merchantDetail = await User.query()
					.select('lat', 'lng')
					.where('id', body.merchant_id)
					.first();
				let pickUp = merchantDetail.lat + ',' + merchantDetail.lng;
				let dropOff = body.lat + ',' + body.lng;
				let dist = await Distance.GetDistanceByLatLng(pickUp, dropOff);
				console.log(dist, 'dist');
				let km = Math.round(dist.data.distanceValue / 1000);

				Delivery = await DeliveryCharges.query()
					.select('delivery_fee', 'minimum_order', 'free_delivery_order')
					.where((builder) => {
						return builder.where('from', '<=', km).andWhere('to', '>=', km);
					})
					.where('merchant_id', body.merchant_id);
				if (Delivery.length > 0) {
				} else {
					Delivery = await DeliveryCharges.query()
						.select('to')
						.where('merchant_id', body.merchant_id)
						.orderBy('to', 'desc')
						.first();

					throw errorresponse(
						res,
						{ code: 300 },
						'Your delivery address is not within the service radius.Please choose another address within ' +
							Delivery.to +
							' km.',
						400
					);
				}
			}
			console.log(body.distance);
			if (body.distance != undefined) {
				Delivery = await DeliveryCharges.query()
					.select('minimum_order')
					.where((builder) => {
						return builder
							.where('from', '<=', body.distance)
							.andWhere('to', '>=', body.distance);
					})
					.where('merchant_id', body.merchant_id)
					.first();
				console.log(Delivery.minimum_order);
				if (
					Delivery != undefined &&
					Delivery.minimum_order > parseFloat(body.subTotal)
				) {
					console.log('DeliveryError');
					throw errorresponse(
						res,
						{ code: 300 },
						'Miniumum order for delivery is $' +
							parseFloat(Delivery.minimum_order) +
							'.Please add more items.',
						400
					);
				}
			}
		}
		for (let i = 0; i < ordered.length; i++) {
			let product = await Product.query()
				.select('status', 'product_quantity_counter')
				.where({ id: ordered[i].product_id, delete_status: false })
				.first();

			if (product) {
				if (product.status != 1) {
					throw errorresponse(
						res,
						{ code: 300 },
						'Item you are looking for is out of Stock.Please Come back latter',
						400
					);
				}
			} else {
				throw errorresponse(
					res,
					{ code: 300 },
					'Item you are looking for is out of Stock.Please Come back latter',
					400
				);
			}

			let query = await Cart.query()
				.select(
					'quantity',
					Knex.raw(
						'(select "selling_price" from "product_pricing" where "product_id" = "cart"."product_id" AND "menu_id" = "cart"."menu_id" AND "order_type_id" = "cart"."temp_order_typeid") as sub_total'
					)
				)
				.where({ user_id: userId, id: ordered[i].cart_id })
				.first();
			console.log(query);
			//

			console.log(typeof query.quantity, query.quantity);
			if (
				parseInt(product.product_quantity_counter) < parseInt(query.quantity)
			) {
				throw errorresponse(
					res,
					{ code: 300 },
					'Item you are looking for is out of Stock.Please Come back latter',
					400
				);
			}
			if (ordered[i].price != query.sub_total) {
				throw errorresponse(
					res,
					{ code: 300 },
					'There is some modification in product please check cart details',
					400
				);
			}

			amountdetail = amountdetail + parseInt(query.sub_total) * query.quantity;
			console.log(amountdetail, 'amountdetail');
			/*
			let modifier_length = ordered[i].cart_product_modifiers_relation.length;
			if (modifier_length > 0) {
				for (var j = 0; j < modifier_length; j++) {
					let modifier = await ModifierProductDetails.query()
						.select('selling_price')
						.where(
							'id',
							ordered[i].cart_product_modifiers_relation[j]
								.modifier_product_detail_id
						)
						.first();
					console.log(modifier);
					if (modifier) {
						if (
							modifier.selling_price !=
							ordered[i].cart_product_modifiers_relation[j].price
						)
							throw badRequestError(
								res,
								'Modifier Oops, price error has occured'
							);

						price = price + query.quantity * modifier.selling_price;
					}
				}
			}
			amountdetail = amountdetail + price;
			*/
		}
		/*
		if (amountdetail != total_price) {
			throw badRequestError(
				res,
				'There is some modification in product please check cart details'
			);
		}
		*/
		let sercretKey;
		if (device_type) {
			if (device_type == 'ios') {
				sercretKey = await Stripe.createPaymentForSecretKey(
					body.total_amount,
					'nzd',
					body.stripeCustomerId,
					'card'
				);
			} else {
				sercretKey = await stripe.createPaymentForSecretKey(
					body.total_amount,
					'nzd',
					body.stripeCustomerId,
					'card'
				);
			}
		} else {
			sercretKey = await stripe.createPaymentForSecretKey(
				body.total_amount,
				'nzd',
				body.stripeCustomerId,
				'card'
			);
		}

		//const stripe = require('./../../middlewares/stripe');

		let responseData;
		if (TableExist == null && TableOccupiedByOrNot == null) {
			responseData = {
				secretKey: sercretKey,
				table_id: null,
				table_no: null,
			};
		} else {
			responseData = {
				secretKey: sercretKey,
				table_id: TableExist.id,
				table_no: TableExist.table_number,
			};
		}

		return okResponse(res, responseData, 'Operation Successfull');
	} catch (err) {
		console.log(err);
		if (err.code != undefined && err.code == 312) {
			throw errorresponse(
				res,
				{ code: 312 },
				'Restaurant doesn’t serves at your selected location.Choosing a different location will clear the items from the cart. Do you want to continue?',
				400
			);
		}

		throw badRequestError(res, err);
	}
};
const placeOrder_v3 = async (req, res) => {
	try {
		let userId = req.user.user_id;
		let body = req.body;
		let device_type = req.body.device_type;
		let err, stripeData;
		// if (!body.transaction_id) {
		// 	throw badRequestError(res, 'Required parameter not found');
		// }

		for (let i = 0; i < body.ordered_products_relation.length; i++) {
			//Get Ordered Product Relation
			let product_data = body.ordered_products_relation[i];
			//Get Product if Avaliable
			let product = await Product.query()
				.select(
					'product_name',
					'product_threshold_quantity',
					'product_quantity_counter',
					'products_per_order_limit'
				)
				.where({
					id: product_data.product_id,
					delete_status: false,
					status: 1,
				})
				.first();

			if (product) {
				//Item is Out of Stock if available Quantity is Less Than Order Quantity
				if (
					product.product_quantity_counter <
					product_data.ordered_product_quantity
				) {
					throw badRequestError(
						res,
						'looking for is Out Of Stock. Please come back later'
					);
				}
			}
			//If Merchant off the status of the product
			else {
				throw badRequestError(
					res,
					'Item you are looking for is Out Of Stock. Please come back later'
				);
			}

			//	console.log(typeof new Date().toISOString);

			/*		let ordered_product = await OrderedProducts.query()
				.sum('ordered_product_quantity')
				.where(function () {
					this.whereRaw('"created_at"::date = ?', [
						moment().format('YYYY-MM-DD'),
					]);
				})
				.andWhere('product_id', product_data.product_id)
				.first();

			if (ordered_product.sum == null) {
				ordered_product.sum = 0;
			}

			if (product) {
				if (
					product.product_quantity_counter - ordered_product.sum ==
					product.product_threshold_quantity
				) {
					let date = new Date().toISOString;
					await Product.query()
						.update({
							product_threshold_date: date,
						})
						.where({
							id: product_data.product_id,
							delete_status: false,
							status: 1,
						})
						.first();
				}
				if (
					product.product_quantity_counter - ordered_product.sum <
					product_data.ordered_product_quantity
				) {
					throw badRequestError(res, 'Out of Stock');
				}
				if (
					product.products_per_order_limit != 0 &&
					product.products_per_order_limit <
						product_data.ordered_product_quantity
				) {
					throw badRequestError(
						res,
						'You cannot Order product ' +
							product.product_name +
							' more than ' +
							product.products_per_order_limit
					);
				}
			} else {
				throw badRequestError(res, 'Item not available at the moment');
			}
			*/
		}
		//stripe

		body.total_amount = (+body.total_amount).toFixed(2);
		// let stripeAmount = body.total_amount * 100;
		// stripeAmount = parseInt((+stripeAmount).toFixed(2));
		let payment_confirm;
		if (device_type) {
			if (device_type == 'ios') {
				payment_confirm = await Stripe.paymentConfirm(body.payment_method);
			} else {
				payment_confirm = await stripe.paymentConfirm(body.payment_method);
			}
		} else {
			payment_confirm = await stripe.paymentConfirm(body.payment_method);
		}

		console.log(payment_confirm);
		body.transaction_id = payment_confirm;
		body.user_id = userId;
		body.order_status = 1;
		const Order = await order
			.query()
			.select('id')
			.orderBy('id', 'desc')
			.first();
		console.log(Order);
		if (Order) {
			body.order_number = 'FAV' + Order.id;
		} else {
			body.order_number = 'FAV1';
		}
		//console.log(body.order_number);

		// return;

		//Order Entry in table
		delete body.payment_method;
		delete body.device_type;
		const addItem = await transaction(order.knex(), (trx) => {
			return order
				.query(trx)
				.insertGraph(body)
				.catch((err) => {
					console.log(err);
				});
		});
		if (addItem) {
			const delete_cart = await Cart.query().delete().where('user_id', userId);

			//--------------------------(START) Code to send order details on mail-----------------------------
			let orderDetails = body;
			let MerchantInfo = await User.query()
				.select('restaurant_name', 'mobile', 'country_isd_code')
				.eager('[user_address_relation, merchant_characteristic_relation]')
				.modifyEager('user_address_relation', (builder) => {
					return builder.select('full_address');
				})
				.modifyEager('merchant_characteristic_relation', (builder) => {
					return builder.select('gst_number');
				})
				.modifyEager('merchant_characteristic_relation', (builder) => {
					return builder.select('gst_number');
				})
				.where('id', orderDetails.merchant_id)
				.first();

			let CustomerInfo = await User.query()
				.select(
					'first_name',
					'last_name',
					'mobile',
					'country_isd_code',
					'email'
				)
				.eager('user_address_relation')
				.modifyEager('user_address_relation', (builder) => {
					return builder.select('full_address');
				})
				.where('id', userId)
				.first();

			let orderType;
			if (
				orderDetails.order_type_id == 1 ||
				orderDetails.order_type_id == 5 ||
				orderDetails.order_type_id == 6
			) {
				orderType = 'DineIn';
			} else if (orderDetails.order_type_id == 2) {
				orderType = 'Pickup';
			} else if (orderDetails.order_type_id == 4) {
				orderType = 'Delivery';
			}

			let table;
			if (orderDetails.table_no && orderDetails.table_no != '') {
				table = orderDetails.table_no;
			} else {
				table = 'N/A';
			}

			let sum;
			for (let i = 0; i < orderDetails.ordered_products_relation.length; i++) {
				sum = 0;
				let product_id = orderDetails.ordered_products_relation[i].product_id;
				let quantity =
					orderDetails.ordered_products_relation[i].ordered_product_quantity;

				let product = await Product.query()
					.select('id', 'product_quantity_counter')
					.where({ id: product_id, delete_status: false, status: 1 })
					.first();

				if (product) {
					if (product.product_quantity_counter - quantity <= 0) {
						//Updating Product Quantity when order placed
						let update_product = await Product.query()
							.update({
								product_quantity_counter: 0,
								status: 2,
							})
							.where({ id: product.id, delete_status: false, status: 1 });
					} else {
						let update_product = await Product.query()
							.update({
								product_quantity_counter:
									product.product_quantity_counter - quantity,
							})
							.where({ id: product.id, delete_status: false, status: 1 });
					}
				}

				for (
					let j = 0;
					j <
					orderDetails.ordered_products_relation[i].products_customization
						.length;
					j++
				) {
					let modifiers = await Modifier.query()
						.select('modifier_name', 'display_name')
						.where(
							'id',
							orderDetails.ordered_products_relation[i].products_customization[
								j
							].modifier_id
						)
						.first();
					// console.log(modifiers)
					if (modifiers.display_name != '') {
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].modifier_name = modifiers.display_name;
					} else {
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].modifier_name = modifiers.modifier_name;
					}
					for (
						let k = 0;
						k <
						orderDetails.ordered_products_relation[i].products_customization[j]
							.order_product_modifier_detail.length;
						k++
					) {
						let modifierPrices = await ModifiersProduct.query()
							.select('modifier_product', 'selling_price')
							.where(
								'id',
								orderDetails.ordered_products_relation[i]
									.products_customization[j].order_product_modifier_detail[k]
									.modifier_product_detail_id
							)
							.first();
						// console.log(modifierPrices)
						sum =
							sum +
							modifierPrices.selling_price *
								orderDetails.ordered_products_relation[i]
									.products_customization[j].order_product_modifier_detail[k]
									.modifier_quantity;
						orderDetails.ordered_products_relation[i].products_customization[
							j
						].order_product_modifier_detail[k].modifier_product =
							modifierPrices.modifier_product;
					}
				}
				// orderDetails.ordered_products_relation[i].PriceWithModifiers = sum + orderDetails.ordered_products_relation[i].product_price;
				//orderDetails.ordered_products_relation[i].PriceWithModifiers = sum + (orderDetails.ordered_products_relation[i].product_price * orderDetails.ordered_products_relation[i].ordered_product_quantity);
				orderDetails.ordered_products_relation[i].PriceWithModifiers =
					sum +
					orderDetails.ordered_products_relation[i].product_price *
						parseInt(
							orderDetails.ordered_products_relation[i].ordered_product_quantity
						);
			}

			let orderDetailsToSendEmail = {
				email: CustomerInfo.email,
				merchant_name: MerchantInfo.restaurant_name,
				merchant_mobile: MerchantInfo.country_isd_code + MerchantInfo.mobile,
				merchant_address: MerchantInfo.user_address_relation[0].full_address,
				customer_name:
					CustomerInfo.first_name + ' ' + CustomerInfo.last_name.charAt(0),
				customer_mobile: CustomerInfo.country_isd_code + CustomerInfo.mobile,
				customer_address: body.delivery_address,
				order_number: body.order_number,
				table: table,
				orderType: orderType,
				order_type_id: body.order_type_id,
				orderStatus: 'Placed',
				paymentStatus: 'Paid',
				order_date: moment().format('YYYY-MM-DD HH:mm'),
				comment: body.comment,
				order_amount: body.order_amount,
				delivery_fee: body.deliver_charges,
				total_amount: body.total_amount,
				gst_number: MerchantInfo.merchant_characteristic_relation.gst_number,
				ordered_products: orderDetails.ordered_products_relation,
			};
			//console.log(body);

			let orderStatusFlowMaintain = await OrderStatusFlowManagement.query()
				.insert({ order_id: addItem.id, order_status: 1 })
				.returning('*');
			if (!orderStatusFlowMaintain)
				throw badRequestError(res, 'Something Went Wrong.');
			if (orderDetailsToSendEmail.email != '') {
				EMAIL.sendOrderDetails(orderDetailsToSendEmail);
			}
			//--------------------------(END) Code to send order details on mail-----------------------------
			console.log(`OrderSocketGet${body.merchant_id}`, 'evnetName');
			req.io.emit(`OrderSocketGet${body.merchant_id}`, {
				order_type_id: body.order_type_id,
			}); //event emit for socket notification admin
			return okResponse(res, {}, 'Order successfully placed');
		}
	} catch (err) {
		console.log(err);
		throw badRequestError(res, err);
	}
};

module.exports = {
	ModifierEditApi,
	deleteCart,
	Reorder,
	placeOrder,
	placeOrderHistory,
	placeOrderwithModifier,
	checkout_without_modifier,
	checkout_with_modifier,
	restaurantListWithFilter,
	restaurantDetail,
	viewCartwithModifier,
	menuListbyId,
	menuList,
	productGroupModifier,
	addToCart,
	addToCartFinal,
	CheckItemInCart,
	viewCart,
	removeItemFromCart,
	removeCartAndAddItem,
	removeCartAndAddScanItem,
	placeOrderHistoryById,
	checkCartDetail,
	CategoryList,
	addtoCart,
	calculatedelivery_charges,
	scanProductAndAddToCart,
	scanProductAndAddToCartVersionQrCode,
	menuListVersionQrCode,
	test_socket_notification,
	checkout_with_modifier_v2,
	placeOrder_v2,
	checkout_with_modifier_v3,
	placeOrder_v3,
};
