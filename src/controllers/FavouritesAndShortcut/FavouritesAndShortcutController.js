"use strict"

/**
 * @author SHUBHAM GUPTA
 */

 //----------------------------------------------FavouritesAndShortcutController (Start)------------------------------------------------------------------

//Import Dependency 3rd Party Modules Here


//Import Models Here
const Favourites = require("../../models/Favourites");

//========================================================================================

/**
 * add to favourites
 * @description: function is used to add item, order or restaurant into favourites
*/
const AddToFavourites = async (req, res) => {
    try {
        let data = req.body;

        if (!data.relation_id) throw badRequestError(res, "Please enter relation_id!");
        if (!data.relation_type) throw badRequestError(res, "Please enter relation_type!");

        //------------Check Favourite Already Exist Or Not (START)------------------
        let checkFavoriteAlreadyExist = Favourites.query().select("*").where({ relation_id: data.relation_id, relation_type: data.relation_type, user_id: req.user.user_id});
        if(checkFavoriteAlreadyExist) throw badRequestError(res, "Added as favorite already!")
        //------------Check Favourite Already Exist Or Not (END)------------------

        data.user_id = req.user.user_id;
        let addFavorite = await Favourites.query().insert(data).returning("*");

        return createdResponse(res, addFavorite, "Added as favourite successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * remove from favourites
 * @description: function is used to remove items, orders or restaurants from favourites
*/
const RemoveFromFavourites = async (req, res) => {
    try {
        let favourite_id = req.query.favourite_id;

        if (!favourite_id) throw badRequestError(res, "Please pass favourite_id into query string!");

        let UnFavorite = await Favourites.query().delete().where({ id: favourite_id, user_id: req.user.user_id}).first().returning("*");

        if(UnFavorite == undefined) throw badRequestError(res, "Record Can't be deleted because it is not exist!");

        return okResponse(res, UnFavorite, "Removed from favourites successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * all favourites List
 * @description: function is used to get list from favourites
*/
const AllFavouritesList = async (req, res) => {
    try {
        let relation_type = req.query.relation_type;
        let allFavorites;
        let limit = req.query.limit ? req.query.limit : 10;

        if (!relation_type) throw badRequestError(res, "Please pass relation_type into query string!");

        if (relation_type == "restaurant") {
            allFavorites = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id", 
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email", 
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ user_id: req.user.user_id, relation_type: relation_type })
                .where(builder => {
                    if (req.query.createdAt != "") {
                        return builder.where("favourites.created_at", "<", req.query.createdAt);
                    }
                })
                .orderBy("favourites.created_at", "desc")
                .limit(limit)
                .returning("*");
        }
        else if (relation_type == "order") {
            allFavorites = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ user_id: req.user.user_id, relation_type: relation_type })
                .where(builder => {
                    if (req.query.createdAt != "") {
                        return builder.where("favourites.created_at", "<", req.query.createdAt);
                    }
                })
                .orderBy("favourites.created_at", "desc")
                .limit(limit)
                .returning("*");
        }
        else if (relation_type == "item") {
            allFavorites = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ user_id: req.user.user_id, relation_type: relation_type })
                .where(builder => {
                    if (req.query.createdAt != "") {
                        return builder.where("favourites.created_at", "<", req.query.createdAt);
                    }
                })
                .orderBy("favourites.created_at", "desc")
                .limit(limit)
                .returning("*");
        }

        return okResponse(res, allFavorites, "Get all favourites successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}



/**
 * Get favourites Details
 * @description: function is used to get favourite details
*/
const GetFavouritesDetails = async (req, res) => {
    try {
        let GetFavorite, favourite_id = req.query.favourite_id;

        if (!favourite_id) throw badRequestError(res, "Please pass favourite_id into query string!");
        
        let GetFavoriteData = await Favourites.query().select("*").where({ user_id: req.user.user_id, id: favourite_id}).first().returning("*");

        if(GetFavoriteData == undefined) throw badRequestError(res, "Favourite with this FavoriteId can't Exist!")

        if(GetFavoriteData.relation_type == "restaurant"){
            GetFavorite = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ 'favourites.id': favourite_id, 'favourites.user_id': req.user.user_id }).first().returning("*");
        }
        else if (GetFavoriteData.relation_type == "order"){
            GetFavorite = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ 'favourites.id': favourite_id, 'favourites.user_id': req.user.user_id }).first().returning("*");
        }
        else if (GetFavoriteData.relation_type == "item") {
            GetFavorite = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ 'favourites.id': favourite_id, 'favourites.user_id': req.user.user_id }).first().returning("*");
        }

        return okResponse(res, GetFavorite, "Get favourites detail successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Add to ShortCut
 * @description: function is used to add to shortcut
 */
const AddToShortcut = async (req, res) => {
    try {
        let favourite_id = req.query.favourite_id;

        if (!favourite_id) throw badRequestError(res, "Please pass favourite_id into query string!");
        
        //----------Check Favorite Exist Or Not (START)-----------------------
        let checkFavorite = await Favourites.query().select("*").where({ id: favourite_id, user_id: req.user.user_id }).first().returning("*");
        if (checkFavorite == undefined) throw badRequestError(res, "Record Can't exist!")
        if (checkFavorite.relation_type == "item") throw badRequestError(res, "Item Can't be added as shortcut!");
        //----------Check Favorite Exist Or Not (END)-----------------------

        //----------Check Record Already added as Shortcut Or Not (START)-------------------
        let checkShortCutAlreadyExist = await Favourites.query().where({ id: favourite_id, shortcut: true}).first().returning("*");
        if (checkShortCutAlreadyExist) throw badRequestError(res, "Already added as shortcut!");
        //----------Check Record Already added as Shortcut Or Not (END)-------------------

        //----------Check Shortcuts can not be added more than 5 (START)-----------------------
        let checkShortcuts = await Favourites.query().select("*").where({ user_id: req.user.user_id, shortcut: true});
        console.log(checkShortcuts.length);        
        if(checkShortcuts.length == 5){
            throw badRequestError(res, "Shortcut Can't be added more than 5!")
        }
        //----------Check Shortcuts can not be added more than 5 (END)-----------------------

        let addShortCut = await Favourites.query().update({ shortcut: true }).where({ id: favourite_id, user_id: req.user.user_id }).first().returning("*");

        return okResponse(res, addShortCut, "Shortcut Added Successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * Remove From ShortCut
 * @description: function is used to remove from shortcut
 */
const RemoveFromShortcut = async (req, res) => {
    try {
        let favourite_id = req.query.favourite_id;

        if (!favourite_id) throw badRequestError(res, "Please pass favoriteId into query string!");

        let checkFavorite = await Favourites.query().select("*").where({ id: favourite_id, user_id: req.user.user_id }).first().returning("*");
        if (checkFavorite == undefined) throw badRequestError(res, "Record Can't exist!")
        
        let RemoveShortCut = await Favourites.query().update({ shortcut: false }).where({ id: favourite_id, user_id: req.user.user_id }).first().returning("*");
        if (RemoveShortCut == undefined) throw badRequestError(res, "Remove From Shortcut is not done!")

        return okResponse(res, RemoveShortCut, "Shortcut Removed Successfully!");
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


/**
 * All ShortCuts
 * @description: functiuon is used to get All Shortcuts
 */
const AllShortcuts = async (req, res) => {
    try {
        let relation_type = req.query.relation_type;
        let limit = req.query.limit ? req.query.limit : 10;
        
        if (!relation_type) throw badRequestError(res, "Please pass relation_type into query string!");        
        let allShortuts;

        if (relation_type == "restaurant") {
            allShortuts = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id", "favourites.shortcut",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ user_id: req.user.user_id, shortcut: true })
                .where(builder => {
                    if (req.query.createdAt != "") {
                        return builder.where("favourites.created_at", "<", req.query.createdAt);
                    }
                })
                .orderBy("favourites.created_at", "desc")
                .limit(limit)
                .returning("*");
        }
        else if (relation_type == "order") {
            allShortuts = await Favourites.query().select("favourites.id", "favourites.relation_id", "favourites.user_id", "favourites.shortcut",
                "favourites.relation_type", "favourites.created_at", "favourites.updated_at", "users.restaurant_name", "users.email",
                "users.mobile", "users.restaurant_website", "users.user_type", "users.user_status", "users.profile_image")
                .innerJoin('users', 'favourites.relation_id', 'users.id')
                .where({ user_id: req.user.user_id, shortcut: true })
                .where(builder => {
                    if (req.query.createdAt != "") {
                        return builder.where("created_at", "<", req.query.createdAt);
                    }
                })
                .orderBy("created_at", "desc")
                .limit(limit)
                .returning("*");
        }

        return okResponse(res, allShortuts, "All Shortcuts get successfully!");
        
    } catch (error) {
        console.log(error);
        throw badRequestError(res, error);
    }
}


//---Export the modules here------
module.exports = {
    AddToFavourites,
    RemoveFromFavourites,
    AllFavouritesList,
    GetFavouritesDetails,
    AddToShortcut,
    RemoveFromShortcut,
    AllShortcuts
}


 //----------------------------------------------FavouritesAndShortcutController (End)------------------------------------------------------------------