exports.up = function (knex) {
	return knex.schema
		.alterTable('cart_product_modifier', (table) => {
			table.dropColumn('cart_id');
		})
		.alterTable('cart_product_modifier_detail', (table) => {
			table.dropColumn('cart_modifier_id');
		});
};

exports.down = function (knex) {};
