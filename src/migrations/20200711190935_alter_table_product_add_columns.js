
exports.up = function (knex) {
    return knex.schema
        .alterTable('product', table => {
            table.boolean('isCommonDineIn');
            table.boolean('isCommonPickUp');
            table.boolean('isCommonDelivery');
        })
};

exports.down = function (knex) {

};
