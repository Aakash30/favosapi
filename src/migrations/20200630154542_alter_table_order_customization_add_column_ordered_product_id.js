
exports.up = function(knex) {
    return knex.schema
        .alterTable("order_customization", (table) => {
            table.integer("ordered_product_id").references('id').inTable('ordered_products');
        })

        .alterTable("orders", (table) => {
            table.dropColumn("order_status");
        })

        .alterTable("orders", (table) => {
            table.integer("order_status").comment('1 means new order, 2 means order accept, 3 means order preparing, 4 means order ready, 5 means order served, 6 means order complete, 7 means order cancel, 8 means payment refunded').defaultTo(1);
        })
};

exports.down = function(knex) {
  
};
