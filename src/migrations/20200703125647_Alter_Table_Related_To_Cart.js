exports.up = function (knex) {
	return knex.schema
		.alterTable('cart_product_modifier', (table) => {
			table.integer('user_id');
		})
		.alterTable('cart_product_modifier_detail', (table) => {
			table.integer('user_id');
		});
};

exports.down = function (knex) {};
