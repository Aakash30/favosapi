
exports.up = function(knex) {
    return knex.schema
        .alterTable("table", (table) => {
            table.string("qrcode_encrypt");
        })

        .alterTable("order_type_merchant_characteristics_management", (table) => {
            table.string("qrcode_encrypt");
        })

        .alterTable("product_pricing", (table) => {
            table.string("qrcode_encrypt");
        })
};

exports.down = function(knex) {
  
};
