
exports.up = function(knex) {
    return knex.schema
        .alterTable('users', table => {
            table.boolean('favourite');
            table.boolean('deactivate_account').defaultTo(false);
            table.string('earning_point');
        })
};

exports.down = function(knex) {
  
};
