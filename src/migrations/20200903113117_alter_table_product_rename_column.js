
exports.up = function(knex) {
    return knex.schema.alterTable("product", (table) => {
        table.renameColumn("product_quantity", "product_quantity_counter");
        table.renameColumn("product_original_quantity", "product_quantity");
    });
};

exports.down = function(knex) {
  
};
