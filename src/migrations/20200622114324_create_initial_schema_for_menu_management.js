
exports.up = function(knex) {
    return knex.schema
      .createTable("menu_management", (table) => {
        table.increments("id").primary();
        table.integer("merchant_id").references("id").inTable("users");
        table.string("menu_name");
        table.boolean("status").defaultTo(true);
        table.boolean("DineIn");
        table.boolean("PickUp");
        table.boolean("Delivery");
        table.date("start_date");
        table.date("end_date");
        table.timestamps(false, true);
      })

      .createTable("menu_order_type", (table) => {
        table.increments("id").primary();
        table.integer("menu_id").references("id").inTable("menu_management");
        table.integer("order_type_id").references("id").inTable("order_type");
        table.timestamps(false, true);
      })

      .createTable("categories", (table) => {
        table.increments("id").primary();
        table.integer("merchant_id").references("id").inTable("users");
        table.string("category_name");
        table.boolean("status").defaultTo(true);
        table.timestamps(false, true);
      })

      .createTable("modifier", (table) => {
        table.increments("id").primary();
        table.integer("merchant_id").references("id").inTable("users");
        table.string("modifier_name");
        table.boolean("status").defaultTo(true);
        table.integer("min_user_must_select");
        table.integer("max_user_must_select");
        table.timestamps(false, true);
      })

      .createTable("modifier_product_details", (table) => {
        table.increments("id").primary();
        table.integer("modifier_id").references("id").inTable("modifier");
        table.string("modifier_product");
        table.boolean("status").defaultTo(true);
        table.integer("selling_price");
        table.integer("compare_at_price");
        table.timestamps(false, true);
      })

      .createTable("modifier_group", (table) => {
        table.increments("id").primary();
        table.integer("merchant_id").references("id").inTable("users");
        table.string("modifier_group_name");
        table.boolean("status").defaultTo(true);
        table.timestamps(false, true);
      })

      .createTable("modifier_group_modifiers", (table) => {
        table.increments("id").primary();
        table
          .integer("modifier_group_id")
          .references("id")
          .inTable("modifier_group");
        table.integer("modifier_id").references("id").inTable("modifier");
        table.boolean("status").defaultTo(true);
        table.timestamps(false, true);
      })

      .createTable("suggestions", (table) => {
        table.increments("id").primary();
        table.string("suggestion");
        table.timestamps(false, true);
      })

      .createTable("product_tags", (table) => {
        table.increments("id").primary();
        table.string("tags");
        table.timestamps(false, true);
      })

      .createTable("product", (table) => {
        table.increments("id").primary();
        table.string("product_name");
        table.string("product_image");
        table.string("product_description");
        table.integer("product_quantity");
        table.integer("product_threshold_quantity");
        table.integer("product_replenish_days");
        table.integer("products_per_order_limit");
        table
          .integer("suggestion_type_id")
          .references("id")
          .inTable("suggestions");
        table.boolean("requires_age_verification");
        table.timestamps(false, true);
      })

      .createTable("product_menu", (table) => {
        table.increments("id").primary();
        table.integer("product_id").references("id").inTable("product");
        table.integer("menu_id").references("id").inTable("menu_management");
        table.boolean("status").defaultTo(true);
        table.timestamps(false, true);
      })

      .createTable("product_category", (table) => {
        table.increments("id").primary();
        table.integer("product_id").references("id").inTable("product");
        table.integer("category_id").references("id").inTable("categories");
        table.string("category_name");
        table.boolean("status").defaultTo(true);
        table.timestamps(false, true);
      })

      .createTable("product_tags_management", (table) => {
        table.increments("id").primary();
        table.integer("product_id").references("id").inTable("product");
        table
          .integer("product_tags_id")
          .references("id")
          .inTable("product_tags");
        table.string("product_tag_name");
        table.timestamps(false, true);
      })

      .createTable("product_modifier_groups", (table) => {
        table.increments("id").primary();
        table.integer("product_id").references("id").inTable("product");
        table
          .integer("modifier_group_id")
          .references("id")
          .inTable("modifier_group");
        table.timestamps(false, true);
      })

      .createTable("product_modifiers", (table) => {
        table.increments("id").primary();
        table.integer("product_id").references("id").inTable("product");
        table.integer("modifier_id").references("id").inTable("modifier");
        table.timestamps(false, true);
      })

      .createTable("product_pricing", (table) => {
        table.increments("id").primary();
        table.integer("product_id").references("id").inTable("product");
        table.integer("order_type_id").references("id").inTable("order_type");
        table.integer("menu_id").references("id").inTable("menu_management");
        table.integer("price");
        table.integer("selling_price");
        table.timestamps(false, true);
      });
};

exports.down = function(knex) {
  
};
