exports.up = function (knex) {
	return knex.schema.alterTable('cart_product_modifier', (table) => {
		table.integer('modifier_quantity');
	});
};

exports.down = function (knex) {};
