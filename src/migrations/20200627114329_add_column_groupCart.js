
exports.up = function(knex) {
    return knex.schema
    .table('cart_product_modifier', table => {
        table.integer("group_id").references("id").inTable("modifier_group");
    })
    .table('cart_product_modifier_detail', table => {
        table.integer("modifier_id").references("id").inTable("modifier");
    })
};

exports.down = function(knex) {
  
};
