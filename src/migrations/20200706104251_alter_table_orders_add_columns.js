
exports.up = function(knex) {
  return knex.schema
        .alterTable("orders", (table) => {
            table.string("order_cancellation_reason");
            table.float("estimated_time");
        })
};

exports.down = function(knex) {
  
};
