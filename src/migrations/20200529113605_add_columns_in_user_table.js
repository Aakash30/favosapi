
exports.up = function(knex) {
    return knex.schema
        .alterTable('users', table => {
            table.integer('opening_status').comment('1 means opening soon, 2 means open, 3 means closed-temporarily, 4 means closed-permanently.');
            table.string('landmark');
            table.boolean('is_restaurant_located').defaultTo(false);
            table.string("details");
        })
};

exports.down = function(knex) {
  
};
