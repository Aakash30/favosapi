
exports.up = function(knex) {
    return knex.schema
        .alterTable("users", (table) => {
            table.dropColumn("deleteRequestTime");
        })

        .alterTable("users", (table) => {
            table.datetime("delete_request_date");
        })

        .alterTable("product", (table) => {
            table.integer("product_original_quantity");
        });
};

exports.down = function(knex) {
  
};
