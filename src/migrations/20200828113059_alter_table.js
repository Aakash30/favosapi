
exports.up = function(knex) {
    return knex.schema
        .alterTable('orders', function (t) {
            t.float('total_amount').alter();
            t.float('order_amount').alter();
        })
};

exports.down = function(knex) {
  
};
