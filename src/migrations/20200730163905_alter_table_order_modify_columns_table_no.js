
exports.up = function(knex) {
    return knex.schema
        .alterTable("orders", (table) => {
            table.dropColumn("table_no");
        })

        .alterTable("orders", (table) => {
            table.string("table_no");
        });
};

exports.down = function(knex) {
  
};
