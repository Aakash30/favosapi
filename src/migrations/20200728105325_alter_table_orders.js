exports.up = function (knex) {
	return knex.schema.alterTable('orders', (table) => {
		table.dropColumn('delivery_address_id');
		table.string('delivery_address');
	});
};

exports.down = function (knex) {};
