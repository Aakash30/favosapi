exports.up = function (knex) {
	return knex.schema.alterTable('product_pricing', function (t) {
		t.float('price').alter();
		t.float('selling_price').alter();
	});
};

exports.down = function (knex) {};
