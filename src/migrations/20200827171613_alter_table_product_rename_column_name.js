
exports.up = function(knex) {
    return knex.schema.alterTable("product", (table) => {
      table.renameColumn("isCommonDineIn", "is_common_dinein");
      table.renameColumn("isCommonPickUp", "is_common_pickup");
      table.renameColumn("isCommonDelivery", "is_common_delivery");
    });
};

exports.down = function(knex) {
  
};
