exports.up = function (knex) {
	return knex.schema
		.alterTable('modifier_product_details', function (t) {
			t.float('compare_at_price').alter();
			t.float('selling_price').alter();
		})
		.alterTable('order_product_modifier_detail', function (t) {
			t.float('selling_price').alter();
		})
		.alterTable('ordered_products', function (t) {
			t.float('product_price').alter();
		});
};

exports.down = function (knex) {};
