exports.up = function (knex) {
	return knex.schema
		.alterTable('cart_product_modifier_detail', (table) => {
			table.integer('modifier_quantity');
		})
		.alterTable('cart_product_modifier', (table) => {
			table.dropColumn('modifier_quantity');
		});
};

exports.down = function (knex) {};
