
exports.up = function(knex) {
    return knex.schema
        .createTable('delivery_charges', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.integer("from");
            table.integer('to');
            table.float('delivery_fee');
            table.float('minimum_order');
            table.float('free_delivery_order');
            table.string('delivery_note');
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
