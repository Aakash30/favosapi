
exports.up = function(knex) {
    return knex.schema
        .alterTable("merchant_characteristics", (table) => {
            table.string("timing");
            table.boolean("opening_status");
            table.boolean("online_order_status").defaultTo(true)
        })
};

exports.down = function(knex) {
  
};
