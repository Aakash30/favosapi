exports.up = function (knex) {
	return knex.schema.alterTable('orders', (table) => {
		table.string('delivery_address');
		table.dropColumn('delivery_address_id');
	});
};

exports.down = function (knex) {};
