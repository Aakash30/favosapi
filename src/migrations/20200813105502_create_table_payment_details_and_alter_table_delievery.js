
exports.up = function(knex) {
    return knex.schema
        .createTable('payment_details', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.string("first_name");
            table.string("last_name");
            table.boolean('use_business_email').defaultTo(false);
            table.string("billing_email");
            table.string("bank_account_number");
            table.string("sort_code");
            table.integer("payment_option").comment('1 means card only, 2 means cash only, 3 means cash and card');
            table.string('delivery_note')
            table.timestamps(false, true);
        })

        .alterTable("delivery_charges", table => {
            table.integer("payment_details_id").references('id').inTable('payment_details');
        });
};

exports.down = function(knex) {
  
};
