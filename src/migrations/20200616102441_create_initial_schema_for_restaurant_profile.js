
exports.up = function(knex) {
    return knex.schema
        .createTable('merchant_type', table => {
            table.increments('id').primary();
            table.string("merchant_type");
            table.timestamps(false, true);
        })

        .createTable('merchant_tags', table => {
            table.increments('id').primary();
            table.integer("merchant_type_id").references('id').inTable('merchant_type');
            table.string("merchant_tags")
            table.timestamps(false, true);
        })

        .createTable('merchant_characteristics', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.integer("merchant_type_id").references('id').inTable('merchant_type');
            table.boolean("seating").comment('true means seating available, false means no seating available.');
            table.string("table").comment('1 means dedicated table, 2 means shared table.');
            table.boolean("isAlcohol").comment('true means alcohol served, false means alcohol not served.');
            table.string("estimated_time_permission").comment('1 means Send to kitchen only, 2 means Send to kitchen and customer.');
            table.string("food_certificate");
            table.string("food_grade");
            table.date("issue_date");
            table.date("expiry_date");
            table.timestamps(false, true);
        })

        .createTable('merchant_tags_characteristics_management', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.integer("merchant_type_id").references('id').inTable('merchant_type');
            table.integer("merchant_tag_id").references('id').inTable('merchant_tags');
            table.string("merchant_tag_name")
            table.timestamps(false, true);
        })

        .createTable('amenties', table => {
            table.increments('id').primary();
            table.string("amenties_name")
            table.timestamps(false, true);
        })

        .createTable('cuisines', table => {
            table.increments('id').primary();
            table.string("cuisine_name")
            table.timestamps(false, true);
        })

        .createTable('merchant_amenties', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.integer("amenties_id").references('id').inTable('amenties');
            table.string("amenties_name")
            table.timestamps(false, true);
        })

        .createTable('merchant_cuisines', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.integer("cuisines_id").references('id').inTable('cuisines');
            table.string("cuisine_name")
            table.timestamps(false, true);
        })

        .createTable('order_type', table => {
            table.increments('id').primary();
            table.integer("parent_id").references('id').inTable('order_type');
            table.string("order_type");
            table.timestamps(false, true);
        })

        .createTable('order_type_merchant_characteristics_management', table => {
            table.increments('id').primary();
            table.integer("merchant_id").references('id').inTable('users');
            table.integer("order_type_id").references('id').inTable('order_type');
            table.integer("max_no_of_reservation_per_person_per_day");
            table.integer("max_no_of_people_per_reservation")
            table.timestamps(false, true);
        })

                
};

exports.down = function(knex) {
  
};
