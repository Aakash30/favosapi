
exports.up = function(knex) {
    return knex.schema
        .alterTable("merchant_characteristics", (table) => {
            table.dropColumn("delivery_note");
        })

        .alterTable("product", table => {
            table.datetime("today_out_of_stock");
        });
};

exports.down = function(knex) {
  
};
