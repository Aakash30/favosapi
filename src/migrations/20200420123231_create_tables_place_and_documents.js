
exports.up = function(knex) {
    return knex.schema
        .createTable('place_added_requested', table => {
            table.increments('id').primary();
            table.string("bussiness_relation");
            table.integer('user_id').references('id').inTable('users');
            table.string("bussiness_name");
            table.string("restaurant_address");
            table.string("restaurant_website");
            table.string("restaurant_mobile");
            table.string("additional_details");
            table.string("restaurant_image");
            table.string("your_mobile");
            table.string("request_approval")
            table.timestamps(false, true);
        })

        .createTable('ownership_documents', table => {
            table.increments('id').primary();
            table.integer("bussiness_request_id").references('id').inTable('place_added_requested');
            table.string('documents');
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
