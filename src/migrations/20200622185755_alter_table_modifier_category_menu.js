
exports.up = function(knex) {
  return knex.schema
    .alterTable("modifier", (table) => {
      table.boolean("delete_status").defaultTo(false);
      table.string("display_name");
    })

    .alterTable("menu_management", (table) => {
      table.boolean("delete_status").defaultTo(false);
    })

    .alterTable("categories", (table) => {
      table.boolean("delete_status").defaultTo(false);
    })

    .alterTable("product", (table) => {
      table.boolean("delete_status").defaultTo(false);
    })

    .alterTable("modifier_group", (table) => {
      table.boolean("delete_status").defaultTo(false);
    });
};

exports.down = function(knex) {
  
};
