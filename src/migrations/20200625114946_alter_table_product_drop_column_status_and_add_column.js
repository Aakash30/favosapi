exports.up = function (knex) {
    return knex.schema
        .alterTable("product", table => {
            table.dropColumn("status");
        })

        .alterTable("product", (table) => {
            table.integer("status").defaultTo(1).comment('1 means active, 2 means out of stock, 3 means out of stock for today and 4 means Inactive.');
        })
};

exports.down = function (knex) {

};
