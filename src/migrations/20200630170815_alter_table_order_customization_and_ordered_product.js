
exports.up = function(knex) {
    return knex.schema
        .alterTable("order_customization", (table) => {
            table.string("modifier_name");
            table.string("group_modifier_name");
        })

        .alterTable("ordered_products", (table) => {
            table.string("product_name");
            table.integer("product_price");
        })
};

exports.down = function(knex) {
  
};
