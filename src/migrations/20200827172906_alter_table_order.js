exports.up = function (knex) {
	return knex.schema.alterTable('orders', function (t) {
		t.float('order_amount').alter();
		t.float('total_amount').alter();
		t.float('deliver_charges').alter();
	});
};

exports.down = function (knex) {};
