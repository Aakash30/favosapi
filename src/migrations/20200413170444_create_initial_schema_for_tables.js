
exports.up = function(knex) {
  
    return knex.schema
    .createTable('country', table => {
        table.increments('id').primary();
        table.string("country_name");
        table.string('country_iso_code');
        table.string('country_isd_code');
        table.timestamps(false, true);
    })

    .createTable('users', table => {
        table.increments('id').primary();
        //refer to primary key of user
        table.integer('parent_id').references('id').inTable('users');
        table.string('email').unique();
        table.string('mobile').unique();
        table.string('restaurant_name');
        table.string('first_name', 120);
        table.string('last_name', 120);
        table.string("gender");
        table.date('date_of_birth');
        table.string('password');
        table.string('profile_image');
        table.string("user_type");
        table.string("login_type");
        table.boolean("notification_toggle").defaultTo(true);
        table.boolean("user_status").defaultTo(true);
        table.string('reset_password_token').comment('token need to reset a password');
        table.string('restaurant_website');        
        table.integer('tax_percent');
        table.integer('country_id').references('id').inTable('country');
        table.string('referr_code');
        table.string('referral_code');
        table.string('first_time_discount');
        table.string('language_select');
        table.integer('otp');
        table.string('account_delete_reason');
        table.boolean('membership');
        table.timestamps(false, true);
    })
  
    .createTable('auth_table', table => {
        table.increments('id').primary();
        table.integer("user_id").references('id').inTable('users');
        table.string("auth_token");
        table.string("device_id");
        table.string("device_type");
        table.string("device_token");
        table.timestamps(false, true);
    })

    .createTable('address_management', table => {
        table.increments('id').primary();
        table.integer('user_id').references('id').inTable('users');
        table.string('full_address');
        table.string('lat');
        table.string('lng');
        table.boolean('active_address');
        table.string("address_type");
    })
};

exports.down = function(knex) {
  
};
