
exports.up = function(knex) {
  return knex.schema
        .alterTable("table", (table) => {
            table.dropColumn("occupied_by");
        })

        .alterTable("table", (table) => {
            table.integer("occupied_by").references("id").inTable("orders");
        })
};

exports.down = function(knex) {
  
};
