
exports.up = function(knex) {
    return knex.schema.createTable("cart", (table) => {
        table.increments("id").primary();
        table.integer("user_id").references("id").inTable("users");
        table.integer("merchant_id").references("id").inTable("users");
        table.integer("product_id").references("id").inTable("product");
        table.integer("menu_id").references("id").inTable("menu_management");
        table.integer("category_id").references("id").inTable("categories");
        table.integer("order_type_id").references("id").inTable("order_type");
        table.integer("quantity");
        table.timestamps(false, true);
      })
      .createTable("cart_product_group", (table) => {
        table.increments("id").primary();
        table.integer("group_id").references("id").inTable("modifier_group");
        table.integer("cart_id").references("id").inTable("cart");
        table.timestamps(false, true);
      })
      .createTable("cart_product_modifier", (table) => {
        table.increments("id").primary();
        table.integer("modifier_id").references("id").inTable("modifier");
        table.integer("cart_group_id").references("id").inTable("cart_product_group");
        table.integer("cart_id").references("id").inTable("cart");
        table.timestamps(false, true);
      })
      .createTable("cart_product_modifier_detail", (table) => {
        table.increments("id").primary();
        table.integer("modifier_product_detail_id").references("id").inTable("modifier_product_details");
        table.integer("cart_modifier_id").references("id").inTable("cart_product_modifier");
        table.timestamps(false, true);
      })
};

exports.down = function(knex) {
  
};
