
exports.up = function(knex) {
    return knex.schema
        .alterTable("users", table => {
            table.dropColumn("first_time_discount");
            table.dropColumn("language_select");
            table.dropColumn("membership");
            table.dropColumn("favourite");
            table.dropColumn("deactivate_account");
            table.dropColumn("business_logo");
            table.dropColumn("tax_percent");
        })
};

exports.down = function(knex) {
  
};
