exports.up = function (knex) {
	return knex.schema.createTable('apple_login_detail', (table) => {
		table.increments('id').primary();
		table.string('appleId');
		table.string('email');
		table.string('firstName');
		table.string('lastName');
		table.timestamps(false, true);
	});
};

exports.down = function (knex) {};
