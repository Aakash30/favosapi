exports.up = function (knex) {
	return knex.schema
		.createTable('order_status', (table) => {
			table.increments('id').primary();
			table.string('name');
			table.string('status').defaultTo(true);
			table.timestamps(false, true);
		})

		.createTable('orders', (table) => {
			table.increments('id').primary();
			table.integer('user_id').references('id').inTable('users');
			table.integer('table_id').references('id').inTable('table');
			table.string('order_number');
			table.integer('order_type_id').references('id').inTable('order_type');
			table.float('order_amount');
			table.float('total_amount');
			table.string('comment');
			table.integer('order_status_id').references('id').inTable('order_status');
			table.string('payment_id');
			table.string('transaction_id');
			table.timestamps(false, true);
		})

		.createTable('ordered_products', (table) => {
			table.increments('id').primary();
			table.integer('order_number_id').references('id').inTable('orders');
			table.integer('product_id').references('id').inTable('product');
			table.integer('ordered_product_quantity');
			table.timestamps(false, true);
		})

		.createTable('order_customization', (table) => {
			table.increments('id').primary();
			table.integer('order_number_id').references('id').inTable('orders');
			table.integer('product_id').references('id').inTable('product');
			table.integer('modifier_id').references('id').inTable('modifier');
			table
				.integer('modifier_group_id')
				.references('id')
				.inTable('modifier_group');
			table.timestamps(false, true);
		});
};

exports.down = function (knex) {};
