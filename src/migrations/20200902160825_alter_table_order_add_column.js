
exports.up = function(knex) {
    return knex.schema.alterTable('orders', (table) => {
        table.string('delivery_lat');
        table.string('delivery_lng');
    });
};

exports.down = function(knex) {
  
};
