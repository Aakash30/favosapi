
exports.up = function(knex) {
    return knex.schema
        .createTable('order_status_management', table => {
            table.increments('id').primary();
            table.integer("order_id").references('id').inTable('orders');
            table.integer("order_status")
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
