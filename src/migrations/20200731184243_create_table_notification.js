
exports.up = function(knex) {
    return knex.schema.createTable("notification", (table) => {
        table.increments("id").primary();
        table.integer("user_id").references("id").inTable("users");
        table.integer("order_id").references("id").inTable("orders");
        table.string("type");
        table.string("message");
        table.boolean("read_status");
        table.timestamps(false, true);
    })
};

exports.down = function(knex) {
  
};
