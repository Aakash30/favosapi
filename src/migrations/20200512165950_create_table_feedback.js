
exports.up = function(knex) {
    return knex.schema
        .createTable('feedback', table => {
            table.increments('id').primary();
            table.integer("user_id").references('id').inTable('users');
            table.integer("merchant").references("id").inTable("users");
            table.string("subject");
            table.string("description");
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
    
};
