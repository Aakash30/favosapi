
exports.up = function(knex) {
    return knex.schema
        .createTable('contact_us', table => {
            table.increments('id').primary();
            table.integer("user_id").references('id').inTable('users');
            table.string("email");
            table.text("how_can_help");
            table.string('merchant_name');
            table.text("message");
            table.string("name");
            table.string("phone");
            table.string("user_type")
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
