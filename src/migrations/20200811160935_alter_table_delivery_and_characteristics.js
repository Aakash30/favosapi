
exports.up = function(knex) {

    return knex.schema
        .alterTable("merchant_characteristics", (table) => {
            table.string("delivery_note");
        })

        .alterTable("delivery_charges", table => {
            table.dropColumn("delivery_note");
        });
};

exports.down = function(knex) {
  
};
