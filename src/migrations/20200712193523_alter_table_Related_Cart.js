exports.up = function (knex) {
	return knex.schema
		.alterTable('cart_product_modifier', (table) => {
			table
				.integer('cart_id')
				.references('id')
				.inTable('cart')
				.onDelete('CASCADE');
		})
		.alterTable('cart_product_modifier_detail', (table) => {
			table
				.integer('cart_modifier_id')
				.references('id')
				.inTable('cart_product_modifier')
				.onDelete('CASCADE');
		});
};

exports.down = function (knex) {};
