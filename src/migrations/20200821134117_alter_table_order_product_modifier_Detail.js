exports.up = function (knex) {
	return knex.schema.alterTable('order_product_modifier_detail', (table) => {
		table.boolean('selling_price');
	});
};

exports.down = function (knex) {};
