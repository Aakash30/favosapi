
exports.up = function(knex) {
    return knex.schema
        .createTable('seating_facility', table => {
            table.increments('id').primary();
            table.integer("seating_facility_id").comment('1 means Outdoor Seating, 2 means Private Dinning and 3 means Indoor Seating.');
            table.integer("merchant_id").references('id').inTable('users');
            table.timestamps(false, true);
        })

        .alterTable('users', table => {
            table.string('lat');
            table.string('lng');
        })
};

exports.down = function(knex) {
  
};
