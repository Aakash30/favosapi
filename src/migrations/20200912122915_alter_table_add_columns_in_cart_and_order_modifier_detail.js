
exports.up = function(knex) {
    return knex.schema
      .alterTable("cart_product_modifier_detail", (table) => {
        table.string("modifier_product_name");
      })

      .alterTable("order_product_modifier_detail", (table) => {
        table.string("modifier_product_name");
      });
};

exports.down = function(knex) {
  
};
