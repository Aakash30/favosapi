
exports.up = function(knex) {
    return knex.schema.alterTable("cart", (table) => {
        table.string("cart_delivery_address");
        table.string("cart_delivery_lat");
        table.string("cart_delivery_lng");
    });
};

exports.down = function(knex) {
  
};
