exports.up = function (knex) {
	return knex.schema.createTable('order_product_modifier_detail', (table) => {
		table.increments('id').primary();
		table
			.integer('modifier_id')
			.references('id')
			.inTable('order_customization');
		table.integer('modifier_quantity');
		table.integer('user_id');
		table.integer('modifier_product_detail_id');
		table.timestamps(false, true);
	});
};

exports.down = function (knex) {};
