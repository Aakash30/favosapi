
exports.up = function(knex) {
    return knex.schema
        .createTable("table", (table) => {
            table.increments("id").primary();
            table.integer("merchant_id").references("id").inTable("users");
            table.string("table_number");
            table.boolean("status").defaultTo(true);
            table.string("occupied_by");
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
