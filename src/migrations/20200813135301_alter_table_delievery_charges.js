
exports.up = function(knex) {
    return knex.schema
        .alterTable("delivery_charges", (table) => {
            table.dropColumn("from");
            table.dropColumn("to");
        })

        .alterTable("delivery_charges", (table) => {
            table.float("from");
            table.float("to");
        })
};

exports.down = function(knex) {
  
};
