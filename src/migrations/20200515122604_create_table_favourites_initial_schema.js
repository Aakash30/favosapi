
exports.up = function(knex) {
    return knex.schema
        .createTable('favourites', table => {
            table.increments('id').primary();
            table.integer("user_id").references('id').inTable('users');
            table.integer('relation_id');
            table.string('relation_type');
            table.string('order_nick_name');
            table.boolean('shortcut').defaultTo(false);
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
