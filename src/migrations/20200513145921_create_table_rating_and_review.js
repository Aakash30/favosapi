
exports.up = function(knex) {
    return knex.schema
        .createTable('review_and_rating', table => {
            table.increments('id').primary();
            table.integer("user_id").references('id').inTable('users');
            table.integer("merchant").references("id").inTable("users");
            table.integer("item_id")
            table.integer("rating");
            table.string("tags");
            table.string("review");
            table.string("review_image");
            table.timestamps(false, true);
        })
};

exports.down = function(knex) {
  
};
