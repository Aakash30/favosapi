exports.up = function (knex) {
	return knex.schema
		.alterTable('users', (table) => {
			table.dropColumn('stripecustomerId');
		})
		.alterTable('users', (table) => {
			table.string('stripecustomerId');
		});
};

exports.down = function (knex) {};
