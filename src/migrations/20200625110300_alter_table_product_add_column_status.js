
exports.up = function(knex) {
    return knex.schema
        .alterTable("product", (table) => {
            table.integer("status").comment('1 means out of stock, 2 means out of stock for today and 3 means Inactive.');
        })

        .alterTable("modifier_product_details", (table) => {
            table.boolean("default");
        })
};

exports.down = function(knex) {
  
};
