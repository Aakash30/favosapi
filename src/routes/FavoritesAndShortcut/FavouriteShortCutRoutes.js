const promiseRouter = require('express-promise-router');
const router = promiseRouter();

/*------Using Passport For Authentication ------*/
const passport = require("passport")
require("../../middlewares/passport");

/*------Is Permission Check Middleware--------*/
const checkPermission = require("../../middlewares/checkPermission");

// Load the dependent Controller ==========================//
const FavoriteManagement = require("../../controllers/mainController").FavouritesAndShortCut;

/**
 *  =================================================================================================================================
 * @description:{ User Need to do login to access these APIs}
 *  =================================================================================================================================
 */

/*------------ Add to Favourites Route -------------*/
router.post("/favourite/addToFavourite", passport.authenticate('jwt', {session: false}), checkPermission.isActiveUser, FavoriteManagement.AddToFavourites);

/*----------- Remove From Favorites ----------------*/
router.delete("/favourite/removeFromFavorites", passport.authenticate('jwt', {session: false}), checkPermission.isActiveUser, FavoriteManagement.RemoveFromFavourites);

/*----------- Get List of All Favorites ----------------*/
router.get("/favourite/allFavoritesList", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, FavoriteManagement.AllFavouritesList);

/*----------- Remove From Favorites ----------------*/
router.get("/favourite/getFavoriteDetails", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, FavoriteManagement.GetFavouritesDetails);

/*----------- Add Favorites into ShortCut ----------------*/
router.get("/shortcut/add", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, FavoriteManagement.AddToShortcut);

/*----------- Remove Favorites into ShortCut ----------------*/
router.get("/shortcut/remove", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, FavoriteManagement.RemoveFromShortcut);

/*----------- All ShortCuts List ----------------*/
router.get("/shortcut/all", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, FavoriteManagement.AllShortcuts);

/**
 * =================================================================================================================================
 *  @description - {Export the router} 
 * */
module.exports = router;