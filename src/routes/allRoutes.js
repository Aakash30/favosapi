const user_routes = require('./User/userRoutes');
const country_routes = require('./Country/CountryRoutes');
const address_routes = require('./Address/addressRoutes');
const favourite_shortcut_routes = require('./FavoritesAndShortcut/FavouriteShortCutRoutes');
const admin_routes = require('./Admin/AdminRoutes');
const restaurant_routes = require('./Restaurant/restaurantRoutes');

module.exports = [
	user_routes,
	country_routes,
	address_routes,
	favourite_shortcut_routes,
	admin_routes,
	restaurant_routes,
];
