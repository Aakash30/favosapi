const promiseRouter = require("express-promise-router");
const router = promiseRouter();

/*------Using Passport For Authentication ------*/
const passport = require("passport")
require("../../middlewares/passport");

// Load AWS Controller to upload images in S3 and update it.
const AWS = require("../../controllers/aws");

/*------Is Permission Check Middleware--------*/
const checkPermission = require("../../middlewares/checkPermission");

// Load the dependent Controller ==========================//
const AdminRestaurantManagement = require("../../controllers/mainController").AdminRestaurantManagement;
const AdminUserManagement = require("../../controllers/mainController").AdminUserManagement;
const AdminAuthManagement =require("../../controllers/mainController").AdminAuthManagement;

/**
 *  =================================================================================================================================
 * @description:{ User Need to do login to access these APIs}
 *  =================================================================================================================================
*/

// passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser,


//======================================User Management Routes (START)=================================================================

/*--------------- Admin Login API --------------*/
router.post("/admin/login", AdminAuthManagement.AdminLogin);

router.post("/admin/forgotPassword", AdminAuthManagement.forgotPassword);

router.get("/admin/verifyLink", AdminAuthManagement.checkToken);

router.post("/admin/resetPassword", AdminAuthManagement.resetPassword);


router.get("/admin/profile", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminAuthManagement.ProfileDetails);

router.patch("/admin/updateProfile", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminAuthManagement.updateProfile);


//======================================User Management Routes (START)=================================================================

/*----------- List All Users Route ----------------*/
router.get("/admin/allUsers", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser,AdminUserManagement.allUsers);

/*----------- Get User Info By Id Route ----------------*/
router.get("/admin/getUserInfo", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminUserManagement.GetUserInfo);

/*----------- Update User Status By Id Route ----------------*/
router.patch("/admin/updateUserStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser,AdminUserManagement.changeUserStatusOfUser);

/*----------- Reset User Password By Id Route ----------------*/
router.patch("/admin/resetUserPassword", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminUserManagement.resetUserPassword);

//======================================User Management Routes (END)=================================================================

/*----------------------------------------------------------------------------------------------------------------------------------------------------*/

//======================================Restaurant Management Routes (START)=================================================================

// AWS.restaurant_image.fields([{ name: "profile_image" }, { name: "business_logo" }])
/*----------- Add Restaurant Route ----------------*/
router.post("/admin/addGeneralDetailsOfMerchant", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.AddMerchantGeneralDetails);

/*----------- Upload merchant files route ----------------*/
router.post("/admin/MerchantFileUpload", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AWS.restaurant_image.single('file'), AdminRestaurantManagement.uploadMerchantFile);

/*----------- Get Restaurant Detail Route ----------------*/
router.get("/admin/getGeneralDetailsOfMerchant", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.GetMerchantGeneralDetailsInfo);

/*----------- Update Restaurant Info Route ----------------*/
router.patch("/admin/updateGeneralDetailsOfMerchant", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.UpdateMerchantGeneralDetails);

/*----------- Update Restaurant Status Route ----------------*/
router.patch("/admin/updateMerchantStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.changeUserStatusOfRestaurant);

/*----------- Convert Restaurant Favos to Non Favos Route ----------------*/
router.patch("/admin/convertNonFavosToFavos", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.convertNonFavosToFavos);

/*----------- List All Restaurant Route ----------------*/
router.get("/admin/allFavosRestaurants", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.allFavosMerchants);

router.get("/admin/allNonFavosRestaurants", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AdminRestaurantManagement.allNonFavosMerchants);

router.post("/admin/addCharacteristics", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.AddMerchantCharacteristics)

router.patch("/admin/updateCharacteristics", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.UpdateMerchantCharacteristics)

router.get("/admin/getCharacteristics", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.getMerchantCharacteristics)

router.get("/admin/listMerchantTypes", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.listMerchantTypes)

router.get("/admin/listMerchantTags", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.listMerchantTags)

router.get("/admin/listAmeneties", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.listAmeneties)

router.get("/admin/listCuisines", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.listCuisines)

router.get("/admin/listOrders", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.ListOrders)

router.get("/admin/getOrderDetails", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.getOrderDetails)

router.post("/admin/addPaymentDeliveryDetails", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.AddPaymentDetailsAndDeliveryCharges)

router.patch("/admin/updatePaymentDeliveryDetails", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.UpdatePaymentDetailsAndDeliveryCharges)

router.get("/admin/getPaymentDeliveryDetails", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.GetPaymentDetailsAndDeliveryCharges)

router.get("/admin/dashboard", passport.authenticate('jwt', { session: false }), AdminRestaurantManagement.GetAdminDashboardData)

//======================================Restaurant Management Routes (END)=================================================================


/**
* =================================================================================================================================
*  @description - {Export the router} 
* */
module.exports = router;