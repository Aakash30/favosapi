const promiseRouter = require("express-promise-router");
const router = promiseRouter();

//----------Using passport for authentication---------
const passport = require("passport");
require("../../middlewares/passport");

//---------Is permission Check Middleware-----------------
const checkPermission = require("../../middlewares/checkPermission");

// load the dependent controller
const AddressManagement = require("../../controllers/mainController").AddressManagement;

/*-------------------Add New Address Data Route------------------------*/
router.post("/address/addNewAddress", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AddressManagement.InsertNewAddress);

/*---------------Update Existing Address Data Route--------------------*/
router.patch("/address/updateExistAddress", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AddressManagement.UpdateExistAddress);

/*---------------Delete Existing Address Data Route--------------------*/
router.delete("/address/deleteAddress", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AddressManagement.DeleteExistAddress);

/*---------------Get Address Details Route--------------------*/
router.get("/address/getAddressDetail", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, AddressManagement.GetAddressDetail);

module.exports = router;