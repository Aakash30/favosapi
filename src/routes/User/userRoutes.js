const promiseRouter = require('express-promise-router');
const router = promiseRouter();

//----------Using passport for authentication---------
const passport = require('passport');
require('../../middlewares/passport');

//---------Is permission Check Middleware-----------------
const checkPermission = require('../../middlewares/checkPermission');

// Load AWS Controller to upload images in S3 and update it.
const AWS = require('../../controllers/aws');
const UserAuthController = require('../../controllers/User/UserAuthController');

// load the dependent controller  =============================
const UserAuthManagement = require('../../controllers/mainController')
	.UserAuthManagement;
const UserManagement = require('../../controllers/mainController')
	.UserManagement;
const UserRestaurantController = require('../../controllers/mainController')
	.UserRestaurantController;

/**
 *  =================================================================================================================================
 * @description:{ User Doesn't Need to do login to access these APIs}
 *  =================================================================================================================================
 */

/*---------------Create User Route--------------------*/
router.post('/user/registerUser', UserAuthManagement.registerUser);
router.post('/user/registerUser_v3', UserAuthManagement.registerUser_v3);
/*---------------User Login Route--------------------*/
router.post('/user/login', UserAuthManagement.UserLogin);

/*---------------User Login_V2 Route--------------------*/
router.post('/user/login_v2', UserAuthManagement.UserLogin_V2);

/*---------------User Social Login Route--------------------*/
router.post('/user/socialLogin', UserAuthManagement.userSocialLogin);

/*---------------User Verification by token to Reset Password--------------------*/
router.post('/user/verifyOtp', UserAuthManagement.verifyOtpController);

/*---------------User Forgot Password Route--------------------*/
router.post('/user/forgotPassword', UserAuthManagement.forgotPassword);

/*---------------User Reset Password--------------------*/
router.post('/user/resetPassword', UserAuthManagement.resetPassword);

/*---------------User Resent Email To Verify--------------------*/
router.get('/user/resentEmail', UserAuthManagement.resentEmailToVerify);

/*---------------User Resent OTP To Verify--------------------*/
router.post('/user/resentOTP', UserAuthManagement.resentOTPtoVerify);

/*---------------User Verify Email -----------------------------*/
router.get('/user/verifyEmail', UserAuthManagement.verifyEmail);

router.post('/web/contactUs', UserManagement.AddContactUsDetails);

/**
 *  =================================================================================================================================
 * @description:{ User Need to do login to access these APIs}
 *  =================================================================================================================================
 */
//AWS.user_image.single('file')
/*---------------Get User's Info By Id --------------------*/
router.get(
	'/user/logout',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserAuthManagement.UserLogout
);

/*---------------Get User's Info By Id --------------------*/
router.get(
	'/user/profileDetails',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.userProfileDetails
);

/*---------------Delete User's Info By Id Route--------------------*/
router.delete(
	'/user/deleteAccount',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.deleteUserAccount
);

/*---------------Update User's Info Route--------------------*/
router.patch(
	'/user/updateProfile',
	passport.authenticate('jwt', { session: false }),
	AWS.user_image.fields([{ name: 'profile_image' }, { name: 'banner_image' }]),
	checkPermission.isActiveUser,
	UserManagement.updateUserProfile
);

/*---------------Change User's Password---------------------------*/
router.post(
	'/user/changePassword',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.changeUserPassword
);

/*---------------User Resent Email To Verify--------------------*/
router.get(
	'/user/resentEmailToVerify',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserAuthManagement.resentEmailToVerify
);

/*---------------- Update User's Notification-------------------*/
router.patch(
	'/user/notification/update',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.updateNotificationStatus
);

/*---------------- Update User Email----------------------------*/
router.patch(
	'/user/updateEmail',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.updateUserEmail
);

/*---------------- Update User Mobile number----------------------------*/
router.patch(
	'/user/updateMobile',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.updateUserMobile
);

/*----------------- User Registered The Business ---------------*/
router.post(
	'/user/place/register',
	passport.authenticate('jwt', { session: false }),
	AWS.business_registered.fields([
		{ name: 'restaurant_image' },
		{ name: 'document' },
	]),
	checkPermission.isActiveUser,
	UserManagement.registerBussiness
);

/*----------------- Get All Registered Business By UserId---------------*/
router.get(
	'/user/place/allRegisteredBusiness',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllregisteredBusiness
);

/*----------------- Get All Requests Business By UserId---------------*/
router.get(
	'/user/place/allRequests',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllrequestedBusiness
);

/*----------------Request Business to Become FAVOS Partner---------------*/
router.post(
	'/user/place/request',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.requestBusiness
);

/*----------------Search For Merchant Request to Become FAVOS Partner---------------*/
router.get(
	'/user/nonEaseRestaurantSearch',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.searchNonFavosMerchants
);

/*----------------Create Feedback ---------------*/
router.post(
	'/feedback/createFeedback',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.CreateFeedback
);

// /*----------------Create Feedback for MERCHANT---------------*/
// router.post("/feedback/toMerchant", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, UserManagement.CreateFeedbackForMerchant);

/*----------------All Feedback for FAVOS---------------*/
router.get(
	'/feedback/allfavosfeedback',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllFeedbackForFavos
);

/*----------------All Feedback for MERCHANT---------------*/
router.get(
	'/feedback/allmerchantfeedback',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllFeedbackForMerchant
);

/*----------------Delete Feedback ---------------*/
router.delete(
	'/feedback/delete',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.deleteFeedback
);

/*----------------Create New Review ---------------*/
router.post(
	'/review/new',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	AWS.review_image.single('review_image'),
	UserManagement.ReviewAndRating
);

/*----------------Create New Review ---------------*/
router.get(
	'/review/getReviews',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.GetReviewAndRating
);

/*----------------All Reviews for Merchant ---------------*/
router.get(
	'/review/allReviewsForMerchant',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllReviewsForMerchant
);

/*----------------All Reviews for Item ---------------*/
router.get(
	'/review/allReviewsForItem',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllReviewsForItem
);

/*----------------Delete Review By Id ---------------*/
router.delete(
	'/review/deleteReviews',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.DeleteReviewAndRating
);

/*----------------Update Review By Id ---------------*/
router.patch(
	'/review/updateReviews',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	AWS.review_image.single('review_image'),
	UserManagement.UpdateReviewAndRating
);

/*----------------Restaurant list by filter Y---------------*/
router.post(
	'/user/restaurant',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.restaurantListWithFilter
);

router.post(
	'/user/scanProductAndAddToCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.scanProductAndAddToCart
);

/*----------------Restaurant list by filter Y---------------*/
router.get(
	'/user/restaurant/:id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.restaurantDetail
);

/*----------------Menu list by restaurant id Y---------------*/
router.get(
	'/user/menu/:id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.menuList
);

/*----------------Menu list menu , category id Y---------------*/
router.get(
	'/user/menu',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.menuListbyId
);

/*----------------Menu list menu , category id Y---------------*/
router.get(
	'/user/groupModifier/:id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.productGroupModifier
);

/*----------------Add to cart Y------------------*/
router.post(
	'/user/addToCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.addToCart
);
router.post(
	'/user/addItemToCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.addToCartFinal
);

router.post(
	'/user/addtocart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.addtoCart
);

/*----------------View cart Y---------------*/
router.post(
	'/user/viewCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.viewCart
);
router.post(
	'/user/view-cart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.viewCartwithModifier
);
/*----------------Remove item from cart Y---------------*/
router.post(
	'/user/removeItemFromCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.removeItemFromCart
);

/*----------------Cart remove and add new item Y---------------*/
router.post(
	'/user/removeCartAndAddItem',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.removeCartAndAddItem
);

router.post(
	'/user/removeCartAndAddScanItem',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.removeCartAndAddScanItem
);

/*----------------Checkout Api without Modifier--------------*/
router.post(
	'/user/checkout',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.checkout_without_modifier
);

//-----------------Checkout with Modifier -----------------//
router.post(
	'/user/checkout-modifier',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.checkout_with_modifier
);
router.post(
	'/user/placeOrder',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.placeOrder
);

router.get('/user/catList', UserRestaurantController.CategoryList);

//-------------------------Order History----------------//
router.post(
	'/user/order-history',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.placeOrderHistory
);

router.get(
	'/user/order-history/:order_id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.placeOrderHistoryById
);

router.post(
	'/user/checkCartDetail',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.checkout_with_modifier
);

router.get(
	'/user/reorder/:order_id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.Reorder
);
router.post(
	'/user/checkinCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.CheckItemInCart
);
router.get(
	'/user/reorder-modifier/:order_id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.Reorder
);

//-----------------Checkout with Modifier -----------------//
// router.post(
// 	'/user/checkCartDetailWithModifier',
// 	passport.authenticate('jwt', { session: false }),
// 	checkPermission.isActiveUser,
// 	UserRestaurantController.checkout_with_modifier
// );

router.get(
	'/user/getkey',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserAuthController.getephemeralKey
);

router.post(
	'/user/deleteCart',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.deleteCart
);

/*----------------All Notification for USER---------------*/
router.get(
	'/user/allNotifications',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.AllNotification
);

/*----------------Notification Count for USER---------------*/
router.get(
	'/user/notificationCount',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserManagement.NotificationCount
);

router.get(
	'/user/menuListByQRCode',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.menuListVersionQrCode
);

router.post(
	'/user/scanProductAndAddToCartVersionQrCode',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.scanProductAndAddToCartVersionQrCode
);

router.post(
	'/user/calculateDeliveryCharges',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.calculatedelivery_charges
);

router.get(
	'/user/editcartModifier/:cart_id/:id',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.ModifierEditApi
);

router.post(
	'/user/test_socket',
	UserRestaurantController.test_socket_notification
);

//V2 api
router.post(
	'/user/getkey_v2',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserAuthController.getephemeralKey_v2
);

router.post(
	'/user/getkey_v3',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserAuthController.getephemeralKey_v3
);

router.post(
	'/user/checkCartDetail_v2',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.checkout_with_modifier_v2
);
router.post(
	'/user/checkCartDetail_v3',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.checkout_with_modifier_v3
);

router.post(
	'/user/placeOrder_v2',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.placeOrder_v2
);

router.post(
	'/user/placeOrder_v3',
	passport.authenticate('jwt', { session: false }),
	checkPermission.isActiveUser,
	UserRestaurantController.placeOrder_v3
);

/**
 * =================================================================================================================================
 *  @description - {Export the router}
 * */
module.exports = router;
