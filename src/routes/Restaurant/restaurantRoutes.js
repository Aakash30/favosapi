const promiseRouter = require("express-promise-router");
const router = promiseRouter();

/*------Using Passport For Authentication ------*/
const passport = require("passport")
require("../../middlewares/passport");

// Load AWS Controller to upload images in S3 and update it.
const AWS = require("../../controllers/aws");

/*------Is Permission Check Middleware--------*/
const checkPermission = require("../../middlewares/checkPermission");

// Load the dependent Controller ==========================//
const MerchantRestaurantController = require("../../controllers/mainController").MerchantRestaurantController;
const MenuManagement = require("../../controllers/mainController").MenuManagement;
const RestaurantOrders = require("../../controllers/mainController").RestaurantOrderManagement;


//======================================Restaurant Management Routes (START)=================================================================

/*----------- Get Restaurant General Details Detail Route ----------------*/
router.get("/restaurant/getGeneralDetailsOfMerchant", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.getGeneralDetailsOfMerchantInfo);

/*----------- Update Restaurant General Details Route ----------------*/
router.patch("/restaurant/updateGeneralDetailsOfMerchant", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.updateGeneralDetailsOfMerchant);

/*----------- Change Restaurant's Password Route ----------------*/
router.post("/restaurant/changePassword", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.changeMerchantPassword);

/*----------- Get Restaurant Characteristicss Route ----------------*/
router.get("/restaurant/getCharacteristics", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.getMerchantCharacteristics);

/*----------- Update Restaurant Characteristicss Route ----------------*/
router.patch("/restaurant/updateCharacteristics", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.UpdateMerchantCharacteristics);

/*---------------Get List Of Order Types Associated With Merchant--------------------*/
router.get("/restaurant/order_types", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.OrderTypesOfMerchant);

//======================================Restaurant Management Routes (END)=================================================================




//======================================Restaurant MENU Management Routes (START)=================================================================

/*----------- Add Menu Route ----------------*/
router.post("/restaurant/addMenu", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.AddMenu);

/*----------- Update Menu Route ----------------*/
router.patch("/restaurant/updateMenu", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.UpdateMenu);

/*----------- List Of Menu Route ----------------*/
router.get("/restaurant/menuList", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.MenuList);

/*----------- Change Menu Status Route ----------------*/
router.patch("/restaurant/changeMenuStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.ChangeMenuStatus);

/*----------- Delete Menu Route ----------------*/
router.patch("/restaurant/deleteMenu", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.deleteMenu);

/*----------- Change Menu Status Route ----------------*/
router.patch("/restaurant/changeMenuOrderTypeStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.ChangeMenuOrderTypeStatus);

//======================================Restaurant MENU Management Routes (END)=================================================================




//======================================Restaurant Category Management Routes (START)=================================================================

/*----------- Add Category Route ----------------*/
router.post("/restaurant/addCategory", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.AddCategory);

/*----------- List Of Categories Route ----------------*/
router.get("/restaurant/categoryList", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.CategoryList);

/*----------- Change Category Status Route ----------------*/
router.patch("/restaurant/changeCategoryStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.ChangeCategoryStatus);

/*----------- Delete Category Route ----------------*/
router.patch("/restaurant/deleteCategory", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.deleteCategory);

/*----------- Get Category Info Route ----------------*/
router.get("/restaurant/getCategoryInfo", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.GetCategoryInfo);

/*----------- Update Category Info Route ----------------*/
router.patch("/restaurant/updateCategory", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.UpdateCategory);

//======================================Restaurant Category Management Routes (END)=================================================================




//======================================Restaurant Modifier Management Routes (START)=================================================================

/*----------- Add Modifier Route ----------------*/
router.post("/restaurant/addModifier", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.AddModifier);

/*----------- List Of Modifier Route ----------------*/
router.get("/restaurant/modifierList", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.ModifierList);

/*----------- Delete Modifier Route ----------------*/
router.patch("/restaurant/deleteModifier", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.deleteModifier);

/*----------- Get Modifier Info Route ----------------*/
router.get("/restaurant/getModifierInfo", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.GetModifierInfo);

/*----------- Update Modifier Info Route ----------------*/
router.patch("/restaurant/updateModifier", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.UpdateModifier);

//======================================Restaurant Modifier Management Routes (END)=================================================================




//======================================Restaurant Modifier Group Management Routes (START)=================================================================

/*----------- Add Modifier Group Route ----------------*/
router.post("/restaurant/addGroupModifier", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.AddGroupModifier);

/*----------- List Of Group Modifier Route ----------------*/
router.get("/restaurant/groupModifierList", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.GroupModifierList);

/*----------- Delete Group Modifier Route ----------------*/
router.patch("/restaurant/deleteGroupModifier", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.deleteGroupModifier);

/*----------- Get Group Modifier Info Route ----------------*/
router.get("/restaurant/getGroupModifierInfo", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.GetGroupModifierInfo);

/*----------- Update Group Modifier Info Route ----------------*/
router.patch("/restaurant/updateGroupModifier", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.UpdateGroupModifier);

//======================================Restaurant Modifier Group Management Routes (END)=================================================================



//======================================Restaurant PRODUCTS Management Routes (START)=================================================================

/*----------- Add Product Route ----------------*/
router.post("/restaurant/addProduct", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.AddProduct);

/*----------- Get Product Details Route ----------------*/
router.get("/restaurant/getProductDetails", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.GetProduct);

/*----------- Get Product List Route ----------------*/
router.get("/restaurant/productList", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.ProductList);

/*----------- Update Product Details Route ----------------*/
router.patch("/restaurant/updateProduct", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.UpdateProduct);

/*----------- Delete Product Route ----------------*/
router.patch("/restaurant/deleteProduct", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.deleteProduct);

/*----------- Delete Product Route ----------------*/
router.patch("/restaurant/deleteProduct", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.deleteProduct);

/*----------- Change Product Status Route ----------------*/
router.patch("/restaurant/changeProductStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.changeProductStatus);

//======================================Restaurant PRODUCTS Management Routes (END)=================================================================




//======================================Restaurant Table Management Routes (START)=================================================================

/*----------- Add Table Route ----------------*/
router.post("/restaurant/addTable", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.AddTable);

/*----------- List the Table Route ----------------*/
router.get("/restaurant/listTable", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.ListTable);

//======================================Restaurant Table Management Routes (END)=================================================================


/*----------- List the product tags Route ----------------*/
router.get("/restaurant/listProductTags", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.listProductTags);

/*----------- List the Suggestions Route ----------------*/
router.get("/restaurant/suggestions", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.listSuggestions);



//======================================Restaurant Order Management Routes (START)=================================================================

/*----------- list orders Route ----------------*/
router.get("/restaurant/orders", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, RestaurantOrders.ListOrders);

/*----------- get order details Route ----------------*/
router.get("/restaurant/orderDetail", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, RestaurantOrders.getOrderDetails);

/*----------- Change order status Route ----------------*/
router.patch("/restaurant/changeOrderStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, RestaurantOrders.changeOrderStatus);

/*----------- Change online order status Route ----------------*/
router.patch("/restaurant/changeOnlineOrderStatus", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, RestaurantOrders.changeOnlineOrderStatus);

/*----------- get order overview Route ----------------*/
router.get("/restaurant/orderOverview", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, RestaurantOrders.OrderOverView);

//======================================Restaurant Order Management Routes (END)=================================================================

router.get("/restaurant/dashboard", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MerchantRestaurantController.GetMerchantDashboardData);

router.get("/restaurant/MenuManagement", passport.authenticate('jwt', { session: false }), checkPermission.isActiveUser, MenuManagement.MenuMenagementCount);

/**
* =================================================================================================================================
*  @description - {Export the router} 
* */
module.exports = router;