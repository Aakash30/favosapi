const promiseRouter = require("express-promise-router");
const router = promiseRouter();

// load the dependent controller
const CountryManagement = require("../../controllers/mainController").CountryManagement;

/*---------------Fetch All Countries Data Route--------------------*/
router.get("/country/getAllCountries", CountryManagement.getAllCountries);

/*---------------Fetch All City According to country Data Route--------------------*/
router.get("/city/getAllCities", CountryManagement.getAllCityAccordingToCountry);

/*---------------Fetch All Countries ISD_Code Data Route--------------------*/
router.get("/country/getAllCountriesISDcodes", CountryManagement.getAllCountryISDcode);

/*---------------Fetch All Countries ISD_Code Data Route--------------------*/
router.get("/country/countryNameByISDcode", CountryManagement.getCountryByISDcode);

module.exports = router;