'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class Cart extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'cart';
	}

	// Optional JSON schema.This is not the database schema! Nothing is generated
	// based on this. This is only used for validation. Whenever a model instance
	// is created it is checked against this schema. http://json-schema.org/.
	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			cart_product_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/Product',
				join: {
					from: 'cart.product_id',
					to: 'product.id',
				},
			},
			cart_user_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/User',
				join: {
					from: 'cart.user_id',
					to: 'users.id',
				},
			},
			cart_merchant_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/User',
				join: {
					from: 'cart.merchant_id',
					to: 'users.id',
				},
			},
			//relation with "cart group" Table
			cart_product_group_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/CartProductGroup',
				join: {
					to: 'cart.id',
					from: 'cart_product_group.cart_id',
				},
			},
			//relation with "cart group" Table
			cart_product_modifiers_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/CartProductModifier',
				join: {
					to: 'cart.id',
					from: 'cart_product_modifier.cart_id',
				},
			},

			modifiers_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductModifiers',
				join: {
					from: 'cart.product_id',
					to: 'product_modifiers.product_id',
				},
			},
			product_modifier_group_get_relation: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/ModifierGroup',
				join: {
					from: 'cart.product_id',
					through: {
						from: 'product_modifier_groups.product_id',
						to: 'product_modifier_groups.modifier_group_id',
					},
					to: 'modifier_group.id',
				},
			},
			product_modifier_get_relation: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Modifier',
				join: {
					from: 'cart.product_id',
					through: {
						from: 'product_modifiers.product_id',
						to: 'product_modifiers.modifier_id',
					},
					to: 'modifier.id',
				},
			},

			group_modifiers_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductModifierGroups',
				join: {
					from: 'cart.product_id',
					to: 'product_modifier_groups.product_id',
				},
			},
			// // relation with "Order type" Table
			// order_type_merchant: {
			//     relation: Model.ManyToManyRelation,
			//     modelClass: __dirname + '/User',
			//     join: {
			//         from: "order_type.id",
			//         through: {
			//             from: "order_type_merchant_characteristics_management.order_type_id",
			//             to: "order_type_merchant_characteristics_management.merchant_id"
			//         },
			//         to: "users.id"
			//     }
			// }
		};
	}
}

module.exports = Cart;
