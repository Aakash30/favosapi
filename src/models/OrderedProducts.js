'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class OrderProducts extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'ordered_products';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			products_relation: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Product',
				join: {
					from: 'ordered_products.product_id',
					to: 'product.id',
				},
			},

			products_customization: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/OrderCustomization',
				join: {
					from: 'ordered_products.id',
					to: 'order_customization.ordered_product_id',
				},
			},
		};
	}
}

module.exports = OrderProducts;
