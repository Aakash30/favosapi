'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class CartProductModifierDetail extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'cart_product_modifier_detail';
	}

	// Optional JSON schema.This is not the database schema! Nothing is generated
	// based on this. This is only used for validation. Whenever a model instance
	// is created it is checked against this schema. http://json-schema.org/.
	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			cart_product_modifier_detail_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/CartProductModifier',
				join: {
					from: 'cart_product_modifier_detail.cart_modifier_id',
					to: 'cart_product_modifier.id',
				},
			},
			cart_modifier_detail_modifier_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/Modifier',
				join: {
					from: 'cart_product_modifier_detail.modifier_id',
					to: 'modifier.id',
				},
			},
			cart_modifier_product_detail_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/ModifierProductDetails',
				join: {
					from: 'cart_product_modifier_detail.modifier_product_detail_id',
					to: 'modifier_product_details.id',
				},
			},
			cart_modifier_products_detail_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ModifierProductDetails',
				join: {
					from: 'cart_product_modifier_detail.modifier_product_detail_id',
					to: 'modifier_product_details.id',
				},
			},
		};
	}
}

module.exports = CartProductModifierDetail;
