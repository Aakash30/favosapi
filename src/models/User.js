'use strict';
//-----Required objection model------------------------
const Model = require('objection').Model;

//--------Import Dependency 3rd Party Modules-----------
const bcryptjs = require('bcryptjs');
const validator = require('validator');
require('dotenv').config();
const jwt = require('jsonwebtoken');

//--------Import Models Here----------------------------
const Country = require('./Country');
const Address = require('./Address');
const AuthTable = require('./AuthTable');
const ReviewAndRating = require('./ReviewAndRating');

class User extends Model {
	static get tableName() {
		return 'users';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
				first_name: { type: 'string', minLength: 1, maxLength: 255 },
				last_name: { type: 'string', minLength: 1, maxLength: 255 },
				// email: { type: "string", minLength: 1, maxLength: 255 },
				password: { type: 'string' },
				mobile: { type: 'string' },
				gender: { type: 'string' },
				date_of_birth: { type: 'date' },
				country_id: { type: 'integer' },
				referr_code: { type: 'string' },
				referral_code: { type: 'string' },
			},
		};
	}

	static get relationMappings() {
		return {
			// Manage relations for user table to other tables

			// relation with "country" Table
			user_country_relation: {
				relation: Model.HasOneRelation,
				modelClass: Country,
				join: {
					to: 'users.country_id',
					from: 'country.id',
				},
			},

			// relation with "address" Table
			user_address_relation: {
				relation: Model.HasManyRelation,
				modelClass: Address,
				join: {
					from: 'users.id',
					to: 'address_management.user_id',
				},
			},

			// relation with "auth_table" Table
			user_auth_relation: {
				relation: Model.HasManyRelation,
				modelClass: AuthTable,
				join: {
					to: 'users.id',
					from: 'auth_table.user_id',
				},
			},

			// relation with "Cuisine" Table
			merchant_cuisine: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Cuisines',
				join: {
					from: 'users.id',
					through: {
						from: 'merchant_cuisines.merchant_id',
						to: 'merchant_cuisines.cuisines_id',
					},
					to: 'cuisines.id',
				},
			},
			// relation with "Amenties" Table
			merchant_amenties: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Amenties',
				join: {
					from: 'users.id',
					through: {
						from: 'merchant_amenties.merchant_id',
						to: 'merchant_amenties.amenties_id',
					},
					to: 'amenties.id',
				},
			},

			// relation with "merchant_amenties" Table
			merchant_characteristics_amenties_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Merchant_Amenties',
				join: {
					to: 'users.id',
					from: 'merchant_amenties.merchant_id',
				},
			},

			//relation of User with "review_and_rating" TABLE
			user_review_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ReviewAndRating',
				join: {
					from: 'users.id',
					to: 'review_and_rating.user_id',
				},
			},
			//relation of Merchant with "review_and_rating" TABLE
			merchant_review_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ReviewAndRating',
				join: {
					from: 'users.id',
					to: 'review_and_rating.merchant',
				},
			},

			// relation with "merchant_characterstic Y" Table
			merchant_characteristic_relation: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Merchant_Characteristics',
				join: {
					from: 'users.id',
					to: 'merchant_characteristics.merchant_id',
				},
			},

			// relation with "Order type" Table
			merchant_order_type: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Order_Type',
				join: {
					from: 'users.id',
					through: {
						from: 'order_type_merchant_characteristics_management.merchant_id',
						to: 'order_type_merchant_characteristics_management.order_type_id',
					},
					to: 'order_type.id',
				},
			},
			// relation with "Menu management" Table
			merchant_menu_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/MenuManagement',
				join: {
					from: 'users.id',
					to: 'menu_management.merchant_id',
				},
			},

			// relation with "Category" Table
			merchant_category_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Categories',
				join: {
					from: 'users.id',
					to: 'categories.merchant_id',
				},
			},

			// relation with "Product" Table
			merchant_product_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Product',
				join: {
					from: 'users.id',
					to: 'product.merchant_id',
				},
			},
			delivery_details: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/PaymentDetails',
				join: {
					from: 'users.id',
					to: 'payment_details.merchant_id',
				},
			},

			// relation with "Modifier" Table
			merchant_modifier_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Modifier',
				join: {
					from: 'users.id',
					to: 'modifier.merchant_id',
				},
			},

			// relation with "Group" Table
			merchant_modifier_group_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ModifierGroup',
				join: {
					from: 'users.id',
					to: 'modifier_group.merchant_id',
				},
			},
			cart_user: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Cart',
				join: {
					from: 'users.id',
					to: 'cart.user_id',
				},
			},
			cart_merchant: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Cart',
				join: {
					from: 'users.id',
					to: 'cart.merchant_id',
				},
			},

			merchant_orders: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Orders',
				join: {
					from: 'users.id',
					to: 'orders.merchant_id',
				},
			},

			user_orders: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Orders',
				join: {
					from: 'users.id',
					to: 'orders.user_id',
				},
			},

			merchants_delivery_charges: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/DeliveryCharges',
				join: {
					from: 'users.id',
					to: 'delivery_charges.merchant_id',
				},
			},
		};
	}

	//--------Function is used to generate auth_token------------
	async getJWT() {
		return await jwt.sign(
			{
				id: this.id,
				role: this.user_type,
			},
			'xIMEE1vdvlvTjac1tGyiJHZusIFBtl'
		);
	}

	//---------Function to compare password----------------------
	async comparePassword(password) {
		if (!password) {
			return false;
		}
		let pass = await bcryptjs.compare(password, this.password);
		return pass;
	}

	//---------Function runs before inserting a record----------------------
	//-----It checks valid emailId and bcryptjs the password-----
	async $beforeInsert() {
		await super.$beforeInsert();
		// if (this.email) {
		//     if (!validator.isEmail(this.email || '')) {
		//         console.log('in before insert');
		//         throw badRequestError(res, "Not a valid email address!");
		//     }
		// }

		if (this.password) {
			this.password = await bcryptjs.hash(this.password, 10);
		}
	}
}

module.exports = User;
