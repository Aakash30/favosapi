"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const order_type = require("./Order_Type");

class Order_Type_Management extends Model {
    // Table name is the only required property
    static get tableName() {
        return "order_type_merchant_characteristics_management";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            user_merchant_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/User',
                join: {
                    from: "order_type_merchant_characteristics_management.merchant_id",
                    to: "users.id"
                }
            },

            // relation with "order type" Table
            order_type_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Order_Type',
                join: {
                    from: "order_type_merchant_characteristics_management.order_type_id",
                    to: "order_type.id"
                }
            }
        }
    }
}

module.exports = Order_Type_Management;