"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class ModifierGroupModifiers extends Model {
  // Table name is the only required property
  static get tableName() {
    return "modifier_group_modifiers";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      modifier_group_manage: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/ModifierGroup',
        join: {
            from: "modifier_group_modifiers.modifier_group_id",
            to: "modifier_group.id"
        }
      },
      group_modifier_manage: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Modifier',
        join: {
            from: "modifier_group_modifiers.modifier_id",
            to: "modifier.id"
        }
      }
    };
  }
}

module.exports = ModifierGroupModifiers;
