'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class OrderCustomization extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'order_customization';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			order_product_modifier_detail: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/OrderProductModifierDetail',
				join: {
					from: 'order_customization.id',
					to: 'order_product_modifier_detail.modifier_id',
				},
			},

			modifier_customization: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Modifier',
				join: {
					from: 'order_customization.modifier_id',
					to: 'modifier.id',
				},
			},

			group_modifier_customization: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/ModifierGroup',
				join: {
					from: 'order_customization.modifier_group_id',
					to: 'modifier_group.id',
				},
			},
		};
	}
}

module.exports = OrderCustomization;
