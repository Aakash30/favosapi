"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class Merchant_Cuisines extends Model {
    // Table name is the only required property
    static get tableName() {
        return "merchant_cuisines";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            merchant_cuisine_user: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/User',
                join: {
                    from: "merchant_cuisines.merchant_id",
                    to: "users.id"
                }
            },

            // relation with "cuisines" Table
            merchant_cuisines: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Cuisines',
                join: {
                    from: "merchant_cuisines.cuisines_id",
                    to: "cuisines.id"
                }
            },
        }
    }
}

module.exports = Merchant_Cuisines;