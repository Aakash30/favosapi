"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class ProductPricing extends Model {
  // Table name is the only required property
  static get tableName() {
    return "product_pricing";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      products_price: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Product',
        join: {
            from: "product_pricing.product_id",
            to: "product.id"
        }
      }
    };
  }
}

module.exports = ProductPricing;
