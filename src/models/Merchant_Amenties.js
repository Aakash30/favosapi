"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("./User");
const Amenties= require("./Amenties");

class Merchant_Amenties extends Model {
    // Table name is the only required property
    static get tableName() {
        return "merchant_amenties";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            merchant_amenties_user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: "merchant_amenties.merchant_id",
                    to: "users.id"
                }
            },

            // relation with "amenties" Table
            merchant_amentie: {
                relation: Model.BelongsToOneRelation,
                modelClass: Amenties,
                join: {
                    from: "merchant_amenties.amenties_id",
                    to: "amenties.id"
                }
            },
        }
    }
}

module.exports = Merchant_Amenties;