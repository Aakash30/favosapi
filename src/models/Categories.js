"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class Categories extends Model {
  // Table name is the only required property
  static get tableName() {
    return "categories";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      //relation with "product" TABLE
      category_merchant: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/User',
        join: {
            from: "categories.merchant_id",
            to: "users.id"
        }
      },
      category_product: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + '/Product',
        join: {
            from: "categories.id",
            through: {
                from: "product_category.category_id",
                to: "product_category.product_id",
            },
            to: "product.id"
        }
      }
    };
  }
}

module.exports = Categories;
