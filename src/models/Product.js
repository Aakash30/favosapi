'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class Product extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'product';
	}

	// Optional JSON schema.This is not the database schema! Nothing is generated
	// based on this. This is only used for validation. Whenever a model instance
	// is created it is checked against this schema. http://json-schema.org/.
	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			product_menus: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/MenuManagement',
				join: {
					from: 'product.id',
					through: {
						from: 'product_menu.product_id',
						to: 'product_menu.menu_id',
					},
					to: 'menu_management.id',
				},
			},
			product_category: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Categories',
				join: {
					from: 'product.id',
					through: {
						from: 'product_category.product_id',
						to: 'product_category.category_id',
					},
					to: 'categories.id',
				},
			},
			// relation with "tags" Table
			product_tag_relation: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/ProductTags',
				join: {
					from: 'product.id',
					through: {
						from: 'product_tags_management.product_id',
						to: 'product_tags_management.product_tags_id',
					},
					to: 'product_tags.id',
				},
			},
			//Rating and review table
			product_review_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ReviewAndRating',
				join: {
					from: 'product.id',
					to: 'review_and_rating.item_id',
				},
			},
			menus_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductMenu',
				join: {
					from: 'product.id',
					to: 'product_menu.product_id',
				},
			},

			categories_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductCategory',
				join: {
					from: 'product.id',
					to: 'product_category.product_id',
				},
			},

			tags_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductTagsManagement',
				join: {
					from: 'product.id',
					to: 'product_tags_management.product_id',
				},
			},

			modifiers_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductModifiers',
				join: {
					from: 'product.id',
					to: 'product_modifiers.product_id',
				},
			},

			group_modifiers_of_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductModifierGroups',
				join: {
					from: 'product.id',
					to: 'product_modifier_groups.product_id',
				},
			},

			product_pricing_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductPricing',
				join: {
					from: 'product.id',
					to: 'product_pricing.product_id',
				},
			},

			// relation with "tags" Table
			product_menu_price_relation: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/MenuManagement',
				join: {
					from: 'product.id',
					through: {
						from: 'product_pricing.product_id',
						to: 'product_pricing.menu_id',
					},
					to: 'menu_management.id',
				},
			},
			// relation with "group modifier" Table
			product_modifier_get_relation: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Modifier',
				join: {
					from: 'product.id',
					through: {
						from: 'product_modifiers.product_id',
						to: 'product_modifiers.modifier_id',
					},
					to: 'modifier.id',
				},
			},

			product_modifier_get_relations: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductModifiers',
				join: {
					from: 'product.id',
					to: 'product_modifiers.product_id',
				},
			},

			// relation with "group modifier" Table
			product_modifier_group_get_relation: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/ModifierGroup',
				join: {
					from: 'product.id',
					through: {
						from: 'product_modifier_groups.product_id',
						to: 'product_modifier_groups.modifier_group_id',
					},
					to: 'modifier_group.id',
				},
			},

			cart_product: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Cart',
				join: {
					from: 'product.id',
					to: 'cart.product_id',
				},
			},
			//cart_product_order_type
			cart_product_order_type: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/Cart',
				join: {
					from: 'product.id',
					to: 'cart.product_id',
				},
			},

			suggestion_type: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Suggestions',
				join: {
					from: 'product.suggestion_type_id',
					to: 'suggestions.id',
				},
			},

			merchant_detail: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/User',
				join: {
					from: 'product.merchant_id',
					to: 'users.id',
				},
			}
		};
	}
}

module.exports = Product;
