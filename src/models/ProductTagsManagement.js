"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class ProductTagsManagement extends Model {
  // Table name is the only required property
  static get tableName() {
    return "product_tags_management";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
       //relation with "product" TABLE
       product_tags_belongsto: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Product',
        join: {
            from: "product_tags_management.product_id",
            to: "product.id"
        }
      },
       //relation with "tags" TABLE
       tags_product_belongsto: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/ProductTags',
        join: {
            from: "product_tags_management.product_tags_id",
            to: "product_tags.id"
        }
      }
    };
  }
}

module.exports = ProductTagsManagement;
