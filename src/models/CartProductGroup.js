"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------


class CartProductGroup extends Model {
    // Table name is the only required property
    static get tableName() {
        return "cart_product_group";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            group_cart_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Cart',
                join: {
                  from: "cart_product_group.cart_id",
                  to: "cart.id"
                }
            },
            cart_modifier_relation: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/CartProductModifier',
                join: {
                  from: "cart_product_group.id",
                  to: "cart_product_modifier.cart_group_id"
                }
            },
            group_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/GroupModifier',
                join: {
                  from: "cart_product_group.group_id",
                  to: "group_modifier.id"
                }
            }
        }
    }
}

module.exports = CartProductGroup;
