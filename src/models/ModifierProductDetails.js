"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class ModifierProductDetails extends Model {
  // Table name is the only required property
  static get tableName() {
    return "modifier_product_details";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      product_detail_modifier: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Modifier',
        join: {
            from: "modifier_product_details.modifier_id",
            to: "modifier.id"
        }
      },
      modifier_product_detail_cart_relation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/CartProductModifierDetail',
        join: {
          from: "modifier_product_details.id",
          to: "cart_product_modifier_detail.modifier_product_detail_id"
        }
    }
    };
  }
}

module.exports = ModifierProductDetails;
