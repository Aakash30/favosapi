"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("./User");

class Feedback extends Model {
    // Table name is the only required property
    static get tableName() {
        return "feedback";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            feedback_merchant_relation: {
                relation: Model.HasOneRelation,
                modelClass: User,
                join: {
                    to: "feedback.merchant",
                    from: "users.id"
                }
            }
        }
    }
}

module.exports = Feedback;