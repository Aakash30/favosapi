"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class MenuManagement extends Model {
  // Table name is the only required property
  static get tableName() {
    return "menu_management";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
        type: "object",
        required: [],
        properties: {
            id: { type: "integer" },
        },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      //relation with "user" TABLE
      menu_merchant: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/User',
        join: {
            from: "menu_management.merchant_id",
            to: "users.id"
        }
      },
      menu_product: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + '/Product',
        join: {
            from: "menu_management.id",
            through: {
                from: "product_menu.menu_id",
                to: "product_menu.product_id",
            },
            to: "product.id"
        }
      },

      menu_order_type_relation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/MenuOrderType',
        join: {
          from: "menu_management.id",
          to: "menu_order_type.menu_id"
        }
      },
    };
  }
}

module.exports = MenuManagement;
