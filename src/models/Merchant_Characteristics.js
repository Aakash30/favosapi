"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const Merchant_Amenties = require("./Merchant_Amenties");
const Merchant_Cuisines = require("./Merchant_Cuisines");
const SeatingFacility = require("./SeatingFacility");
const merchantTags = require("./Merchant_tags_chracteristics");
const OrderTypeManagement = require("./Order_Type_Management");

class Characteristics extends Model {
    // Table name is the only required property
    static get tableName() {
        return "merchant_characteristics";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {

            // relation with "merchant_amenties" Table
            merchant_amenties_relation: {
                relation: Model.HasManyRelation,
                modelClass: Merchant_Amenties,
                join: {
                    to: "merchant_characteristics.merchant_id",
                    from: "merchant_amenties.merchant_id"
                }
            },

            // relation with "merchant_cuisines" Table
            merchant_cuisines_relation: {
                relation: Model.HasManyRelation,
                modelClass: Merchant_Cuisines,
                join: {
                    to: "merchant_characteristics.merchant_id",
                    from: "merchant_cuisines.merchant_id"
                }
            },

            // relation with "seating_facility" Table
            seating_facility_relation: {
                relation: Model.HasManyRelation,
                modelClass: SeatingFacility,
                join: {
                    to: "merchant_characteristics.merchant_id",
                    from: "seating_facility.merchant_id"
                }
            },

            merchant_tags_relation: {
                relation: Model.HasManyRelation,
                modelClass: merchantTags,
                join: {
                    to: "merchant_characteristics.merchant_id",
                    from: "merchant_tags_characteristics_management.merchant_id"
                }
            },

            order_type_management_relation: {
                relation: Model.HasManyRelation,
                modelClass: OrderTypeManagement,
                join: {
                    to: "merchant_characteristics.merchant_id",
                    from: "order_type_merchant_characteristics_management.merchant_id"
                }
            },

            // relation with "merchant_characterstic Y" Table
            merchant_user_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/User',
                join: {
                    from: "merchant_characteristics.merchant_id",
                    to: "users.id"
                }
            },

            merchant_type_relation: {
                relation: Model.HasOneRelation,
                modelClass: __dirname + '/MerchantType',
                join: {
                    from: "merchant_characteristics.merchant_type_id",
                    to: "merchant_type.id"
                }
            }
        }
    }
}

module.exports = Characteristics;