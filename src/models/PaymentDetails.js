"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("./User");
const Delivery = require("./DeliveryCharges");

class PaymentDetails extends Model {
    // Table name is the only required property
    static get tableName() {
        return "payment_details";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            merchants_payment_details: {
                relation: Model.HasOneRelation,
                modelClass: User,
                join: {
                    to: "payment_details.merchant_id",
                    from: "users.id"
                }
            },

            // relation with "users" Table
            delivery_charges_relation: {
                relation: Model.HasManyRelation,
                modelClass: Delivery,
                join: {
                    to: "payment_details.id",
                    from: "delivery_charges.payment_details_id"
                }
            }
        }
    }
}

module.exports = PaymentDetails;