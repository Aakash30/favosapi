"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const Orders = require("./Orders");

class Table extends Model {
    // Table name is the only required property
    static get tableName() {
        return "table";
    }

    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
          order_relation: {
            relation: Model.HasOneRelation,
            modelClass: Orders,
            join: {
              from: "orders.id",
              to: "table.occupied_by",
            },
          },
        };
    }
}

module.exports = Table;