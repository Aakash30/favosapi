"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class Cuisines extends Model {
    // Table name is the only required property
    static get tableName() {
        return "cuisines";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "User" Table
            cuisine_merchant: {
                relation: Model.ManyToManyRelation,
                modelClass: __dirname + '/User',
                join: {
                    from: "cuisines.id",
                    through: {
                        from: "merchant_cuisines.cuisines_id",
                        to: "merchant_cuisines.merchant_id",
                    },
                    to: "users.id"
                }
            },
        }
    }
}

module.exports = Cuisines;