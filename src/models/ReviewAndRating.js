"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("./User");

class ReviewAndRating extends Model {
    // Table name is the only required property
    static get tableName() {
        return "review_and_rating";
    }

    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            //relation with "users" TABLE
            review_user_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: "review_and_rating.user_id",
                    to: "users.id"
                }
            },
            //relation with "users" TABLE
            review_merchant_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: "review_and_rating.merchant",
                    to: "users.id"
                }
            },
            //relation with "users" TABLE
            review_product_relation: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Product',
                join: {
                    from: "review_and_rating.item_id",
                    to: "product.id"
                }
            }
        }
    }
}

module.exports = ReviewAndRating;