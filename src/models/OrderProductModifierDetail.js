'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class OrderProductModifierDetail extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'order_product_modifier_detail';
	}

	// Optional JSON schema.This is not the database schema! Nothing is generated
	// based on this. This is only used for validation. Whenever a model instance
	// is created it is checked against this schema. http://json-schema.org/.
	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			modifier_product_detail_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ModifierProductDetails',
				join: {
					from: 'order_product_modifier_detail.modifier_product_detail_id',
					to: 'modifier_product_details.id',
				},
			},
			modifier_product_detail_relations: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/ModifierProductDetails',
				join: {
					from: 'order_product_modifier_detail.modifier_product_detail_id',
					to: 'modifier_product_details.id',
				},
			},
		};
	}
}

module.exports = OrderProductModifierDetail;
