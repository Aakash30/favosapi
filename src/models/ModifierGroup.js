"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class ModifierGroup extends Model {
  // Table name is the only required property
  static get tableName() {
    return "modifier_group";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      group_merchant: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/User',
        join: {
            from: "modifier_group.merchant_id",
            to: "users.id"
        }
      },
      // relation with "User" Table
      merchant_modifier_relation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname +'/User',
        join: {
          from: "modifier_group.merchant_id",
          to: "users.id",
        }
      },
      group_modifier_management: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + '/Modifier',
        join: {
            from: "modifier_group.id",
            through: {
                from: "modifier_group_modifiers.modifier_group_id",
                to: "modifier_group_modifiers.modifier_id",
            },
            to: "modifier.id"
        }
      },

      modifier_group_relation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/ModifierGroupModifiers',
        join: {
          from: "modifier_group.id",
          to: "modifier_group_modifiers.modifier_group_id",
        }
      },

      product_modifier_groups: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/ProductModifierGroups',
        join: {
          from: "modifier_group.id",
          to: "product_modifier_groups.modifier_group_id",
        }
      },
      modifier_group_cart_relation: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/CartProductGroup',
        join: {
          from: "modifier_group.id",
          to: "cart_product_group.group_id"
        }
     },
     modifier_cart_modifier_relation: {
      relation: Model.BelongsToOneRelation,
      modelClass: __dirname + '/CartProductModifier',
      join: {
        from: "modifier_group.id",
        to: "cart_product_modifier.group_id"
      }
    }
    };
  }
}

module.exports = ModifierGroup;
