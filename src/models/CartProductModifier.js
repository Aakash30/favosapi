'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class CartProductModifier extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'cart_product_modifier';
	}

	// Optional JSON schema.This is not the database schema! Nothing is generated
	// based on this. This is only used for validation. Whenever a model instance
	// is created it is checked against this schema. http://json-schema.org/.
	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			cart_product_modifier_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/CartProductGroup',
				join: {
					from: 'cart_product_modifier.cart_group_id',
					to: 'cart_product_group.id',
				},
			},
			cart_modifier_group_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/GroupModifier',
				join: {
					from: 'cart_product_modifier.group_id',
					to: 'modifier_group.id',
				},
			},
			cart_modifier_relation: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/Modifier',
				join: {
					from: 'cart_product_modifier.modifier_id',
					to: 'modifier.id',
				},
			},
			cart_modifier_detail_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/CartProductModifierDetail',
				join: {
					from: 'cart_product_modifier.id',
					to: 'cart_product_modifier_detail.cart_modifier_id',
				},
			},
		};
	}
}

module.exports = CartProductModifier;
