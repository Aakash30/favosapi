"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class ProductModifiers extends Model {
  // Table name is the only required property
  static get tableName() {
    return "product_modifiers";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      modifier: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Modifier',
        join: {
            from: "product_modifiers.modifier_id",
            to: "modifier.id"
        }
      },
      product: {
          relation: Model.HasOneRelation,
          modelClass: __dirname + '/Product',
          join: {
              from: "product_modifiers.product_id",
              to: "product.id"
          }
      }

    };
  }
}

module.exports = ProductModifiers;
