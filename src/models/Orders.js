'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;
const Address = require('./Address');
const Characteristics = require('./Merchant_Characteristics')
//-----------Import Models Here-----------

class Orders extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'orders';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			orders_of_users: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/User',
				join: {
					from: 'orders.user_id',
					to: 'users.id',
				},
			},

			orders_of_merchant: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/User',
				join: {
					from: 'orders.merchant_id',
					to: 'users.id',
				},
			},
			merchant_address_relation: {
				relation: Model.HasManyRelation,
				modelClass: Address,
				join: {
					from: 'orders.merchant_id',
					to: 'address_management.user_id',
				},
			},

			orders_of_table: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Table',
				join: {
					from: 'orders.table_id',
					to: 'table.id',
				},
			},

			order_type_relation: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Order_Type',
				join: {
					from: 'orders.order_type_id',
					to: 'order_type.id',
				},
			},

			ordered_products_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/OrderedProducts',
				join: {
					from: 'orders.id',
					to: 'ordered_products.order_number_id',
				},
			},

			order_customization_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/OrderCustomization',
				join: {
					from: 'orders.id',
					to: 'order_customization.order_number_id',
				},
			},

			merchant_cuisine: {
				relation: Model.ManyToManyRelation,
				modelClass: __dirname + '/Cuisines',
				join: {
					from: 'orders.merchant_id',
					through: {
						from: 'merchant_cuisines.merchant_id',
						to: 'merchant_cuisines.cuisines_id',
					},
					to: 'cuisines.id',
				},
			},

			order_status_management: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/OrderStatusManagement',
				join: {
					from: 'orders.id',
					to: 'order_status_management.order_id',
				},
			},

			characteristics_of_merchant: {
				relation: Model.BelongsToOneRelation,
				modelClass: Characteristics,
				join: {
					from: 'orders.merchant_id',
					to: 'merchant_characteristics.merchant_id',
				},
			},
		};
	}
}

module.exports = Orders;
