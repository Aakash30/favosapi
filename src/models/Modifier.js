"use strict";

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------

class Modifier extends Model {
  // Table name is the only required property
  static get tableName() {
    return "modifier";
  }

  // Optional JSON schema.This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: "object",
      required: [],
      properties: {
        id: { type: "integer" },
      },
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      modifier_merchant: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/User',
        join: {
            from: "modifier.merchant_id",
            to: "users.id"
        }
      },
      // relation with "product detail" Table
      modifier_product_detail_relation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname +'/ModifierProductDetails',
        join: {
          from: "modifier.id",
          to: "modifier_product_details.modifier_id",
        }
      },
      modifier_group_management: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + '/ModifierGroup',
        join: {
            from: "modifier.id",
            through: {
                from: "modifier_group_modifiers.modifier_id",
                to: "modifier_group_modifiers.modifier_group_id",
            },
            to: "modifier_group.id"
        }
      },


      products_associated_with_modifiers: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/ProductModifiers',
        join: {
          from: "modifier.id",
          to: "product_modifiers.modifier_id",
        }
      },
        
      modifier_cart_product_relation: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/CartProductModifier',
        join: {
          from: "modifier.id",
          to: "cart_product_modifier.modifier_id"
        }
      },
       // relation with "product modifier" Table
       product_modifier_get_relation: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + '/Product',
        join: {
            from: "modifier.id",
            through: {
                from: "product_modifiers.modifier_id",
                to: "product_modifiers.product_id"
            },
            to: "product.id"
        }
      },

      applied_to_group_modifiers: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/ModifierGroupModifiers',
        join: {
          from: "modifier.id",
          to: "modifier_group_modifiers.modifier_id"
        }
      }
    };
  }
}

module.exports = Modifier;
