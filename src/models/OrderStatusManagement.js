"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const Orders = require("./Orders");

class OrderStatusManagement extends Model {
    // Table name is the only required property
    static get tableName() {
        return "order_status_management";
    }

    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            order_status_process: {
                relation: Model.BelongsToOneRelation,
                modelClass: Orders,
                join: {
                    from: "orders.id",
                    to: "order_status_management.order_id",
                },
            },
        };
    }
}

module.exports = OrderStatusManagement;