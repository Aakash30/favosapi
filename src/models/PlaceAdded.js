"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("../models/User");
const Documents = require("./Documents");
const NonEaseRestaurants = require("./NonEaseRestaurants");

class Place extends Model {
    // Table name is the only required property
    static get tableName() {
        return "place_added_requested";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            //relation with "users" TABLE
            place_user_relation: {
                relation: Model.HasOneRelation,
                // The related model. This can be either a Model subclass constructor or an
                // absolute file path to a module that exports one. We use the file path version
                // here to prevent require loops.
                modelClass: User,
                join: {
                    from: "place_added_requested.user_id",
                    to: "users.id"
                }
            },
            
            ownership_documents: {
                relation: Model.HasManyRelation,
                modelClass: Documents,
                join: {
                    from: "place_added_requested.id",
                    to: "ownership_documents.bussiness_request_id" 
                }
            },

            non_ease_relation: {
                relation: Model.HasOneRelation,
                modelClass: NonEaseRestaurants,
                join: {
                    from: "place_added_requested.bussiness_request_id",
                    to: "non_ease_restaurants.id"
                }
            }
        }
    }
}

module.exports = Place;