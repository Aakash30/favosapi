"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("../models/User");

class ContactUs extends Model {
    // Table name is the only required property
    static get tableName() {
        return "contact_us";
    }

    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" },
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            //relation with "users" TABLE
            user_relation: {
                relation: Model.HasOneRelation,
                modelClass: User,
                join: {
                    from: "contact_us.user_id",
                    to: "users.id"
                }
            }
        }
    }
}

module.exports = ContactUs;