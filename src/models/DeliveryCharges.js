"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("./User");

class DeliveryCharges extends Model {
    // Table name is the only required property
    static get tableName() {
        return "delivery_charges";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            merchants_delivery_charges: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    to: "delivery_charges.merchant_id",
                    from: "users.id"
                }
            }
        }
    }
}

module.exports = DeliveryCharges;