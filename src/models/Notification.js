"use strict"

//--------Import Objection Model--------
const Model = require("objection").Model;

//-----------Import Models Here-----------
const User = require("./User");
const Orders = require("./Orders");

class Notification extends Model {
    // Table name is the only required property
    static get tableName() {
        return "notification";
    }

    // Optional JSON schema.This is not the database schema! Nothing is generated
    // based on this. This is only used for validation. Whenever a model instance
    // is created it is checked against this schema. http://json-schema.org/.
    static get jsonSchema() {
        return {
            type: "object",
            required: [],
            properties: {
                id: { type: "integer" }
            }
        };
    }

    // This object defines the relations to other models.
    static get relationMappings() {
        return {
            // relation with "users" Table
            user_notification: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    to: "notification.user_id",
                    from: "users.id"
                }
            },

            // relation with "orders" Table
            order_notification: {
                relation: Model.BelongsToOneRelation,
                modelClass: Orders,
                join: {
                    to: "notification.order_id",
                    from: "orders.id"
                }
            },
        }
    }
}

module.exports = Notification;