'use strict';

//--------Import Objection Model--------
const Model = require('objection').Model;

//-----------Import Models Here-----------

class ProductMenu extends Model {
	// Table name is the only required property
	static get tableName() {
		return 'product_menu';
	}

	// Optional JSON schema.This is not the database schema! Nothing is generated
	// based on this. This is only used for validation. Whenever a model instance
	// is created it is checked against this schema. http://json-schema.org/.
	static get jsonSchema() {
		return {
			type: 'object',
			required: [],
			properties: {
				id: { type: 'integer' },
			},
		};
	}

	// This object defines the relations to other models.
	static get relationMappings() {
		return {
			//relation with "product" TABLE
			product_menu_belongsto: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + '/MenuManagement',
				join: {
					from: 'product_menu.menu_id',
					to: 'menu_management.id',
				},
			},
			// menu_order_types: {
			// 	relation: Model.HasManyRelation,
			// 	modelClass: __dirname + '/ProductPricing',
			// 	join: {
			// 		from: 'product_menu.menu_id',
			// 		to: 'product_pricing.menu_id',
			// 	},
			// },

			product_categories_relation: {
				relation: Model.HasManyRelation,
				modelClass: __dirname + '/ProductCategory',
				join: {
					from: 'product_menu.product_id',
					to: 'product_category.product_id',
				},
			},

			menus: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/MenuManagement',
				join: {
					from: 'product_menu.menu_id',
					to: 'menu_management.id',
				},
			},

			products_associated: {
				relation: Model.HasOneRelation,
				modelClass: __dirname + '/Product',
				join: {
					from: 'product_menu.product_id',
					to: 'product.id',
				},
			}
		};
	}
}

module.exports = ProductMenu;
