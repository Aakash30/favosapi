// const to = require('./middlewares').to;
const gcm = require("node-gcm");
const moment = require("moment");
const path = require("path");

const settings = {
    gcm_user: {
        // AAAAt3iFfoE: APA91bHcxHWRhRmUg3RALsC8ii5utC9cqsrW4imxyr4vnBsq0GqJ_58N9twC4P8z - QYIypTmxRELayALOhXkQbTfR9kkwTrh_liNfQwXe1MuLmR0D2UxKQlL1PjNwKrNCy0Cvz_O4rQX
        id: "AAAAt3iFfoE:APA91bHcxHWRhRmUg3RALsC8ii5utC9cqsrW4imxyr4vnBsq0GqJ_58N9twC4P8z-QYIypTmxRELayALOhXkQbTfR9kkwTrh_liNfQwXe1MuLmR0D2UxKQlL1PjNwKrNCy0Cvz_O4rQX"
    }
};
const PushNotifications = require("node-pushnotifications");
const push = new PushNotifications(settings);
const apn = require("apn");

const p8file = path.join(__dirname, "../certs/AuthKey_YX9BL8S83Q.p8")

var options = {
    token: {
        key: p8file,
        keyId: "YX9BL8S83Q",
        teamId: "3DV2LANF77"
    },
    production: true
};

var apnProvider = new apn.Provider(options);
var gcmUserSender = new gcm.Sender(settings.gcm_user.id);
module.exports.push = push;



module.exports.sendPushNotif = async (notif , options) => {
    let err, topic;
    // console.log('---------------------', notif);

    let data = notif;
    const device = data.device_token;
    const device_type = data.device_type;
    const message_noti = data.content;
    const type_noti = data.type;

    if (device_type == "android" || device_type == "Android") {
      console.log("enetered in android");
      if (data.user_type == "USER") packagename = "nz.co.favos";

      let body_data_in_pushnoti = {
        message: notif.content,
        type: notif.type,
        noti_count: notif.noti_count,
        orderId: notif.orderId,
        notificationId: notif.notificationId,
        orderstatusId: notif.orderstatusId,
        orderTypeId: notif.orderTypeId,
      };

      var message = new gcm.Message({
        collapseKey: "demo",
        priority: "high",
        contentAvailable: true,
        delayWhileIdle: true,
        restrictedPackageName: packagename,
        data: {
          body: body_data_in_pushnoti,
        },
      });

      // Specify which registration IDs to deliver the message to
      var regTokens = [device];
      // console.log("device token", regTokens)

      /////////////start//////////////
      // Actually send the message
      gcmUserSender.sendNoRetry(
        message,
        { registrationTokens: regTokens },
        function (err, response) {
          // console.log("Sended notification");
          if (err) console.error(err);
          else console.log(response);
          // console.log(message);
        }
      );
    }
    else if (device_type == "ios" || device_type == "IOS") {
        topic = "nz.co.favos"; //bundule identifier
        
        var note = new apn.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        // note.badge = notif.noti_count;
        note.noti_count = notif.noti_count;
        note.sound = "ping.aiff";
        note.alert = notif.content;
        note.message = notif.content;
        note.type = notif.type;
        note.time = moment().format("YYYY-MM-DD HH:MM");
        note.payload = {
            messageFrom: "FAVOS",
            message: message_noti,
            type: type_noti,
            orderId: notif.orderId,
            notificationId: notif.notificationId,
            orderstatusId: notif.orderstatusId,
            orderTypeId: notif.orderTypeId
        };
        note.topic = topic;

        note.orderId = notif.orderId;
        note.notificationId = notif.notificationId;
        note.orderstatusId = notif.orderstatusId;
        note.orderTypeId = notif.orderTypeId;
        console.log('note payload is ; ', note);

        // notif.device_token='9EB1C76D9400DA3BD077BE8A377A169DB1BF322304EF268B191C44F2D22F8DB3';
        // console.log("------------------------------------");

        apnProvider
            .send(note, notif.device_token)
            .then(result => {
                console.log("notification sent to iOs");
                console.log(result);
            })
            .catch(err => {
                // console.log("error sending notification");
                console.log(err);
            });
    }
};