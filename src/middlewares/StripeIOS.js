'use strict';

const stripe = require('stripe')('sk_test_9FdDvh5jMnmhGoFBkcWITTU1004hBUydX7');
//const stripe = require('stripe')('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');

const createPayment = async (params) => {
	let [err, data] = await to(
		stripe.charges.create({
			amount: params.amount,
			currency: params.currency,
			source: params.tknId,
			description: 'Order amount',
			capture: false,
		})
	);
	console.log('data', params);
	if (err) {
		throw badRequestError(err.message);
		//return {'status': false, 'msg':err.message};
	}

	return {
		status: true,
		data: { txnId: data.id, amount: data.id, url: data.receipt_url },
	};
};

const GetephemeralKey = async (api_version, customer) => {
	console.log('GetephemeralKey---------');
	let key = await stripe.ephemeralKeys.create(
		{ customer: customer },
		{ apiVersion: api_version }
	);
	return key;
};

const GetCustomerID = async (body) => {
	console.log('GetCustomerID', body);
	const customer = await stripe.customers.create(body);
	return customer.id;
};

const capturePayment = async (params) => {
	let err, data;
	[err, data] = await to(stripe.charges.capture(params));
	if (err) {
		throw badRequestError(err.message);
	}
	return data.id;
};

///New Functionality for payment
const createPaymentForSecretKey = async (
	chargeAmt,
	currency,
	customer,
	paymentMethod
) => {
	console.log(
		'CreatePayment---------',
		chargeAmt,
		currency,
		customer,
		paymentMethod
	);

	let stripeAmount = (+chargeAmt).toFixed(2) * 100;
	stripeAmount = parseInt((+stripeAmount).toFixed(2));
	console.log(stripeAmount, 'stripeAmount');
	const paymentIntent = await stripe.paymentIntents.create({
		amount: stripeAmount,
		currency: currency,
		//payment_method: paymentMethod,
		payment_method_types: ['card'],
		//save_payment_method: false
		customer: customer,
		capture_method: 'manual',
	});

	console.log(paymentIntent, 'paymentIntent');
	const clientSecret = paymentIntent.client_secret;
	return clientSecret;
};

const paymentConfirm = async (paymentId) => {
	console.log('Stripe -- PaymentConfirm', paymentId);
	const confirm = await stripe.paymentIntents.retrieve(paymentId);
	if (confirm.id != undefined) {
		return confirm.id;
	}
};

const capturePaymentIntent = async (params) => {
	let err, data;
	[err, data] = await to(stripe.paymentIntents.capture(params));
	if (err) {
		throw badRequestError(err.message);
	}
	return data.id;
};

const cancelPaymentIntent = async (params) => {
	let data;
	data = await to(stripe.paymentIntents.cancel(params));
	return data.id;
};

module.exports = {
	GetephemeralKey,
	createPayment,
	GetCustomerID,
	capturePayment,
	createPaymentForSecretKey,
	paymentConfirm,
	capturePaymentIntent,
	cancelPaymentIntent,
};
