var distance = require('google-distance');
distance.apiKey = 'AIzaSyDQ-Pk2biATaxiz8_qAzUl2SITRhgNv2r4';
const GetDistanceByLatLng = async (pickUp, dropOff) => {
	return new Promise(async function (resolve, reject) {
		distance.get(
			{
				index: 1,
				origin: pickUp,
				destination: dropOff,
			},
			async function (err, data) {
				if (err) {
					//console.log(err);

					reject({ status: false, code: 312, data: err });
				}
				if (data != undefined) {
					resolve({ data: data, status: true });
				}
			}
		);
	});
};
module.exports = {
	GetDistanceByLatLng,
};
