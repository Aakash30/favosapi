const dotenv = require('dotenv').config({ path: './../../.env' });

const creds = {
	accessKeyId: process.env.accessKeyId,
	secretAccessKey: process.env.secretAccessKey,
	region: process.env.region, //ap-south-1
};
//console.log(creds);
const bucketName = 'favos';

const bucketRegion = process.env.region;

module.exports = {
	creds,
	bucketName,
	bucketRegion,
};
