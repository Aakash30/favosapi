const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require("twilio")(accountSid, authToken);
module.exports.sendSms = async (data) => {
  //let body = req.body;
  console.log(data);
  client.messages
    .create({
      body: data.message,
      from: "17089985066", //twilio number
      to: data.to,
    })
    .then((message) => console.log(message, "message"))
    .catch((error) => {
      //Log friendly error
      console.error(error.toString());
    });
};
