const sgMail = require("@sendgrid/mail");
const sendgrid = require("../../config/index").sendgrid;
sgMail.setApiKey(sendgrid);

const pug = require("pug");
const ejs = require("ejs");
const path = require("path");

/**
 * @description: Function Used during forgot password Of User
 */
module.exports.sendOtpByEmail = async (user_name, user_email, send_otp) => {
    const msg = {
        to: user_email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-6738d5882d254c619fc2caf4f8793036",
        dynamic_template_data: {
            name: user_name,
            email: user_email,
            otp: send_otp
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
};


/**
 * @description: Function Used When we send link by mail to verify User Email
 */
module.exports.sendEmailToVerifyEmail = async (user_name, user_email, link) => {
    const msg = {
        to: user_email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        subject: 'Verify Email!',
        html: (pug.renderFile('/var/www/html/favos_api/public/template/pug/verify.pug', { name: user_name, link: link }))
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


/**
 * @description: Function Used To Send Credentials
 */
module.exports.sendCredentialsByEmail = async (user_name, link, user_email, password) => {
    const msg = {
        to: user_email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-87a2090769154f1e8ed1b46c3158f2a3",
        dynamic_template_data: {
            name: user_name,
            link: link,
            email: user_email,
            password: password
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


//---------------Function Used during forgot password Of Admin--------------
module.exports.sendEmail = async (to, subject, link) => {
    const msg = {
        to: to,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-f4a65e57bbee4a569b0dde7f998d9c06",
        dynamic_template_data: {
            email: to,
            heading: subject,
            link: link
        }
    };
    // console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        //console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
        //Extract error msg
        const { message, code, response } = error;
        //Extract response msg
        const { headers, body } = response;
    });
};


/**
 * @description: Function Used To Send ContactUs Customer
 */
module.exports.sendContactUsDetailToCustomer = async (user_name, user_email) => {
    const msg = {
        to: user_email,
        from: {
            name: 'Favos - Intelligent Ordering & POS',
            email :"hello@favos.co"
        },
        templateId: "d-07720d0d9eb0428c9e87d8b56ea38547",
        dynamic_template_data: {
            name: user_name
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


/**
 * @description: Function Used To Send ContactUs Merchant
 */
module.exports.sendContactUsDetailToMerchant = async (user_name, user_email) => {
    const msg = {
      to: user_email,
      from: {
        name: "Favos - Intelligent Ordering & POS",
        email: "hello@favos.co",
      },
      templateId: "d-014854808af54010ab58b0bc23b13132",
      dynamic_template_data: {
        name: user_name,
      },
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}



/**
 * @description: Function Used When we send link by mail to verify User Email
 */
module.exports.sendOrderDetails = async (data) => {
    // console.log(path.join(__dirname, "../../public/template/pug/orderDetails.ejs"), data)
    let file = await ejs.renderFile(path.join(__dirname, "../../public/template/pug/orderDetails.ejs"), { data })
    console.log(typeof (file))

    const msg = {
        to: data.email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        subject: `FAVOS - New Order ${data.order_number} Placed`,
        html: file
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


/**
 * @description: Function Used to send confirmed order details
 */
module.exports.sendConfirmedOrderDetails = async (data) => {
    
    const msg = {
        to: data.email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-e72d1369bc9641969baf5210661443b0",
        dynamic_template_data: {
            name: data.user_name,
            order_number: data.order_number,
            merchant_name: data.merchant_name,
            order_type: data.order_type,
            accepted_date: data.accepted_date
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


/**
 * @description: Function Used to send ready for pickup order details
 */
module.exports.sendReadyForPickupOrderDetails = async (data) => {

    const msg = {
        to: data.email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-9dd634c091f542809f2ccb41e1be9b49",
        dynamic_template_data: {
            name: data.user_name,
            merchant_name: data.merchant_name,
            order_number: data.order_number
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


/**
 * @description: Function Used to send dispatching delivery order details
 */
module.exports.sendDispatchForDeliveryOrderDetails = async (data) => {

    const msg = {
        to: data.email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-8dc3ab1d9779489fbe15d19ce6c6ba3a",
        dynamic_template_data: {
            name: data.user_name,
            merchant_name: data.merchant_name,
            order_number: data.order_number,
            delivery_address: data.delivery_address
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}


/**
 * @description: Function Used to send cancelled order details
 */
module.exports.sendCancelledOrderDetails = async (data) => {

    const msg = {
        to: data.email,
        from: {
            name: 'Favos',
            email :"support@favos.co"
        },
        templateId: "d-707ea8acd3c44318bc503f604ea2849c",
        dynamic_template_data: {
            name: data.user_name,
            order_number: data.order_number,
            merchant_name: data.merchant_name,
            order_cancellation_reason: data.order_cancellation_reason
        }
    };

    //  console.log("msg===", msg);
    sgMail.send(msg).then(() => {
        console.log("sendgrid mail sended success");
    }).catch(error => {
        //Log friendly error
        //console.log("Error in sendGrid -------------- ");
        console.error(error.toString());
    });
}